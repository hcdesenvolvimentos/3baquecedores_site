<?php



/**

 * Plugin Name: Base 3B Aquecedores



 * Description: Controle base do tema Base 3B Aquecedores .

 * Version: 0.1

 * Author: Agência Palupa

 * Author URI: http://www.palupa.com.br

 * Licence: GPL2

 */





function base3baquecedores () {



		// TIPOS DE CONTEÚDO

		conteudos3baquecedores();



		// TAXONOMIA

		taxonomia3baquecedores();



		// META BOXES

		metaboxes3baquecedores();



	}



/****************************************************

* TIPOS DE CONTEÚDO

*****************************************************/



	function conteudos3baquecedores (){



		// TIPOS DE CONTEÚDO



		tipoDestaque();



		tipoProduto();



		tipoDepoimentos();



		tipoParceiro();



		/* ALTERAÇÃO DO TÍTULO PADRÃO */

		add_filter( 'enter_title_here', 'trocarTituloPadrao' );

		function trocarTituloPadrao($titulo){



			switch (get_current_screen()->post_type) {



				case 'destaque':

					$titulo = 'Título do destaque';

				break;



				case 'produto':

					$titulo = 'Referência do Produto';

				break;



				case 'depoimento':

					$titulo = 'Título do depoimento';

				break;



				case 'parceiro':

					$titulo = 'Nome do Parceiro';

				break;



				default:

				break;

			}



		    return $titulo;



		}



	}



		// CUSTOM POST TYPE DESTAQUES

		function tipoDestaque() {



			$rotulosDestaque = array(

									'name'               => 'Destaque',

									'singular_name'      => 'destaque',

									'menu_name'          => 'Destaques',

									'name_admin_bar'     => 'Destaques',

									'add_new'            => 'Adicionar novo',

									'add_new_item'       => 'Adicionar novo destaque',

									'new_item'           => 'Novo destaque',

									'edit_item'          => 'Editar destaque',

									'view_item'          => 'Ver destaque',

									'all_items'          => 'Todos os destaques',

									'search_items'       => 'Buscar destaques',

									'parent_item_colon'  => 'Dos destaques',

									'not_found'          => 'Nenhum destaque cadastrado.',

									'not_found_in_trash' => 'Nenhum destaque na lixeira.'

								);



			$argsDestaque 	= array(

									'labels'             => $rotulosDestaque,

									'public'             => true,

									'publicly_queryable' => true,

									'show_ui'            => true,

									'show_in_menu'       => true,

									'menu_position'		 => 4,

									'menu_icon'          => 'dashicons-megaphone',

									'query_var'          => true,

									'rewrite'            => array( 'slug' => 'destaque' ),

									'capability_type'    => 'post',

									'has_archive'        => true,

									'hierarchical'       => false,

									'supports'           => array( 'title', 'thumbnail' )

								);



			// REGISTRA O TIPO CUSTOMIZADO

			register_post_type('destaque', $argsDestaque);



		}



		// CUSTOM POST TYPE PRODUtOS

		function tipoProduto() {



			$rotulosProduto = array(

									'name'               => 'Produto',

									'singular_name'      => 'destaque',

									'menu_name'          => 'Produtos',

									'name_admin_bar'     => 'Produtos',

									'add_new'            => 'Adicionar novo',

									'add_new_item'       => 'Adicionar novo produto',

									'new_item'           => 'Novo produtos',

									'edit_item'          => 'Editar produto',

									'view_item'          => 'Ver produtos',

									'all_items'          => 'Todos os produtos',

									'search_items'       => 'Buscar produtos',

									'parent_item_colon'  => 'Dos produtos',

									'not_found'          => 'Nenhum produto cadastrado.',

									'not_found_in_trash' => 'Nenhum produto na lixeira.'

								);



			$argsProduto 	= array(

									'labels'             => $rotulosProduto,

									'public'             => true,

									'publicly_queryable' => true,

									'show_ui'            => true,

									'show_in_menu'       => true,

									'menu_position'		 => 4,

									'menu_icon'          => 'dashicons-products',

									'query_var'          => true,

									'rewrite'            => array( 'slug' => 'produto' ),

									'capability_type'    => 'post',

									'has_archive'        => true,

									'hierarchical'       => false,

									'supports'           => array( 'title', 'thumbnail' )

								);



			// REGISTRA O TIPO CUSTOMIZADO

			register_post_type('produto', $argsProduto);



		}



		// CUSTOM POST TYPE DEPOIMENTO

		function tipoDepoimentos() {



			$rotulosDepoimentos = array(

									'name'               => 'Depoimento',

									'singular_name'      => 'Depoimento',

									'menu_name'          => 'Depoimentos',

									'name_admin_bar'     => 'depoimento',

									'add_new'            => 'Adicionar novo',

									'add_new_item'       => 'Adicionar novo depoimento',

									'new_item'           => 'Novo depoimento',

									'edit_item'          => 'Editar depoimento',

									'view_item'          => 'Ver depoimento',

									'all_items'          => 'Todos depoimentos',

									'search_items'       => 'Buscar depoimentos',

									'parent_item_colon'  => 'Dos depoimentos',

									'not_found'          => 'Nenhum depoimento cadastrado.',

									'not_found_in_trash' => 'Nenhum depoimento na lixeira.'

								);



			$argsDepoimentos 	= array(

									'labels'             => $rotulosDepoimentos,

									'public'             => true,

									'publicly_queryable' => true,

									'show_ui'            => true,

									'show_in_menu'       => true,

									'menu_position'		 => 4,

									'menu_icon'          => 'dashicons-heart',

									'query_var'          => true,

									'rewrite'            => array( 'slug' => 'depoimentos' ),

									'capability_type'    => 'post',

									'has_archive'        => true,

									'hierarchical'       => false,

									'supports'           => array( 'title', 'thumbnail' )

								);



			// REGISTRA O TIPO CUSTOMIZADO

			register_post_type('depoimentos', $argsDepoimentos);



		}



		// CUSTOM POST TYPE PARCEIRO

		function tipoParceiro() {



			$rotulosParceiro = array(

									'name'               => 'Parceiro',

									'singular_name'      => 'Parceiro',

									'menu_name'          => 'Parceiros',

									'name_admin_bar'     => 'parceiro',

									'add_new'            => 'Adicionar novo',

									'add_new_item'       => 'Adicionar novo parceiro',

									'new_item'           => 'Novo parceiro',

									'edit_item'          => 'Editar parceiro',

									'view_item'          => 'Ver parceiro',

									'all_items'          => 'Todos parceiros',

									'search_items'       => 'Buscar parceiros',

									'parent_item_colon'  => 'Dos parceiros',

									'not_found'          => 'Nenhum parceiro cadastrado.',

									'not_found_in_trash' => 'Nenhum parceiro na lixeira.'

								);



			$argsParceiro 	= array(

									'labels'             => $rotulosParceiro,

									'public'             => true,

									'publicly_queryable' => true,

									'show_ui'            => true,

									'show_in_menu'       => true,

									'menu_position'		 => 4,

									'menu_icon'          => 'dashicons-businessman',

									'query_var'          => true,

									'rewrite'            => array( 'slug' => 'parceiro' ),

									'capability_type'    => 'post',

									'has_archive'        => true,

									'hierarchical'       => false,

									'supports'           => array( 'title', 'thumbnail' )

								);



			// REGISTRA O TIPO CUSTOMIZADO

			register_post_type('parceiro', $argsParceiro);



		}





/****************************************************

* TAXONOMIA

*****************************************************/

	function taxonomia3baquecedores () {



		taxonomiaCategoriaProduto();



	}



		// TAXONOMIA DE PRODUTO

		function taxonomiaCategoriaProduto() {



			$rotulosCategoriaProduto = array(

												'name'              => 'Categorias de Produto',

												'singular_name'     => 'Categoria de Produto',

												'search_items'      => 'Buscar categorias de Produto',

												'all_items'         => 'Todas categorias de Produto',

												'parent_item'       => 'Categoria de Produto pai',

												'parent_item_colon' => 'Categoria de Produto pai:',

												'edit_item'         => 'Editar categoria de Produto',

												'update_item'       => 'Atualizar categoria de Produto',

												'add_new_item'      => 'Nova categoria de Produto',

												'new_item_name'     => 'Nova categoria',

												'menu_name'         => 'Categorias de Produto',

											);



			$argsCategoriaProduto 		= array(

												'hierarchical'      => true,

												'labels'            => $rotulosCategoriaProduto,

												'show_ui'           => true,

												'show_admin_column' => true,

												'query_var'         => true,

												'rewrite'           => array( 'slug' => 'categoria-produto' ),

											);



			register_taxonomy( 'categoriaProduto', array( 'produto' ), $argsCategoriaProduto );



		}







/****************************************************

* META BOXES

*****************************************************/

	function metaboxes3baquecedores(){



		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );



	}



		function registraMetaboxes( $metaboxes ){



			$prefix = 'TresB_';



			// METABOX DE PRODUTO

			$metaboxes[] = array(



				'id'			=> 'detalhesMetaboxProduto',

				'title'			=> 'Atributos do produto: : ',

				'pages' 		=> array( 'produto' ),

				'context' 		=> 'normal',

				'priority' 		=> 'high',

				'autosave' 		=> false,

				'fields' 		=> array(



					array(

						'name'  => 'Tipo do produto: ',

						'id'    => "{$prefix}produto_tipo",

						'desc'  => 'Tipo (Aquecedor a gás, pressurizador, etc...',

						'type'  => 'text'

					),

					array(

						'name'  => 'Link catálago: ',

						'id'    => "{$prefix}produto_catalago",

						'type'  => 'text'

					),



					array(

						'name'  => 'Marca: ',

						'id'    => "{$prefix}produto_marca",

						'desc'  => 'Nome da empresa fabricante.',

						'type'  => 'text'

					),



					array(

						'name'  => 'Descrição: ',

						'id'    => "{$prefix}produto_descricao",

						'desc'  => 'Breve descrição do produto.',

						'type'  => 'wysiwyg'

					),



					array(

						'name'  			=> 'Imagens: ',

						'id'    			=> "{$prefix}produto_imagens",

						'desc'  			=> 'Upload de imagens do produto (Máximo 6 imagens)',

						'type'  			=> 'image_advanced',

						'max_file_uploads' 	=> 6

					),



					/*array(

						'name'  => 'Dados técnicos:',

						'id'    => "{$prefix}produto_dados",

						'desc'  => 'Características do produto (Consumo máximo de gás, Vazão de água, Dimensões, etc...).',

						'type'  => 'text',

						'clone' => 'true',

						'max_clone' => '6'

					),*/





					array(

						'name'  => 'Aplicações:',

						'id'    => "{$prefix}produto_aplicacoes",

						'desc'  => 'Onde ou como este produto pode ser usado.',

						'type'  => 'text',

						'clone' => 'true',

						'max_clone' => '4'

					),



					array(

						'name'  => 'Benefícios:',

						'id'    => "{$prefix}produto_beneficios",

						'desc'  => 'Benefícios para o cliente.',

						'type'  => 'text',

						'clone' => 'true',

						'max_clone' => '4'

					),



					array(

						'name'  => 'tabela:',

						'id'    => "{$prefix}produto_tabela",

						'desc'  => 'Tabela com os dados técnicos do produto',

						'type'  => 'wysiwyg',

					)



				),



				'validation' => array(

				    'rules'    => array(

				        "{$prefix}produto_descricao" => array(

				            'maxlength' => 500,

				        ),

				    ),

				    // Optional override of default error messages

				    'messages' => array(

				        "{$prefix}produto_descricao" => array(

				            'maxlength' => __( 'O seu texto de descrição precisa ser menor.', 'your-prefix' ),

				        ),

				    )

				)



			);



			// METABOX DE DEPOIMENTOS

			$metaboxes[] = array(



				'id'			=> 'detalhesMetaboxDepoimento',

				'title'			=> 'Detalhe do depoimento',

				'pages' 		=> array( 'depoimentos' ),

				'context' 		=> 'normal',

				'priority' 		=> 'high',

				'autosave' 		=> false,

				'fields' 		=> array(



					array(

						'name'  => 'Depoimento: ',

						'id'    => "{$prefix}depoimento_texto",

						'desc'  => '',

						'type'  => 'text'

					),



					array(

						'name'  => 'Autor depoimento: ',

						'id'    => "{$prefix}depoimento_autor",

						'desc'  => '',

						'type'  => 'text'

					),



					array(

						'name'  => 'Informação sobre o autor: ',

						'id'    => "{$prefix}depoimento_autor_info",

						'desc'  => '',

						'type'  => 'text'

					),



				),

				'validation' => array(

				    'rules'    => array(

				        "{$prefix}depoimento_texto" => array(

				            'maxlength' => 500,

				        ),

				    ),

				    // Optional override of default error messages

				    'messages' => array(

				        "{$prefix}depoimento_texto" => array(

				            'maxlength' => __( 'O seu texto de descrição precisa ser menor.', 'your-prefix' ),

				        ),

				    )

				)



			);



			// METABOX DE DESTAQUES

			$metaboxes[] = array(



				'id'			=> 'detalhesMetaboxDestaques',

				'title'			=> 'Detalhe do Destaque',

				'pages' 		=> array( 'destaque' ),

				'context' 		=> 'normal',

				'priority' 		=> 'high',

				'autosave' 		=> false,

				'fields' 		=> array(



					array(

						'name'  => 'Link do destaque: ',

						'id'    => "{$prefix}destaque_link",

						'desc'  => 'Para onde o usuário será redirecionado caso clique no botão.',

						'type'  => 'text'

					),



					array(

						'name'  => 'Breve descrição: ',

						'id'    => "{$prefix}destaque_descricao",

						'desc'  => 'Texto curto de descrição.',

						'type'  => 'text'

					),



				),

				'validation' => array(

				    'rules'    => array(

				        "{$prefix}destaque_descricao" => array(

				            'maxlength' => 100,

				        ),

				    ),

				    // Optional override of default error messages

				    'messages' => array(

				        "{$prefix}destaque_descricao" => array(

				            'maxlength' => __( 'O seu texto de descrição precisa ser menor.', 'your-prefix' ),

				        ),

				    )

				)



			);



			// METABOX DE PARCEIROS

			$metaboxes[] = array(



				'id'			=> 'detalhesMetaboxParceiros',

				'title'			=> 'Detalhe do Parceiro',

				'pages' 		=> array( 'parceiro' ),

				'context' 		=> 'normal',

				'priority' 		=> 'high',

				'autosave' 		=> false,

				'fields' 		=> array(



					array(

						'name'  => 'Link do parceiro: ',

						'id'    => "{$prefix}parceiro_link",

						'desc'  => 'Para onde o usuário será redirecionado caso clique no logo.',

						'type'  => 'text'

					)



				)



			);



			return $metaboxes;

		}



	/****************************************************

	* SHORTCODES

	*****************************************************/

	function shortcodes3baquecedores(){



	}



	/****************************************************

	* ATALHOS VISUAL COMPOSER

	*****************************************************/

	function visualcomposer3baquecedores(){



	    if (class_exists('WPBakeryVisualComposer')){



		}



	}



  	/****************************************************

	* AÇÕES

	*****************************************************/



	// INICIA A FUNÇÃO PRINCIPAL

	add_action('init', 'base3baquecedores');



	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES

	//add_action( 'add_meta_boxes', 'metaboxjs');



	// FLUSHS

	function rewrite_flush() {



    	base3baquecedores();



   		flush_rewrite_rules();

	}



	register_activation_hook( __FILE__, 'rewrite_flush' );