<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package tresbaquecedores
 */
global $configuracao;
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];
//FACEBOOK - 
$facebook = $configuracao['opt-facebook'];
//TWITTER - 
$twitter = $configuracao['opt-twitter'];
//YOUTUBE -
$youtube = $configuracao['opt-youtube'];
global $wp;
$current_url = home_url(add_query_arg(array(),$wp->request));

get_header(); ?>

	<!-- PÁGINA BLOG -->
	<div class="pg pg-blog-postagem" >
		<div class="container">
			<div class="row">
				<?php get_sidebar(); ?>
				

				<div class="col-md-10">
					<div class="sub-titulo">
						<p>Nosso Blog</p>
					</div>

					<div class="area-do-postagem">
						<div class="foto-postagem" style="background: url(<?php echo $foto ?>);"></div>
						<h2><?php echo get_the_title(); ?></h2>
						<span><b><?php the_time('j') ?></b> <?php the_time('\d\e F \ Y') ?></span>
						<p><?php echo get_the_content(); ?></p>
					
					</div>
					<div class="redes-sociais">
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $current_url ?>&t=TITLE" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<a href="http://twitter.com/share?text=Confira%20esta%20notícia:&hashtags=post_compartilhado,artigo_interessante" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<!-- 	<a href="<?php echo $youtube ?>" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a> -->
					</div>
					<div class="disqus">
						<?php
							//If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>		
					</div>
					
					<div class="paginador">
					<?php previous_post('%','[ <i class="fa fa-angle-left" aria-hidden="true"></i> anterior', 'no') ?>
					<?php next_post('%','	próximo <i class="fa fa-angle-right" aria-hidden="true"></i> ]', 'no') ?>
					
					</div>
					
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();
