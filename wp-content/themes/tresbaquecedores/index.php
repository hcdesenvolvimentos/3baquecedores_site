	<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package tresbaquecedores
 */

get_header(); ?>
<!-- PÁGINA BLOG -->
<div class="pg pg-blog">
	<div class="container">
		<div class="row">
			<?php get_sidebar(); ?>
			<div class="col-md-10">
				<!-- NOSSO BLOG -->
				<section>
					<div class="sub-titulo">
						<p><?php echo $configuracao['opt-blog-titulo'] ?></p>
					</div>

					<div class="areaTituloFrase text-center">
						
						<p><?php echo $configuracao['opt-blog-frase'] ?></p>
					</div>


					<div class="area-blog">
						<ul>
						<?php
							$the_query = new WP_Query( 'posts_per_page=-1' );
							$i = 0;
							 $cores = array(
			            		0 => "52, 62, 93, 0.59",
			            		1 => "184, 79, 70, 0.74",
			            		2 => "189, 135, 68, 0.75",
			            		3 => "52, 62, 93, 0.59",
			            	);
							while ($the_query -> have_posts()) : $the_query -> the_post();
								$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$foto = $foto[0];

								if ($i == 4) { $i = 0;}

						?>
									<li >
										<a href="<?php echo get_permalink(); ?>" style="background:url(<?php echo $foto ?>);">
											<div class="lente" style="background:rgba(<?php echo $cores[$i] ?>);">
												<h4><?php echo get_the_title(); ?></h4>
											</div>
										</a>
										<span><b><?php the_time('j') ?></b> <?php the_time('\d\e F \d\e Y') ?></span>
										<p>
											<?php
												$textoresumido = get_the_content();
												$textocurto = substr($textoresumido, 0, 80).'...';
												echo $textocurto;
											?>
										</p>

										<a href="<?php echo get_permalink(); ?>" class="ver-mais">[leia mais...]</a>
									</li>

						<?php $i++; endwhile; wp_reset_query(); ?>

						</ul>

					</div>
					<div class="paginador">
						<ul>
						<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
						</ul>
					</div>
				</section>

			</div>
		</div>
	</div>
</div>
<?php

get_footer();
