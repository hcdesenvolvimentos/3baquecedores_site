<?php
/**
 * tresbaquecedores functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package tresbaquecedores
 */

if ( ! function_exists( 'tresbaquecedores_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function tresbaquecedores_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on tresbaquecedores, use a find and replace
	 * to change 'tresbaquecedores' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'tresbaquecedores', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'tresbaquecedores' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'tresbaquecedores_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'tresbaquecedores_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tresbaquecedores_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tresbaquecedores_content_width', 640 );
}
add_action( 'after_setup_theme', 'tresbaquecedores_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function tresbaquecedores_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'tresbaquecedores' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'tresbaquecedores' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'tresbaquecedores_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function tresbaquecedores_scripts() {

	//FONT GOOGLE
	wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Sriracha');

	//JAVA SCRIPT
	wp_enqueue_script( '3baquecedores-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js' );
	wp_enqueue_script( '3baquecedores-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( '3baquecedores-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( '3baquecedores-bootsrap', get_template_directory_uri() . '/js/bootstrap.min.js' );
	wp_enqueue_script( '3baquecedores-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/jquery.fancybox.pack.js' );
	wp_enqueue_script( '3baquecedores-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-buttons.js' );
	wp_enqueue_script( '3baquecedores-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-media.js' );
	wp_enqueue_script( '3baquecedores-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-thumbs.js' );
	wp_enqueue_script( '3baquecedores-font-owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js' );
	// wp_enqueue_script( '3baquecedores-jquery-facybox', get_template_directory_uri() . '/js/jquery.fancybox.pack.js' );
	wp_enqueue_script( '3baquecedores-geral', get_template_directory_uri() . '/js/geral.js' );

	//CSS
	wp_enqueue_style( '3baquecedores-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style( '3baquecedores-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
	wp_enqueue_style( '3baquecedores-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/jquery.fancybox.css');
	wp_enqueue_style( '3baquecedores-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-buttons.css');
	wp_enqueue_style( '3baquecedores-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-thumbs.css');
	wp_enqueue_style( '3baquecedores-font-owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css');
	wp_enqueue_style( '3baquecedores-flaticon', get_template_directory_uri() . '/flaticon/flaticon.css');
	wp_enqueue_style( '3baquecedores-jquery-fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css');
	// wp_enqueue_style( '3baquecedores-style-site', get_template_directory_uri() . '/css/site.css');
	wp_enqueue_style( '3baquecedores', get_stylesheet_uri() );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'tresbaquecedores_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 *  Configurações via redux
 */
if (class_exists('ReduxFramework')) {
	require_once (get_template_directory() . '/redux/sample-config.php');
}

/**
 *  Novo tamanho excerpt
 */
function novo_tamanho_do_resumo($length) {
	return 11;
}
add_filter('excerpt_length', 'novo_tamanho_do_resumo');


// VERSIONAMENTO DE FOLHAS DE ESTILO
function my_wp_default_styles($styles)
{
	$styles->default_version = "01112016";
}
add_action("wp_default_styles", "my_wp_default_styles");

/*
* FUNÇÃO PARA MOSTRAR AS IMAGENS DA GALERIA DO REDUX
*/
function wp_get_attachment( $attachment_id ) {
    $attachment = get_post( $attachment_id );
    return array(
        'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
        'caption' => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href' => get_permalink( $attachment->ID ),
        'src' => $attachment->guid,
        'title' => $attachment->post_title
    );
}


//PAGINAÇÃO
function pagination($pages = '', $range = 4){
    $showitems = ($range * 2)+1;

    global $paged;
    if(empty($paged)) $paged = 1;

    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }

    if(1 != $pages)
    {
        echo "<div class=\"paginador\">";
        //if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
        //if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";

$htmlPaginas = "";
        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                $htmlPaginas .= ($paged == $i)? '<li ><a href="' . get_pagenum_link($i) . '" class="numero selecionado">' . $i . '</a></li>' : '<li ><a href="' . get_pagenum_link($i) . '" class="numero">' . $i . '</a></li>';
            }
        }

        if ($paged < $pages && $showitems < $pages) echo '<a href="' . get_pagenum_link($paged + 1) . '" class="esquerda"><i class="fa fa-chevron-left" aria-hidden="true"></a></i></a>';
        echo $htmlPaginas;

if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo '<a href="' . get_pagenum_link($pages) . '" class="direita"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>';
        echo "</div>\n";
    }
}
