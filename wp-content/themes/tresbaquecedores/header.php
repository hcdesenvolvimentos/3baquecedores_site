<?php

/**

 * The header for our theme.

 *

 * This is the template that displays all of the <head> section and everything up until <div id="content">

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package tresbaquecedores

 */

global $configuracao;



?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

<meta charset="<?php bloginfo( 'charset' ); ?>">

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="profile" href="http://gmpg.org/xfn/11">

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">



<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>



	<header class="<?php if (is_front_page()) { echo "topo paginas-internas";} else { echo "topo paginas-internas";} ?>">	



		<!-- INFORMAÇÕES DE CONTATO -->

		<div class="info-contato">

				

					<p class="correcaoX"><?php echo $configuracao['opt-endereco']; ?></p>

					<p><?php echo $configuracao['opt-telefone']; ?></p>

					<p><?php echo $configuracao['opt-email']; ?></p>

				

			</div>

		

		</div>

		

		<div class="<?php if (is_front_page()) { echo "row correcaoY";} else { echo "row cor-background";} ?>">



			<div class="col-md-3 logo">

				<a href="<?php echo home_url('/'); ?>" title="<?php echo $configuracao['opt-Slogan']; ?>" style="text-shadow: -1px -1px 2px rgba(255, 255, 255, 1);">

					<h1>3B Aquecedores</h1>					

				</a>

			</div>



			<div class="col-md-9">

				<!-- MENU  -->		

				<div class="navbar" role="navigation">	

									

					<!-- MENU MOBILE TRIGGER -->

					<button type="button" id="botao-menu" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">

						<span class="sr-only"></span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>

					</button>



					<!--  MENU MOBILE-->

					<div class="row navbar-header">			



						<nav class="collapse navbar-collapse" id="collapse">



							<ul class="nav navbar-nav">										

							

								<li><a href="<?php echo home_url('/'); ?>" id="inicio">Início</a></li>	

								<li><a href="<?php echo home_url('/quem-somos/'); ?>" id="quem-somos">Quem Somos</a></li>	

								<li class="dropdown">

									<a href="<?php echo home_url('/produto/'); ?>" class="dropdown-toggle" id="produto">Produtos</a>

									<ul class="dropdown-menu">

										<li>

										<?php



												// DEFINE A TAXONOMIA

												$taxonomia = 'categoriaProduto';



												// LISTA AS CATEGORIAS PAI DOS SABORES

												$categoriaProdutos = get_terms( $taxonomia, array(

													'orderby'    => 'count',

													'hide_empty' => 0,

													'parent'	 => 0

												));

												

												

												

												foreach ($categoriaProdutos as $categoriaProduto) {

													$nome = $categoriaProduto->name;				



											?>									

											<a href="<?php echo get_category_link($categoriaProduto->term_id); ?>"><?php echo $nome ?></a>

											<?php } ?>	

											

																				

										</li>																																					

									</ul>							

								</li>

								<li><a href="<?php echo home_url('/assistencia-tecnica/'); ?>" id="tecnica">Assistência técnica</a></li>						

								<li><a href="<?php echo home_url('/blog/'); ?>" id="blog">Blog</a></li>

								

								<li><a href="<?php echo home_url('/contato/'); ?>" id="contato">Contato</a></li>																			

								

							</ul>



						</nav>						



					</div>			

					

				</div>

			</div>

		</div>



	</header>	