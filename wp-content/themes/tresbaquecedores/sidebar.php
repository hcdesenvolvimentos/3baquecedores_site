<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tresbaquecedores
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

	<div class="col-md-2">
		<div class="sidebar">
			<p>Categorias</p>
			<?php

				// CATEGORIA ATUAL
				$categoriaAtual = get_the_category();
				$categoriaAtual = $categoriaAtual[0]->cat_name;

				// LISTA DE CATEGORIAS
				$arrayCategorias = array();
				$categorias=get_categories($args);
				foreach($categorias as $categoria) {
				$arrayCategorias[$categoria->cat_ID] = $categoria->name;
				$nomeCategoria = $arrayCategorias[$categoria->cat_ID];


			?>
			<a href="<?php echo get_category_link($categoria->cat_ID); ?>"><?php echo $nomeCategoria; ?></a>
			<?php } ?>
		</div>
		<div class="mais-vistos">
		<p>	Mais vistos</p>
			<ul>
				 <?php 
				 	$popularpost = new WP_Query( array( 'posts_per_page' => 5, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC' ) );
					
					while ( $popularpost->have_posts() ) : $popularpost->the_post();
					$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$foto = $foto[0];
					
				 ?>
				<li>
					<a href="<?php echo get_permalink(); ?>">
						<div class="foto" style="background: url(<?php echo $foto ?>);"></div>
						<p><?php echo get_the_title(); ?></p>
						<span><?php the_time('j \F \ Y') ?></span>
					</a>
				</li>
				<?php  endwhile; wp_reset_query(); ?>
			</ul>

		</div>
	</div>
	<aside id="secondary" class="widget-area" role="complementary">
					
				</aside><!-- #secondary -->