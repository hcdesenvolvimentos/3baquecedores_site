$(function(){

	/*******************************************************
    *  CARROSSEL DESTAQUE PÁGINA INICIAL
	*******************************************************/
	$(window).bind('scroll', function () {

		var alturaScroll = $(window).scrollTop()

		if (alturaScroll > 50) {
			$(".paginas-internas").addClass('class_name');
		}else{
			$(".paginas-internas").removeClass('class_name');
		}
	});
	$(document).ready(function() {

  		$("#carrossel-topo").owlCarousel({

			items : 1,
	        dots: true,
	        //loop: true,
	        lazyLoad: true,
	        mouseDrag: false,
	        autoplay:true,
		    autoplayTimeout:5000,
		    autoplayHoverPause:true,
		    animateOut: 'fadeOut',
		    smartSpeed: 450,
		    autoplaySpeed: 4000,
  		});
  		var carrossel_destaque = $("#carrossel-topo").data('owlCarousel');
		$('.navegacaoDestqueTras').click(function(){ carrossel_destaque.prev(); });
		$('.navegacaoDestqueFrent').click(function(){ carrossel_destaque.next(); });

 	});
 	/*******************************************************
    *  CARROSSEL DE DEPOIMENTOS
	*******************************************************/

	$(document).ready(function() {

  		$("#carrossel-depoimentos").owlCarousel({

			items : 1,
	        dots: true,
	        loop: true,
	        lazyLoad: true,
	        mouseDrag: true,
	        autoplay:false,
		    autoplayTimeout:5000,
		    autoplayHoverPause:true,
		    animateOut: 'fadeOut',
		    smartSpeed: 450,
		    autoplaySpeed: 4000,
  		});
  		var carrossel_depoimentos = $("#carrossel-depoimentos").data('owlCarousel');
		$('.navegacaoDepoimentosFrent').click(function(){ carrossel_depoimentos.prev(); });
		$('.navegacaoDepoimentosTras').click(function(){ carrossel_depoimentos.next(); });

 	});

 	/*******************************************************
    *  CARROSSEL DE PARCEIROS
	*******************************************************/

	$(document).ready(function() {

  		$("#carrossel-parceiros").owlCarousel({

			items : 4,
	        dots: true,
	        loop: true,
	        lazyLoad: true,
	        mouseDrag: true,
	        autoplay:true,
		    autoplayTimeout:5000,
		    autoplayHoverPause:true,
		    animateOut: 'fadeOut',
		    smartSpeed: 450,
		    autoplaySpeed: 4000,
		     responsiveClass:false,
			        responsive:{
			            320:{
			                items:1
			            },
			            425:{
			                items:2
			            },
			            768:{
			                items:2
			            },
			            1024:{
			                items:4
			            },
			        }
  		});



 	});
 	/*******************************************************
    *  CARROSSEL DE PARCEIROS
	*******************************************************/

	$(document).ready(function() {

  		$("#carrossel-single-produto").owlCarousel({

			items : 4,
	        dots: true,
	        loop: true,
	        lazyLoad: true,
	        mouseDrag: true,
	        autoplay:true,
		    autoplayTimeout:5000,
		    autoplayHoverPause:true,
		    animateOut: 'fadeOut',
		    smartSpeed: 450,
		    autoplaySpeed: 4000,
		     responsiveClass:false,
			        responsive:{
			            320:{
			                items:1
			            },
			            425:{
			                items:2
			            },
			            768:{
			                items:2
			            },
			            1024:{
			                items:4
			            },
			        }
  		});
  		var carrossel_produtos = $("#carrossel-single-produto").data('owlCarousel');
		$('.navegacaoProdutosFrent').click(function(){ carrossel_produtos.prev(); });
		$('.navegacaoProdutosTras').click(function(){ carrossel_produtos.next(); });



 	});

 	$("a#fancy").fancybox({
		'titleShow' : false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		closeBtn    : true,
		arrows      : true,
		nextClick   : true
	});

 	$(".navegacaoDestqueFrent").mouseenter(function(e){
 		$(".foto-anterior").fadeIn();
 	});

 	$(".navegacaoDestqueFrent").mouseleave(function(e){
 		$(".foto-anterior").fadeOut();
 	});

 	$(".navegacaoDestqueTras").mouseenter(function(e){
 		$(".foto-posterior").fadeIn();
 	});

 	$(".navegacaoDestqueTras").mouseleave(function(e){
 		$(".foto-posterior").fadeOut();
 	});


 	// HOVER MENU
		$('li.dropdown').mouseover(function(e){
			$(this).addClass('open');
			$(this).find('a.dropdown-toggle');
		});
		$('li.dropdown').mouseout(function(e){
			$(this).find('a.dropdown-toggle');
		});
		$('li.dropdown ul').mouseout(function(){
			$(this).parent().removeClass('open');
		});
		$('li.dropdown').mouseout(function(){
			$(this).removeClass('open');
		});





});
