<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package tresbaquecedores
 */

get_header(); ?>

	<div class="pg pg-blog">
	<div class="container">
		<div class="row">
			<?php get_sidebar(); ?>
			<div class="col-md-10">
				<!-- NOSSO BLOG -->
				<section>
					<div class="sub-titulo">
						<p>Nosso blog</p>
					</div>

					<div class="areaTituloFrase">
						<strong><?php echo $configuracao['opt-blog-titulo'] ?></strong>
						<p><?php echo $configuracao['opt-blog-frase'] ?></p>
					</div>

					<div class="area-blog">
						<ul>
						<?php
							$i = 0;
							 $cores = array(
			            		0 => "52, 62, 93, 0.59",
			            		1 => "184, 79, 70, 0.74",
			            		2 => "189, 135, 68, 0.75",
			            		3 => "52, 62, 93, 0.59",
			            	);
							if ( have_posts() ) :while ( have_posts() ) : the_post();
								$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$foto = $foto[0];

								if ($i == 4) { $i = 0;}
						?>
							<li >
								<a href="<?php echo get_permalink(); ?>" style="background:url(<?php echo $foto ?>);">
									<div class="lente" style="background:rgba(<?php echo $cores[$i] ?>);">
										<h4><?php echo get_the_title(); ?></h4>
									</div>
								</a>
								<span><b><?php the_time('j') ?></b> <?php the_time('\d\e F \d\e Y') ?></span>
								<p>
									<?php
										$textoresumido = get_the_content();
										$textocurto = substr($textoresumido, 0, 80).'...';
										echo $textocurto;
									?>
								</p>

								<a href="<?php echo get_permalink(); ?>" class="ver-mais">[leia mais...]</a>
							</li>
						<?php $i++; endwhile;endif; wp_reset_query(); ?>


						</ul>

					</div>
					<div class="paginador">
						<ul>
						<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
						</ul>
					</div>
				</section>

			</div>
		</div>
	</div>
</div>

<?php

get_footer();
