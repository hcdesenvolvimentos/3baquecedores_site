<?php

/**

 * The template for displaying all single posts.

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post

 *

 * @package tresbaquecedores

 */

global $configuracao;

$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

$foto = $foto[0];



$IMAGEM = $configuracao['opt-produtos-foto-cabecalho']['url'];



$tipo = rwmb_meta('TresB_produto_tipo');

$marca = rwmb_meta('TresB_produto_marca');

$descricao = rwmb_meta('TresB_produto_descricao');

$imagens = rwmb_meta('TresB_produto_imagens');

$dados = rwmb_meta('TresB_produto_dados');

$aplicacoes = rwmb_meta('TresB_produto_aplicacoes');

$beneficios = rwmb_meta('TresB_produto_beneficios');
$produto_catalago = rwmb_meta('TresB_produto_catalago');



get_header(); ?>



<!-- PÁGINA DE DETALHES DO PRODUTO -->

	<div class="pg pg-produto" style="display:">

		<div class="container">

			<div class="bg-produtos" style="background: url(<?php echo $IMAGEM  ?>);"></div>

			<!-- SIDEBAR -->

			<div class="sidebar-produtos">

				<?php



				// DEFINE A TAXONOMIA

				$taxonomia = 'categoriaProduto';



				// LISTA AS CATEGORIAS PAI DOS SABORES

				$categoriaProdutos = get_terms( $taxonomia, array(

					'orderby'    => 'count',

					'hide_empty' => 0,

					'parent'	 => 0

				));







				foreach ($categoriaProdutos as $categoriaProduto) {

					$nome = $categoriaProduto->name;



				//RECUPERANDO CATEGORA ATUAL

				$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

				$term->name;

				//VERIFICANDO SE CATEGORIA NOME É IGUAL A CATEGIRA ATUAL

				if ($nome == $term->name) {





			?>



				<a class="bordas" style="font-weight:bold" href="<?php echo get_category_link($categoriaProduto->term_id); ?>"><?php echo $nome ?></a>

			<?php }else{?>



				<a  href="<?php echo get_category_link($categoriaProduto->term_id); ?>"><?php echo $nome ?></a>



			<?php }} ?>

			</div>



			<!-- INFORMAÇÕES DE DANOS TÉCNICOS -->

			<section class="area-produto">

				<div class="row">

					<div class="col-md-4">

						<div class="foto-produto">

							<img src="<?php echo $foto  ?>" class="img-responsive" alt="">

						</div>

						<div class="fotos">

							<ul>

							<?php

								foreach ($imagens  as $imagem ) {



							 ?>

								<li>

									<a id="fancy" rel="gallery1" href="<?php 	echo $imagem['url'] ?>" ><img class="img-responsive" src=" <?php 	echo $imagem['url'] ?> " alt="" ></a>

								</li>

								<?php 	}; ?>

							</ul>

						</div>

					</div>

					<div class="col-md-8 correcaoX">

						<!-- INFORMAÇÕES DO PRODUTO -->

						<div class="info-produto">

							<h2><?php echo $tipo ?></h2>

							<span><?php echo $marca ?></span>

							<p><?php echo $descricao ?> </p>

							<a href=" <?php 	echo $produto_catalago  ?> " class="catalago" target="_blank">Ver catálago</a>

						</div>





						<div class="detalhe-info-produto">

							<div class="row">
								<?php if ($aplicacoes):  ?>
								<div class="col-sm-6">

									<div class="aplicacoes">

										<p>Aplicações</p>

										<?php 	if ($aplicacoes[0] != null) {



										?>

										<span><i class="fa fa-chevron-down icon"></i><?php echo $aplicacoes[0]; ?></span>

										<?php 	};

											if ($aplicacoes[1] != null) {

										?>

										<span><i class="fa fa-chevron-down icon"></i><?php echo $aplicacoes[1]; ?></span>

										<?php 	};

											if ($aplicacoes[2] != null) {

										 ?>

										<span><i class="fa fa-chevron-down icon"></i><?php echo $aplicacoes[2]; ?></span>

										<?php

											};

											if ($aplicacoes[3] != null) {

										 ?>

										<span><i class="fa fa-chevron-down icon"></i><?php echo $aplicacoes[3]; ?></span>

											<?php 	}; ?>

									</div>

								</div>
								<?php endif; ?>
								
								<?php if ($beneficios): ?>
								<div class="col-sm-6">

									<div class="beneficios">

										<p>Benefícios</p>

										<?php 	if ($beneficios[0] != null) {



										?>

										<span><i class="fa fa-chevron-down" aria-hidden="true"></i></i><?php echo $beneficios[0]; ?></span>

										<?php 	};

											if ($beneficios[1] != null) {

										?>

										<span><i class="fa fa-chevron-down" aria-hidden="true"></i></i><?php echo $beneficios[1]; ?></span>

										<?php 	};

											if ($beneficios[2] != null) {

										 ?>

										<span><i class="fa fa-chevron-down" aria-hidden="true"></i></i><?php echo $beneficios[2]; ?></span>

										<?php

											};

											if ($beneficios[3] != null) {

										 ?>

										<span><i class="fa fa-chevron-down" aria-hidden="true"></i></i><?php echo $beneficios[3]; ?></span>

											<?php 	}; ?>



									</div>

								</div
								<?php endif; ?>

							</div>

						</div>

						<div class="caixa-info">

							<div class="dados-tecnicos">

								<p><strong>Dados técnicos</strong></p>

								<div class="row bg">

									<div class="col-sm-12 dados">

										<?php echo rwmb_meta('TresB_produto_tabela'); ?>

									</div>

								</div>

							</div>

							<div class="icon1 flaticon-commercial-delivery-symbol-of-a-list-on-clipboard-on-a-box-package"></div>

						</div>

					</div>

				</div>

			</section>



			<div class="titulo">

				<p>Outros produtos</p>

			</div>



			<!-- CARROSSEL -->

			<section class="carrossel-produtos">



				<button class="navegacaoProdutosFrent hidden-xs"><i class="fa fa-angle-left"></i></button>

				<button class="navegacaoProdutosTras hidden-xs"><i class="fa fa-angle-right"></i></button>





				<div id="carrossel-single-produto" class="owl-Carousel">

					<?php

						// LOOP DE DESTAQUE

						$produtos = new WP_Query( array( 'post_type' => 'produto', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 8 ) );



		                while ( $produtos->have_posts() ) : $produtos->the_post();

		                	$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

							$foto = $foto[0];

							$tipo = rwmb_meta('TresB_produto_tipo');

							$nome =	explode(",", get_the_title());

					?>

					<div class="item">

						<section class="produto">

							<ul>

								<li>

									<a href="<?php echo get_permalink(); ?>" class="foto" ><img src="<?php echo $foto  ?>" alt=""></a>

									<h2><?php 	echo $tipo ?></h2>



									<p><?php 	echo $nome[0] ?></p>

									<p><?php 	echo $nome[1] ?></p>

									<a href="<?php echo get_permalink(); ?>" class="ver">Ver detalhes</a>

								</li>

							</ul>

						</section>

					</div>

					<?php  endwhile; wp_reset_query(); ?>

				</div>

			</section>

		</div>

	</div>

<?php



get_footer();

