<?php

/**

 * Template Name: Quem Somos

 * Description: Página de contato 3B Aquecedores

 *

 * @package tresbaquecedores

 */

global $configuracao;

$img  = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');

$img  = $img[0];

//TEXTO PRINCIPAL

  $texto_quem_somos	= $configuracao['opt-quem-somos-texto'];



//NÚMERO E TEXTO PRIMEIRO ITEM 1

  $quem_somos_item1	= $configuracao['opt-quem-somos-item1'];

  $item1 = explode(";", $quem_somos_item1);



//NÚMERO E TEXTO PRIMEIRO ITEM 2

  $quem_somos_item2	= $configuracao['opt-quem-somos-item2'];

  $item2 = explode(";", $quem_somos_item2);

//NÚMERO E TEXTO PRIMEIRO ITEM 3

  $quem_somos_item3	= $configuracao['opt-quem-somos-item3'];

  $item3 = explode(";", $quem_somos_item3);

//NÚMERO E TEXTO PRIMEIRO ITEM 4 -

  $quem_somos_item4	= $configuracao['opt-quem-somos-item4'];

  $item4 = explode(";", $quem_somos_item4);



//GALERIA DE IMAGENS

  $galeria_fotos = $configuracao['opt-quem-somos-galeria'];



//ENDEREÇO

  $endereco = $configuracao['opt-endereco'];

//TELEFONE

  $telefone = $configuracao['opt-telefone'];

//EMAIL

  $email = $configuracao['opt-email'];



get_header(); ?>



<!-- PÁGINA QUEM SOMOS -->

	<div class="pg pg-quem-somos">

		<div class="container">

			<!-- QUEM SOMOS 3B AQUECEDORES -->

			<section>

				<div class="sub-titulo">

					<p><?php echo $configuracao['opt-quem-somos-titulo']; ?></p>

				</div>



				<div class="texto-quem-somos"><?php echo $texto_quem_somos; ?></div>



				<!-- INFORMAÇÕES DA EMPRESA  -->
				<section class="info-3b">

					<div class="row">

						<div class="col-sm-3 info">

							<span class="flaticon-mission icon"></span>

							<small> <?php echo $item1[0] ?></small>

							<p><?php echo $item1[1] ?></p>

						</div>

						<div class="col-sm-3 info">

							<span class="flaticon-product-realise icon"></span>

							<small><?php echo $item2[0]; ?></small>

							<p><?php echo $item2[1]; ?></p>

						</div>

						<div class="col-sm-3 info">

							<span class="flaticon-marketing icon"></span>

							<small><?php echo $item3[0]; ?></small>

							<p><?php echo $item3[1]; ?></p>

						</div>

						<div class="col-sm-3 info">

							<span class="flaticon-plan-idea icon"></span>

							<small><?php echo $item4[0]; ?></small>

							<p><?php echo $item4[1]; ?></p>

						</div>

					</div>

				</section>

				<div class="foto-quem-somos">

					<div class="foto" style="background: url(<?php echo $img ?>);">

						<div class="lente">

							<p class="subtitulo">Saiba como nos encontrar</p>



							<div class="info">

								<p><i class="flaticon-map-pin-silhouette icon"></i><?php echo $endereco  ?></p>

								<p><i class="flaticon-telephone icon"></i> <?php echo $telefone  ?></p>

								<p><a href="malito:<?php echo $email ?>"><i class="flaticon-close-envelope icon"></i> <?php echo $email  ?></a></p>

							</div>

						</div>

					</div>

				</div>

			</section>



		





			<!-- GALERIA DE FOTOS -->

			<div class="galeria">



				<div class="titulo">

					<p>galeria de fotos</p>

				</div>



				<ul>



					<?php

					    $myGalleryIDs = explode(',', $configuracao['opt-quem-somos-galeria']);

					    foreach($myGalleryIDs as $myPhotoID):

					        $photoArray = wp_get_attachment($myPhotoID);

				    ?>

						    <li>

								<a id="fancy" rel="gallery1" href="<?php echo wp_get_attachment_url( $myPhotoID ); ?>" style="background: url(<?php echo wp_get_attachment_url( $myPhotoID ); ?>);"><div class="lente"></div></a>

							</li>

					<?php endforeach; ?>



				</ul>



			</div>



		</div>

	</div>





<?php get_footer(); ?>