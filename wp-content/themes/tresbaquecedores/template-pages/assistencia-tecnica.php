<?php

/**

 * Template Name: Assistência técnica

 * Description: Página de Assistência técnica 3B Aquecedores

 *

 * @package tresbaquecedores

 */

$img  = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');

$img  = $img[0];

//PÁGINA ASSISTÊNCIA TÉCNICA 

//IMAGEM DO CABEÇALHO  

$foto = $configuracao['opt-assistencia-foto-cabecalho']['url'];

//TEXTO DO CABEÇALHO  

$texto = $configuracao['opt-assistencia-texto'];



$TELEFONE =  $configuracao['opt-telefone'];

get_header(); ?>

<!-- PÁGINA DE ASSISTÊNCIA TÉCNICA -->

<div class="pg pg-assistencia-tecnica">

	<div class="container">

		<!-- FOTO  -->

		<div class="bg-assistencia" style="background: url(<?php echo $foto ?>);">

			<div class="lente">

				<p><?php echo get_the_title(); ?></p>

				<span><?php echo $texto; ?> </span>

			</div>			

		</div>



		<!-- FORMULÁRIO DE CONTANTO  -->

		<section class="row">

			<div class="col-md-12 text-center">

				<div class="logo">

					<img src="<?php bloginfo('template_directory'); ?>/

img/logo3b.png" class="img-responsive" alt="" style="display:none">

				</div>

				<div class="info">

					<p>Venda & Assistência técnica</p>

					<span>Ligue e solicite a presença de nossos profissionais</span>

					<small><?php echo $TELEFONE ?></small>

				</div>

			</div>

			<div class="col-md-12">

				<div class="form">

					<?php echo do_shortcode('[contact-form-7 id="158" title="Formulário Assistência técnica"]'); ?>

				</div>

			</div>

		</section>

	</div>

</div>



<?php get_footer(); ?>