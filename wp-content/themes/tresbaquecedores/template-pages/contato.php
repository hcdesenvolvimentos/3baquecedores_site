<?php
/**
 * Template Name: Contato
 * Description: Página de contato 3B Aquecedores
 *
 * @package tresbaquecedores
 */

get_header(); ?>

<div class="pg pg-contato">
	<div class="container">
		<div class="titulo">
			<p><?php echo $configuracao['opt-contato-titulo'] ?></p>
		</div>

		<div class="areaTituloFrase text-center">
			<strong></strong>
			<p><?php echo $configuracao['opt-contato-frase'] ?></p>
		</div>

		<section class="contato">
			<div class="row">
				<div class="col-md-6">
					<div class="info-contato">
						<div class="logo">
							<img src="<?php bloginfo('template_directory'); ?>/img/logo3b.png" height="272" width="251" alt="">
							<p>3b aquecedores</br> vendas & assistência técnica</p>
							<span><?php echo $configuracao['opt-horario']; ?></span>
						</div>

						<div class="endereco" style="display: none;">
							<ul>
								<li><span class="flaticon-map-pin-silhouette"></span><p><?php echo $configuracao['opt-endereco']; ?></p></li>
								<li><span class="flaticon-telephone"></span><p><?php echo $configuracao['opt-telefone']; ?></p></li>
								<li><span class="flaticon-close-envelope"></span><p><?php echo $configuracao['opt-email']; ?></p></li>
							</ul>

						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-contato">
						<p style="display:none;">Entre em contato conosco</p>
						<span class="subt" style="display:none;">Preencha o formulário abaixo e envie</span>

						<?php echo do_shortcode('[contact-form-7 id="61" title="Contato"]'); ?>

					</div>
				</div>
			</div>

		</section>

	</div>
	<iframe class="mapaContato" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3603.0148372836043!2d-49.23067768449994!3d-25.437767483784807!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce507bf61fd8b%3A0xb36c7de945d66d59!2s3B+Aquecedores+a+G%C3%A1s!5e0!3m2!1spt-BR!2sbr!4v1504549149429" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

<?php get_footer(); ?>