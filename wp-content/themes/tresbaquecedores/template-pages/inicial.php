<?php

/**

 * Template Name: Inicial

 * Description: Página inicial 3B Aquecedores

 *

 * @package tresbaquecedores

 */





get_header(); ?>



<div class="carrossel">



	<div class="botoes-destaque">

		<div class="botao">

			<button class="navegacaoDestqueTras hidden-xs"><i class="fa fa-angle-right"></i> </button>

		</div>

		<div class="botao2">

			<button class="navegacaoDestqueFrent hidden-xs"><i class="fa fa-angle-left"></i> </button>

		</div>



	</div>



	<!-- CARROSSEL -->

	<div id="carrossel-topo" class="owl-Carousel">



		<?php



			// LOOP DE DESTAQUE



			$destaquesPost = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );





			$destaquesPost = [ 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ];



			/*	PEGANDO A ÚLTIMA E A PRIMEIRA FOTO DO CUSTOM POST TYPE	*/

            $posts = get_posts($destaquesPost);

            $primeiro = $posts[0];

            $ultimo = end($posts);

            $primeiraFoto = wp_get_attachment_image_src( get_post_thumbnail_id($primeiro->ID), 'full' );

            $primeiraFoto = $primeiraFoto[0];

            $ultimaFoto = wp_get_attachment_image_src( get_post_thumbnail_id($ultimo->ID), 'full' );

            $ultimaFoto = $ultimaFoto[0];



        	/* TRANSFORMA EM UMA QUERY PARA SER PERCORRIDO */

        	$destaquesPost = new WP_Query($destaquesPost);





            while ( $destaquesPost->have_posts() ) : $destaquesPost->the_post();



				$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

				$foto = $foto[0];

		?>



		<div class="item" style="background: url(<?php echo "$foto"; ?>);">

			<div class="lente">

				<!-- SE HOUVER POST ANTERIOR MOSTRA A FOTO SE NÃO MOSTRA A FOTO DO ULTIMO POST DISPONÍVEL -->

				<?php $prevPost = get_previous_post(); if($prevPost) { $prevFoto = wp_get_attachment_image_src( get_post_thumbnail_id($prevPost->ID), 'full' ); $prevFoto = $prevFoto[0]; } else { $prevFoto = $ultimaFoto; }; ?>



				<div class="foto-anterior" style=" background: url(<?= "$prevFoto"; ?>) no-repeat;"></div>

				<a href="<?php echo rwmb_meta('TresB_destaque_link'); ?>" class="linkBanner"></a>

				<div class="descicao">



					<!-- TÍTULO -->

					<h2><?php echo get_the_title(); ?></h2>



					<!-- BREVE DESCRIÇÃO -->

					<p><?php echo rwmb_meta('TresB_destaque_descricao'); ?></p>



					<a href="<?php echo rwmb_meta('TresB_destaque_link'); ?>">Saiba mais</a>

				</div>



				<!-- SE HOUVER POST POSTERIOR MOSTRA A FOTO SE NÃO MOSTRA A FOTO DO PRIMEIRO POST DISPONÍVEL -->

				<?php $nextPost = get_next_post(); if($nextPost) { $nextFoto = wp_get_attachment_image_src( get_post_thumbnail_id($nextPost->ID), 'full' ); $nextFoto = $nextFoto[0]; } else { $nextFoto = $primeiraFoto; } ; ?>



				<div class="foto-posterior" style=" background: url(<?= "$nextFoto"; ?>) no-repeat;"></div>

			</div>

		</div>



		<?php endwhile; wp_reset_query(); ?>



	</div>

</div>



<!-- PÁGINA INICIAL -->

<div class="pg pg-inicial">

	<div class="container">
		<?php 
				//LOOP DE POST CATEGORIA DESTAQUE				
				$produtos = new WP_Query(array(
					'post_type'     => 'produto',
					'posts_per_page'   => 3,
					'tax_query'     => array(
						array(
							'taxonomy' => 'categoriaProduto',
							'field'    => 'slug',
							'terms'    => 'aquecedores-de-passagem',
							)
						)
					)
				);

				if ($produtos):
			?>

		<!-- PRODUTOS -->
		<section class="produto">

			<div class="titulo">

				<p>Produtos</p>

			</div>
			<ul>

			<?php
				// LOOP DE POST
				while ( $produtos->have_posts() ) : $produtos->the_post();
					$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$foto = $foto[0];
					$tipo = rwmb_meta('TresB_produto_tipo');
					$nome =	explode(",", get_the_title());
					$marca = rwmb_meta('TresB_produto_marca');
			 ?>
				<li>
					<a href="<?php echo get_permalink(); ?>" class="foto" style="background: url(<?php echo $foto  ?>);">
					<div class="areaTexto">	
						<h2><?php 	echo $tipo  ?></h2>

						<p><?php 	echo $nome[0] ?></p>
						<p><?php 	echo $nome[1] ?></p>
					</div>
				</a>
					<h2 style="color:#ae3433"><?php 	echo $tipo  ?></h2>
					<p style="color: #30415a;"><?php echo $marca ?></p>
					<p style="color: #30415a;"><?php 	echo $nome[1] ?></p>
					<a href="<?php echo get_permalink(); ?>" class="ver">Ver detalhes</a>
				</li>
			<?php  endwhile; wp_reset_query(); ?>
			</ul>

		</section>

		<?php endif; ?>

		<!-- ÁREA QUEM SOMOS -->

		<section>

			<div class="titulo">

				<p>Quem Somos</p>

			</div>



			<!-- TETXTO QUEM SOMOS -->

			<div class="texto-quem-somos">

				<p>	<?php echo $configuracao['opt-inicial-sobre']; ?> </p>

				<a href="<?php echo home_url('/quem-somos/'); ?>"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>

			</div>

		</section>



		<!-- O QUE FAZEMOS -->

		<section class="row altura">

			<div class="col-md-4">

				<div class="foto-oquefazemos" style="background:url(<?php echo $configuracao['opt-inicial-sobre-foto']['url'] ?>);"></div>

			</div>



			<div class="col-md-8">

				<div class="info-oque-fazemos">

					<div class="titulo">

						<p>	O que fazemos ?</p>

					</div>

					<div class="row">

						<div class="col-md-6 text-center">

							<div class="info">

								<i ></i>

								<div class="alinhamento"><p class="text-center" id="A"><?php echo $configuracao['opt-inicial-tituloServicoEsquerda']; ?></p></div>

								<?php

									$itensAreaEsquerda = $configuracao['opt-inicial-instalacao'];

									foreach ($itensAreaEsquerda as $itemAreaEsquerda):

								?>

								<span><?php echo $itemAreaEsquerda; ?> </span>

								<?php endforeach;?>

							</div>

						</div>

						<div class="col-md-6 text-center">

							<div class="info">

								<i ></i>

								<div class="alinhamento"><p id="B"><?php echo $configuracao['opt-inicial-tituloServicoDireita']; ?></p></div>

								<?php

									$itensAreaDireita = $configuracao['opt-inicial-assistencia'];

									foreach ($itensAreaDireita as $itemAreaDireita):

								?>

								<span><?php echo $itemAreaDireita; ?> </span>

								<?php endforeach;?>

							</div>

						</div>

					</div>

				</div>

			</div>

		</section>



		<!-- DEPOIMENTOS -->

		<section>

			<div class="sub-titulo">

				<p>	Depoimentos</p>

			</div>



			<!-- CARROSSEL DE DEPOIMENTOS -->

			<div class="carrossel-depoimentos">

				<div id="carrossel-depoimentos" class="owl-Carousel">



					<?php

						// LOOP DE DESTAQUE

						$depoimentosPost = new WP_Query( array( 'post_type' => 'depoimentos', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );



			            while ( $depoimentosPost->have_posts() ) : $depoimentosPost->the_post();

						$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

						$foto = $foto[0];

					?>



					<div class="item" style="background: url(<?php echo "$foto"; ?>);">

						<div class="lente">

							<p><i class="fa fa-quote-left" aria-hidden="true"></i><?php echo rwmb_meta('TresB_depoimento_texto'); ?></p>

							<span><?php echo rwmb_meta('TresB_depoimento_autor'); ?></span>

							<small><?php echo rwmb_meta('TresB_depoimento_autor_info'); ?></small>

						</div>

					</div>



					<?php endwhile; wp_reset_query(); ?>



				</div>

				<a href="<?php echo home_url('/depoimentos/'); ?>" class="navegacaoDepoimentosFrent hidden-xs">+</a>

			</div>

		</section>



		<!-- PARCEIROS -->

		<section>

			<div class="titulo">

				<p>	 Marcas que trabalhamos</p>

			</div>

			<span class="frase"><?php echo $configuracao['opt-inicial-parceiros']; ?></span>



			<!-- CARROSSEL DE DEPOIMENTOS -->

			<div class="carrossel-parceiros">

				<div id="carrossel-parceiros" class="owl-Carousel">



					<?php

						// LOOP DE DESTAQUE

						$parceirosPost = new WP_Query( array( 'post_type' => 'parceiro', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );



			            while ( $parceirosPost->have_posts() ) : $parceirosPost->the_post();



						$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

						$foto = $foto[0];

					?>



					<div class="item">

						<a href="<?php echo rwmb_meta('TresB_parceiro_link'); ?>" target="_blank"><img class="img-responsive" src="<?php echo "$foto"; ?>" alt=""></a>

					</div>



					<?php endwhile; wp_reset_query(); ?>



				</div>

			</div>

		</section>



		<!-- NOSSO BLOG -->

		<section>

			<div class="titulo">

				<p>Nosso blog</p>

			</div>



			<div class="area-blog">

				<ul>



					<?php

						// LOOP DE DESTAQUE

						$parceirosPost = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 3 ) );



			            $cores = array(

		            		1 => "52, 62, 93, 0.59",

		            		2 => "184, 79, 70, 0.74",

		            		3 => "189, 135, 68, 0.75",

		            	);

		            	$i = 1;

			            while ( $parceirosPost->have_posts() ) : $parceirosPost->the_post();



						$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

						$foto = $foto[0];



					?>



					<li >

						<a href="<?php echo get_permalink(); ?>" style="background:url(<?php echo "$foto"; ?>);">

							<div class="lente" style="background:rgba(<?php echo $cores[$i]; ?>);">

								<h4><?php echo get_the_title(); ?></h4>

							</div>

						</a>

						<span><b><?php the_time('d') ?> </b>de <?php the_time('F') ?> <?php the_time('Y') ?></span>

						<p><?php echo the_excerpt(); ?></p>

						<a href="<?php echo get_permalink(); ?>" class="ver-mais">[leia mais...]</a>

					</li>



					<?php $i++; endwhile; wp_reset_query(); ?>



				</ul>

			</div>

		</section>



		<!-- NOSSO PRODUTOS -->

		<section style="display: none;">

			<div class="titulo">

				<p>Produtos</p>

			</div>



			<div class="produtos">

				<ul>

					<?php





						// DEFINE A TAXONOMIA

						$taxonomia = 'categoriaProduto';



						// LISTA AS CATEGORIAS PAI DOS SABORES

						$categoriaProdutos = get_terms( $taxonomia, array(

							'orderby'    => 'count',

							'hide_empty' => 0,

							'parent'	 => 0

						));



						foreach ($categoriaProdutos as $categoriaProduto) {

							$nome = $categoriaProduto->name;

							$foto = $categoriaProduto->description;



							$categoriaAtivaImg = z_taxonomy_image_url($categoriaProduto->term_id);

					?>



					<li style="background: url(<?php echo $foto; ?>);">

						<a href="<?php echo get_category_link($categoriaProduto->term_id); ?>">

							<div class="lente">

								<h2><?php echo $nome ; ?></h2>

							</div>

						</a>

					</li>



					<?php } ?>



				</ul>

			</div>

		</section>





	</div>

</div>



<?php get_footer(); ?>