<?php

/**

 * The template for displaying the footer.

 *

 * Contains the closing of the #content div and all content after.

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package tresbaquecedores

 */

global $configuracao;

//INFORMAÇÕES

	$ENDERECO = $configuracao['opt-endereco'];

	$endereco = explode(",", $ENDERECO);

	$TELEFONE= $configuracao['opt-telefone'];

	$EMAIL = $configuracao['opt-email'];

	$HORÁRIO = $configuracao['opt-horario'];

	$FRASE = $configuracao['opt-newsletter-frase'];



//LINKS REDES SOCIAIS

	$FACEBOOK = $configuracao['opt-facebook'];

	$TWITTER = $configuracao['opt-twitter'];

	$YOUTUBE = $configuracao['opt-youtube'];

?>

<!-- RODAPÉ -->

<footer class="rodape">



	<!-- ÁREA FORMULÁRIO DE NOVIDADES  -->

	<section class="bg-fundo">

		<div class="container">

			<div class="row">

				<!-- LOGO  -->

				<div class="col-sm-6">

					<div class="logo-rodape">

						<img src="<?php bloginfo('template_directory'); ?>/img/logo3b.png" class="img-responsive" alt="">

					</div>

				</div>



				<!-- FORMULÁRIO DE NOVIDADES  -->

				<div class="col-sm-6">

					<div class="form-rodape">

						<p><?php echo $FRASE; ?></p>





						<!--START Scripts : this is the script part you can add to the header of your theme-->

						<script type="text/javascript" src="http://3baquecedores.pixd.com.br/wp-includes/js/jquery/jquery.js?ver=2.7.1"></script>

						<script type="text/javascript" src="http://3baquecedores.pixd.com.br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.7.1"></script>

						<script type="text/javascript" src="http://3baquecedores.pixd.com.br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.1"></script>

						<script type="text/javascript" src="http://3baquecedores.pixd.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.1"></script>

						<script type="text/javascript">

		                /* <![CDATA[ */

		                var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://3baquecedores.pixd.com.br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};

		                /* ]]> */

		                </script><script type="text/javascript" src="http://3baquecedores.pixd.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.1"></script>

						<!--END Scripts-->



						<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html5735ce54b9772-2" class="wysija-msg ajax"></div><form id="form-wysija-html5735ce54b9772-2" method="post" action="#wysija" class="widget_wysija html_wysija">



						   <div class="form-group">

							    <label class="hidden" for="nome completo">Nome completo:</label>



							    	<input type="text" name="wysija[user][firstname]" placeholder="nome completo" class="form-control validate[required]" title="Nome completo"  value="" />



							</div>





						    <span class="abs-req">

						        <input type="text" name="wysija[user][abs][firstname]" class="wysija-input validated[abs][firstname]" value="" />

						    </span>







						   <div class="form-group">

							    <label class="hidden" for="Seu email">Seu email:</label>





						    	<input  placeholder="seu email" type="text" name="wysija[user][email]" class="form-control validate[required,custom[email]]" title="Email"  value="" />

					    	</div>





						    <span class="abs-req">

						        <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />

						    </span>



							<input class="enviar wysija-submit-field" type="submit" value="Cadastre-se" />



						    <input type="hidden" name="form_id" value="2" />

						    <input type="hidden" name="action" value="save" />

						    <input type="hidden" name="controller" value="subscribers" />

						    <input type="hidden" value="1" name="wysija-page" />





						        <input type="hidden" name="wysija[user_list][list_ids]" value="3" />



						 </form></div>

					</div>

				</div>

			</div>

		</div>

	</section>



	<!-- ÁREA INFORMAÇÕES DO RODAPÉ -->

	<section class="area-info-rodape">

		<div class="container">

			<div class="row">

				<div class="col-sm-6 correcaoX">

					<p class="subtitulo">Informações <span>de <small>Contato</small></span></p>

					<div class="endreco">

						<p class="rua"><i class="flaticon-map-pin-silhouette icon"></i> <?php echo $endereco[0]  ?></p>

						<p class="bairro"><i class="flaticon-map-pin-silhouette  icon" style="opacity: 0;"></i> <?php echo $endereco[1]  ?></p>

						<p><i class="flaticon-telephone icon"></i> <?php echo $TELEFONE ?></p>

						<p><a href="malito:<?php echo $EMAIL ?>"><i class="flaticon-close-envelope icon"></i> <?php echo $EMAIL ?></a></p>

					</div>

					<div class="redes-sociais">

						<a href="<?php echo $FACEBOOK ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>

						<a href="<?php echo $TWITTER ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>

						<a href="<?php echo $YOUTUBE ?>" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a>

					</div>

				</div>

				<div class="col-sm-6 text-center">

					<p class="subtitulo titulomapa">Mapa <span>do <small>Site</small></span></p>



						<div class="mapa">

							<a href="<?php echo home_url('/quem-somos/'); ?>">Quem somos</a>

							<a href="<?php echo home_url('produto/'); ?>">Produtos</a>

							<a href="<?php echo home_url('/assistencia-tecnica/'); ?>">Assistência técnica</a>

							<a href="<?php echo home_url('/blog/'); ?>">Blog</a>

							<a href="<?php echo home_url('/depoimentos/'); ?>">Depoimentos</a>

							<a href="<?php echo home_url('/contato/'); ?>">Contato</a>

						</div>

				</div>

			</div>

		</div>

	</section>



	<div class="copyright">

		<p><i class="fa fa-copyright" aria-hidden="true"></i> Copyright 2016 <span>3B Aquecedores</span>  - Todos os direitos reservados.</p>

	</div>



</footer>

<div class="col-md-12" style="text-align: center; background: #a23537;">

  <small>

    <span style="vertical-align: text-bottom;color:#fff;">Desenvolvido por</span>

    <a href="http://palupa.com.br/" target="_blank" title="Desenvolvido por Palupa Marketing" style="display: inline-block;"><img src="<?php echo get_template_directory_uri(); ?>/img/palupaLogo.png" style="max-height: 15px;"></a>

  </small>

</div>

<?php wp_footer(); ?>



</body>

</html>

