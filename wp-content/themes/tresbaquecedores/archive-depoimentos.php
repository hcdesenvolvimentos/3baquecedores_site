<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package tresbaquecedores
 */

get_header(); ?>

	<!-- PÁGINA DE DEPOIMENTOS -->
	<div class="pg pg-depoimentos">
		<div class="container">
			<div class="titulo">
				<p><?php echo $configuracao['opt-depoimentos-titulo'] ?></p>
			</div>

			<div class="areaTituloFrase">
			<strong></strong>
			<p><?php echo $configuracao['opt-depoimentos-frase'] ?></p>
			</div>


			<section class="depoimentos">
				<?php
					// LOOP DE DESTAQUE
					$depoimentos = new WP_Query( array( 'post_type' => 'depoimento', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 8 ) );
	                $i = 0;
	                 $cores = array(
	            		0 => "191, 78, 77, 0.6",
	            		1 => "177, 115, 39, 0.73",
	            		2 => "81, 89, 102, 0.81",
	            	);
	               if ( have_posts() ) :while ( have_posts() ) : the_post();
	                	$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$foto = $foto[0];
						$depoimento = rwmb_meta('TresB_depoimento_texto');
						$autor = rwmb_meta('TresB_depoimento_autor');
						$empresa = rwmb_meta('TresB_depoimento_autor_info');

						if ($i == 4) { $i = 0;}
				?>

					<div class="depoimento" style="background: url(<?php echo $foto ?>);">
						<div class="lente" style="background: rgba(<?php echo $cores[$i] ?>)">
							<p><?php echo $depoimento ?></p>
							<span><?php echo $autor ?></span>
							<small><?php echo $empresa  ?></small>
						</div>
					</div>

				<?php $i++; endwhile;endif; wp_reset_query(); ?>
			</section>

			<div class="paginador">
				<ul>
					<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
				</ul>
			</div>
		</div>
	</div>

<?php

get_footer();
