<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package tresbaquecedores
 */global $configuracao;
$IMAGEM = $configuracao['opt-produtos-foto-cabecalho']['url'];

$categoriaAtivaImg = z_taxonomy_image_url($categoriaProduto->term_id);
get_header(); ?>
	
<div class="pg pg-produtos" style="display:">
	<div class="container">
		<div class="bg-produtos" style="background: url(<?php echo $categoriaAtivaImg ?>);"></div>
		<!-- SIDEBAR -->
		<div class="sidebar-produtos">
		<?php

			// DEFINE A TAXONOMIA
			$taxonomia = 'categoriaProduto';

			// LISTA AS CATEGORIAS PAI DOS SABORES
			$categoriaProdutos = get_terms( $taxonomia, array(
				'orderby'    => 'count',
				'hide_empty' => 0,
				'parent'	 => 0
			));
			
			
			
			foreach ($categoriaProdutos as $categoriaProduto) {
				$nome = $categoriaProduto->name;				

						
				//RECUPERANDO CATEGORA ATUAL
				$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
				$term->name;
				//VERIFICANDO SE CATEGORIA NOME É IGUAL A CATEGIRA ATUAL
				if ($nome == $term->name) {
		 	
		 
		?>

			<a class="bordas" style="font-weight:bold" href="<?php echo get_category_link($categoriaProduto->term_id); ?>"><?php echo $nome ?></a>
		<?php }else{?>	

			<a  href="<?php echo get_category_link($categoriaProduto->term_id); ?>"><?php echo $nome ?></a>

		<?php }} ?>	

		</div>
		
		<!-- PRODUTOS -->
		<section class="produto">
			<ul>
			<?php 	
				if ( have_posts() ) :while ( have_posts() ) : the_post();	
					$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$foto = $foto[0];
					$tipo = rwmb_meta('TresB_produto_tipo');
					$nome =	explode(",", get_the_title());
			 ?>
				<li>
					<a href="<?php echo get_permalink(); ?>" class="foto" style="background: url(<?php echo $foto  ?>);"></a>
					<h2><?php 	echo $tipo  ?></h2>
					
					<p><?php 	echo $nome[0] ?></p>
					<p><?php 	echo $nome[1] ?></p>
					<a href="<?php echo get_permalink(); ?>" class="ver">Ver detalhes</a>
				</li>		
			<?php  endwhile;endif; wp_reset_query(); ?>					
			</ul>
			
		</section>

		<div class="paginador">
			<ul>	
			<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
			</ul>
		</div>
	</div>
</div>

<?php get_footer(); ?>
