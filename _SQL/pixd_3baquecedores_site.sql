-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 05/12/2017 às 11:03
-- Versão do servidor: 5.5.58-cll
-- Versão do PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `pixd_3baquecedores_site`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_aiowps_events`
--

CREATE TABLE `3b_aiowps_events` (
  `id` bigint(20) NOT NULL,
  `event_type` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `event_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_or_host` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer_info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_data` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_aiowps_failed_logins`
--

CREATE TABLE `3b_aiowps_failed_logins` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_attempt_ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_aiowps_global_meta`
--

CREATE TABLE `3b_aiowps_global_meta` (
  `meta_id` bigint(20) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta_key1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_key2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_key3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_key4` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_key5` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value4` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value5` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_aiowps_login_activity`
--

CREATE TABLE `3b_aiowps_login_activity` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logout_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `login_country` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `browser_type` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `3b_aiowps_login_activity`
--

INSERT INTO `3b_aiowps_login_activity` (`id`, `user_id`, `user_login`, `login_date`, `logout_date`, `login_ip`, `login_country`, `browser_type`) VALUES
(1, 1, '3baquecedores', '2016-05-12 11:04:09', '2016-05-13 15:33:37', '::1', '', ''),
(2, 1, '3baquecedores', '2016-05-13 09:42:01', '2016-05-13 15:33:37', '::1', '', ''),
(3, 1, '3baquecedores', '2016-05-13 17:54:18', '0000-00-00 00:00:00', '200.150.69.12', '', ''),
(4, 1, '3baquecedores', '2016-05-16 10:36:45', '0000-00-00 00:00:00', '200.150.69.12', '', ''),
(5, 1, '3baquecedores', '2016-05-16 12:11:11', '0000-00-00 00:00:00', '200.150.69.12', '', ''),
(6, 1, '3baquecedores', '2016-05-17 15:36:08', '0000-00-00 00:00:00', '200.150.69.12', '', ''),
(7, 1, '3baquecedores', '2016-05-18 18:15:51', '0000-00-00 00:00:00', '200.150.69.12', '', ''),
(8, 1, '3baquecedores', '2016-06-22 15:30:07', '2016-08-01 13:09:00', '187.95.126.204', '', ''),
(9, 1, '3baquecedores', '2016-06-28 11:55:47', '2016-08-01 13:09:00', '187.95.126.204', '', ''),
(10, 1, '3baquecedores', '2016-07-04 14:45:15', '2016-08-01 13:09:00', '187.95.126.204', '', ''),
(11, 1, '3baquecedores', '2016-07-29 10:26:51', '2016-08-01 13:09:00', '187.95.126.204', '', ''),
(12, 1, '3baquecedores', '2016-08-01 11:12:00', '2016-08-01 13:09:00', '187.95.126.204', '', ''),
(13, 1, '3baquecedores', '2016-08-01 13:10:18', '0000-00-00 00:00:00', '187.95.126.204', '', ''),
(14, 1, '3baquecedores', '2016-08-09 10:00:47', '0000-00-00 00:00:00', '187.95.126.204', '', ''),
(15, 1, '3baquecedores', '2016-08-16 18:09:53', '0000-00-00 00:00:00', '187.95.126.204', '', ''),
(16, 1, '3baquecedores', '2016-08-19 10:37:42', '0000-00-00 00:00:00', '187.95.126.204', '', ''),
(17, 1, '3baquecedores', '2016-08-19 14:36:20', '0000-00-00 00:00:00', '187.95.126.204', '', ''),
(18, 1, '3baquecedores', '2016-10-17 14:39:09', '0000-00-00 00:00:00', '187.95.127.204', '', ''),
(19, 1, '3baquecedores', '2016-10-18 12:25:27', '0000-00-00 00:00:00', '187.95.127.204', '', ''),
(20, 1, '3baquecedores', '2017-04-12 17:20:25', '0000-00-00 00:00:00', '187.95.127.204', '', ''),
(21, 1, '3baquecedores', '2017-05-18 10:01:47', '0000-00-00 00:00:00', '187.95.127.204', '', ''),
(22, 1, '3baquecedores', '2017-07-27 15:47:49', '0000-00-00 00:00:00', '187.95.127.236', '', ''),
(23, 1, '3baquecedores', '2017-07-31 11:19:23', '0000-00-00 00:00:00', '168.181.48.186', '', ''),
(24, 1, '3baquecedores', '2017-09-01 10:50:50', '2017-09-05 12:13:43', '170.82.201.156', '', ''),
(25, 1, '3baquecedores', '2017-09-04 10:19:21', '2017-09-05 12:13:43', '170.82.201.156', '', ''),
(26, 1, '3baquecedores', '2017-09-04 13:34:00', '2017-09-05 12:13:43', '170.82.201.156', '', ''),
(27, 1, '3baquecedores', '2017-09-05 12:08:00', '2017-09-05 12:13:43', '170.82.201.156', '', ''),
(28, 1, '3baquecedores', '2017-09-13 09:58:14', '0000-00-00 00:00:00', '170.82.201.156', '', ''),
(29, 1, '3baquecedores', '2017-09-20 17:13:34', '0000-00-00 00:00:00', '187.95.111.67', '', ''),
(30, 1, '3baquecedores', '2017-09-26 14:11:53', '0000-00-00 00:00:00', '187.95.127.39', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_aiowps_login_lockdown`
--

CREATE TABLE `3b_aiowps_login_lockdown` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lockdown_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `release_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `failed_login_ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `lock_reason` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `unlock_key` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_aiowps_permanent_block`
--

CREATE TABLE `3b_aiowps_permanent_block` (
  `id` bigint(20) NOT NULL,
  `blocked_ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `block_reason` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `country_origin` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `blocked_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unblock` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_commentmeta`
--

CREATE TABLE `3b_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_comments`
--

CREATE TABLE `3b_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_links`
--

CREATE TABLE `3b_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_options`
--

CREATE TABLE `3b_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `3b_options`
--

INSERT INTO `3b_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://3baquecedores.pixd.com.br/', 'yes'),
(2, 'home', 'http://3baquecedores.pixd.com.br/', 'yes'),
(3, 'blogname', '3B Aquecedores', 'yes'),
(4, 'blogdescription', 'Desde 1990 levando Conforto &amp; Segurança para você!', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', '3baquecedores@palupa.com.br', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '12', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '12', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:188:{s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:25:\"index.php?xsl=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"destaque/?$\";s:28:\"index.php?post_type=destaque\";s:41:\"destaque/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:36:\"destaque/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:28:\"destaque/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=destaque&paged=$matches[1]\";s:10:\"produto/?$\";s:27:\"index.php?post_type=produto\";s:40:\"produto/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=produto&feed=$matches[1]\";s:35:\"produto/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=produto&feed=$matches[1]\";s:27:\"produto/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=produto&paged=$matches[1]\";s:14:\"depoimentos/?$\";s:31:\"index.php?post_type=depoimentos\";s:44:\"depoimentos/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?post_type=depoimentos&feed=$matches[1]\";s:39:\"depoimentos/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?post_type=depoimentos&feed=$matches[1]\";s:31:\"depoimentos/page/([0-9]{1,})/?$\";s:49:\"index.php?post_type=depoimentos&paged=$matches[1]\";s:11:\"parceiro/?$\";s:28:\"index.php?post_type=parceiro\";s:41:\"parceiro/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=parceiro&feed=$matches[1]\";s:36:\"parceiro/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=parceiro&feed=$matches[1]\";s:28:\"parceiro/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=parceiro&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"destaque/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"destaque/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"destaque/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"destaque/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"destaque/([^/]+)/embed/?$\";s:41:\"index.php?destaque=$matches[1]&embed=true\";s:29:\"destaque/([^/]+)/trackback/?$\";s:35:\"index.php?destaque=$matches[1]&tb=1\";s:49:\"destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:44:\"destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:37:\"destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&paged=$matches[2]\";s:44:\"destaque/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&cpage=$matches[2]\";s:33:\"destaque/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?destaque=$matches[1]&page=$matches[2]\";s:25:\"destaque/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"destaque/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"destaque/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"destaque/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:35:\"produto/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"produto/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"produto/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"produto/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"produto/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"produto/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"produto/([^/]+)/embed/?$\";s:40:\"index.php?produto=$matches[1]&embed=true\";s:28:\"produto/([^/]+)/trackback/?$\";s:34:\"index.php?produto=$matches[1]&tb=1\";s:48:\"produto/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?produto=$matches[1]&feed=$matches[2]\";s:43:\"produto/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?produto=$matches[1]&feed=$matches[2]\";s:36:\"produto/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?produto=$matches[1]&paged=$matches[2]\";s:43:\"produto/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?produto=$matches[1]&cpage=$matches[2]\";s:32:\"produto/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?produto=$matches[1]&page=$matches[2]\";s:24:\"produto/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"produto/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"produto/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"produto/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"produto/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"produto/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:39:\"depoimentos/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:49:\"depoimentos/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:69:\"depoimentos/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"depoimentos/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"depoimentos/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:45:\"depoimentos/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:28:\"depoimentos/([^/]+)/embed/?$\";s:44:\"index.php?depoimentos=$matches[1]&embed=true\";s:32:\"depoimentos/([^/]+)/trackback/?$\";s:38:\"index.php?depoimentos=$matches[1]&tb=1\";s:52:\"depoimentos/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?depoimentos=$matches[1]&feed=$matches[2]\";s:47:\"depoimentos/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?depoimentos=$matches[1]&feed=$matches[2]\";s:40:\"depoimentos/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?depoimentos=$matches[1]&paged=$matches[2]\";s:47:\"depoimentos/([^/]+)/comment-page-([0-9]{1,})/?$\";s:51:\"index.php?depoimentos=$matches[1]&cpage=$matches[2]\";s:36:\"depoimentos/([^/]+)(?:/([0-9]+))?/?$\";s:50:\"index.php?depoimentos=$matches[1]&page=$matches[2]\";s:28:\"depoimentos/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:38:\"depoimentos/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:58:\"depoimentos/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"depoimentos/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"depoimentos/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:34:\"depoimentos/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:36:\"parceiro/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"parceiro/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"parceiro/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"parceiro/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"parceiro/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"parceiro/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"parceiro/([^/]+)/embed/?$\";s:41:\"index.php?parceiro=$matches[1]&embed=true\";s:29:\"parceiro/([^/]+)/trackback/?$\";s:35:\"index.php?parceiro=$matches[1]&tb=1\";s:49:\"parceiro/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?parceiro=$matches[1]&feed=$matches[2]\";s:44:\"parceiro/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?parceiro=$matches[1]&feed=$matches[2]\";s:37:\"parceiro/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?parceiro=$matches[1]&paged=$matches[2]\";s:44:\"parceiro/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?parceiro=$matches[1]&cpage=$matches[2]\";s:33:\"parceiro/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?parceiro=$matches[1]&page=$matches[2]\";s:25:\"parceiro/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"parceiro/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"parceiro/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"parceiro/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"parceiro/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"parceiro/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:58:\"categoria-produto/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?categoriaProduto=$matches[1]&feed=$matches[2]\";s:53:\"categoria-produto/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?categoriaProduto=$matches[1]&feed=$matches[2]\";s:34:\"categoria-produto/([^/]+)/embed/?$\";s:49:\"index.php?categoriaProduto=$matches[1]&embed=true\";s:46:\"categoria-produto/([^/]+)/page/?([0-9]{1,})/?$\";s:56:\"index.php?categoriaProduto=$matches[1]&paged=$matches[2]\";s:28:\"categoria-produto/([^/]+)/?$\";s:38:\"index.php?categoriaProduto=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=64&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:13:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";i:2;s:41:\"base-3baquecedores/base-3baquecedores.php\";i:3;s:39:\"categories-images/categories-images.php\";i:4;s:36:\"contact-form-7/wp-contact-form-7.php\";i:5;s:32:\"disqus-comment-system/disqus.php\";i:6;s:33:\"duplicate-post/duplicate-post.php\";i:7;s:9:\"hello.php\";i:8;s:21:\"meta-box/meta-box.php\";i:9;s:37:\"post-types-order/post-types-order.php\";i:10;s:24:\"wordpress-seo/wp-seo.php\";i:11;s:29:\"wp-mail-smtp/wp_mail_smtp.php\";i:12;s:28:\"wysija-newsletters/index.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:2:{i:0;s:110:\"C:\\wamp\\www\\projetos\\3baquecedores_site/wp-content/plugins/all-in-one-wp-security-and-firewall/wp-security.php\";i:1;s:0:\"\";}', 'no'),
(40, 'template', 'tresbaquecedores', 'yes'),
(41, 'stylesheet', 'tresbaquecedores', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '106', 'yes'),
(84, 'page_on_front', '64', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '36686', 'yes'),
(92, '3b_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:69:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:10:\"copy_posts\";b:1;s:18:\"wysija_newsletters\";b:1;s:18:\"wysija_subscribers\";b:1;s:13:\"wysija_config\";b:1;s:16:\"wysija_theme_tab\";b:1;s:16:\"wysija_style_tab\";b:1;s:22:\"wysija_stats_dashboard\";b:1;s:15:\"wpseo_bulk_edit\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:10:\"copy_posts\";b:1;s:15:\"wpseo_bulk_edit\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(93, 'WPLANG', 'pt_BR', 'yes'),
(94, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;s:9:\"show_date\";b:1;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:1:{i:0;s:14:\"recent-posts-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'cron', 'a:9:{i:1492029096;a:1:{s:24:\"aiowps_hourly_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1492060131;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1492090106;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1492103351;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1492104696;a:1:{s:23:\"aiowps_daily_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1501181677;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1501181911;a:1:{s:14:\"dsq_sync_forum\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1501182086;a:1:{s:25:\"wpseo_ping_search_engines\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}s:7:\"version\";i:2;}', 'yes'),
(134, 'theme_mods_twentysixteen', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1462813950;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(135, 'current_theme', 'tresbaquecedores', 'yes'),
(136, 'theme_mods_twentyfifteen', 'a:2:{i:0;b:0;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1462814130;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(137, 'theme_switched', '', 'yes'),
(138, 'theme_mods_tresbaquecedores', 'a:2:{i:0;b:0;s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(139, 'recently_activated', 'a:0:{}', 'yes'),
(162, 'aiowpsec_db_version', '1.9', 'yes'),
(163, 'aio_wp_security_configs', 'a:80:{s:19:\"aiowps_enable_debug\";s:0:\"\";s:36:\"aiowps_remove_wp_generator_meta_info\";s:0:\"\";s:25:\"aiowps_prevent_hotlinking\";s:0:\"\";s:28:\"aiowps_enable_login_lockdown\";s:0:\"\";s:28:\"aiowps_allow_unlock_requests\";s:0:\"\";s:25:\"aiowps_max_login_attempts\";s:1:\"3\";s:24:\"aiowps_retry_time_period\";s:1:\"5\";s:26:\"aiowps_lockout_time_length\";s:2:\"60\";s:28:\"aiowps_set_generic_login_msg\";s:0:\"\";s:26:\"aiowps_enable_email_notify\";s:0:\"\";s:20:\"aiowps_email_address\";s:27:\"3baquecedores@palupa.com.br\";s:27:\"aiowps_enable_forced_logout\";s:0:\"\";s:25:\"aiowps_logout_time_period\";s:2:\"60\";s:39:\"aiowps_enable_invalid_username_lockdown\";s:0:\"\";s:32:\"aiowps_unlock_request_secret_key\";s:20:\"c6oxfoyt9kmjgkzi3wwq\";s:26:\"aiowps_enable_whitelisting\";s:0:\"\";s:27:\"aiowps_allowed_ip_addresses\";s:0:\"\";s:27:\"aiowps_enable_login_captcha\";s:0:\"\";s:34:\"aiowps_enable_custom_login_captcha\";s:0:\"\";s:25:\"aiowps_captcha_secret_key\";s:20:\"r9qokwab2jx4fci063ci\";s:42:\"aiowps_enable_manual_registration_approval\";s:0:\"\";s:39:\"aiowps_enable_registration_page_captcha\";s:0:\"\";s:27:\"aiowps_enable_random_prefix\";s:0:\"\";s:31:\"aiowps_enable_automated_backups\";s:0:\"\";s:26:\"aiowps_db_backup_frequency\";s:1:\"4\";s:25:\"aiowps_db_backup_interval\";s:1:\"2\";s:26:\"aiowps_backup_files_stored\";s:1:\"2\";s:32:\"aiowps_send_backup_email_address\";s:0:\"\";s:27:\"aiowps_backup_email_address\";s:27:\"3baquecedores@palupa.com.br\";s:27:\"aiowps_disable_file_editing\";s:0:\"\";s:37:\"aiowps_prevent_default_wp_file_access\";s:0:\"\";s:22:\"aiowps_system_log_file\";s:9:\"error_log\";s:26:\"aiowps_enable_blacklisting\";s:0:\"\";s:26:\"aiowps_banned_ip_addresses\";s:0:\"\";s:28:\"aiowps_enable_basic_firewall\";s:0:\"\";s:31:\"aiowps_enable_pingback_firewall\";s:0:\"\";s:34:\"aiowps_block_debug_log_file_access\";s:0:\"\";s:26:\"aiowps_disable_index_views\";s:0:\"\";s:30:\"aiowps_disable_trace_and_track\";s:0:\"\";s:28:\"aiowps_forbid_proxy_comments\";s:0:\"\";s:29:\"aiowps_deny_bad_query_strings\";s:0:\"\";s:34:\"aiowps_advanced_char_string_filter\";s:0:\"\";s:25:\"aiowps_enable_5g_firewall\";s:0:\"\";s:25:\"aiowps_enable_6g_firewall\";s:0:\"\";s:26:\"aiowps_enable_custom_rules\";s:0:\"\";s:19:\"aiowps_custom_rules\";s:0:\"\";s:25:\"aiowps_enable_404_logging\";s:0:\"\";s:28:\"aiowps_enable_404_IP_lockout\";s:0:\"\";s:30:\"aiowps_404_lockout_time_length\";s:2:\"60\";s:28:\"aiowps_404_lock_redirect_url\";s:16:\"http://127.0.0.1\";s:31:\"aiowps_enable_rename_login_page\";s:0:\"\";s:28:\"aiowps_enable_login_honeypot\";s:0:\"\";s:43:\"aiowps_enable_brute_force_attack_prevention\";s:0:\"\";s:30:\"aiowps_brute_force_secret_word\";s:0:\"\";s:24:\"aiowps_cookie_brute_test\";s:0:\"\";s:44:\"aiowps_cookie_based_brute_force_redirect_url\";s:16:\"http://127.0.0.1\";s:59:\"aiowps_brute_force_attack_prevention_pw_protected_exception\";s:0:\"\";s:51:\"aiowps_brute_force_attack_prevention_ajax_exception\";s:0:\"\";s:19:\"aiowps_site_lockout\";s:0:\"\";s:23:\"aiowps_site_lockout_msg\";s:0:\"\";s:30:\"aiowps_enable_spambot_blocking\";s:0:\"\";s:29:\"aiowps_enable_comment_captcha\";s:0:\"\";s:31:\"aiowps_enable_autoblock_spam_ip\";s:0:\"\";s:33:\"aiowps_spam_ip_min_comments_block\";s:0:\"\";s:32:\"aiowps_enable_automated_fcd_scan\";s:0:\"\";s:25:\"aiowps_fcd_scan_frequency\";s:1:\"4\";s:24:\"aiowps_fcd_scan_interval\";s:1:\"2\";s:28:\"aiowps_fcd_exclude_filetypes\";s:0:\"\";s:24:\"aiowps_fcd_exclude_files\";s:0:\"\";s:26:\"aiowps_send_fcd_scan_email\";s:0:\"\";s:29:\"aiowps_fcd_scan_email_address\";s:27:\"3baquecedores@palupa.com.br\";s:27:\"aiowps_fcds_change_detected\";b:0;s:22:\"aiowps_copy_protection\";s:0:\"\";s:40:\"aiowps_prevent_site_display_inside_frame\";s:0:\"\";s:32:\"aiowps_prevent_users_enumeration\";s:0:\"\";s:43:\"aiowps_instantly_lockout_specific_usernames\";a:0:{}s:35:\"aiowps_lockdown_enable_whitelisting\";s:0:\"\";s:36:\"aiowps_lockdown_allowed_ip_addresses\";s:0:\"\";s:35:\"aiowps_enable_registration_honeypot\";s:0:\"\";s:38:\"aiowps_disable_xmlrpc_pingback_methods\";s:0:\"\";}', 'yes'),
(167, 'duplicate_post_copyexcerpt', '1', 'yes'),
(168, 'duplicate_post_copyattachments', '1', 'yes'),
(169, 'duplicate_post_copychildren', '', 'yes'),
(170, 'duplicate_post_copystatus', '1', 'yes'),
(171, 'duplicate_post_taxonomies_blacklist', '', 'yes'),
(172, 'duplicate_post_show_row', '1', 'yes'),
(173, 'duplicate_post_show_adminbar', '1', 'yes'),
(174, 'duplicate_post_show_submitbox', '1', 'yes'),
(176, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"4.8.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1462804306;s:7:\"version\";s:5:\"4.4.2\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(177, 'widget_wysija', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(178, 'wysija_post_type_updated', '1462815115', 'yes'),
(179, 'wysija_post_type_created', '1462815115', 'yes'),
(180, 'installation_step', '16', 'yes'),
(181, 'wysija', 'YToxODp7czo5OiJmcm9tX25hbWUiO3M6MTM6IjNiYXF1ZWNlZG9yZXMiO3M6MTI6InJlcGx5dG9fbmFtZSI7czoxMzoiM2JhcXVlY2Vkb3JlcyI7czoxNToiZW1haWxzX25vdGlmaWVkIjtzOjI3OiIzYmFxdWVjZWRvcmVzQHBhbHVwYS5jb20uYnIiO3M6MTA6ImZyb21fZW1haWwiO3M6MTQ6ImluZm9AbG9jYWxob3N0IjtzOjEzOiJyZXBseXRvX2VtYWlsIjtzOjE0OiJpbmZvQGxvY2FsaG9zdCI7czoxNToiZGVmYXVsdF9saXN0X2lkIjtpOjE7czoxNzoidG90YWxfc3Vic2NyaWJlcnMiO3M6MToiMiI7czoxNjoiaW1wb3J0d3BfbGlzdF9pZCI7aToyO3M6MTg6ImNvbmZpcm1fZW1haWxfbGluayI7aTo1O3M6MTI6InVwbG9hZGZvbGRlciI7czo2NjoiQzpcd2FtcFx3d3dccHJvamV0b3NcM2JhcXVlY2Vkb3Jlc19zaXRlL3dwLWNvbnRlbnQvdXBsb2Fkc1x3eXNpamFcIjtzOjk6InVwbG9hZHVybCI7czo3MToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy8zYmFxdWVjZWRvcmVzX3NpdGUvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS8iO3M6MTY6ImNvbmZpcm1fZW1haWxfaWQiO2k6MjtzOjk6Imluc3RhbGxlZCI7YjoxO3M6MjA6Im1hbmFnZV9zdWJzY3JpcHRpb25zIjtiOjE7czoxNDoiaW5zdGFsbGVkX3RpbWUiO2k6MTQ2MjgxNTExOTtzOjE3OiJ3eXNpamFfZGJfdmVyc2lvbiI7czo1OiIyLjcuMSI7czoxMToiZGtpbV9kb21haW4iO3M6OToibG9jYWxob3N0IjtzOjE2OiJ3eXNpamFfd2hhdHNfbmV3IjtzOjg6IjIuNy4xMS4zIjt9', 'yes'),
(182, 'wysija_reinstall', '0', 'no'),
(183, 'wysija_schedules', 'a:5:{s:5:\"queue\";a:3:{s:13:\"next_schedule\";i:1492032000;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:6:\"bounce\";a:3:{s:13:\"next_schedule\";i:1462901522;s:13:\"prev_schedule\";i:0;s:7:\"running\";b:0;}s:5:\"daily\";a:3:{s:13:\"next_schedule\";i:1492103490;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:6:\"weekly\";a:3:{s:13:\"next_schedule\";i:1492433128;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:7:\"monthly\";a:3:{s:13:\"next_schedule\";i:1493079524;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}}', 'yes'),
(184, 'cpto_options', 'a:5:{s:23:\"show_reorder_interfaces\";a:6:{s:4:\"post\";s:4:\"show\";s:10:\"attachment\";s:4:\"show\";s:8:\"destaque\";s:4:\"show\";s:7:\"produto\";s:4:\"show\";s:10:\"depoimento\";s:4:\"show\";s:8:\"parceiro\";s:4:\"show\";}s:8:\"autosort\";i:1;s:9:\"adminsort\";i:1;s:10:\"capability\";s:13:\"switch_themes\";s:21:\"navigation_sort_apply\";i:1;}', 'yes'),
(187, 'mail_from', '', 'yes'),
(188, 'mail_from_name', '', 'yes'),
(189, 'mailer', 'smtp', 'yes'),
(190, 'mail_set_return_path', 'false', 'yes'),
(191, 'smtp_host', 'localhost', 'yes'),
(192, 'smtp_port', '25', 'yes'),
(193, 'smtp_ssl', 'none', 'yes'),
(194, 'smtp_auth', '', 'yes'),
(195, 'smtp_user', '', 'yes'),
(196, 'smtp_pass', '', 'yes'),
(197, 'wpseo', 'a:25:{s:14:\"blocking_files\";a:0:{}s:15:\"ms_defaults_set\";b:0;s:7:\"version\";s:3:\"5.1\";s:12:\"company_logo\";s:0:\"\";s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:0:\"\";s:20:\"disableadvanced_meta\";b:1;s:19:\"onpage_indexability\";b:1;s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:12:\"website_name\";s:0:\"\";s:22:\"alternate_website_name\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";b:0;s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:20:\"enable_setting_pages\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:0;s:18:\"first_activated_on\";i:1499972072;}', 'yes'),
(198, 'wpseo_permalinks', 'a:9:{s:15:\"cleanpermalinks\";b:0;s:24:\"cleanpermalink-extravars\";s:0:\"\";s:29:\"cleanpermalink-googlecampaign\";b:0;s:31:\"cleanpermalink-googlesitesearch\";b:0;s:15:\"cleanreplytocom\";b:0;s:10:\"cleanslugs\";b:1;s:18:\"redirectattachment\";b:0;s:17:\"stripcategorybase\";b:0;s:13:\"trailingslash\";b:0;}', 'yes'),
(199, 'wpseo_titles', 'a:109:{s:10:\"title_test\";i:0;s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:5:\"noodp\";b:0;s:15:\"usemetakeywords\";b:0;s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:40:\"%%name%%, Autor em %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:66:\"Você pesquisou por %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:44:\"Página não encontrada %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:18:\"metakey-home-wpseo\";s:0:\"\";s:20:\"metakey-author-wpseo\";s:0:\"\";s:22:\"noindex-subpages-wpseo\";b:0;s:20:\"noindex-author-wpseo\";b:0;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"metakey-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:13:\"showdate-post\";b:0;s:16:\"hideeditbox-post\";b:0;s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"metakey-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:13:\"showdate-page\";b:0;s:16:\"hideeditbox-page\";b:0;s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"metakey-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:19:\"showdate-attachment\";b:0;s:22:\"hideeditbox-attachment\";b:0;s:18:\"title-tax-category\";s:44:\"%%term_title%% %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:20:\"metakey-tax-category\";s:0:\"\";s:24:\"hideeditbox-tax-category\";b:0;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:44:\"%%term_title%% %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:20:\"metakey-tax-post_tag\";s:0:\"\";s:24:\"hideeditbox-tax-post_tag\";b:0;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"Arquivos %%term_title%% %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:23:\"metakey-tax-post_format\";s:0:\"\";s:27:\"hideeditbox-tax-post_format\";b:0;s:23:\"noindex-tax-post_format\";b:0;s:14:\"title-destaque\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:17:\"metadesc-destaque\";s:0:\"\";s:16:\"metakey-destaque\";s:0:\"\";s:16:\"noindex-destaque\";b:0;s:17:\"showdate-destaque\";b:0;s:20:\"hideeditbox-destaque\";b:0;s:13:\"title-produto\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:16:\"metadesc-produto\";s:0:\"\";s:15:\"metakey-produto\";s:0:\"\";s:15:\"noindex-produto\";b:0;s:16:\"showdate-produto\";b:0;s:19:\"hideeditbox-produto\";b:0;s:17:\"title-depoimentos\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:20:\"metadesc-depoimentos\";s:0:\"\";s:19:\"metakey-depoimentos\";s:0:\"\";s:19:\"noindex-depoimentos\";b:0;s:20:\"showdate-depoimentos\";b:0;s:23:\"hideeditbox-depoimentos\";b:0;s:14:\"title-parceiro\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:17:\"metadesc-parceiro\";s:0:\"\";s:16:\"metakey-parceiro\";s:0:\"\";s:16:\"noindex-parceiro\";b:0;s:17:\"showdate-parceiro\";b:0;s:20:\"hideeditbox-parceiro\";b:0;s:13:\"title-wysijap\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:16:\"metadesc-wysijap\";s:0:\"\";s:15:\"metakey-wysijap\";s:0:\"\";s:15:\"noindex-wysijap\";b:0;s:16:\"showdate-wysijap\";b:0;s:19:\"hideeditbox-wysijap\";b:0;s:24:\"title-ptarchive-destaque\";s:25:\"Destaques- 3B Aquecedores\";s:27:\"metadesc-ptarchive-destaque\";s:0:\"\";s:26:\"metakey-ptarchive-destaque\";s:0:\"\";s:26:\"bctitle-ptarchive-destaque\";s:0:\"\";s:26:\"noindex-ptarchive-destaque\";b:0;s:23:\"title-ptarchive-produto\";s:24:\"Produtos- 3B Aquecedores\";s:26:\"metadesc-ptarchive-produto\";s:0:\"\";s:25:\"metakey-ptarchive-produto\";s:0:\"\";s:25:\"bctitle-ptarchive-produto\";s:0:\"\";s:25:\"noindex-ptarchive-produto\";b:0;s:27:\"title-ptarchive-depoimentos\";s:28:\"Depoimentos - 3B Aquecedores\";s:30:\"metadesc-ptarchive-depoimentos\";s:0:\"\";s:29:\"metakey-ptarchive-depoimentos\";s:0:\"\";s:29:\"bctitle-ptarchive-depoimentos\";s:0:\"\";s:29:\"noindex-ptarchive-depoimentos\";b:0;s:24:\"title-ptarchive-parceiro\";s:25:\"Parceiros- 3B Aquecedores\";s:27:\"metadesc-ptarchive-parceiro\";s:0:\"\";s:26:\"metakey-ptarchive-parceiro\";s:0:\"\";s:26:\"bctitle-ptarchive-parceiro\";s:0:\"\";s:26:\"noindex-ptarchive-parceiro\";b:0;s:26:\"title-tax-categoriaProduto\";s:44:\"%%term_title%% %%page%% %%sep%% %%sitename%%\";s:29:\"metadesc-tax-categoriaProduto\";s:0:\"\";s:28:\"metakey-tax-categoriaProduto\";s:0:\"\";s:32:\"hideeditbox-tax-categoriaProduto\";b:0;s:28:\"noindex-tax-categoriaProduto\";b:0;}', 'yes'),
(200, 'wpseo_social', 'a:20:{s:9:\"fb_admins\";a:0:{}s:12:\"fbconnectkey\";s:32:\"2cb524b1e14fc2e24332ac9d8e0f475b\";s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:14:\"plus-publisher\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:7:\"summary\";s:11:\"youtube_url\";s:0:\"\";s:15:\"google_plus_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(201, 'wpseo_rss', 'a:2:{s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:54:\"O post %%POSTLINK%% apareceu primeiro em %%BLOGLINK%%.\";}', 'yes'),
(202, 'wpseo_internallinks', 'a:10:{s:20:\"breadcrumbs-404crumb\";s:33:\"Erro 404: Página não encontrada\";s:23:\"breadcrumbs-blog-remove\";b:0;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:13:\"Arquivos para\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:7:\"Início\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:19:\"Você pesquisou por\";s:15:\"breadcrumbs-sep\";s:7:\"&raquo;\";s:23:\"post_types-post-maintax\";i:0;}', 'yes'),
(203, 'wpseo_xml', 'a:17:{s:22:\"disable_author_sitemap\";b:1;s:22:\"disable_author_noposts\";b:1;s:16:\"enablexmlsitemap\";b:1;s:16:\"entries-per-page\";i:1000;s:14:\"excluded-posts\";s:0:\"\";s:38:\"user_role-administrator-not_in_sitemap\";b:0;s:31:\"user_role-editor-not_in_sitemap\";b:0;s:31:\"user_role-author-not_in_sitemap\";b:0;s:36:\"user_role-contributor-not_in_sitemap\";b:0;s:35:\"user_role-subscriber-not_in_sitemap\";b:0;s:30:\"post_types-post-not_in_sitemap\";b:0;s:30:\"post_types-page-not_in_sitemap\";b:0;s:36:\"post_types-attachment-not_in_sitemap\";b:1;s:33:\"post_types-wysijap-not_in_sitemap\";b:0;s:34:\"taxonomies-category-not_in_sitemap\";b:0;s:34:\"taxonomies-post_tag-not_in_sitemap\";b:0;s:37:\"taxonomies-post_format-not_in_sitemap\";b:0;}', 'yes'),
(204, 'wpseo_flush_rewrite', '1', 'yes'),
(211, 'wysija_last_php_cron_call', '1512458369', 'yes'),
(215, 'wysija_check_pn', '1492028399.99', 'yes'),
(216, 'wysija_last_scheduled_check', '1492028399', 'yes'),
(252, 'r_notice_data', '{\"type\":\"redux-message\",\"title\":\"<strong>Redux Framework: New Documentation coming!<\\/strong><br\\/>\",\"message\":\"We\'ve heard your screams and we\'re completely redoing our documentation. For a preview of things to come, please <a href=\\\"http:\\/\\/reduxframework.github.io\\/docs.redux.io\\/getting-started\\/\\\">visit our new docs repo<\\/a>. P.S. You can contribute! If you have an idea for a doc, <a href=\\\"https:\\/\\/github.com\\/reduxframework\\/docs.redux.io\\/issues\\\">post your ideas here<\\/a>. \",\"color\":\"rgba(0,162,227,1)\"}', 'yes'),
(253, 'redux_blast', '1462889991', 'yes'),
(254, 'redux_version_upgraded_from', '3.6.5', 'yes'),
(255, '_transient_timeout__redux_activation_redirect', '1512458405', 'no'),
(256, '_transient__redux_activation_redirect', '1', 'no');
INSERT INTO `3b_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(257, 'redux_demo', 'a:103:{s:8:\"last_tab\";s:1:\"1\";s:12:\"opt-endereco\";s:0:\"\";s:12:\"opt-telefone\";N;s:9:\"opt-email\";s:0:\"\";s:12:\"opt-checkbox\";s:1:\"1\";s:15:\"opt-multi-check\";a:3:{i:1;s:1:\"1\";i:2;s:0:\"\";i:3;s:0:\"\";}s:20:\"opt-checkbox-sidebar\";a:1:{s:9:\"sidebar-1\";s:0:\"\";}s:9:\"opt-radio\";s:1:\"2\";s:12:\"opt-sortable\";a:3:{s:8:\"Text One\";s:6:\"Item 1\";s:8:\"Text Two\";s:6:\"Item 2\";s:10:\"Text Three\";s:6:\"Item 3\";}s:18:\"opt-check-sortable\";a:3:{s:3:\"cb1\";s:0:\"\";s:3:\"cb2\";s:1:\"1\";s:3:\"cb3\";s:0:\"\";}s:12:\"text-example\";s:12:\"Default Text\";s:17:\"text-example-hint\";s:12:\"Default Text\";s:16:\"text-placeholder\";s:0:\"\";s:13:\"opt-multitext\";a:1:{i:0;s:0:\"\";}s:8:\"password\";a:2:{s:8:\"username\";s:13:\"3baquecedores\";s:8:\"password\";s:8:\"3@pp3965\";}s:12:\"opt-textarea\";s:12:\"Default Text\";s:10:\"opt-editor\";s:27:\"Powered by Redux Framework.\";s:15:\"opt-editor-tiny\";s:34:\"<p>Powered by Redux Framework.</p>\";s:15:\"opt-editor-full\";s:0:\"\";s:18:\"opt-ace-editor-css\";s:31:\"#header{\r\n   margin: 0 auto;\r\n}\";s:17:\"opt-ace-editor-js\";s:41:\"jQuery(document).ready(function(){\r\n\r\n});\";s:18:\"opt-ace-editor-php\";s:29:\"<?php\r\n    echo \"PHP String\";\";s:15:\"opt-color-title\";s:7:\"#000000\";s:16:\"opt-color-footer\";s:7:\"#dd9933\";s:16:\"opt-color-header\";a:2:{s:4:\"from\";s:7:\"#1e73be\";s:2:\"to\";s:7:\"#00897e\";}s:14:\"opt-color-rgba\";a:3:{s:5:\"color\";s:7:\"#7e33dd\";s:5:\"alpha\";s:2:\".8\";s:4:\"rgba\";s:20:\"rgba(126,51,221,0.8)\";}s:14:\"opt-link-color\";a:3:{s:7:\"regular\";s:4:\"#aaa\";s:5:\"hover\";s:4:\"#bbb\";s:6:\"active\";s:4:\"#ccc\";}s:17:\"opt-palette-color\";s:3:\"red\";s:14:\"opt-background\";a:7:{s:16:\"background-color\";s:0:\"\";s:17:\"background-repeat\";s:0:\"\";s:15:\"background-size\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:19:\"background-position\";s:0:\"\";s:16:\"background-image\";s:0:\"\";s:5:\"media\";a:4:{s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}}s:17:\"opt-header-border\";a:6:{s:10:\"border-top\";s:3:\"3px\";s:12:\"border-right\";s:3:\"3px\";s:13:\"border-bottom\";s:3:\"3px\";s:11:\"border-left\";s:3:\"3px\";s:12:\"border-style\";s:5:\"solid\";s:12:\"border-color\";s:7:\"#1e73be\";}s:26:\"opt-header-border-expanded\";a:6:{s:10:\"border-top\";s:3:\"3px\";s:12:\"border-right\";s:3:\"3px\";s:13:\"border-bottom\";s:3:\"3px\";s:11:\"border-left\";s:3:\"3px\";s:12:\"border-style\";s:5:\"solid\";s:12:\"border-color\";s:7:\"#1e73be\";}s:14:\"opt-dimensions\";a:3:{s:5:\"width\";s:5:\"200px\";s:6:\"height\";s:5:\"100px\";s:5:\"units\";s:2:\"px\";}s:20:\"opt-dimensions-width\";a:2:{s:5:\"width\";s:5:\"200px\";s:5:\"units\";s:2:\"px\";}s:11:\"opt-spacing\";a:4:{s:10:\"margin-top\";s:1:\"1\";s:12:\"margin-right\";s:1:\"2\";s:13:\"margin-bottom\";s:1:\"3\";s:11:\"margin-left\";s:1:\"4\";}s:20:\"opt-spacing-expanded\";a:5:{s:10:\"margin-top\";s:3:\"1px\";s:12:\"margin-right\";s:3:\"2px\";s:13:\"margin-bottom\";s:3:\"3px\";s:11:\"margin-left\";s:3:\"4px\";s:5:\"units\";s:2:\"px\";}s:11:\"opt-gallery\";s:0:\"\";s:9:\"opt-media\";a:5:{s:3:\"url\";s:52:\"http://s.wordpress.org/style/images/codeispoetry.png\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:12:\"media-no-url\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:16:\"media-no-preview\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:17:\"opt-random-upload\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:10:\"opt-slides\";a:1:{i:0;a:9:{s:5:\"title\";s:0:\"\";s:11:\"description\";s:0:\"\";s:3:\"url\";s:0:\"\";s:4:\"sort\";s:1:\"0\";s:13:\"attachment_id\";s:0:\"\";s:5:\"thumb\";s:0:\"\";s:5:\"image\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";}}s:12:\"section-test\";s:0:\"\";s:18:\"section-test-media\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:14:\"opt-button-set\";s:1:\"2\";s:20:\"opt-button-set-multi\";a:5:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"0\";i:3;s:1:\"2\";i:4;s:1:\"3\";}s:9:\"switch-on\";s:1:\"1\";s:10:\"switch-off\";s:0:\"\";s:13:\"switch-parent\";s:1:\"0\";s:13:\"switch-child1\";s:0:\"\";s:13:\"switch-child2\";s:0:\"\";s:10:\"opt-select\";s:1:\"2\";s:21:\"opt-select-stylesheet\";s:11:\"default.css\";s:19:\"opt-select-optgroup\";s:1:\"2\";s:16:\"opt-multi-select\";a:2:{i:0;s:1:\"2\";i:1;s:1:\"3\";}s:16:\"opt-select-image\";s:0:\"\";s:21:\"opt-select-categories\";s:0:\"\";s:16:\"opt-select-pages\";s:0:\"\";s:20:\"opt-select-post-type\";s:0:\"\";s:16:\"opt-select-posts\";s:0:\"\";s:16:\"opt-select-roles\";s:0:\"\";s:18:\"opt-select-elusive\";s:0:\"\";s:16:\"opt-select-users\";s:0:\"\";s:23:\"opt-image-select-layout\";s:1:\"2\";s:16:\"opt-image-select\";s:1:\"2\";s:16:\"opt-select_image\";s:0:\"\";s:16:\"opt-slider-label\";s:3:\"250\";s:15:\"opt-slider-text\";s:2:\"75\";s:16:\"opt-slider-float\";s:3:\"0.5\";s:11:\"opt-spinner\";s:2:\"40\";s:19:\"opt-typography-body\";a:10:{s:11:\"font-family\";s:26:\"Arial,Helvetica,sans-serif\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:6:\"Normal\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:10:\"text-align\";s:0:\"\";s:9:\"font-size\";s:4:\"30px\";s:11:\"line-height\";s:0:\"\";s:5:\"color\";s:7:\"#dd9933\";}s:14:\"opt-typography\";a:11:{s:11:\"font-family\";s:4:\"Abel\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-backup\";s:0:\"\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:3:\"700\";s:7:\"subsets\";s:0:\"\";s:10:\"text-align\";s:0:\"\";s:9:\"font-size\";s:4:\"33px\";s:11:\"line-height\";s:4:\"40px\";s:5:\"color\";s:4:\"#333\";}s:14:\"opt-datepicker\";s:0:\"\";s:19:\"opt-homepage-layout\";a:3:{s:7:\"enabled\";a:5:{s:7:\"placebo\";s:7:\"placebo\";s:10:\"highlights\";s:10:\"Highlights\";s:6:\"slider\";s:6:\"Slider\";s:10:\"staticpage\";s:11:\"Static Page\";s:8:\"services\";s:8:\"Services\";}s:8:\"disabled\";a:1:{s:7:\"placebo\";s:7:\"placebo\";}s:6:\"backup\";a:1:{s:7:\"placebo\";s:7:\"placebo\";}}s:21:\"opt-homepage-layout-2\";a:2:{s:8:\"disabled\";a:3:{s:7:\"placebo\";s:7:\"placebo\";s:10:\"highlights\";s:10:\"Highlights\";s:6:\"slider\";s:6:\"Slider\";}s:7:\"enabled\";a:3:{s:7:\"placebo\";s:7:\"placebo\";s:10:\"staticpage\";s:11:\"Static Page\";s:8:\"services\";s:8:\"Services\";}}s:14:\"opt-text-email\";s:13:\"test@test.com\";s:18:\"opt-text-post-type\";a:7:{s:10:\"attachment\";s:6:\"Mídia\";s:10:\"depoimento\";s:11:\"Depoimentos\";s:8:\"destaque\";s:9:\"Destaques\";s:4:\"page\";s:8:\"Páginas\";s:8:\"parceiro\";s:9:\"Parceiros\";s:4:\"post\";s:5:\"Posts\";s:7:\"produto\";s:8:\"Produtos\";}s:14:\"opt-multi-text\";a:1:{i:0;b:0;}s:12:\"opt-text-url\";s:25:\"http://reduxframework.com\";s:16:\"opt-text-numeric\";s:1:\"0\";s:22:\"opt-text-comma-numeric\";s:1:\"0\";s:25:\"opt-text-no-special-chars\";s:1:\"0\";s:20:\"opt-text-str_replace\";s:53:\"Thisthisisaspaceisthisisaspacethethisisaspacedefault.\";s:21:\"opt-text-preg_replace\";s:10:\"no numbers\";s:24:\"opt-text-custom_validate\";s:1:\"0\";s:20:\"opt-textarea-no-html\";s:27:\"No HTML is allowed in here.\";s:17:\"opt-textarea-html\";s:24:\"HTML is allowed in here.\";s:22:\"opt-textarea-some-html\";s:29:\"Some HTML is allowed in here.\";s:15:\"opt-textarea-js\";s:0:\"\";s:18:\"opt-required-basic\";s:0:\"\";s:23:\"opt-required-basic-text\";s:0:\"\";s:19:\"opt-required-nested\";s:0:\"\";s:29:\"opt-required-nested-buttonset\";s:11:\"button-text\";s:24:\"opt-required-nested-text\";s:0:\"\";s:28:\"opt-required-nested-textarea\";s:0:\"\";s:26:\"opt-required-nested-editor\";s:0:\"\";s:23:\"opt-required-nested-ace\";s:0:\"\";s:19:\"opt-required-select\";s:10:\"no-sidebar\";s:32:\"opt-required-select-left-sidebar\";s:0:\"\";s:33:\"opt-required-select-right-sidebar\";s:0:\"\";s:9:\"wpml-text\";s:0:\"\";s:15:\"wpml-multicheck\";a:3:{i:1;s:0:\"\";i:2;s:0:\"\";i:3;s:0:\"\";}s:19:\"opt-customizer-only\";s:1:\"2\";s:17:\"opt-checkbox-data\";i:0;}', 'yes'),
(258, 'redux_demo-transients', 'a:3:{s:14:\"changed_values\";a:0:{}s:9:\"last_save\";i:1462893023;s:7:\"notices\";a:1:{s:8:\"warnings\";a:0:{}}}', 'yes'),
(282, 'configuracao', 'a:37:{s:8:\"last_tab\";s:1:\"1\";s:17:\"opt-inicial-sobre\";s:196:\"<b>A 3B Aquecedores</b> se apoia na experiência de anos e na credibilidade de seus clientes para oferecer, a Curitiba e Região Metropolitana, o melhor atendimento e produtos das melhores marcas.\";s:22:\"opt-inicial-sobre-foto\";a:5:{s:3:\"url\";s:74:\"http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/manutencao.jpg\";s:2:\"id\";s:3:\"354\";s:6:\"height\";s:3:\"374\";s:5:\"width\";s:3:\"390\";s:9:\"thumbnail\";s:82:\"http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/manutencao-150x150.jpg\";}s:33:\"opt-inicial-tituloServicoEsquerda\";s:11:\"Atendimento\";s:22:\"opt-inicial-instalacao\";a:4:{i:0;s:28:\"Venda de produto sob demanda\";i:1;s:34:\"Instalação de aquecedores a gás\";i:2;s:38:\"Instalação de bombas pressurizadoras\";i:3;s:34:\"Instalação de medidores de gás.\";}s:32:\"opt-inicial-tituloServicoDireita\";s:21:\"Assistência Técnica\";s:23:\"opt-inicial-assistencia\";a:4:{i:0;s:34:\"Manutenção de aquecedores a gás\";i:1;s:33:\"Manutenção de aquecedor central\";i:2;s:35:\"Manutenção de aquecedor elétrico\";i:3;s:37:\"Troca de resistências e termostatos.\";}s:21:\"opt-inicial-parceiros\";s:224:\"O clima perfeito para o seu banho necessita de uma estrutura competente, que oferece o suporte ideal para arrancar um sorriso de felicidade nos bons momentos do seu dia. Confira as marcas em que a 3B Aquecedores é parceria:\";s:21:\"opt-quem-somos-titulo\";s:53:\" O clima perfeito para os grandes momentos do seu dia\";s:20:\"opt-quem-somos-texto\";s:1056:\"Para que um banho traga qualidade, bem-estar e, principalmente, um sorriso relaxante, o processo vai muito além do que conhecemos: experimente chegar a casa, dar um abraço na família e ir para o banho. Ligue sua ducha e deixe a sensação de tranquilidade te lavar. Tudo isso, com a temperatura já regulada e com a água na pressurização certa. Ou seja, no clima perfeito.<br class=\"blank\" /><br class=\"blank\" />\r\n\r\nPor entender muito bem desse momento, a <b>3B Aquecedores</b>, há mais de 25 anos no mercado atendendo Curitiba e Região Metropolitana, sabe como oferecer o melhor banho às pessoas. Aquecedores a gás, pressurizadores, medidores, entre tantos outros equipamentos que tornam o clima do seu banho especial.<br class=\"blank\" /><br class=\"blank\" />\r\n\r\nCom uma extensa equipe de profissionais, dividida entre as áreas de venda, assistência técnica, instalação, faturamento e administração, a <b>3B</b> conta também com uma frota de veículos leves e médios, os quais servem atentamente a todas as necessidades de seus clientes.\";s:20:\"opt-quem-somos-item1\";s:25:\"100%;Clientes satisfeitos\";s:20:\"opt-quem-somos-item2\";s:21:\"20+;Principais marcas\";s:20:\"opt-quem-somos-item3\";s:39:\"+100;Clientes satisfeitos a cada semana\";s:20:\"opt-quem-somos-item4\";s:27:\"+200;Variedades de produtos\";s:22:\"opt-quem-somos-galeria\";s:23:\"56,57,58,59,298,299,300\";s:19:\"opt-produtos-titulo\";s:0:\"\";s:18:\"opt-produtos-frase\";s:0:\"\";s:27:\"opt-produtos-foto-cabecalho\";a:5:{s:3:\"url\";s:70:\"http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/banner.png\";s:2:\"id\";s:2:\"60\";s:6:\"height\";s:3:\"275\";s:5:\"width\";s:4:\"1220\";s:9:\"thumbnail\";s:78:\"http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/banner-150x150.png\";}s:26:\"opt-produto-foto-cabecalho\";a:5:{s:3:\"url\";s:70:\"http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/banner.png\";s:2:\"id\";s:2:\"60\";s:6:\"height\";s:3:\"275\";s:5:\"width\";s:4:\"1220\";s:9:\"thumbnail\";s:78:\"http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/banner-150x150.png\";}s:30:\"opt-assistencia-foto-cabecalho\";a:5:{s:3:\"url\";s:78:\"http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/bg-assistencia.png\";s:2:\"id\";s:2:\"70\";s:6:\"height\";s:3:\"393\";s:5:\"width\";s:4:\"1220\";s:9:\"thumbnail\";s:86:\"http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/bg-assistencia-150x150.png\";}s:21:\"opt-assistencia-texto\";s:428:\"\r\nSempre faça uma revisão preventiva de seu aquecedor a gás. A 3B Aquecedores conta com uma Assistência Técnica Especializada para qualquer tipo de eventualidade com seu aparelho, utilizando equipamento de teste que identifica rapidamente os defeitos, além de técnicos e engenheiros especializados que melhor atenderão os clientes.\r\n\r\nPreencha o formulário abaixo, um de nossos atendentes entrará em contato com você.\";s:15:\"opt-blog-titulo\";s:54:\"A temperatura ideal para um bom banho de informação.\";s:14:\"opt-blog-frase\";s:230:\"Sente-se e aqueça sua mente: conheça um pouco mais sobre aquecedores a gás, informações técnicas, curiosidades sobre banho e tudo o que envolve esse relaxante momento para que o seu clima esteja sempre perfeito e agradável.\";s:22:\"opt-depoimentos-titulo\";s:77:\"Quem busca qualidade no banho encontra o clima perfeito com a 3B Aquecedores.\";s:21:\"opt-depoimentos-frase\";s:0:\"\";s:18:\"opt-contato-titulo\";s:59:\"Aqueça seus melhores momentos.  Fale com a 3B Aquecedores.\";s:17:\"opt-contato-frase\";s:69:\"Preencha o campo abaixo e envie suas informações à 3B Aquecedores.\";s:12:\"opt-endereco\";s:71:\"R. Delegado Leopoldo Belczak, 780 – Capão da Imbuia, Curitiba – PR\";s:12:\"opt-telefone\";s:14:\"(41) 3366-2939\";s:9:\"opt-email\";s:23:\"3b@3baquecedores.com.br\";s:11:\"opt-horario\";s:48:\"Atendimento de segunda a sexta, das 08h às 18h.\";s:20:\"opt-newsletter-frase\";s:82:\"<b>Assine nosso newsletter</b> e receba novidades e promoções da 3B Aquecedores.\";s:12:\"opt-facebook\";s:56:\"https://www.facebook.com/3B-Aquecedores-512387188804160/\";s:11:\"opt-twitter\";s:31:\"https://twitter.com/?lang=pt-br\";s:11:\"opt-youtube\";s:24:\"https://www.youtube.com/\";s:10:\"opt-Slogan\";s:29:\"Seu banho no clima  perfeito.\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(283, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:2:{s:20:\"opt-quem-somos-texto\";s:1140:\"Para que um banho traga qualidade, bem-estar e, principalmente, um sorriso relaxante, o processo vai muito além do que conhecemos: experimente chegar a casa, dar um abraço na família e ir para o banho. Ligue sua ducha e deixe a sensação de tranquilidade te lavar. Tudo isso, com a temperatura já regulada e com a água na pressurização certa. Ou seja, no clima perfeito.<br class=\"blank\" /><br class=\"blank\" />\r\n\r\nPor entender muito bem desse momento, a <b>3B Aquecedores</b>, há mais de 25 anos no mercado atendendo Curitiba e Região Metropolitana, sabe como oferecer o melhor banho às pessoas. Aquecedores a gás, pressurizadores, medidores, entre tantos outros equipamentos que tornam o clima do seu banho especial.<br class=\"blank\" /><br class=\"blank\" />\r\n\r\nCom uma extensa equipe de profissionais, dividida entre as áreas de venda, assistência técnica, instalação, faturamento e administração, a <b>3B</b> conta também com uma frota de veículos leves e médios, os quais servem atentamente a todas as necessidades de seus clientes.<br class=\"blank\" /><br class=\"blank\" />\r\n\r\n<br class=\"blank\" /><br class=\"blank\" />\";s:21:\"opt-assistencia-texto\";s:430:\"\r\n\r\nSempre faça uma revisão preventiva de seu aquecedor a gás. A 3B Aquecedores conta com uma Assistência Técnica Especializada para qualquer tipo de eventualidade com seu aparelho, utilizando equipamento de teste que identifica rapidamente os defeitos, além de técnicos e engenheiros especializados que melhor atenderão os clientes.\r\n\r\nPreencha o formulário abaixo, um de nossos atendentes entrará em contato com você.\";}s:9:\"last_save\";i:1505307571;}', 'yes'),
(317, 'duplicate_post_copydate', '', 'yes'),
(318, 'duplicate_post_blacklist', '', 'yes'),
(319, 'duplicate_post_title_prefix', '', 'yes'),
(320, 'duplicate_post_title_suffix', '', 'yes'),
(321, 'duplicate_post_roles', 'a:2:{i:0;s:13:\"administrator\";i:1;s:6:\"editor\";}', 'yes'),
(361, '_transient_timeout_plugin_slugs', '1506532326', 'no'),
(362, '_transient_plugin_slugs', 'a:13:{i:0;s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";i:1;s:41:\"base-3baquecedores/base-3baquecedores.php\";i:2;s:39:\"categories-images/categories-images.php\";i:3;s:36:\"contact-form-7/wp-contact-form-7.php\";i:4;s:32:\"disqus-comment-system/disqus.php\";i:5;s:33:\"duplicate-post/duplicate-post.php\";i:6;s:9:\"hello.php\";i:7;s:28:\"wysija-newsletters/index.php\";i:8;s:21:\"meta-box/meta-box.php\";i:9;s:37:\"post-types-order/post-types-order.php\";i:10;s:35:\"redux-framework/redux-framework.php\";i:11;s:29:\"wp-mail-smtp/wp_mail_smtp.php\";i:12;s:24:\"wordpress-seo/wp-seo.php\";}', 'no'),
(430, 'category_children', 'a:0:{}', 'yes'),
(610, 'CPT_configured', 'TRUE', 'yes'),
(624, 'wysija_queries', '', 'no'),
(625, 'wysija_queries_errors', '', 'no'),
(626, 'wysija_msg', '', 'no'),
(668, 'z_taxonomy_image5', 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto3.png', 'yes'),
(669, 'wpseo_taxonomy_meta', 'a:1:{s:16:\"categoriaProduto\";a:3:{i:5;a:1:{s:13:\"wpseo_linkdex\";s:2:\"20\";}i:6;a:1:{s:13:\"wpseo_linkdex\";s:2:\"20\";}i:7;a:1:{s:13:\"wpseo_linkdex\";s:2:\"15\";}}}', 'yes'),
(671, 'z_taxonomy_image6', 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto2.png', 'yes'),
(673, 'z_taxonomy_image7', 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto1.png', 'yes'),
(680, 'disqus_active', '1', 'yes'),
(681, 'disqus_version', '2.85', 'yes'),
(682, 'disqus_forum_url', '3baquecedores', 'yes'),
(683, 'dsq_external_js', '1', 'yes'),
(684, 'disqus_cc_fix', '1', 'yes'),
(685, 'disqus_api_key', 'tIacvWSZQhlIazj9k2bSF1A2cvUpb2kY9PKcvzjxtJCeeU3czqgKgJ1JmdinaYfM', 'yes'),
(686, 'disqus_user_api_key', 'DjNudWRZiCHmOXYQ6EXZVYAdJIUiWfhwlvzNWjELWgPpOrtcyvNvKhDM44d1FLtp', 'yes'),
(687, 'disqus_replace', 'all', 'yes'),
(935, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:27:\"3baquecedores@palupa.com.br\";s:7:\"version\";s:5:\"4.5.7\";s:9:\"timestamp\";i:1488851907;}', 'yes'),
(1985, '_transient_doing_cron', '1512458370.9026100635528564453125', 'yes'),
(2004, '_site_transient_timeout_browser_8ce1bea2f653ca2ce71bdf6183bef333', '1501786071', 'yes'),
(2005, '_site_transient_browser_8ce1bea2f653ca2ce71bdf6183bef333', 'a:9:{s:8:\"platform\";s:7:\"Windows\";s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"59.0.3071.115\";s:10:\"update_url\";s:28:\"http://www.google.com/chrome\";s:7:\"img_src\";s:49:\"http://s.wordpress.org/images/browsers/chrome.png\";s:11:\"img_src_ssl\";s:48:\"https://wordpress.org/images/browsers/chrome.png\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}', 'yes'),
(2008, 'duplicate_post_copytitle', '1', 'yes'),
(2009, 'duplicate_post_copyslug', '1', 'yes'),
(2010, 'duplicate_post_copycontent', '1', 'yes'),
(2011, 'duplicate_post_copythumbnail', '1', 'yes'),
(2012, 'duplicate_post_copytemplate', '1', 'yes'),
(2013, 'duplicate_post_copyformat', '1', 'yes'),
(2014, 'duplicate_post_copyauthor', '0', 'yes'),
(2015, 'duplicate_post_copypassword', '0', 'yes'),
(2016, 'duplicate_post_copycomments', '0', 'yes'),
(2017, 'duplicate_post_copymenuorder', '1', 'yes'),
(2018, 'duplicate_post_types_enabled', 'a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}', 'yes'),
(2019, 'duplicate_post_show_bulkactions', '1', 'yes'),
(2020, 'duplicate_post_version', '3.2', 'yes'),
(2021, 'duplicate_post_show_notice', '0', 'no'),
(2024, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2025, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2026, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2029, 'db_upgraded', '', 'yes'),
(2033, 'can_compress_scripts', '1', 'no'),
(2039, 'wpseo_sitemap_cache_validator_global', 'uZsD', 'no'),
(2040, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:3:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-4.8.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-4.8.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.8.2\";s:7:\"version\";s:5:\"4.8.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.8.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.8.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.8.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.8.2-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-4.8.2-partial-0.zip\";s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.8.2\";s:7:\"version\";s:5:\"4.8.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:3:\"4.8\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-4.8.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-4.8.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.8.2\";s:7:\"version\";s:5:\"4.8.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1506445914;s:15:\"version_checked\";s:3:\"4.8\";s:12:\"translations\";a:0:{}}', 'no'),
(2041, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1506445926;s:7:\"checked\";a:13:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:5:\"4.2.8\";s:41:\"base-3baquecedores/base-3baquecedores.php\";s:3:\"0.1\";s:39:\"categories-images/categories-images.php\";s:5:\"2.5.3\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"4.8.1\";s:32:\"disqus-comment-system/disqus.php\";s:4:\"2.87\";s:33:\"duplicate-post/duplicate-post.php\";s:3:\"3.2\";s:9:\"hello.php\";s:3:\"1.6\";s:28:\"wysija-newsletters/index.php\";s:8:\"2.7.11.3\";s:21:\"meta-box/meta-box.php\";s:6:\"4.12.2\";s:37:\"post-types-order/post-types-order.php\";s:7:\"1.9.3.3\";s:35:\"redux-framework/redux-framework.php\";s:5:\"3.6.5\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:6:\"0.10.1\";s:24:\"wordpress-seo/wp-seo.php\";s:3:\"5.1\";}s:8:\"response\";a:6:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";O:8:\"stdClass\":8:{s:2:\"id\";s:49:\"w.org/plugins/all-in-one-wp-security-and-firewall\";s:4:\"slug\";s:35:\"all-in-one-wp-security-and-firewall\";s:6:\"plugin\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:11:\"new_version\";s:5:\"4.2.9\";s:3:\"url\";s:66:\"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip\";s:6:\"tested\";s:5:\"4.8.1\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":8:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:3:\"4.9\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/contact-form-7.4.9.zip\";s:6:\"tested\";s:5:\"4.8.1\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:28:\"wysija-newsletters/index.php\";O:8:\"stdClass\":8:{s:2:\"id\";s:32:\"w.org/plugins/wysija-newsletters\";s:4:\"slug\";s:18:\"wysija-newsletters\";s:6:\"plugin\";s:28:\"wysija-newsletters/index.php\";s:11:\"new_version\";s:8:\"2.7.12.1\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/wysija-newsletters/\";s:7:\"package\";s:70:\"https://downloads.wordpress.org/plugin/wysija-newsletters.2.7.12.1.zip\";s:6:\"tested\";s:5:\"4.8.2\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":8:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:6:\"4.12.3\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:51:\"https://downloads.wordpress.org/plugin/meta-box.zip\";s:6:\"tested\";s:5:\"4.8.1\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:37:\"post-types-order/post-types-order.php\";O:8:\"stdClass\":8:{s:2:\"id\";s:30:\"w.org/plugins/post-types-order\";s:4:\"slug\";s:16:\"post-types-order\";s:6:\"plugin\";s:37:\"post-types-order/post-types-order.php\";s:11:\"new_version\";s:7:\"1.9.3.5\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/post-types-order/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/post-types-order.1.9.3.5.zip\";s:6:\"tested\";s:5:\"4.8.1\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":8:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:3:\"5.5\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/wordpress-seo.5.5.zip\";s:6:\"tested\";s:5:\"4.8.2\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:6:{s:39:\"categories-images/categories-images.php\";O:8:\"stdClass\":6:{s:2:\"id\";s:31:\"w.org/plugins/categories-images\";s:4:\"slug\";s:17:\"categories-images\";s:6:\"plugin\";s:39:\"categories-images/categories-images.php\";s:11:\"new_version\";s:5:\"2.5.3\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/categories-images/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/categories-images.2.5.3.zip\";}s:32:\"disqus-comment-system/disqus.php\";O:8:\"stdClass\":6:{s:2:\"id\";s:35:\"w.org/plugins/disqus-comment-system\";s:4:\"slug\";s:21:\"disqus-comment-system\";s:6:\"plugin\";s:32:\"disqus-comment-system/disqus.php\";s:11:\"new_version\";s:4:\"2.87\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/disqus-comment-system/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/disqus-comment-system.2.87.zip\";}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":6:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:3:\"3.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/duplicate-post.3.2.zip\";}s:9:\"hello.php\";O:8:\"stdClass\":6:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":6:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:5:\"3.6.5\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.5.zip\";}s:29:\"wp-mail-smtp/wp_mail_smtp.php\";O:8:\"stdClass\":6:{s:2:\"id\";s:26:\"w.org/plugins/wp-mail-smtp\";s:4:\"slug\";s:12:\"wp-mail-smtp\";s:6:\"plugin\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:11:\"new_version\";s:6:\"0.10.1\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/wp-mail-smtp/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/wp-mail-smtp.0.10.1.zip\";}}}', 'no'),
(2043, 'wpseo_sitemap_1_cache_validator', 'tvl8', 'no'),
(2044, 'wpseo_sitemap_produto_cache_validator', 'tvle', 'no'),
(2047, 'wpseo_sitemap_categoriaProduto_cache_validator', 'tvlb', 'no'),
(2127, '_site_transient_timeout_available_translations', '1501521581', 'no');
INSERT INTO `3b_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2128, '_site_transient_available_translations', 'a:108:{s:2:\"af\";a:8:{s:8:\"language\";s:2:\"af\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-23 21:35:47\";s:12:\"english_name\";s:9:\"Afrikaans\";s:11:\"native_name\";s:9:\"Afrikaans\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/af.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"af\";i:2;s:3:\"afr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Gaan voort\";}}s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-09 03:55:46\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:5:\"4.7.5\";s:7:\"updated\";s:19:\"2017-01-26 15:42:35\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.5/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"as\";a:8:{s:8:\"language\";s:2:\"as\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-22 18:59:07\";s:12:\"english_name\";s:8:\"Assamese\";s:11:\"native_name\";s:21:\"অসমীয়া\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/as.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"as\";i:2;s:3:\"asm\";i:3;s:3:\"asm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-06 00:09:27\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-12 20:34:31\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:3:\"bel\";a:8:{s:8:\"language\";s:3:\"bel\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-17 20:31:44\";s:12:\"english_name\";s:10:\"Belarusian\";s:11:\"native_name\";s:29:\"Беларуская мова\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/translation/core/4.8/bel.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"be\";i:2;s:3:\"bel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Працягнуць\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-18 19:16:01\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Напред\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-04 16:58:43\";s:12:\"english_name\";s:7:\"Bengali\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:2:\"bo\";a:8:{s:8:\"language\";s:2:\"bo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-05 09:44:12\";s:12:\"english_name\";s:7:\"Tibetan\";s:11:\"native_name\";s:21:\"བོད་ཡིག\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/bo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bo\";i:2;s:3:\"tib\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"མུ་མཐུད།\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-04 20:20:28\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-16 11:47:56\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-02 17:25:51\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:5:\"cs_CZ\";a:8:{s:8:\"language\";s:5:\"cs_CZ\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 08:46:26\";s:12:\"english_name\";s:5:\"Czech\";s:11:\"native_name\";s:12:\"Čeština‎\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/cs_CZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cs\";i:2;s:3:\"ces\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Pokračovat\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-14 13:21:24\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-14 23:24:44\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsæt\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-15 21:25:12\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:14:\"de_CH_informal\";a:8:{s:8:\"language\";s:14:\"de_CH_informal\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-15 08:50:23\";s:12:\"english_name\";s:30:\"German (Switzerland, Informal)\";s:11:\"native_name\";s:21:\"Deutsch (Schweiz, Du)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/4.8/de_CH_informal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-08 16:08:42\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-15 19:58:49\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/translation/core/4.8/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:3:\"dzo\";a:8:{s:8:\"language\";s:3:\"dzo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-06-29 08:59:03\";s:12:\"english_name\";s:8:\"Dzongkha\";s:11:\"native_name\";s:18:\"རྫོང་ཁ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"dz\";i:2;s:3:\"dzo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-21 18:05:57\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-17 08:09:19\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:5:\"4.7.5\";s:7:\"updated\";s:19:\"2017-01-26 15:53:43\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.5/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-15 07:18:00\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-15 05:14:35\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-23 16:48:27\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-27 10:36:23\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-20 00:55:30\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/es_AR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-16 17:22:41\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/es_MX.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-07 00:53:01\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/es_VE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:5:\"4.7.5\";s:7:\"updated\";s:19:\"2017-01-26 15:54:37\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.5/es_GT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:5:\"4.7.5\";s:7:\"updated\";s:19:\"2017-01-26 15:54:37\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.5/es_CO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-28 20:09:49\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/es_CL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-09 09:36:22\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/es_PE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-18 10:53:33\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/es_ES.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"es\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 16:37:11\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-21 08:00:44\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-09 15:50:45\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-08 18:25:22\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-23 06:47:57\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-07 13:48:37\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-05 17:58:06\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-08-23 17:41:37\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-17 20:40:15\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"gu\";a:8:{s:8:\"language\";s:2:\"gu\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-07 12:07:46\";s:12:\"english_name\";s:8:\"Gujarati\";s:11:\"native_name\";s:21:\"ગુજરાતી\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/gu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gu\";i:2;s:3:\"guj\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"ચાલુ રાખવું\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-15 13:33:29\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"המשך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-16 17:29:16\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-19 08:19:39\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:48:39\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Folytatás\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-03 16:21:10\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-12 12:20:50\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:5:\"4.7.5\";s:7:\"updated\";s:19:\"2017-04-13 13:55:54\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.5/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-04 13:01:37\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-25 11:16:15\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"続ける\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-12 09:20:11\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:3:\"kab\";a:8:{s:8:\"language\";s:3:\"kab\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-16 18:44:50\";s:12:\"english_name\";s:6:\"Kabyle\";s:11:\"native_name\";s:9:\"Taqbaylit\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/translation/core/4.8/kab.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"kab\";i:3;s:3:\"kab\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Kemmel\";}}s:2:\"km\";a:8:{s:8:\"language\";s:2:\"km\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-07 02:07:59\";s:12:\"english_name\";s:5:\"Khmer\";s:11:\"native_name\";s:27:\"ភាសាខ្មែរ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/km.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"km\";i:2;s:3:\"khm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"បន្ត\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-19 07:08:35\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:3:\"ckb\";a:8:{s:8:\"language\";s:3:\"ckb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:48:25\";s:12:\"english_name\";s:16:\"Kurdish (Sorani)\";s:11:\"native_name\";s:13:\"كوردی‎\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ckb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ku\";i:3;s:3:\"ckb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"به‌رده‌وام به‌\";}}s:2:\"lo\";a:8:{s:8:\"language\";s:2:\"lo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 09:59:23\";s:12:\"english_name\";s:3:\"Lao\";s:11:\"native_name\";s:21:\"ພາສາລາວ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lo\";i:2;s:3:\"lao\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"ຕໍ່​ໄປ\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-05 11:43:04\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:2:\"lv\";a:8:{s:8:\"language\";s:2:\"lv\";s:7:\"version\";s:5:\"4.7.5\";s:7:\"updated\";s:19:\"2017-03-17 20:40:40\";s:12:\"english_name\";s:7:\"Latvian\";s:11:\"native_name\";s:16:\"Latviešu valoda\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.5/lv.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lv\";i:2;s:3:\"lav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Turpināt\";}}s:5:\"mk_MK\";a:8:{s:8:\"language\";s:5:\"mk_MK\";s:7:\"version\";s:5:\"4.7.5\";s:7:\"updated\";s:19:\"2017-01-26 15:54:41\";s:12:\"english_name\";s:10:\"Macedonian\";s:11:\"native_name\";s:31:\"Македонски јазик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.5/mk_MK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mk\";i:2;s:3:\"mkd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Продолжи\";}}s:5:\"ml_IN\";a:8:{s:8:\"language\";s:5:\"ml_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:43:32\";s:12:\"english_name\";s:9:\"Malayalam\";s:11:\"native_name\";s:18:\"മലയാളം\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ml\";i:2;s:3:\"mal\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"തുടരുക\";}}s:2:\"mn\";a:8:{s:8:\"language\";s:2:\"mn\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 07:29:35\";s:12:\"english_name\";s:9:\"Mongolian\";s:11:\"native_name\";s:12:\"Монгол\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mn\";i:2;s:3:\"mon\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"Үргэлжлүүлэх\";}}s:2:\"mr\";a:8:{s:8:\"language\";s:2:\"mr\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-05 19:40:47\";s:12:\"english_name\";s:7:\"Marathi\";s:11:\"native_name\";s:15:\"मराठी\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/mr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mr\";i:2;s:3:\"mar\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"सुरु ठेवा\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:5:\"4.7.5\";s:7:\"updated\";s:19:\"2017-03-05 09:45:10\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.5/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.1.18\";s:7:\"updated\";s:19:\"2015-03-26 15:57:42\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.1.18/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ဆောင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-26 11:11:30\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:5:\"ne_NP\";a:8:{s:8:\"language\";s:5:\"ne_NP\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-23 11:30:58\";s:12:\"english_name\";s:6:\"Nepali\";s:11:\"native_name\";s:18:\"नेपाली\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/ne_NP.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ne\";i:2;s:3:\"nep\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:43:\"जारी राख्नुहोस्\";}}s:5:\"nl_BE\";a:8:{s:8:\"language\";s:5:\"nl_BE\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-20 17:04:00\";s:12:\"english_name\";s:15:\"Dutch (Belgium)\";s:11:\"native_name\";s:20:\"Nederlands (België)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/nl_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:5:\"4.7.5\";s:7:\"updated\";s:19:\"2017-02-16 13:24:21\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/4.7.5/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-18 18:26:58\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-08 13:05:53\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-02 13:47:38\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pa_IN\";a:8:{s:8:\"language\";s:5:\"pa_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-16 05:19:43\";s:12:\"english_name\";s:7:\"Punjabi\";s:11:\"native_name\";s:18:\"ਪੰਜਾਬੀ\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pa\";i:2;s:3:\"pan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"ਜਾਰੀ ਰੱਖੋ\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-19 13:38:04\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.1.18\";s:7:\"updated\";s:19:\"2015-03-29 22:19:48\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.1.18/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"دوام ورکړه\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-23 10:24:37\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-13 23:57:05\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:3:\"rhg\";a:8:{s:8:\"language\";s:3:\"rhg\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-16 13:03:18\";s:12:\"english_name\";s:8:\"Rohingya\";s:11:\"native_name\";s:8:\"Ruáinga\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"rhg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-15 10:32:19\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-15 13:54:09\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:3:\"sah\";a:8:{s:8:\"language\";s:3:\"sah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-21 02:06:41\";s:12:\"english_name\";s:5:\"Sakha\";s:11:\"native_name\";s:14:\"Сахалыы\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"sah\";i:3;s:3:\"sah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Салҕаа\";}}s:5:\"si_LK\";a:8:{s:8:\"language\";s:5:\"si_LK\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 06:00:52\";s:12:\"english_name\";s:7:\"Sinhala\";s:11:\"native_name\";s:15:\"සිංහල\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"si\";i:2;s:3:\"sin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:44:\"දිගටම කරගෙන යන්න\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-15 09:02:13\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-08 15:29:14\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Nadaljuj\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:5:\"4.7.5\";s:7:\"updated\";s:19:\"2017-04-24 08:35:30\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.5/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-08 11:06:53\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-18 17:49:44\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:3:\"szl\";a:8:{s:8:\"language\";s:3:\"szl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-24 19:58:14\";s:12:\"english_name\";s:8:\"Silesian\";s:11:\"native_name\";s:17:\"Ślōnskŏ gŏdka\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"szl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:13:\"Kōntynuować\";}}s:5:\"ta_IN\";a:8:{s:8:\"language\";s:5:\"ta_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:22:47\";s:12:\"english_name\";s:5:\"Tamil\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"தொடரவும்\";}}s:2:\"te\";a:8:{s:8:\"language\";s:2:\"te\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:47:39\";s:12:\"english_name\";s:6:\"Telugu\";s:11:\"native_name\";s:18:\"తెలుగు\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/te.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"te\";i:2;s:3:\"tel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"కొనసాగించు\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:48:43\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-30 02:38:08\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-19 13:54:12\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"tt_RU\";a:8:{s:8:\"language\";s:5:\"tt_RU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-20 20:20:50\";s:12:\"english_name\";s:5:\"Tatar\";s:11:\"native_name\";s:19:\"Татар теле\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tt\";i:2;s:3:\"tat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"дәвам итү\";}}s:3:\"tah\";a:8:{s:8:\"language\";s:3:\"tah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-06 18:39:39\";s:12:\"english_name\";s:8:\"Tahitian\";s:11:\"native_name\";s:10:\"Reo Tahiti\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"ty\";i:2;s:3:\"tah\";i:3;s:3:\"tah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-05 09:23:39\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:9:\"Uyƣurqə\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-01 22:52:09\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"ur\";a:8:{s:8:\"language\";s:2:\"ur\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-02 09:17:00\";s:12:\"english_name\";s:4:\"Urdu\";s:11:\"native_name\";s:8:\"اردو\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/ur.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ur\";i:2;s:3:\"urd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"جاری رکھیں\";}}s:5:\"uz_UZ\";a:8:{s:8:\"language\";s:5:\"uz_UZ\";s:7:\"version\";s:5:\"4.7.5\";s:7:\"updated\";s:19:\"2017-05-13 09:55:38\";s:12:\"english_name\";s:5:\"Uzbek\";s:11:\"native_name\";s:11:\"O‘zbekcha\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.5/uz_UZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uz\";i:2;s:3:\"uzb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Davom etish\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-06-15 11:24:18\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/4.8/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_HK\";a:8:{s:8:\"language\";s:5:\"zh_HK\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-16 05:48:05\";s:12:\"english_name\";s:19:\"Chinese (Hong Kong)\";s:11:\"native_name\";s:16:\"香港中文版	\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/zh_HK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:3:\"4.8\";s:7:\"updated\";s:19:\"2017-07-05 10:14:12\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:5:\"4.7.5\";s:7:\"updated\";s:19:\"2017-01-26 15:54:45\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.5/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}}', 'no'),
(2131, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1506445915;s:7:\"checked\";a:3:{s:16:\"tresbaquecedores\";s:0:\"\";s:13:\"twentyfifteen\";s:3:\"1.8\";s:15:\"twentyseventeen\";s:3:\"1.3\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(2149, 'wpseo_sitemap_destaque_cache_validator', '5kipU', 'no'),
(2169, 'categoriaProduto_children', 'a:0:{}', 'yes'),
(2170, 'wpseo_sitemap_274_cache_validator', '5yN3w', 'no'),
(2206, '_site_transient_timeout_browser_f9694186c5800b9905943d3f44ede836', '1505912298', 'no'),
(2207, '_site_transient_browser_f9694186c5800b9905943d3f44ede836', 'a:9:{s:8:\"platform\";s:7:\"Windows\";s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"60.0.3112.113\";s:10:\"update_url\";s:28:\"http://www.google.com/chrome\";s:7:\"img_src\";s:49:\"http://s.wordpress.org/images/browsers/chrome.png\";s:11:\"img_src_ssl\";s:48:\"https://wordpress.org/images/browsers/chrome.png\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}', 'no'),
(2210, '_transient_timeout_select2-css_style_cdn_is_up', '1505393954', 'no'),
(2211, '_transient_select2-css_style_cdn_is_up', '1', 'no'),
(2212, '_transient_timeout_select2-js_script_cdn_is_up', '1505393954', 'no'),
(2213, '_transient_select2-js_script_cdn_is_up', '1', 'no'),
(2214, '_transient_is_multi_author', '0', 'yes'),
(2215, '_transient_tresbaquecedores_categories', '2', 'yes'),
(2220, '_site_transient_timeout_browser_a941b544cd189540f5b3be387648280d', '1506543218', 'no'),
(2221, '_site_transient_browser_a941b544cd189540f5b3be387648280d', 'a:9:{s:8:\"platform\";s:7:\"Windows\";s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"61.0.3163.91\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:49:\"http://s.wordpress.org/images/browsers/chrome.png\";s:11:\"img_src_ssl\";s:48:\"https://wordpress.org/images/browsers/chrome.png\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}', 'no'),
(2226, '_transient_timeout_users_online', '1506447713', 'no'),
(2227, '_transient_users_online', 'a:1:{i:0;a:3:{s:7:\"user_id\";i:1;s:13:\"last_activity\";d:1506435113;s:10:\"ip_address\";s:13:\"187.95.127.39\";}}', 'no'),
(2228, '_site_transient_timeout_theme_roots', '1506447715', 'no'),
(2229, '_site_transient_theme_roots', 'a:3:{s:16:\"tresbaquecedores\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";}', 'no'),
(2230, '_site_transient_timeout_browser_a9db4d03969fdd98d377b682b063efe6', '1507050715', 'no'),
(2231, '_site_transient_browser_a9db4d03969fdd98d377b682b063efe6', 'a:9:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"61.0.3163.100\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}', 'no'),
(2232, '_transient_timeout_wpseo-dashboard-totals', '1506532315', 'no'),
(2233, '_transient_wpseo-dashboard-totals', 'a:1:{i:1;a:1:{i:3;a:5:{s:8:\"seo_rank\";s:2:\"na\";s:5:\"title\";s:31:\"Posts sem palavra-chave em foco\";s:5:\"class\";s:15:\"wpseo-glance-na\";s:10:\"icon_class\";s:2:\"na\";s:5:\"count\";s:2:\"14\";}}}', 'no');

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_postmeta`
--

CREATE TABLE `3b_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `3b_postmeta`
--

INSERT INTO `3b_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(8, 25, '_wp_attached_file', '2016/05/assessoria.jpg'),
(9, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:609;s:6:\"height\";i:449;s:4:\"file\";s:22:\"2016/05/assessoria.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"assessoria-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"assessoria-300x221.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:221;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:22:\"assessoria-600x442.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:442;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(10, 28, '_wp_attached_file', '2016/05/foto-destaque.png'),
(11, 28, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1220;s:6:\"height\";i:785;s:4:\"file\";s:25:\"2016/05/foto-destaque.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"foto-destaque-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"foto-destaque-300x193.png\";s:5:\"width\";i:300;s:6:\"height\";i:193;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"foto-destaque-768x494.png\";s:5:\"width\";i:768;s:6:\"height\";i:494;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"foto-destaque-1024x659.png\";s:5:\"width\";i:1024;s:6:\"height\";i:659;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:25:\"foto-destaque-600x386.png\";s:5:\"width\";i:600;s:6:\"height\";i:386;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(13, 27, '_edit_last', '1'),
(14, 27, '3B_destaque_link', 'http://www.3baquecedores.com.br/'),
(15, 27, '3B_destaque_descricao', 'Venda de aquecedores'),
(16, 27, '_edit_lock', '1504548910:1'),
(17, 30, '_wp_attached_file', '2016/05/foto-que-fazemos.png'),
(18, 30, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:609;s:6:\"height\";i:361;s:4:\"file\";s:28:\"2016/05/foto-que-fazemos.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"foto-que-fazemos-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"foto-que-fazemos-300x178.png\";s:5:\"width\";i:300;s:6:\"height\";i:178;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:28:\"foto-que-fazemos-600x356.png\";s:5:\"width\";i:600;s:6:\"height\";i:356;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(19, 29, '_thumbnail_id', '351'),
(20, 29, '_edit_last', '1'),
(21, 29, '3B_destaque_link', 'http://www.3baquecedores.com.br/'),
(22, 29, '3B_destaque_descricao', 'Assistência Técnica'),
(23, 29, '_edit_lock', '1504548909:1'),
(29, 32, '_wp_attached_file', '2016/05/produto.png'),
(30, 32, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:192;s:6:\"height\";i:294;s:4:\"file\";s:19:\"2016/05/produto.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(31, 33, '_wp_attached_file', '2016/05/produto-carrossel.png'),
(32, 33, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:153;s:6:\"height\";i:224;s:4:\"file\";s:29:\"2016/05/produto-carrossel.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"produto-carrossel-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(33, 34, '_wp_attached_file', '2016/05/foto-produto.png'),
(34, 34, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:24:\"2016/05/foto-produto.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"foto-produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"foto-produto-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(83, 34, '_edit_lock', '1462971355:1'),
(84, 37, '_wp_attached_file', '2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg'),
(85, 37, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:344;s:6:\"height\";i:344;s:4:\"file\";s:83:\"2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:2:\"Ð\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:4:\"8144\";s:8:\"keywords\";a:0:{}}}'),
(86, 38, '_wp_attached_file', '2016/05/aquecedor-de-agua-a-gas.jpg'),
(87, 38, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:130;s:6:\"height\";i:130;s:4:\"file\";s:35:\"2016/05/aquecedor-de-agua-a-gas.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(90, 39, '_thumbnail_id', '28'),
(91, 39, '_edit_last', '1'),
(92, 39, '3B_depoimento_texto', 'O aquecedor é ótimo! Tem vazão para um banho perfeito, é bastante compacto (ocupa espaço bem menor que meu antigo aparelho). Entrega mais que perfeito, muitos dias antes do prazo!!'),
(93, 39, '3B_depoimento_autor', 'João'),
(94, 39, '3B_depoimento_autor_info', 'Cliente'),
(95, 39, '_edit_lock', '1463069953:1'),
(96, 40, '_thumbnail_id', '30'),
(97, 40, '_edit_last', '1'),
(98, 40, '3B_depoimento_texto', 'Nossas Central termica estava com problemas trouxemos uma empresa para concertar mas não puderam resolver, ai então decidimos chamar a 3B, ai então foi tudo mais fácil e rápido!'),
(99, 40, '3B_depoimento_autor', 'Maria'),
(100, 40, '3B_depoimento_autor_info', 'Cliente'),
(101, 40, '_edit_lock', '1463067584:1'),
(102, 40, '_dp_original', '39'),
(103, 41, '_thumbnail_id', '28'),
(104, 41, '_edit_last', '1'),
(105, 41, '3B_depoimento_texto', 'O aquecedor é ótimo! Tem vazão para um banho perfeito, é bastante compacto (ocupa espaço bem menor que meu antigo aparelho). Entrega mais que perfeito, muitos dias antes do prazo!!'),
(106, 41, '3B_depoimento_autor', 'João'),
(107, 41, '3B_depoimento_autor_info', 'Cliente'),
(108, 41, '_edit_lock', '1463067574:1'),
(109, 41, '_dp_original', '39'),
(110, 43, '_thumbnail_id', '30'),
(111, 43, '_edit_last', '1'),
(112, 43, '3B_depoimento_texto', 'Nossas Central termica estava com problemas trouxemos uma empresa para concertar mas não puderam resolver, ai então decidimos chamar a 3B, ai então foi tudo mais fácil e rápido!'),
(113, 43, '3B_depoimento_autor', 'Maria'),
(114, 43, '3B_depoimento_autor_info', 'Cliente'),
(115, 43, '_edit_lock', '1463074958:1'),
(117, 43, '_dp_original', '40'),
(118, 45, '_wp_attached_file', '2016/05/logo1.png'),
(119, 45, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:218;s:6:\"height\";i:182;s:4:\"file\";s:17:\"2016/05/logo1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"logo1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(120, 44, '_thumbnail_id', '45'),
(121, 44, '_edit_last', '1'),
(122, 44, '_edit_lock', '1463148687:1'),
(123, 47, '_wp_attached_file', '2016/05/logo2.png'),
(124, 47, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:217;s:6:\"height\";i:182;s:4:\"file\";s:17:\"2016/05/logo2.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"logo2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(125, 46, '_thumbnail_id', '47'),
(126, 46, '_edit_last', '1'),
(127, 46, '_edit_lock', '1463148685:1'),
(128, 49, '_wp_attached_file', '2016/05/logo3.png'),
(129, 49, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:218;s:6:\"height\";i:182;s:4:\"file\";s:17:\"2016/05/logo3.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"logo3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(130, 48, '_thumbnail_id', '49'),
(131, 48, '_edit_last', '1'),
(132, 48, '_edit_lock', '1463148684:1'),
(133, 50, '_edit_last', '1'),
(134, 50, '_edit_lock', '1470075278:1'),
(135, 51, '_wp_attached_file', '2016/05/logo4.png'),
(136, 51, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:218;s:6:\"height\";i:182;s:4:\"file\";s:17:\"2016/05/logo4.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"logo4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(137, 50, '_thumbnail_id', '51'),
(138, 52, '_wp_attached_file', '2016/05/fotos1.png'),
(139, 52, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:305;s:6:\"height\";i:223;s:4:\"file\";s:18:\"2016/05/fotos1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"fotos1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"fotos1-300x219.png\";s:5:\"width\";i:300;s:6:\"height\";i:219;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(140, 53, '_wp_attached_file', '2016/05/fotos2.png'),
(141, 53, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:304;s:6:\"height\";i:223;s:4:\"file\";s:18:\"2016/05/fotos2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"fotos2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"fotos2-300x220.png\";s:5:\"width\";i:300;s:6:\"height\";i:220;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(142, 54, '_wp_attached_file', '2016/05/fotos3.png'),
(143, 54, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:304;s:6:\"height\";i:223;s:4:\"file\";s:18:\"2016/05/fotos3.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"fotos3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"fotos3-300x220.png\";s:5:\"width\";i:300;s:6:\"height\";i:220;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(144, 55, '_wp_attached_file', '2016/05/fotos4.png'),
(145, 55, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:304;s:6:\"height\";i:223;s:4:\"file\";s:18:\"2016/05/fotos4.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"fotos4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"fotos4-300x220.png\";s:5:\"width\";i:300;s:6:\"height\";i:220;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(146, 56, '_wp_attached_file', '2016/05/fotos1-1.png'),
(147, 56, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:305;s:6:\"height\";i:223;s:4:\"file\";s:20:\"2016/05/fotos1-1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"fotos1-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"fotos1-1-300x219.png\";s:5:\"width\";i:300;s:6:\"height\";i:219;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(148, 57, '_wp_attached_file', '2016/05/fotos2-1.png'),
(149, 57, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:304;s:6:\"height\";i:223;s:4:\"file\";s:20:\"2016/05/fotos2-1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"fotos2-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"fotos2-1-300x220.png\";s:5:\"width\";i:300;s:6:\"height\";i:220;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(150, 58, '_wp_attached_file', '2016/05/fotos3-1.png'),
(151, 58, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:304;s:6:\"height\";i:223;s:4:\"file\";s:20:\"2016/05/fotos3-1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"fotos3-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"fotos3-1-300x220.png\";s:5:\"width\";i:300;s:6:\"height\";i:220;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(152, 59, '_wp_attached_file', '2016/05/fotos4-1.png'),
(153, 59, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:304;s:6:\"height\";i:223;s:4:\"file\";s:20:\"2016/05/fotos4-1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"fotos4-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"fotos4-1-300x220.png\";s:5:\"width\";i:300;s:6:\"height\";i:220;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(154, 60, '_wp_attached_file', '2016/05/banner.png'),
(155, 60, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1220;s:6:\"height\";i:275;s:4:\"file\";s:18:\"2016/05/banner.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"banner-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"banner-300x68.png\";s:5:\"width\";i:300;s:6:\"height\";i:68;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"banner-768x173.png\";s:5:\"width\";i:768;s:6:\"height\";i:173;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"banner-1024x231.png\";s:5:\"width\";i:1024;s:6:\"height\";i:231;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:18:\"banner-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(156, 61, '_form', '<div class=\"form-group\">\n    <label for=\"nome\" class=\"hidden\">*Nome completo</label>\n    [text*  nome id:nome placeholder \"Nome completo*\"]\n</div>\n\n<div class=\"form-group\">\n    <label for=\"email\" class=\"hidden\">*E-mail</label>\n    [email* email id:email placeholder \"E-mail*\" ]\n</div>\n\n<div class=\"form-group\">\n    <label for=\"tel\" class=\"hidden\">*Telefone</label>\n    [tel  tel id:tel placeholder \"Telefone*\"]\n</div>\n\n<div class=\"form-group mesagem\" style=\"border: none;\">\n    <label for=\"mensagem\" class=\"hidden\">*Mensagem</label>\n    [textarea*  mensagem id:mensagem placeholder \"Mensagem*\"]\n</div>\n\n[submit class:botao \"Enviar\"]'),
(157, 61, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:66:\"Mensagem enviada pelo formulario de contato do site 3B Aquecedores\";s:6:\"sender\";s:36:\"[nome] <3baquecedores@palupa.com.br>\";s:9:\"recipient\";s:27:\"3baquecedores@palupa.com.br\";s:4:\"body\";s:71:\"De: [nome] <[email]>\nTelefone: [tel]\n\nCorpo da mensagem:\n[mensagem]\n\n--\";s:18:\"additional_headers\";s:17:\"Reply-To: [email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(158, 61, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:31:\"3B Aquecedores \"[your-subject]\"\";s:6:\"sender\";s:44:\"3B Aquecedores <3baquecedores@palupa.com.br>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:143:\"Corpo da mensagem:\n[your-message]\n\n--\nEste e-mail foi enviado de um formulário de contato em 3B Aquecedores (http://3baquecedores.pixd.com.br)\";s:18:\"additional_headers\";s:37:\"Reply-To: 3baquecedores@palupa.com.br\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(159, 61, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:44:\"Obrigado pela sua mensagem. Ela foi enviada.\";s:12:\"mail_sent_ng\";s:85:\"Houve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde.\";s:16:\"validation_error\";s:70:\"Um ou mais campos têm um erro. Por favor verifique e tente novamente.\";s:4:\"spam\";s:86:\"Houve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde..\";s:12:\"accept_terms\";s:74:\"Você deve aceitar os termos e condições antes de enviar a sua mensagem.\";s:16:\"invalid_required\";s:25:\"O campo é obrigatório .\";s:16:\"invalid_too_long\";s:23:\"O campo é muito longo.\";s:17:\"invalid_too_short\";s:23:\"O campo é muito curto.\";s:12:\"invalid_date\";s:31:\"O formato da data é incorreta.\";s:14:\"date_too_early\";s:41:\"A data é antes da primeira um permitido.\";s:13:\"date_too_late\";s:41:\"A data é após o mais recente permitida.\";s:13:\"upload_failed\";s:46:\"Houve um erro desconhecido upload do arquivo .\";s:24:\"upload_file_type_invalid\";s:67:\"Você não tem permissão para fazer upload de arquivos deste tipo.\";s:21:\"upload_file_too_large\";s:26:\"O arquivo é muito grande.\";s:23:\"upload_failed_php_error\";s:45:\"Ocorreu um erro ao fazer o upload do arquivo.\";s:14:\"invalid_number\";s:34:\"O formato de número é inválido.\";s:16:\"number_too_small\";s:47:\"O número é menor do que o mínimo permitido .\";s:16:\"number_too_large\";s:47:\"O número é maior do que o máximo permitido .\";s:23:\"quiz_answer_not_correct\";s:44:\"A resposta ao questionário é incorrecto ..\";s:17:\"captcha_not_match\";s:35:\"O código digitado está incorreto.\";s:13:\"invalid_email\";s:45:\"O endereço de e -mail digitado é inválido.\";s:11:\"invalid_url\";s:19:\"A URL é inválido.\";s:11:\"invalid_tel\";s:35:\"O número de telefone é inválido.\";}'),
(160, 61, '_additional_settings', ''),
(161, 61, '_locale', 'pt_BR'),
(162, 62, '_edit_last', '1'),
(163, 62, '_edit_lock', '1463057128:1'),
(164, 62, '_wp_page_template', 'template-pages/contato.php'),
(165, 64, '_edit_last', '1'),
(166, 64, '_edit_lock', '1504532076:1'),
(167, 64, '_wp_page_template', 'template-pages/inicial.php'),
(168, 66, '_edit_last', '1'),
(169, 66, '_edit_lock', '1505307410:1'),
(170, 66, '_wp_page_template', 'template-pages/quem-somos.php'),
(171, 68, '_wp_attached_file', '2016/05/arrows-1.png'),
(172, 68, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:20:\"2016/05/arrows-1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"arrows-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"arrows-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(173, 69, '_wp_attached_file', '2016/05/banner-1.png'),
(174, 69, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1220;s:6:\"height\";i:275;s:4:\"file\";s:20:\"2016/05/banner-1.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"banner-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner-1-300x68.png\";s:5:\"width\";i:300;s:6:\"height\";i:68;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"banner-1-768x173.png\";s:5:\"width\";i:768;s:6:\"height\";i:173;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"banner-1-1024x231.png\";s:5:\"width\";i:1024;s:6:\"height\";i:231;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:20:\"banner-1-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(175, 70, '_wp_attached_file', '2016/05/bg-assistencia.png'),
(176, 70, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1220;s:6:\"height\";i:393;s:4:\"file\";s:26:\"2016/05/bg-assistencia.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"bg-assistencia-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"bg-assistencia-300x97.png\";s:5:\"width\";i:300;s:6:\"height\";i:97;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"bg-assistencia-768x247.png\";s:5:\"width\";i:768;s:6:\"height\";i:247;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"bg-assistencia-1024x330.png\";s:5:\"width\";i:1024;s:6:\"height\";i:330;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:26:\"bg-assistencia-600x193.png\";s:5:\"width\";i:600;s:6:\"height\";i:193;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(177, 71, '_wp_attached_file', '2016/05/bg-depoimentos.png'),
(178, 71, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1220;s:6:\"height\";i:327;s:4:\"file\";s:26:\"2016/05/bg-depoimentos.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"bg-depoimentos-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"bg-depoimentos-300x80.png\";s:5:\"width\";i:300;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"bg-depoimentos-768x206.png\";s:5:\"width\";i:768;s:6:\"height\";i:206;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"bg-depoimentos-1024x274.png\";s:5:\"width\";i:1024;s:6:\"height\";i:274;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:26:\"bg-depoimentos-600x161.png\";s:5:\"width\";i:600;s:6:\"height\";i:161;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(179, 72, '_wp_attached_file', '2016/05/bg-rodape.png'),
(180, 72, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1220;s:6:\"height\";i:259;s:4:\"file\";s:21:\"2016/05/bg-rodape.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"bg-rodape-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"bg-rodape-300x64.png\";s:5:\"width\";i:300;s:6:\"height\";i:64;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"bg-rodape-768x163.png\";s:5:\"width\";i:768;s:6:\"height\";i:163;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"bg-rodape-1024x217.png\";s:5:\"width\";i:1024;s:6:\"height\";i:217;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:21:\"bg-rodape-600x127.png\";s:5:\"width\";i:600;s:6:\"height\";i:127;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(181, 73, '_wp_attached_file', '2016/05/blog1.png'),
(182, 73, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:299;s:6:\"height\";i:191;s:4:\"file\";s:17:\"2016/05/blog1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"blog1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(183, 74, '_wp_attached_file', '2016/05/blog2.png'),
(184, 74, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:299;s:6:\"height\";i:191;s:4:\"file\";s:17:\"2016/05/blog2.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"blog2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(185, 75, '_wp_attached_file', '2016/05/blog3.png'),
(186, 75, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:299;s:6:\"height\";i:191;s:4:\"file\";s:17:\"2016/05/blog3.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"blog3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(187, 76, '_wp_attached_file', '2016/05/contato1.jpg'),
(188, 76, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2000;s:6:\"height\";i:1333;s:4:\"file\";s:20:\"2016/05/contato1.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"contato1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"contato1-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"contato1-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"contato1-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:20:\"contato1-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(189, 77, '_wp_attached_file', '2016/05/depoimento.png'),
(190, 77, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1220;s:6:\"height\";i:327;s:4:\"file\";s:22:\"2016/05/depoimento.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"depoimento-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"depoimento-300x80.png\";s:5:\"width\";i:300;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"depoimento-768x206.png\";s:5:\"width\";i:768;s:6:\"height\";i:206;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"depoimento-1024x274.png\";s:5:\"width\";i:1024;s:6:\"height\";i:274;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:22:\"depoimento-600x161.png\";s:5:\"width\";i:600;s:6:\"height\";i:161;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(191, 78, '_wp_attached_file', '2016/05/depoimento2.png'),
(192, 78, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1220;s:6:\"height\";i:327;s:4:\"file\";s:23:\"2016/05/depoimento2.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"depoimento2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"depoimento2-300x80.png\";s:5:\"width\";i:300;s:6:\"height\";i:80;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"depoimento2-768x206.png\";s:5:\"width\";i:768;s:6:\"height\";i:206;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"depoimento2-1024x274.png\";s:5:\"width\";i:1024;s:6:\"height\";i:274;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:23:\"depoimento2-600x161.png\";s:5:\"width\";i:600;s:6:\"height\";i:161;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(193, 79, '_wp_attached_file', '2016/05/foto1.png'),
(194, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:406;s:6:\"height\";i:377;s:4:\"file\";s:17:\"2016/05/foto1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"foto1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"foto1-300x279.png\";s:5:\"width\";i:300;s:6:\"height\";i:279;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(195, 80, '_wp_attached_file', '2016/05/foto2.png'),
(196, 80, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:406;s:6:\"height\";i:377;s:4:\"file\";s:17:\"2016/05/foto2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"foto2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"foto2-300x279.png\";s:5:\"width\";i:300;s:6:\"height\";i:279;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(197, 81, '_wp_attached_file', '2016/05/foto3.png'),
(198, 81, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:406;s:6:\"height\";i:377;s:4:\"file\";s:17:\"2016/05/foto3.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"foto3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"foto3-300x279.png\";s:5:\"width\";i:300;s:6:\"height\";i:279;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(199, 82, '_wp_attached_file', '2016/05/foto-blog.png'),
(200, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:700;s:6:\"height\";i:313;s:4:\"file\";s:21:\"2016/05/foto-blog.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"foto-blog-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"foto-blog-300x134.png\";s:5:\"width\";i:300;s:6:\"height\";i:134;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:21:\"foto-blog-600x268.png\";s:5:\"width\";i:600;s:6:\"height\";i:268;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(201, 83, '_wp_attached_file', '2016/05/foto-destaque-1.png'),
(202, 83, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1220;s:6:\"height\";i:785;s:4:\"file\";s:27:\"2016/05/foto-destaque-1.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"foto-destaque-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"foto-destaque-1-300x193.png\";s:5:\"width\";i:300;s:6:\"height\";i:193;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"foto-destaque-1-768x494.png\";s:5:\"width\";i:768;s:6:\"height\";i:494;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"foto-destaque-1-1024x659.png\";s:5:\"width\";i:1024;s:6:\"height\";i:659;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:27:\"foto-destaque-1-600x386.png\";s:5:\"width\";i:600;s:6:\"height\";i:386;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(203, 84, '_wp_attached_file', '2016/05/foto-produto-1.png'),
(204, 84, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:26:\"2016/05/foto-produto-1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"foto-produto-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"foto-produto-1-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(205, 85, '_wp_attached_file', '2016/05/foto-que-fazemos-1.png'),
(206, 85, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:609;s:6:\"height\";i:361;s:4:\"file\";s:30:\"2016/05/foto-que-fazemos-1.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"foto-que-fazemos-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"foto-que-fazemos-1-300x178.png\";s:5:\"width\";i:300;s:6:\"height\";i:178;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:30:\"foto-que-fazemos-1-600x356.png\";s:5:\"width\";i:600;s:6:\"height\";i:356;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(207, 86, '_wp_attached_file', '2016/05/foto-quemsomos.png'),
(208, 86, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1129;s:6:\"height\";i:393;s:4:\"file\";s:26:\"2016/05/foto-quemsomos.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"foto-quemsomos-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"foto-quemsomos-300x104.png\";s:5:\"width\";i:300;s:6:\"height\";i:104;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"foto-quemsomos-768x267.png\";s:5:\"width\";i:768;s:6:\"height\";i:267;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"foto-quemsomos-1024x356.png\";s:5:\"width\";i:1024;s:6:\"height\";i:356;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:26:\"foto-quemsomos-600x209.png\";s:5:\"width\";i:600;s:6:\"height\";i:209;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(209, 87, '_wp_attached_file', '2016/05/fotos1-2.png'),
(210, 87, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:305;s:6:\"height\";i:223;s:4:\"file\";s:20:\"2016/05/fotos1-2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"fotos1-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"fotos1-2-300x219.png\";s:5:\"width\";i:300;s:6:\"height\";i:219;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(211, 88, '_wp_attached_file', '2016/05/fotos2-2.png'),
(212, 88, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:304;s:6:\"height\";i:223;s:4:\"file\";s:20:\"2016/05/fotos2-2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"fotos2-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"fotos2-2-300x220.png\";s:5:\"width\";i:300;s:6:\"height\";i:220;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(213, 89, '_wp_attached_file', '2016/05/fotos3-2.png'),
(214, 89, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:304;s:6:\"height\";i:223;s:4:\"file\";s:20:\"2016/05/fotos3-2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"fotos3-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"fotos3-2-300x220.png\";s:5:\"width\";i:300;s:6:\"height\";i:220;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(215, 90, '_wp_attached_file', '2016/05/fotos4-2.png'),
(216, 90, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:304;s:6:\"height\";i:223;s:4:\"file\";s:20:\"2016/05/fotos4-2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"fotos4-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"fotos4-2-300x220.png\";s:5:\"width\";i:300;s:6:\"height\";i:220;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(217, 91, '_wp_attached_file', '2016/05/fundo-rodape.png'),
(218, 91, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1220;s:6:\"height\";i:550;s:4:\"file\";s:24:\"2016/05/fundo-rodape.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"fundo-rodape-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"fundo-rodape-300x135.png\";s:5:\"width\";i:300;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"fundo-rodape-768x346.png\";s:5:\"width\";i:768;s:6:\"height\";i:346;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:25:\"fundo-rodape-1024x462.png\";s:5:\"width\";i:1024;s:6:\"height\";i:462;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:24:\"fundo-rodape-600x270.png\";s:5:\"width\";i:600;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(219, 92, '_wp_attached_file', '2016/05/hover.png');
INSERT INTO `3b_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(220, 92, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:71;s:6:\"height\";i:70;s:4:\"file\";s:17:\"2016/05/hover.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(221, 93, '_wp_attached_file', '2016/05/logo1-1.png'),
(222, 93, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:218;s:6:\"height\";i:182;s:4:\"file\";s:19:\"2016/05/logo1-1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"logo1-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(223, 94, '_wp_attached_file', '2016/05/logo2-1.png'),
(224, 94, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:217;s:6:\"height\";i:182;s:4:\"file\";s:19:\"2016/05/logo2-1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"logo2-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(225, 95, '_wp_attached_file', '2016/05/logo3-1.png'),
(226, 95, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:218;s:6:\"height\";i:182;s:4:\"file\";s:19:\"2016/05/logo3-1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"logo3-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(227, 96, '_wp_attached_file', '2016/05/logo3b.png'),
(228, 96, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:256;s:6:\"height\";i:326;s:4:\"file\";s:18:\"2016/05/logo3b.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"logo3b-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"logo3b-236x300.png\";s:5:\"width\";i:236;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(229, 97, '_wp_attached_file', '2016/05/logo4-1.png'),
(230, 97, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:218;s:6:\"height\";i:182;s:4:\"file\";s:19:\"2016/05/logo4-1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"logo4-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(231, 98, '_wp_attached_file', '2016/05/logo-contato.png'),
(232, 98, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:191;s:6:\"height\";i:218;s:4:\"file\";s:24:\"2016/05/logo-contato.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"logo-contato-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(233, 99, '_wp_attached_file', '2016/05/logo-marca.png'),
(234, 99, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:130;s:6:\"height\";i:165;s:4:\"file\";s:22:\"2016/05/logo-marca.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"logo-marca-130x150.png\";s:5:\"width\";i:130;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(235, 100, '_wp_attached_file', '2016/05/logo-rodape.png'),
(236, 100, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:251;s:6:\"height\";i:272;s:4:\"file\";s:23:\"2016/05/logo-rodape.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"logo-rodape-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(237, 101, '_wp_attached_file', '2016/05/marketing.png'),
(238, 101, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:21:\"2016/05/marketing.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"marketing-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"marketing-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(239, 66, '_thumbnail_id', '86'),
(240, 102, '_wp_attached_file', '2016/05/miniatura.png'),
(241, 102, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:21:\"2016/05/miniatura.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(242, 103, '_wp_attached_file', '2016/05/miniatura2.png'),
(243, 103, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:22:\"2016/05/miniatura2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(244, 104, '_wp_attached_file', '2016/05/money-increase.png'),
(245, 104, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:26:\"2016/05/money-increase.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"money-increase-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"money-increase-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(246, 106, '_edit_last', '1'),
(247, 106, '_edit_lock', '1463063778:1'),
(253, 109, '_edit_last', '1'),
(254, 109, '_edit_lock', '1463063898:1'),
(255, 109, '_thumbnail_id', '74'),
(257, 109, '_yoast_wpseo_primary_category', '2'),
(258, 111, '_edit_last', '1'),
(259, 111, '_edit_lock', '1463065941:1'),
(260, 111, '_thumbnail_id', '73'),
(262, 111, '_yoast_wpseo_primary_category', '3'),
(263, 113, '_edit_last', '1'),
(264, 113, '_edit_lock', '1463065951:1'),
(265, 113, '_thumbnail_id', '75'),
(267, 113, '_yoast_wpseo_primary_category', '4'),
(270, 117, '_wp_old_slug', 'necaucibus-ipsum-pulvinar-atin-tincidunts-3'),
(271, 117, '_edit_last', '1'),
(272, 117, '_edit_lock', '1463065949:1'),
(273, 117, '_thumbnail_id', '75'),
(274, 117, '_yoast_wpseo_primary_category', '4'),
(275, 117, '_dp_original', '113'),
(278, 119, '_edit_last', '1'),
(279, 119, '_edit_lock', '1463065952:1'),
(280, 119, '_thumbnail_id', '73'),
(281, 119, '_yoast_wpseo_primary_category', '3'),
(282, 119, '_dp_original', '111'),
(285, 121, '_edit_last', '1'),
(286, 121, '_edit_lock', '1463065945:1'),
(287, 121, '_thumbnail_id', '74'),
(288, 121, '_yoast_wpseo_primary_category', '2'),
(289, 121, '_dp_original', '109'),
(292, 123, '_edit_last', '1'),
(293, 123, '_edit_lock', '1463065939:1'),
(294, 123, '_thumbnail_id', '74'),
(295, 123, '_yoast_wpseo_primary_category', '2'),
(296, 123, '_dp_original', '109'),
(299, 125, '_edit_last', '1'),
(300, 125, '_edit_lock', '1463065940:1'),
(301, 125, '_thumbnail_id', '74'),
(302, 125, '_yoast_wpseo_primary_category', '2'),
(303, 125, '_dp_original', '109'),
(306, 127, '_edit_last', '1'),
(307, 127, '_edit_lock', '1463073376:1'),
(308, 127, '_thumbnail_id', '73'),
(309, 127, '_yoast_wpseo_primary_category', '3'),
(310, 127, '_dp_original', '111'),
(313, 129, '_wp_old_slug', 'necaucibus-ipsum-pulvinar-atin-tincidunts-5'),
(314, 129, '_wp_old_slug', 'necaucibus-ipsum-pulvinar-atin-tincidunts-3'),
(315, 129, '_edit_last', '1'),
(316, 129, '_edit_lock', '1463065950:1'),
(317, 129, '_thumbnail_id', '75'),
(318, 129, '_yoast_wpseo_primary_category', '4'),
(320, 129, '_dp_original', '117'),
(323, 131, '_wp_old_slug', 'aquecedores-pequenos-para-uso-em-casa-5'),
(324, 131, '_edit_last', '1'),
(325, 131, '_edit_lock', '1463063898:1'),
(326, 131, '_thumbnail_id', '74'),
(327, 131, '_yoast_wpseo_primary_category', '2'),
(329, 131, '_dp_original', '121'),
(332, 133, '_wp_old_slug', 'necaucibus-ipsum-pulvinar-atin-tincidunts-5'),
(333, 133, '_edit_last', '1'),
(334, 133, '_edit_lock', '1463065947:1'),
(335, 133, '_thumbnail_id', '73'),
(336, 133, '_yoast_wpseo_primary_category', '3'),
(338, 133, '_dp_original', '127'),
(341, 135, '_edit_last', '1'),
(342, 135, '_edit_lock', '1463065953:1'),
(343, 135, '_thumbnail_id', '73'),
(344, 135, '_yoast_wpseo_primary_category', '3'),
(345, 135, '_dp_original', '111'),
(348, 137, '_wp_old_slug', 'necaucibus-ipsum-pulvinar-atin-tincidunts-6'),
(349, 137, '_wp_old_slug', 'necaucibus-ipsum-pulvinar-atin-tincidunts-5'),
(350, 137, '_wp_old_slug', 'necaucibus-ipsum-pulvinar-atin-tincidunts-3'),
(351, 137, '_edit_last', '1'),
(352, 137, '_edit_lock', '1463065942:1'),
(353, 137, '_thumbnail_id', '75'),
(354, 137, '_yoast_wpseo_primary_category', '4'),
(356, 137, '_dp_original', '129'),
(357, 29, 'TresB_destaque_link', '#'),
(358, 27, 'TresB_destaque_link', '#'),
(359, 29, 'TresB_destaque_descricao', 'Banho frio nunca mais.'),
(360, 27, 'TresB_destaque_descricao', 'Escolha o melhor clima para o seu banho.'),
(361, 43, 'TresB_depoimento_texto', 'Quisque non risus convallis congue tellus non lobortis velit suspendisse potenti liquam faucibus justo eu sodales vestibulum nunc odio dapibus.'),
(362, 43, 'TresB_depoimento_autor', 'Magna sitamet imperd'),
(363, 43, 'TresB_depoimento_autor_info', 'Gerencia nacional/PR'),
(377, 41, 'TresB_depoimento_texto', 'Quisque non risus convallis congue tellus non lobortis velit suspendisse potenti liquam faucibus justo eu sodales vestibulum nunc odio dapibus.'),
(378, 40, 'TresB_depoimento_texto', 'Quisque non risus convallis congue tellus non lobortis velit suspendisse potenti liquam faucibus justo eu sodales vestibulum nunc odio dapibus.'),
(379, 39, 'TresB_depoimento_texto', 'Quisque non risus convallis congue tellus non lobortis velit suspendisse potenti liquam faucibus justo eu sodales vestibulum nunc odio dapibus.'),
(380, 41, 'TresB_depoimento_autor', 'Magna sitamet imperd'),
(381, 40, 'TresB_depoimento_autor', 'Magna sitamet imperd'),
(382, 39, 'TresB_depoimento_autor', 'Magna sitamet imperd'),
(383, 41, 'TresB_depoimento_autor_info', 'Gerencia nacional/PR'),
(384, 40, 'TresB_depoimento_autor_info', 'Gerencia nacional/PR'),
(385, 39, 'TresB_depoimento_autor_info', 'Gerencia nacional/PR'),
(386, 27, '_thumbnail_id', '350'),
(392, 153, '_wp_old_slug', 'joao'),
(393, 153, '_thumbnail_id', '28'),
(394, 153, '_edit_last', '1'),
(395, 153, '3B_depoimento_texto', 'O aquecedor é ótimo! Tem vazão para um banho perfeito, é bastante compacto (ocupa espaço bem menor que meu antigo aparelho). Entrega mais que perfeito, muitos dias antes do prazo!!'),
(396, 153, '3B_depoimento_autor', 'João'),
(397, 153, '3B_depoimento_autor_info', 'Cliente'),
(398, 153, '_edit_lock', '1463069953:1'),
(399, 153, 'TresB_depoimento_texto', 'Quisque non risus convallis congue tellus non lobortis velit suspendisse potenti liquam faucibus justo eu sodales vestibulum nunc odio dapibus.'),
(400, 153, 'TresB_depoimento_autor', 'Magna sitamet imperd'),
(401, 153, 'TresB_depoimento_autor_info', 'Gerencia nacional/PR'),
(402, 153, '_dp_original', '39'),
(403, 154, '_thumbnail_id', '28'),
(404, 154, '_edit_last', '1'),
(405, 154, '3B_depoimento_texto', 'O aquecedor é ótimo! Tem vazão para um banho perfeito, é bastante compacto (ocupa espaço bem menor que meu antigo aparelho). Entrega mais que perfeito, muitos dias antes do prazo!!'),
(406, 154, '3B_depoimento_autor', 'João'),
(407, 154, '3B_depoimento_autor_info', 'Cliente'),
(408, 154, '_edit_lock', '1463406335:1'),
(410, 154, 'TresB_depoimento_texto', 'Quisque non risus convallis congue tellus non lobortis velit suspendisse potenti liquam faucibus justo eu sodales vestibulum nunc odio dapibus.'),
(411, 154, 'TresB_depoimento_autor', 'Magna sitamet imperd'),
(412, 154, 'TresB_depoimento_autor_info', 'Gerencia nacional/PR'),
(413, 154, '_dp_original', '41'),
(414, 155, '_edit_last', '1'),
(415, 155, '_edit_lock', '1504623293:1'),
(416, 155, '_thumbnail_id', '70'),
(417, 155, '_wp_page_template', 'template-pages/assistencia-tecnica.php'),
(418, 158, '_form', '<div class=\"form-group\"><label class=\"hidden\" for=\"nome\">Nome</label>[text* Nome id:Nome class:form-control placeholder \"*Nome\"]</div><div class=\"form-group\"><label class=\"hidden\" for=\"nome\">Nome</label>[email* email id:email class:form-control akismet:author_email \"*E-mail\"]</div><div class=\"form-group\"><label class=\"hidden\" for=\"nome\">Nome</label>[text* Empresa id:Empresa class:form-control placeholder \"*Empresa\"]</div><div class=\"form-group\"><label class=\"hidden\" for=\"nome\">Nome</label>[tel* Telefone id:Telefone class:form-control placeholder \"*Telefone\"]</div>\n<div class=\"form-group\"><label class=\"hidden\" for=\"nome\">Nome</label>[text Cidade id:Cidade class:form-control placeholder \"Cidade\"]</div><div class=\"form-group\"><label class=\"hidden\" for=\"nome\">Nome</label>[text CEP id:CEP class:form-control placeholder \"CEP\"]</div>\n<div class=\"form-group-mensagem\"><label class=\"\" for=\"nome\">*Descrição</label>[textarea* Mensagem id:Mensagem class:Mensagem ]</div>[submit id:enviar class:enviar \"Enviar\"]'),
(419, 158, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:80:\"Mensagem enviada pelo formulario de Assistência técnica do site 3B Aquecedores\";s:6:\"sender\";s:36:\"[Nome] <3baquecedores@palupa.com.br>\";s:9:\"recipient\";s:27:\"3baquecedores@palupa.com.br\";s:4:\"body\";s:241:\"De:[Nome]\nEmail:[email]\nEmpresa:[Empresa]\nTelefone:[Telefone]\nCidade[Cidade]\nCep:[CEP]\nProdução/Solução:[ProduoSoluo]\n\nCorpo da mensagem:\n[Mensagem]\n\n--\nEste e-mail foi enviado de um formulário de Assistência técnica em 3B Aquecedores\";s:18:\"additional_headers\";s:17:\"Reply-To: [email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(420, 158, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:31:\"3B Aquecedores \"[your-subject]\"\";s:6:\"sender\";s:44:\"3B Aquecedores <3baquecedores@palupa.com.br>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:143:\"Corpo da mensagem:\n[your-message]\n\n--\nEste e-mail foi enviado de um formulário de contato em 3B Aquecedores (http://3baquecedores.pixd.com.br)\";s:18:\"additional_headers\";s:37:\"Reply-To: 3baquecedores@palupa.com.br\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(421, 158, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:44:\"Obrigado pela sua mensagem. Ela foi enviada.\";s:12:\"mail_sent_ng\";s:85:\"Houve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde.\";s:16:\"validation_error\";s:70:\"Um ou mais campos têm um erro. Por favor verifique e tente novamente.\";s:4:\"spam\";s:86:\"Houve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde..\";s:12:\"accept_terms\";s:74:\"Você deve aceitar os termos e condições antes de enviar a sua mensagem.\";s:16:\"invalid_required\";s:25:\"O campo é obrigatório .\";s:16:\"invalid_too_long\";s:23:\"O campo é muito longo.\";s:17:\"invalid_too_short\";s:23:\"O campo é muito curto.\";s:12:\"invalid_date\";s:31:\"O formato da data é incorreta.\";s:14:\"date_too_early\";s:41:\"A data é antes da primeira um permitido.\";s:13:\"date_too_late\";s:41:\"A data é após o mais recente permitida.\";s:13:\"upload_failed\";s:46:\"Houve um erro desconhecido upload do arquivo .\";s:24:\"upload_file_type_invalid\";s:67:\"Você não tem permissão para fazer upload de arquivos deste tipo.\";s:21:\"upload_file_too_large\";s:26:\"O arquivo é muito grande.\";s:23:\"upload_failed_php_error\";s:45:\"Ocorreu um erro ao fazer o upload do arquivo.\";s:14:\"invalid_number\";s:34:\"O formato de número é inválido.\";s:16:\"number_too_small\";s:47:\"O número é menor do que o mínimo permitido .\";s:16:\"number_too_large\";s:47:\"O número é maior do que o máximo permitido .\";s:23:\"quiz_answer_not_correct\";s:44:\"A resposta ao questionário é incorrecto ..\";s:17:\"captcha_not_match\";s:35:\"O código digitado está incorreto.\";s:13:\"invalid_email\";s:45:\"O endereço de e -mail digitado é inválido.\";s:11:\"invalid_url\";s:19:\"A URL é inválido.\";s:11:\"invalid_tel\";s:0:\"\";}'),
(422, 158, '_additional_settings', ''),
(423, 158, '_locale', 'pt_BR'),
(438, 160, '_wp_attached_file', '2016/05/miniatura-1.png'),
(439, 160, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(440, 161, '_wp_attached_file', '2016/05/miniatura2-1.png'),
(441, 161, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(442, 162, '_wp_attached_file', '2016/05/miniatura-2.png'),
(443, 162, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(477, 165, '_wp_attached_file', '2016/05/foto-produto-2.png'),
(478, 165, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:26:\"2016/05/foto-produto-2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(480, 166, '_wp_attached_file', '2016/05/miniatura2-2.png'),
(481, 166, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(482, 167, '_wp_attached_file', '2016/05/miniatura-3.png'),
(483, 167, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(486, 168, '_wp_attached_file', '2016/05/miniatura-4.png'),
(487, 168, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(492, 169, '_wp_attached_file', '2016/05/miniatura-5.png'),
(493, 169, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-5.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(494, 170, '_wp_attached_file', '2016/05/miniatura2-3.png'),
(495, 170, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(496, 171, '_wp_attached_file', '2016/05/miniatura-6.png'),
(497, 171, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-6.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(526, 173, '_wp_attached_file', '2016/05/miniatura-2.png'),
(527, 173, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(528, 173, '_dp_original', '162'),
(529, 174, '_wp_attached_file', '2016/05/miniatura-1.png'),
(530, 174, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(531, 174, '_dp_original', '160'),
(532, 175, '_wp_attached_file', '2016/05/miniatura2-1.png'),
(533, 175, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(534, 175, '_dp_original', '161'),
(535, 176, '_wp_attached_file', '2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg'),
(536, 176, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:344;s:6:\"height\";i:344;s:4:\"file\";s:83:\"2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:2:\"Ð\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:4:\"8144\";s:8:\"keywords\";a:0:{}}}'),
(537, 176, '_dp_original', '37'),
(538, 178, '_wp_attached_file', '2016/05/aquecedor-de-agua-a-gas.jpg'),
(539, 178, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:130;s:6:\"height\";i:130;s:4:\"file\";s:35:\"2016/05/aquecedor-de-agua-a-gas.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(540, 178, '_dp_original', '38'),
(567, 180, '_wp_attached_file', '2016/05/miniatura-4.png'),
(568, 180, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(569, 180, '_dp_original', '168'),
(571, 181, '_wp_attached_file', '2016/05/miniatura-3.png'),
(572, 181, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(573, 181, '_dp_original', '167'),
(576, 182, '_wp_attached_file', '2016/05/miniatura2-2.png'),
(578, 182, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(580, 182, '_dp_original', '166'),
(586, 183, '_wp_attached_file', '2016/05/foto-produto-2.png'),
(588, 183, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:26:\"2016/05/foto-produto-2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(590, 183, '_dp_original', '165'),
(604, 184, '_wp_attached_file', '2016/05/miniatura2-3.png'),
(605, 184, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(606, 184, '_dp_original', '170'),
(607, 185, '_wp_attached_file', '2016/05/miniatura-6.png'),
(608, 185, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-6.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(609, 185, '_dp_original', '171'),
(610, 186, '_wp_attached_file', '2016/05/miniatura-5.png'),
(611, 186, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-5.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(612, 186, '_dp_original', '169'),
(613, 187, '_wp_attached_file', '2016/05/produto-carrossel.png'),
(614, 187, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:153;s:6:\"height\";i:224;s:4:\"file\";s:29:\"2016/05/produto-carrossel.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"produto-carrossel-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(615, 187, '_dp_original', '33'),
(616, 188, '_wp_attached_file', '2016/05/foto-produto.png'),
(617, 188, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:24:\"2016/05/foto-produto.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"foto-produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"foto-produto-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(618, 188, '_edit_lock', '1462971355:1'),
(619, 188, '_dp_original', '34'),
(620, 189, '_wp_attached_file', '2016/05/produto.png'),
(621, 189, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:192;s:6:\"height\";i:294;s:4:\"file\";s:19:\"2016/05/produto.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(622, 189, '_dp_original', '32'),
(650, 192, '_wp_attached_file', '2016/05/foto-produto.png'),
(651, 192, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:24:\"2016/05/foto-produto.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"foto-produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"foto-produto-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(652, 192, '_edit_lock', '1462971355:1'),
(654, 192, '_dp_original', '188'),
(656, 193, '_wp_attached_file', '2016/05/produto.png'),
(657, 193, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:192;s:6:\"height\";i:294;s:4:\"file\";s:19:\"2016/05/produto.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(661, 193, '_dp_original', '189'),
(668, 194, '_wp_attached_file', '2016/05/miniatura2-3.png'),
(670, 194, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(675, 194, '_dp_original', '184'),
(681, 195, '_wp_attached_file', '2016/05/miniatura-6.png'),
(683, 195, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-6.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(688, 195, '_dp_original', '185'),
(693, 197, '_wp_attached_file', '2016/05/miniatura-5.png'),
(694, 197, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-5.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(697, 197, '_dp_original', '186'),
(698, 198, '_wp_attached_file', '2016/05/foto-produto-2.png'),
(699, 198, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:26:\"2016/05/foto-produto-2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(701, 198, '_dp_original', '183'),
(702, 199, '_wp_attached_file', '2016/05/produto-carrossel.png'),
(704, 199, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:153;s:6:\"height\";i:224;s:4:\"file\";s:29:\"2016/05/produto-carrossel.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"produto-carrossel-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(709, 199, '_dp_original', '187'),
(711, 200, '_wp_attached_file', '2016/05/miniatura-4.png'),
(714, 200, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(719, 200, '_dp_original', '180'),
(726, 201, '_wp_attached_file', '2016/05/miniatura-3.png'),
(728, 201, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(733, 201, '_dp_original', '181'),
(740, 203, '_wp_attached_file', '2016/05/miniatura2-2.png'),
(741, 203, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(744, 203, '_dp_original', '182'),
(745, 204, '_wp_attached_file', '2016/05/miniatura-2.png'),
(747, 204, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(750, 204, '_dp_original', '173'),
(756, 205, '_wp_attached_file', '2016/05/miniatura-1.png'),
(758, 205, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(763, 205, '_dp_original', '174'),
(770, 207, '_wp_attached_file', '2016/05/miniatura2-1.png'),
(772, 207, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(777, 207, '_dp_original', '175'),
(783, 208, '_wp_attached_file', '2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg'),
(784, 208, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:344;s:6:\"height\";i:344;s:4:\"file\";s:83:\"2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:2:\"Ð\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:4:\"8144\";s:8:\"keywords\";a:0:{}}}'),
(786, 209, '_wp_attached_file', '2016/05/miniatura-2.png'),
(787, 208, '_dp_original', '176'),
(788, 209, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(790, 209, '_dp_original', '162'),
(795, 210, '_wp_attached_file', '2016/05/aquecedor-de-agua-a-gas.jpg'),
(797, 210, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:130;s:6:\"height\";i:130;s:4:\"file\";s:35:\"2016/05/aquecedor-de-agua-a-gas.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(799, 211, '_wp_attached_file', '2016/05/miniatura-1.png'),
(802, 211, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(804, 211, '_dp_original', '160'),
(805, 210, '_dp_original', '178'),
(812, 212, '_wp_attached_file', '2016/05/miniatura2-1.png'),
(814, 212, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(816, 212, '_dp_original', '161'),
(822, 213, '_wp_attached_file', '2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg'),
(824, 213, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:344;s:6:\"height\";i:344;s:4:\"file\";s:83:\"2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:2:\"Ð\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:4:\"8144\";s:8:\"keywords\";a:0:{}}}'),
(826, 213, '_dp_original', '37'),
(828, 215, '_wp_attached_file', '2016/05/aquecedor-de-agua-a-gas.jpg'),
(829, 216, '_wp_attached_file', '2016/05/miniatura-4.png');
INSERT INTO `3b_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(830, 215, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:130;s:6:\"height\";i:130;s:4:\"file\";s:35:\"2016/05/aquecedor-de-agua-a-gas.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(831, 216, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(833, 215, '_dp_original', '38'),
(834, 216, '_dp_original', '168'),
(836, 217, '_wp_attached_file', '2016/05/miniatura-3.png'),
(838, 217, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(840, 217, '_dp_original', '167'),
(846, 218, '_wp_attached_file', '2016/05/miniatura2-2.png'),
(848, 218, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(850, 218, '_dp_original', '166'),
(857, 219, '_wp_attached_file', '2016/05/foto-produto-2.png'),
(859, 219, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:26:\"2016/05/foto-produto-2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(861, 219, '_dp_original', '165'),
(869, 220, '_wp_attached_file', '2016/05/miniatura2-3.png'),
(870, 220, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(871, 220, '_dp_original', '170'),
(872, 221, '_wp_attached_file', '2016/05/miniatura-6.png'),
(873, 221, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-6.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(874, 221, '_dp_original', '171'),
(875, 223, '_wp_attached_file', '2016/05/miniatura-5.png'),
(876, 223, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-5.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(877, 223, '_dp_original', '169'),
(878, 224, '_wp_attached_file', '2016/05/produto-carrossel.png'),
(880, 224, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:153;s:6:\"height\";i:224;s:4:\"file\";s:29:\"2016/05/produto-carrossel.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"produto-carrossel-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(881, 224, '_dp_original', '33'),
(882, 225, '_wp_attached_file', '2016/05/foto-produto.png'),
(884, 225, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:24:\"2016/05/foto-produto.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"foto-produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"foto-produto-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(886, 225, '_edit_lock', '1462971355:1'),
(888, 225, '_dp_original', '34'),
(894, 226, '_wp_attached_file', '2016/05/produto.png'),
(896, 226, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:192;s:6:\"height\";i:294;s:4:\"file\";s:19:\"2016/05/produto.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(898, 226, '_dp_original', '32'),
(915, 228, '_wp_attached_file', '2016/05/foto-produto.png'),
(916, 228, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:24:\"2016/05/foto-produto.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"foto-produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"foto-produto-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(917, 228, '_edit_lock', '1462971355:1'),
(920, 228, '_dp_original', '188'),
(921, 229, '_wp_attached_file', '2016/05/produto.png'),
(923, 229, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:192;s:6:\"height\";i:294;s:4:\"file\";s:19:\"2016/05/produto.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(928, 229, '_dp_original', '189'),
(935, 230, '_wp_attached_file', '2016/05/miniatura2-3.png'),
(937, 230, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(942, 230, '_dp_original', '184'),
(948, 232, '_wp_attached_file', '2016/05/miniatura-6.png'),
(950, 232, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-6.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(955, 232, '_dp_original', '185'),
(959, 233, '_wp_attached_file', '2016/05/miniatura-5.png'),
(960, 233, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-5.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(961, 234, '_wp_attached_file', '2016/05/foto-produto-2.png'),
(963, 234, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:26:\"2016/05/foto-produto-2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(965, 233, '_dp_original', '186'),
(968, 234, '_dp_original', '183'),
(973, 235, '_wp_attached_file', '2016/05/produto-carrossel.png'),
(975, 235, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:153;s:6:\"height\";i:224;s:4:\"file\";s:29:\"2016/05/produto-carrossel.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"produto-carrossel-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(977, 236, '_wp_attached_file', '2016/05/miniatura-4.png'),
(980, 236, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(983, 235, '_dp_original', '187'),
(985, 236, '_dp_original', '180'),
(994, 238, '_wp_attached_file', '2016/05/miniatura-3.png'),
(996, 238, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1001, 238, '_dp_original', '181'),
(1010, 239, '_wp_attached_file', '2016/05/miniatura2-2.png'),
(1012, 239, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1017, 239, '_dp_original', '182'),
(1019, 240, '_wp_attached_file', '2016/05/miniatura-2.png'),
(1021, 240, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1027, 240, '_dp_original', '173'),
(1034, 242, '_wp_attached_file', '2016/05/miniatura-1.png'),
(1036, 242, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1041, 242, '_dp_original', '174'),
(1044, 243, '_wp_attached_file', '2016/05/miniatura2-1.png'),
(1045, 243, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1046, 244, '_wp_attached_file', '2016/05/miniatura-2.png'),
(1048, 244, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1049, 244, '_dp_original', '162'),
(1050, 243, '_dp_original', '175'),
(1053, 245, '_wp_attached_file', '2016/05/miniatura-1.png'),
(1055, 246, '_wp_attached_file', '2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg'),
(1056, 245, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1058, 246, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:344;s:6:\"height\";i:344;s:4:\"file\";s:83:\"2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:2:\"Ð\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:4:\"8144\";s:8:\"keywords\";a:0:{}}}'),
(1059, 245, '_dp_original', '160'),
(1064, 246, '_dp_original', '176'),
(1068, 247, '_wp_attached_file', '2016/05/miniatura2-1.png'),
(1070, 247, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1072, 248, '_wp_attached_file', '2016/05/aquecedor-de-agua-a-gas.jpg'),
(1073, 247, '_dp_original', '161'),
(1075, 248, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:130;s:6:\"height\";i:130;s:4:\"file\";s:35:\"2016/05/aquecedor-de-agua-a-gas.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1080, 248, '_dp_original', '178'),
(1083, 249, '_wp_attached_file', '2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg'),
(1086, 249, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:344;s:6:\"height\";i:344;s:4:\"file\";s:83:\"2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:2:\"Ð\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:4:\"8144\";s:8:\"keywords\";a:0:{}}}'),
(1088, 249, '_dp_original', '37'),
(1093, 251, '_wp_attached_file', '2016/05/aquecedor-de-agua-a-gas.jpg'),
(1094, 251, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:130;s:6:\"height\";i:130;s:4:\"file\";s:35:\"2016/05/aquecedor-de-agua-a-gas.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1095, 251, '_dp_original', '38'),
(1096, 252, '_wp_attached_file', '2016/05/miniatura-4.png'),
(1098, 252, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1100, 252, '_dp_original', '168'),
(1101, 253, '_wp_attached_file', '2016/05/miniatura-3.png'),
(1102, 253, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1104, 253, '_dp_original', '167'),
(1111, 254, '_wp_attached_file', '2016/05/miniatura2-2.png'),
(1113, 254, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1115, 254, '_dp_original', '166'),
(1122, 256, '_wp_attached_file', '2016/05/foto-produto-2.png'),
(1124, 256, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:26:\"2016/05/foto-produto-2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1126, 256, '_dp_original', '165'),
(1135, 257, '_wp_attached_file', '2016/05/miniatura2-3.png'),
(1136, 257, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1137, 257, '_dp_original', '170'),
(1144, 258, '_wp_attached_file', '2016/05/miniatura-6.png'),
(1146, 258, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-6.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1148, 258, '_dp_original', '171'),
(1155, 259, '_wp_attached_file', '2016/05/miniatura-5.png'),
(1157, 259, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-5.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1159, 259, '_dp_original', '169'),
(1166, 261, '_wp_attached_file', '2016/05/produto-carrossel.png'),
(1168, 261, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:153;s:6:\"height\";i:224;s:4:\"file\";s:29:\"2016/05/produto-carrossel.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"produto-carrossel-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1170, 261, '_dp_original', '33'),
(1173, 262, '_wp_attached_file', '2016/05/foto-produto.png'),
(1174, 263, '_wp_attached_file', '2016/05/foto-produto.png'),
(1175, 262, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:24:\"2016/05/foto-produto.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"foto-produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"foto-produto-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1176, 263, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:24:\"2016/05/foto-produto.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"foto-produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"foto-produto-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1177, 262, '_edit_lock', '1462971355:1'),
(1178, 263, '_edit_lock', '1462971355:1'),
(1179, 262, '_dp_original', '34'),
(1181, 263, '_dp_original', '188'),
(1184, 264, '_wp_attached_file', '2016/05/produto.png'),
(1186, 264, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:192;s:6:\"height\";i:294;s:4:\"file\";s:19:\"2016/05/produto.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1188, 264, '_dp_original', '32'),
(1190, 265, '_wp_attached_file', '2016/05/produto.png'),
(1193, 265, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:192;s:6:\"height\";i:294;s:4:\"file\";s:19:\"2016/05/produto.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1197, 265, '_dp_original', '189'),
(1203, 267, '_wp_attached_file', '2016/05/miniatura2-3.png'),
(1205, 267, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1210, 267, '_dp_original', '184'),
(1218, 268, '_wp_attached_file', '2016/05/miniatura-6.png'),
(1220, 268, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-6.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1223, 268, '_dp_original', '185'),
(1226, 269, '_wp_attached_file', '2016/05/foto-produto-2.png'),
(1228, 269, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:26:\"2016/05/foto-produto-2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1230, 270, '_wp_attached_file', '2016/05/miniatura-5.png'),
(1233, 270, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-5.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1237, 269, '_dp_original', '183'),
(1239, 270, '_dp_original', '186'),
(1244, 271, '_wp_attached_file', '2016/05/miniatura-4.png'),
(1246, 271, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1248, 272, '_wp_attached_file', '2016/05/produto-carrossel.png'),
(1251, 272, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:153;s:6:\"height\";i:224;s:4:\"file\";s:29:\"2016/05/produto-carrossel.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"produto-carrossel-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1254, 271, '_dp_original', '180'),
(1257, 272, '_dp_original', '187'),
(1264, 273, '_wp_attached_file', '2016/05/miniatura-3.png'),
(1266, 273, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1269, 273, '_dp_original', '181'),
(1270, 275, '_wp_attached_file', '2016/05/miniatura-2.png'),
(1271, 275, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1273, 276, '_wp_attached_file', '2016/05/miniatura2-2.png'),
(1274, 276, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1275, 275, '_dp_original', '173'),
(1277, 276, '_dp_original', '182'),
(1278, 274, '_wp_old_slug', 'linha-pratatherm-1000f-bosch'),
(1280, 277, '_wp_attached_file', '2016/05/miniatura-1.png'),
(1281, 277, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1283, 277, '_dp_original', '174'),
(1284, 274, '_edit_last', '1'),
(1285, 274, '_edit_lock', '1476801362:1'),
(1286, 274, '3B_produto_ref', 'Therm 1000F'),
(1287, 274, '3B_produto_marca', 'Lorenzetti'),
(1288, 274, '3B_produto_descricao', 'O Aquecedor de Água a Gás Lorenzetti LZ 1600D é ideal para uso de até 2 duchas. Possui exaustão forçada e chama modulante que permite que a temperatura da água mantenha-se estável. O produto é compatível com gás liquefeito de petróleo e tem vazão de água de 15 litros por minuto. O aquecedor liga ao se abrir registro de água.'),
(1289, 278, '_wp_attached_file', '2016/05/miniatura2-1.png'),
(1290, 274, '3B_produto_dados', 'a:6:{i:0;s:32:\"Consumo máximo de gás:2,3 kg/h\";i:1;s:36:\"Vazão de água com DT 20°: 15L/min\";i:2;s:15:\"Rendimento: 84%\";i:3;s:37:\"Dimensões (LxAxP): 35x65,5x17,5 (cm)\";i:4;s:16:\"Garantia: 3 anos\";i:5;s:32:\"Classificação PBE - Inmetro: A\";}'),
(1291, 278, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1292, 274, '3B_produto_aplicacoes', 'a:4:{i:0;s:13:\"Para 2 duchas\";i:1;s:40:\"Voluptatem cum incidunt, maiores numquam\";i:2;s:39:\"Ipsa voluptas porro in saepe reiciendis\";i:3;s:19:\"Saepem cum incidunt\";}'),
(1294, 274, '3B_produto_beneficios', 'a:4:{i:0;s:28:\"Controle eletrônico digital\";i:1;s:23:\"Acendimento automático\";i:2;s:22:\"Sistemas de segurança\";i:3;s:39:\"Ipsa voluptas porro in saepe reiciendis\";}'),
(1295, 274, '_yoast_wpseo_primary_categoriaProduto', '6'),
(1296, 278, '_dp_original', '175'),
(1297, 274, '3B_produto_tipo', 'Aquecedor a gás'),
(1298, 274, '3B_produto_imagens', '33'),
(1299, 274, '3B_produto_imagens', '37'),
(1300, 274, '3B_produto_imagens', '38'),
(1302, 279, '_wp_attached_file', '2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg'),
(1303, 274, 'TresB_produto_tipo', 'Bombas pressurizadas'),
(1304, 279, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:344;s:6:\"height\";i:344;s:4:\"file\";s:83:\"2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:2:\"Ð\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:4:\"8144\";s:8:\"keywords\";a:0:{}}}'),
(1305, 274, 'TresB_produto_marca', 'Texius'),
(1308, 274, 'TresB_produto_dados', 'a:6:{i:0;s:33:\"-Consumo máximo de gás:2,3 kg/h\";i:1;s:33:\"-Consumo máximo de gás:2,3 kg/h\";i:2;s:33:\"-Consumo máximo de gás:2,3 kg/h\";i:3;s:33:\"-Consumo máximo de gás:2,3 kg/h\";i:4;s:33:\"-Consumo máximo de gás:2,3 kg/h\";i:5;s:33:\"-Consumo máximo de gás:2,3 kg/h\";}'),
(1309, 279, '_dp_original', '176'),
(1310, 274, 'TresB_produto_aplicacoes', 'a:4:{i:0;s:25:\"Tratamentos de efluentes;\";i:1;s:36:\"Bombeamento de líquidos agressivos;\";i:2;s:22:\"Água desmineralizada;\";i:3;s:48:\"Produtos químicos em geral (consultar fábrica)\";}'),
(1311, 274, 'TresB_produto_beneficios', 'a:3:{i:0;s:20:\"Fácil manutenção;\";i:1;s:26:\"Toda em Inox - não oxida;\";i:2;s:54:\"Peças de reposição (corpo, rotor e selo mecânico);\";}'),
(1315, 274, '_thumbnail_id', '315'),
(1316, 280, '_wp_old_slug', 'linha-brancatherm-1200f-bosch'),
(1317, 281, '_wp_attached_file', '2016/05/aquecedor-de-agua-a-gas.jpg'),
(1318, 281, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:130;s:6:\"height\";i:130;s:4:\"file\";s:35:\"2016/05/aquecedor-de-agua-a-gas.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1320, 281, '_dp_original', '178'),
(1321, 282, '_wp_attached_file', '2016/05/miniatura-2.png'),
(1323, 282, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1324, 280, '_edit_last', '1'),
(1325, 282, '_dp_original', '162'),
(1326, 280, '_edit_lock', '1476801363:1'),
(1327, 280, '3B_produto_ref', 'KO 200I'),
(1328, 280, '3B_produto_marca', 'Bosch'),
(1329, 280, '3B_produto_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa voluptas porro in saepe reiciendis magnam, repudiandae culpa ipsam ut harum repellendus consequuntur laborum voluptatem cum incidunt, maiores numquam dolor nostrum.'),
(1330, 280, '3B_produto_dados', 'a:6:{i:0;s:32:\"Consumo máximo de gás:2,3 kg/h\";i:1;s:39:\"Vazão de água com DT 20°: 19,5 l/min\";i:2;s:15:\"Rendimento: 84%\";i:3;s:37:\"Dimensões (LxAxP): 35x65,5x17,5 (cm)\";i:4;s:16:\"Garantia: 3 anos\";i:5;s:32:\"Classificação PBE - Inmetro: A\";}'),
(1331, 280, '3B_produto_aplicacoes', 'a:4:{i:0;s:13:\"Dolor nostrum\";i:1;s:40:\"Voluptatem cum incidunt, maiores numquam\";i:2;s:39:\"Ipsa voluptas porro in saepe reiciendis\";i:3;s:19:\"Saepem cum incidunt\";}'),
(1332, 280, '3B_produto_beneficios', 'a:4:{i:0;s:20:\"Dolorum maxime amet.\";i:1;s:21:\"Et omnis pariatur est\";i:2;s:19:\"Saepem cum incidunt\";i:3;s:39:\"Ipsa voluptas porro in saepe reiciendis\";}'),
(1333, 284, '_wp_attached_file', '2016/05/miniatura-1.png'),
(1334, 280, '_yoast_wpseo_primary_categoriaProduto', '5'),
(1335, 284, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1337, 284, '_dp_original', '160'),
(1338, 280, '3B_produto_tipo', 'Aquecedor a gás'),
(1339, 280, '3B_produto_imagens', '32'),
(1340, 280, '3B_produto_imagens', '33'),
(1341, 280, '3B_produto_imagens', '34'),
(1342, 280, 'TresB_produto_dados', 'a:5:{i:0;s:33:\"-Consumo máximo de gás:2,3 kg/h\";i:1;s:33:\"-Consumo máximo de gás:2,3 kg/h\";i:2;s:33:\"-Consumo máximo de gás:2,3 kg/h\";i:3;s:33:\"-Consumo máximo de gás:2,3 kg/h\";i:4;s:33:\"-Consumo máximo de gás:2,3 kg/h\";}'),
(1343, 280, 'TresB_produto_aplicacoes', 'a:2:{i:0;s:9:\"Chuveiro;\";i:1;s:9:\"Torneira;\";}'),
(1345, 283, '_wp_old_slug', 'linha-pratatherm-1000f-bosch'),
(1346, 280, '_thumbnail_id', '315'),
(1347, 280, 'TresB_produto_tipo', 'Aquecedor a gás'),
(1348, 285, '_wp_attached_file', '2016/05/miniatura2-1.png'),
(1349, 280, 'TresB_produto_marca', 'Komeco'),
(1350, 285, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1351, 280, 'TresB_produto_descricao', 'Aquecimento por acumulação: esse tipo de aquecedor a gás armazena uma quantidade razoável de água quente em seu interior, fazendo com que a água quente chegue rapidamente ao ponto de uso.'),
(1352, 285, '_dp_original', '161'),
(1356, 283, '_edit_last', '1'),
(1357, 283, '_edit_lock', '1476802399:1'),
(1358, 283, '3B_produto_ref', 'Therm 1000F'),
(1359, 283, '3B_produto_marca', 'Komeco'),
(1360, 286, '_wp_attached_file', '2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg'),
(1361, 283, '3B_produto_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa voluptas porro in saepe reiciendis magnam, repudiandae culpa ipsam ut harum repellendus consequuntur laborum voluptatem cum incidunt, maiores numquam dolor nostrum.'),
(1362, 286, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:344;s:6:\"height\";i:344;s:4:\"file\";s:83:\"2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:83:\"aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:2:\"Ð\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:4:\"8144\";s:8:\"keywords\";a:0:{}}}'),
(1363, 283, '3B_produto_dados', 'a:6:{i:0;s:32:\"Consumo máximo de gás:2,3 kg/h\";i:1;s:39:\"Vazão de água com DT 20°: 19,5 l/min\";i:2;s:15:\"Rendimento: 84%\";i:3;s:37:\"Dimensões (LxAxP): 35x65,5x17,5 (cm)\";i:4;s:16:\"Garantia: 3 anos\";i:5;s:32:\"Classificação PBE - Inmetro: A\";}'),
(1364, 286, '_dp_original', '37'),
(1365, 283, '3B_produto_aplicacoes', 'a:4:{i:0;s:13:\"Dolor nostrum\";i:1;s:40:\"Voluptatem cum incidunt, maiores numquam\";i:2;s:39:\"Ipsa voluptas porro in saepe reiciendis\";i:3;s:19:\"Saepem cum incidunt\";}'),
(1366, 287, '_wp_attached_file', '2016/05/miniatura-4.png'),
(1367, 283, '3B_produto_beneficios', 'a:4:{i:0;s:20:\"Dolorum maxime amet.\";i:1;s:21:\"Et omnis pariatur est\";i:2;s:19:\"Saepem cum incidunt\";i:3;s:39:\"Ipsa voluptas porro in saepe reiciendis\";}'),
(1368, 287, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1369, 283, '_yoast_wpseo_primary_categoriaProduto', '5'),
(1370, 287, '_dp_original', '168'),
(1371, 283, '3B_produto_tipo', 'Aquecedor a gás'),
(1372, 283, '3B_produto_imagens', '34'),
(1373, 283, '3B_produto_imagens', '32'),
(1374, 283, '3B_produto_imagens', '33'),
(1375, 288, '_wp_attached_file', '2016/05/aquecedor-de-agua-a-gas.jpg'),
(1376, 283, 'TresB_produto_dados', 'a:6:{i:0;s:33:\"-Consumo máximo de gás:2,3 kg/h\";i:1;s:33:\"-Consumo máximo de gás:2,3 kg/h\";i:2;s:33:\"-Consumo máximo de gás:2,3 kg/h\";i:3;s:33:\"-Consumo máximo de gás:2,3 kg/h\";i:4;s:33:\"-Consumo máximo de gás:2,3 kg/h\";i:5;s:33:\"-Consumo máximo de gás:2,3 kg/h\";}'),
(1377, 288, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:130;s:6:\"height\";i:130;s:4:\"file\";s:35:\"2016/05/aquecedor-de-agua-a-gas.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1378, 283, 'TresB_produto_aplicacoes', 'a:2:{i:0;s:9:\"Chuveiro;\";i:1;s:9:\"Torneira;\";}'),
(1379, 288, '_dp_original', '38'),
(1380, 283, 'TresB_produto_beneficios', 'a:4:{i:0;s:19:\"Exaustão forçada;\";i:1;s:16:\"Display digital;\";i:2;s:55:\"Regulagem de temperatura pelo controle de água e gás;\";i:3;s:37:\"Botão para seleção verão/inverno;\";}'),
(1381, 289, '_wp_attached_file', '2016/05/miniatura-3.png');
INSERT INTO `3b_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1383, 289, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1384, 274, '_dp_original', '36'),
(1385, 283, 'TresB_produto_tipo', 'Aquecedor a gás'),
(1386, 289, '_dp_original', '167'),
(1387, 283, 'TresB_produto_marca', 'Bosch'),
(1388, 283, 'TresB_produto_descricao', 'Aquecimento por acumulação: esse tipo de aquecedor a gás armazena uma quantidade razoável de água quente em seu interior, fazendo com que a água quente chegue rapidamente ao ponto de uso.'),
(1392, 290, '_wp_attached_file', '2016/05/miniatura2-2.png'),
(1393, 290, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1394, 290, '_dp_original', '166'),
(1395, 291, '_wp_attached_file', '2016/05/miniatura2-3.png'),
(1396, 291, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:24:\"2016/05/miniatura2-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1397, 291, '_dp_original', '170'),
(1398, 292, '_wp_attached_file', '2016/05/foto-produto-2.png'),
(1399, 292, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:26:\"2016/05/foto-produto-2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"foto-produto-2-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1400, 292, '_dp_original', '165'),
(1401, 293, '_wp_attached_file', '2016/05/miniatura-6.png'),
(1402, 280, '_dp_original', '35'),
(1403, 293, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-6.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1404, 293, '_dp_original', '171'),
(1405, 294, '_wp_attached_file', '2016/05/miniatura-5.png'),
(1406, 294, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:117;s:6:\"height\";i:109;s:4:\"file\";s:23:\"2016/05/miniatura-5.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1407, 294, '_dp_original', '169'),
(1408, 295, '_wp_attached_file', '2016/05/produto-carrossel.png'),
(1409, 295, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:153;s:6:\"height\";i:224;s:4:\"file\";s:29:\"2016/05/produto-carrossel.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"produto-carrossel-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1410, 295, '_dp_original', '33'),
(1411, 296, '_wp_attached_file', '2016/05/foto-produto.png'),
(1412, 296, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:291;s:6:\"height\";i:459;s:4:\"file\";s:24:\"2016/05/foto-produto.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"foto-produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"foto-produto-190x300.png\";s:5:\"width\";i:190;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1413, 296, '_edit_lock', '1462971355:1'),
(1414, 296, '_dp_original', '34'),
(1415, 297, '_wp_attached_file', '2016/05/produto.png'),
(1416, 297, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:192;s:6:\"height\";i:294;s:4:\"file\";s:19:\"2016/05/produto.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"produto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1417, 297, '_dp_original', '32'),
(1418, 283, '_dp_original', '31'),
(1419, 50, 'TresB_parceiro_link', '#'),
(1420, 48, 'TresB_parceiro_link', '#'),
(1421, 46, 'TresB_parceiro_link', '#'),
(1422, 44, 'TresB_parceiro_link', '#'),
(1423, 298, '_wp_attached_file', '2016/05/banner-2.png'),
(1424, 298, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1220;s:6:\"height\";i:275;s:4:\"file\";s:20:\"2016/05/banner-2.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"banner-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner-2-300x68.png\";s:5:\"width\";i:300;s:6:\"height\";i:68;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"banner-2-768x173.png\";s:5:\"width\";i:768;s:6:\"height\";i:173;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"banner-2-1024x231.png\";s:5:\"width\";i:1024;s:6:\"height\";i:231;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:20:\"banner-2-600x135.png\";s:5:\"width\";i:600;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1425, 299, '_wp_attached_file', '2016/05/foto-que-fazemos-2.png'),
(1426, 299, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:609;s:6:\"height\";i:361;s:4:\"file\";s:30:\"2016/05/foto-que-fazemos-2.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"foto-que-fazemos-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"foto-que-fazemos-2-300x178.png\";s:5:\"width\";i:300;s:6:\"height\";i:178;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:30:\"foto-que-fazemos-2-600x356.png\";s:5:\"width\";i:600;s:6:\"height\";i:356;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1427, 300, '_wp_attached_file', '2016/05/foto-quemsomos-1.png'),
(1428, 300, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1129;s:6:\"height\";i:393;s:4:\"file\";s:28:\"2016/05/foto-quemsomos-1.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"foto-quemsomos-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"foto-quemsomos-1-300x104.png\";s:5:\"width\";i:300;s:6:\"height\";i:104;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"foto-quemsomos-1-768x267.png\";s:5:\"width\";i:768;s:6:\"height\";i:267;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"foto-quemsomos-1-1024x356.png\";s:5:\"width\";i:1024;s:6:\"height\";i:356;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:28:\"foto-quemsomos-1-600x209.png\";s:5:\"width\";i:600;s:6:\"height\";i:209;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1429, 301, '_edit_last', '1'),
(1430, 301, '3B_destaque_link', 'http://www.3baquecedores.com.br/'),
(1431, 301, '3B_destaque_descricao', 'Venda de aquecedores'),
(1432, 301, '_edit_lock', '1504548911:1'),
(1433, 301, 'TresB_destaque_link', '#'),
(1434, 301, 'TresB_destaque_descricao', 'As melhores marcas em aquecedores para os bons momentos do seu dia.'),
(1436, 302, '_wp_attached_file', '2016/05/foto-destaque.png'),
(1437, 302, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1220;s:6:\"height\";i:785;s:4:\"file\";s:25:\"2016/05/foto-destaque.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"foto-destaque-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"foto-destaque-300x193.png\";s:5:\"width\";i:300;s:6:\"height\";i:193;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"foto-destaque-768x494.png\";s:5:\"width\";i:768;s:6:\"height\";i:494;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"foto-destaque-1024x659.png\";s:5:\"width\";i:1024;s:6:\"height\";i:659;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:25:\"foto-destaque-600x386.png\";s:5:\"width\";i:600;s:6:\"height\";i:386;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1438, 302, '_dp_original', '28'),
(1439, 301, '_dp_original', '27'),
(1440, 303, '_wp_attached_file', '2016/05/hover-1.png'),
(1441, 303, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:71;s:6:\"height\";i:70;s:4:\"file\";s:19:\"2016/05/hover-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1442, 301, '_thumbnail_id', '358'),
(1443, 127, 'wpb_post_views_count', '0'),
(1445, 127, 'dsq_thread_id', '4824658045'),
(1448, 123, 'dsq_thread_id', '4831194634'),
(1453, 125, 'dsq_thread_id', '4831276364'),
(1457, 304, '_edit_last', '1'),
(1458, 304, '_edit_lock', '1470075025:1'),
(1460, 304, 'TresB_depoimento_texto', 'Quisque non risus convallis congue tellus non lobortis velit suspendisse potenti liquam faucibus justo eu sodales vestibulum nunc odio dapibus.'),
(1461, 304, 'TresB_depoimento_autor', 'Magna sitamet imperd'),
(1462, 304, 'TresB_depoimento_autor_info', 'Gerencia nacional/PR'),
(1534, 283, 'TresB_tabela', '<style type=\"text/css\">\n.tg  {border-collapse:collapse;border-spacing:0;}\n.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}\n.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}\n.tg .tg-baqh{text-align:center;vertical-align:top}\n.tg .tg-yw4l{vertical-align:top}\n@media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}\n</style>\n<div class=\"tg-wrap\">\n<table class=\"tg\" style=\"undefined;table-layout: fixed; width: 700px;\">\n<colgroup>\n<col style=\"width: 180px;\" />\n<col style=\"width: 180px;\" />\n<col style=\"width: 170px;\" />\n<col style=\"width: 170px;\" /> </colgroup>\n<tbody>\n<tr>\n<th class=\"tg-baqh\" colspan=\"4\">Especificações técnicas</th>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\">Tipo de gás</td>\n<td class=\"tg-yw4l\">GLP</td>\n<td class=\"tg-yw4l\">GN</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" rowspan=\"2\">Potência nominal</td>\n<td class=\"tg-yw4l\">KW</td>\n<td class=\"tg-yw4l\">12,5</td>\n<td class=\"tg-yw4l\">12,6</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\"> Kcal/h</td>\n<td class=\"tg-yw4l\"> 10.778</td>\n<td class=\"tg-yw4l\"> 10.822</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Consumo máx. gás</td>\n<td class=\"tg-yw4l\"> 0,91 kg/h</td>\n<td class=\"tg-yw4l\"> 1.14 m³/h</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Rendimento</td>\n<td class=\"tg-yw4l\"> 84%</td>\n<td class=\"tg-yw4l\"> 84%</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Capacidade de vazão*</td>\n<td class=\"tg-yw4l\" colspan=\"2\"> 7,5 l/min</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Pontos simultâneos**</td>\n<td class=\"tg-yw4l\" colspan=\"2\"> 1 ducha de 8 l/min</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Dimensões (A x L x P)</td>\n<td class=\"tg-yw4l\" colspan=\"2\"> 463 x 310 x 125 mm</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Diâmetro da Chaminé</td>\n<td class=\"tg-yw4l\" colspan=\"2\"> 60 mm</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Alimentação Elétrica</td>\n<td class=\"tg-yw4l\" colspan=\"2\"> 127 V ou 220V</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Classificação PBE</td>\n<td class=\"tg-yw4l\" colspan=\"2\"> A</td>\n</tr>\n</tbody>\n</table>\n</div>\n'),
(1535, 283, '_wp_old_slug', 'aquecedor-a-gas-6'),
(1536, 283, 'TresB_produto_tabela', '<div class=\"tg-wrap\">\n<table class=\"tg\" style=\"undefined;table-layout: fixed; width: auto;\">\n<colgroup>\n<col style=\"width: 180px;\" />\n<col style=\"width: 180px;\" />\n<col style=\"width: 170px;\" />\n<col style=\"width: 170px;\" /> </colgroup>\n<tbody>\n<tr>\n<th class=\"tg-baqh\" colspan=\"4\"></th>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"><strong>Tipo de gás</strong></td>\n<td class=\"tg-yw4l\"><strong>GLP</strong></td>\n<td class=\"tg-yw4l\"><strong>GN</strong></td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" rowspan=\"2\">Potência nominal</td>\n<td class=\"tg-yw4l\">KW</td>\n<td class=\"tg-yw4l\">12,5</td>\n<td class=\"tg-yw4l\">12,6</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\"> Kcal/h</td>\n<td class=\"tg-yw4l\"> 10.778</td>\n<td class=\"tg-yw4l\"> 10.822</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Consumo máx. gás</td>\n<td class=\"tg-yw4l\"> 0,91 kg/h</td>\n<td class=\"tg-yw4l\"> 1.14 m³/h</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Rendimento</td>\n<td class=\"tg-yw4l\"> 84%</td>\n<td class=\"tg-yw4l\"> 84%</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Capacidade de vazão*</td>\n<td class=\"tg-yw4l\" colspan=\"2\"> 7,5 l/min</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Pontos simultâneos**</td>\n<td class=\"tg-yw4l\" colspan=\"2\"> 1 ducha de 8 l/min</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Dimensões (A x L x P)</td>\n<td class=\"tg-yw4l\" colspan=\"2\"> 463 x 310 x 125 mm</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Diâmetro da Chaminé</td>\n<td class=\"tg-yw4l\" colspan=\"2\"> 60 mm</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Alimentação Elétrica</td>\n<td class=\"tg-yw4l\" colspan=\"2\"> 127 V ou 220V</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"2\"> Classificação PBE</td>\n<td class=\"tg-yw4l\" colspan=\"2\"> A</td>\n</tr>\n</tbody>\n</table>\n</div>\n'),
(1537, 280, '_wp_old_slug', 'aquecedor-a-gas-2-5'),
(1538, 280, 'TresB_produto_beneficios', 'a:4:{i:0;s:46:\"róprio para instalação dentro de banheiros;\";i:1;s:126:\"Única linha que permite a instalação dentro dos banheiros devido ao seu sistema de fluxo balanceado de exaustão dos gases;\";i:2;s:29:\"Sensor de presença de chama;\";i:3;s:49:\"Maior segurança e tranquilidade para o usuário;\";}'),
(1539, 280, 'TresB_produto_tabela', '<div class=\"tg-wrap\">\n<table class=\"tg\" style=\"table-layout: fixed; width: 753px; height: 427px;\">\n<colgroup>\n<col style=\"width: 195px;\" />\n<col style=\"width: 195px;\" />\n<col style=\"width: 184px;\" />\n<col style=\"width: 184px;\" /> </colgroup>\n<tbody>\n<tr>\n<th class=\"tg-baqh\" colspan=\"4\"></th>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> <strong>Tipo de gás</strong></td>\n<td class=\"tg-yw4l\"> <strong>GN</strong></td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Vazão de água com acréscimo de 20° (L/min)</td>\n<td class=\"tg-yw4l\"> 7.5</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Classificação INMETRO</td>\n<td class=\"tg-yw4l\"> A</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Rendimento</td>\n<td class=\"tg-yw4l\"> 84%</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Potência nominal nas condições padrão</td>\n<td class=\"tg-yw4l\"> 10.922 kcal/h (12,7 kW)</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Consumo máximo de gás</td>\n<td class=\"tg-yw4l\"> 1,115 m³/h</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Pressão de gás - dinâmico (mmca.)</td>\n<td class=\"tg-yw4l\"> 200</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Ignição</td>\n<td class=\"tg-yw4l\"> Automática</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Pressão de água (mca) mínima</td>\n<td class=\"tg-yw4l\">  3,4</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Pressão de água (mca) ideal de trabalho</td>\n<td class=\"tg-yw4l\"> &gt;10</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Pressão de água (mca) máxima</td>\n<td class=\"tg-yw4l\"> 60</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Vazão min. para acendimento (L/min)</td>\n<td class=\"tg-yw4l\"> 4,5</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Diâmetro da chaminé (mm)</td>\n<td class=\"tg-yw4l\"> 60 / 90</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Dimensões L x A x P (cm)</td>\n<td class=\"tg-yw4l\"> 33,0 x 51,7 x 12,2</td>\n</tr>\n</tbody>\n</table>\n</div>\n'),
(1540, 274, '_wp_old_slug', 'lz-1600-digital-5'),
(1541, 274, 'TresB_produto_tabela', '<div class=\"tg-wrap\">\n<table class=\"tg\" style=\"undefined;table-layout: fixed; width: auto;\">\n<colgroup>\n<col style=\"width: 195px;\" />\n<col style=\"width: 195px;\" />\n<col style=\"width: 21px;\" />\n<col style=\"width: 364px;\" /> </colgroup>\n<tbody>\n<tr>\n<th class=\"tg-baqh\" colspan=\"4\"></th>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> <strong>Tipo de gás</strong></td>\n<td class=\"tg-yw4l\"> <strong>GN</strong></td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Potência</td>\n<td class=\"tg-yw4l\"> ½ CV a 4,0 CV</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Voltagem</td>\n<td class=\"tg-yw4l\"> Trifásica 220/380/440  V</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Rotação</td>\n<td class=\"tg-yw4l\"> 3.500 RPM</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Frequência</td>\n<td class=\"tg-yw4l\"> 60 HZ</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Proteção</td>\n<td class=\"tg-yw4l\"> IP55</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Rotor</td>\n<td class=\"tg-yw4l\">semiaberto ou fechado  em Aço Inox AISI 316</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Voluta</td>\n<td class=\"tg-yw4l\"> Aço Inox AISI 316</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Intermediário</td>\n<td class=\"tg-yw4l\"> Aço Inox AISI 316</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Selo</td>\n<td class=\"tg-yw4l\"> Ø 13 mm em Viton</td>\n</tr>\n<tr>\n<td class=\"tg-yw4l\" colspan=\"3\"> Eixo</td>\n<td class=\"tg-yw4l\"> Aço Inox AISI 316</td>\n</tr>\n</tbody>\n</table>\n</div>\n'),
(1542, 315, '_wp_attached_file', '2016/05/fallbackProduto.png'),
(1543, 315, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:251;s:6:\"height\";i:272;s:4:\"file\";s:27:\"2016/05/fallbackProduto.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"fallbackProduto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1544, 283, '_thumbnail_id', '315'),
(1550, 316, '_wp_attached_file', '2016/05/casalFeliz-e1470060792237.jpg'),
(1551, 316, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1336;s:6:\"height\";i:891;s:4:\"file\";s:37:\"2016/05/casalFeliz-e1470060792237.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"casalFeliz-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"casalFeliz-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"casalFeliz-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"casalFeliz-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:22:\"casalFeliz-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1552, 316, '_wp_attachment_backup_sizes', 'a:1:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:2508;s:6:\"height\";i:1672;s:4:\"file\";s:14:\"casalFeliz.jpg\";}}'),
(1553, 64, '_yoast_wpseo_focuskw_text_input', 'aquecedor, aquecedor de passagem, aquecedor solar, aquecedores, aquecedores de acumulação, aquecedores de ambiente, bomba, bomba pressurizadora, caldeiras, caldeira murais, central térmica, termica, fluxostato, pressurizador, medidores, gás, gas, medidores de gas, 315 HFBE, hidromax, bosch, harman noritz, komeco, lao industria, orbis, rinnai, texius'),
(1554, 64, '_yoast_wpseo_focuskw', 'aquecedor, aquecedor de passagem, aquecedor solar, aquecedores, aquecedores de acumulação, aquecedores de ambiente, bomba, bomba pressurizadora, caldeiras, caldeira murais, central térmica, termica, fluxostato, pressurizador, medidores, gás, gas, medidores de gas, 315 HFBE, hidromax, bosch, harman noritz, komeco, lao industria, orbis, rinnai, texius'),
(1555, 64, '_yoast_wpseo_title', '3B Aquecedores:: desde 1990 levando Conforto & Segurança para você!'),
(1556, 64, '_yoast_wpseo_metadesc', 'Os Melhores Aquecedores para Você e sua Família! Com garantia de entrega para todo o Brasil!'),
(1557, 64, '_yoast_wpseo_linkdex', '13'),
(1561, 27, '_wp_old_slug', '3b-aquecedores__trashed'),
(1562, 317, '_edit_last', '1'),
(1563, 317, 'TresB_depoimento_texto', 'Depoimento exemplo'),
(1564, 317, 'TresB_depoimento_autor', 'Autor exemplo'),
(1565, 317, 'TresB_depoimento_autor_info', 'empresa exemplo / PR'),
(1566, 317, '_edit_lock', '1470075121:1'),
(1567, 304, '_thumbnail_id', '302'),
(1568, 317, '_thumbnail_id', '302'),
(1570, 283, 'TresB_produto_imagens', '315'),
(1571, 280, 'TresB_produto_imagens', '315'),
(1572, 274, 'TresB_produto_imagens', '315'),
(1574, 127, 'dsq_needs_sync', '1'),
(1575, 123, 'dsq_needs_sync', '1'),
(1576, 322, '_edit_last', '1'),
(1577, 322, '_edit_lock', '1501185804:1'),
(1578, 322, 'TresB_produto_tipo', 'IN-180D  '),
(1580, 322, 'TresB_produto_aplicacoes', 'a:0:{}'),
(1581, 322, 'TresB_produto_beneficios', 'a:0:{}'),
(1582, 322, '_yoast_wpseo_primary_categoriaProduto', '5'),
(1583, 322, '_yoast_wpseo_content_score', '30'),
(1584, 323, '_wp_attached_file', '2017/07/aquecedor-IN-180D.jpg'),
(1585, 323, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2452;s:6:\"height\";i:4243;s:4:\"file\";s:29:\"2017/07/aquecedor-IN-180D.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"aquecedor-IN-180D-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"aquecedor-IN-180D-173x300.jpg\";s:5:\"width\";i:173;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"aquecedor-IN-180D-768x1329.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1329;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:30:\"aquecedor-IN-180D-592x1024.jpg\";s:5:\"width\";i:592;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:30:\"aquecedor-IN-180D-600x1038.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:1038;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1337682460\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1586, 324, '_wp_attached_file', '2017/07/Capturar.png'),
(1587, 324, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:365;s:6:\"height\";i:297;s:4:\"file\";s:20:\"2017/07/Capturar.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"Capturar-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"Capturar-300x244.png\";s:5:\"width\";i:300;s:6:\"height\";i:244;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1588, 322, '_thumbnail_id', '323'),
(1589, 322, 'TresB_produto_descricao', '<p>- Controle digital de temperatura<br />\n- Chama modulante - Exaustão forçada por ventoinha<br />\n- Sistema computadorizado<br />\n- Sistemas integrados de segurança<br />\n- Auto diagnóstico<br />\n- Capacidade de 17,5 a 28 litros/minuto. Para 2 a 3 pontos.</p>\n'),
(1590, 322, 'TresB_produto_tabela', '<p><img class=\"alignnone size-medium wp-image-324\" src=\"http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/Capturar-300x244.png\" alt=\"\" width=\"300\" height=\"244\" /></p>\n'),
(1591, 322, 'TresB_produto_marca', 'INOVA - Digital Eletrônico (Bivolt)'),
(1598, 325, '_edit_last', '1'),
(1599, 325, '_edit_lock', '1501183086:1'),
(1600, 326, '_wp_attached_file', '2017/07/aquecedor-IN-230D_3523-2.jpg'),
(1601, 326, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1941;s:6:\"height\";i:3198;s:4:\"file\";s:36:\"2017/07/aquecedor-IN-230D_3523-2.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"aquecedor-IN-230D_3523-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"aquecedor-IN-230D_3523-2-182x300.jpg\";s:5:\"width\";i:182;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"aquecedor-IN-230D_3523-2-768x1265.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1265;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"aquecedor-IN-230D_3523-2-622x1024.jpg\";s:5:\"width\";i:622;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:36:\"aquecedor-IN-230D_3523-2-600x989.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:989;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1315910257\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1602, 325, '_thumbnail_id', '326'),
(1603, 325, 'TresB_produto_tipo', 'IN-230D '),
(1604, 325, 'TresB_produto_marca', 'INOVA'),
(1605, 325, 'TresB_produto_descricao', '<p>- Controle digital de temperatura<br />\n- Chama modulante - Exaustão forçada por ventoinha<br />\n- Sistema computadorizado<br />\n- Sistemas integrados de segurança<br />\n- Auto diagnóstico<br />\n- Capacidade de 17,5 a 28 litros/minuto. Para 2 a 3 pontos.</p>\n'),
(1606, 325, 'TresB_produto_imagens', '326'),
(1607, 325, 'TresB_produto_aplicacoes', 'a:0:{}'),
(1608, 325, 'TresB_produto_beneficios', 'a:0:{}'),
(1609, 325, 'TresB_produto_tabela', '<p><img class=\"alignnone size-medium wp-image-324\" src=\"http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/Capturar-300x244.png\" alt=\"\" width=\"300\" height=\"244\" /></p>\n'),
(1610, 325, '_yoast_wpseo_primary_categoriaProduto', '5'),
(1611, 325, '_yoast_wpseo_content_score', '30'),
(1612, 327, '_edit_last', '1'),
(1613, 327, '_edit_lock', '1501188198:1'),
(1614, 328, '_wp_attached_file', '2017/07/asd.png'),
(1615, 328, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:353;s:6:\"height\";i:420;s:4:\"file\";s:15:\"2017/07/asd.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"asd-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"asd-252x300.png\";s:5:\"width\";i:252;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1616, 329, '_wp_attached_file', '2017/07/aquecedor-IN-600_1000_1800_2200_3540-2.jpg'),
(1617, 329, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1448;s:6:\"height\";i:2650;s:4:\"file\";s:50:\"2017/07/aquecedor-IN-600_1000_1800_2200_3540-2.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:50:\"aquecedor-IN-600_1000_1800_2200_3540-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:50:\"aquecedor-IN-600_1000_1800_2200_3540-2-164x300.jpg\";s:5:\"width\";i:164;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:51:\"aquecedor-IN-600_1000_1800_2200_3540-2-768x1406.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1406;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:51:\"aquecedor-IN-600_1000_1800_2200_3540-2-560x1024.jpg\";s:5:\"width\";i:560;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:51:\"aquecedor-IN-600_1000_1800_2200_3540-2-600x1098.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:1098;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1315911084\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1618, 327, '_thumbnail_id', '329'),
(1619, 327, 'TresB_produto_tipo', 'IN-800EFP '),
(1620, 327, 'TresB_produto_marca', 'INOVA - Exaustão Forçada (Bivolt)'),
(1621, 327, 'TresB_produto_descricao', '<p>- Exaustão forçada por ventoinha<br />\n- Mostrador digital de temperatura<br />\n- Sistema integrado de segurança<br />\n- Capacidade de 8 a 17,5 litros/minuto para até 2 pontos.</p>\n'),
(1622, 327, 'TresB_produto_aplicacoes', 'a:0:{}'),
(1623, 327, 'TresB_produto_beneficios', 'a:0:{}'),
(1624, 327, 'TresB_produto_tabela', '<p><a href=\"http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/800.png\" target=\"_blank\" rel=\"noopener\"><img class=\"alignnone size-medium wp-image-348\" src=\"http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/800-275x300.png\" alt=\"\" width=\"275\" height=\"300\" /></a></p>\n'),
(1625, 327, '_yoast_wpseo_primary_categoriaProduto', '5'),
(1626, 327, '_yoast_wpseo_content_score', '30'),
(1627, 330, '_edit_last', '1'),
(1628, 330, '_edit_lock', '1501189244:1'),
(1629, 330, '_thumbnail_id', '329'),
(1630, 330, 'TresB_produto_tipo', 'IN-1800EFP'),
(1631, 330, 'TresB_produto_marca', 'INOVA -Exaustão Forçada (Bivolt)'),
(1632, 330, 'TresB_produto_descricao', '<p>- Exaustão forçada por ventoinha<br />\n- Mostrador digital de temperatura<br />\n- Sistema integrado de segurança<br />\n- Capacidade de 8 a 17,5 litros/minuto para até 2 pontos.</p>\n'),
(1633, 330, 'TresB_produto_aplicacoes', 'a:0:{}'),
(1634, 330, 'TresB_produto_beneficios', 'a:0:{}'),
(1635, 330, 'TresB_produto_tabela', '<p><img class=\"alignnone size-medium wp-image-328\" src=\"http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/asd-252x300.png\" alt=\"\" width=\"252\" height=\"300\" /></p>\n'),
(1636, 330, '_yoast_wpseo_primary_categoriaProduto', '5'),
(1637, 330, '_yoast_wpseo_content_score', '30'),
(1638, 322, 'TresB_produto_imagens', '323'),
(1639, 322, 'TresB_produto_imagens', '324'),
(1640, 331, '_edit_last', '1'),
(1641, 331, '_edit_lock', '1501186506:1'),
(1642, 332, '_wp_attached_file', '2017/07/assas.png'),
(1643, 332, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:372;s:6:\"height\";i:279;s:4:\"file\";s:17:\"2017/07/assas.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"assas-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"assas-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1644, 333, '_wp_attached_file', '2017/07/GWH_350_520_E.tif'),
(1645, 334, '_wp_attached_file', '2017/07/GWH_350_520_E.jpg'),
(1646, 334, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1620;s:6:\"height\";i:2803;s:4:\"file\";s:25:\"2017/07/GWH_350_520_E.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"GWH_350_520_E-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"GWH_350_520_E-173x300.jpg\";s:5:\"width\";i:173;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"GWH_350_520_E-768x1329.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1329;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"GWH_350_520_E-592x1024.jpg\";s:5:\"width\";i:592;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:26:\"GWH_350_520_E-600x1038.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:1038;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1432139768\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1647, 331, '_thumbnail_id', '334'),
(1648, 331, 'TresB_produto_tipo', 'GWH 520 CTDE'),
(1649, 331, 'TresB_produto_marca', 'BOSCH - Automudalação Eletrônica'),
(1650, 331, 'TresB_produto_descricao', '<p>- Praticiade com a função memória<br />\n- Módulo banheira<br />\n- Bivolt<br />\n- Sensor de detecação de chama,<br />\n- Limitador de temperatura,<br />\n- Monitoramento de exaustão,<br />\n- Válvula de segurança e termofusível.</p>\n'),
(1651, 331, 'TresB_produto_aplicacoes', 'a:0:{}'),
(1652, 331, 'TresB_produto_beneficios', 'a:0:{}'),
(1653, 331, 'TresB_produto_tabela', '<p><a href=\"http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/520.png\" target=\"_blank\" rel=\"noopener\"><img class=\"alignnone size-medium wp-image-347\" src=\"http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/520-300x252.png\" alt=\"\" width=\"300\" height=\"252\" /></a></p>\n'),
(1654, 331, '_yoast_wpseo_primary_categoriaProduto', '5'),
(1655, 331, '_yoast_wpseo_content_score', '30'),
(1657, 335, '_edit_last', '1'),
(1658, 335, '_edit_lock', '1501185715:1'),
(1659, 336, '_wp_attached_file', '2017/07/3323.png'),
(1660, 336, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:359;s:6:\"height\";i:276;s:4:\"file\";s:16:\"2017/07/3323.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"3323-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"3323-300x231.png\";s:5:\"width\";i:300;s:6:\"height\";i:231;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1661, 335, '_thumbnail_id', '334'),
(1662, 335, 'TresB_produto_tipo', 'GWH 350 CTDE'),
(1663, 335, 'TresB_produto_marca', 'BOSCH - Automudalação Eletrônica'),
(1664, 335, 'TresB_produto_descricao', '<p>- Praticiade com a função memória<br />\n- Módulo banheira<br />\n- Bivolt<br />\n- Sensor de detecação de chama,<br />\n- Limitador de temperatura,<br />\n- Monitoramento de exaustão,<br />\n- Válvula de segurança e termofusível.</p>\n'),
(1665, 335, 'TresB_produto_aplicacoes', 'a:0:{}'),
(1666, 335, 'TresB_produto_beneficios', 'a:0:{}'),
(1667, 335, 'TresB_produto_tabela', '<p><img class=\"alignnone size-medium wp-image-336\" src=\"http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/3323-300x231.png\" alt=\"\" width=\"300\" height=\"231\" /></p>\n'),
(1668, 335, '_yoast_wpseo_primary_categoriaProduto', '5'),
(1669, 335, '_yoast_wpseo_content_score', '30'),
(1670, 337, '_edit_last', '1'),
(1671, 337, '_edit_lock', '1501186403:1'),
(1672, 338, '_wp_attached_file', '2017/07/444.png'),
(1673, 338, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:639;s:6:\"height\";i:329;s:4:\"file\";s:15:\"2017/07/444.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"444-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"444-300x154.png\";s:5:\"width\";i:300;s:6:\"height\";i:154;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:15:\"444-600x309.png\";s:5:\"width\";i:600;s:6:\"height\";i:309;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1674, 337, 'TresB_produto_tipo', 'GWH 720 CTDE'),
(1675, 337, 'TresB_produto_marca', 'BOSCH - Automudalação Eletrônica'),
(1676, 337, 'TresB_produto_descricao', '<p>- Praticiade com a função memória<br />\n- Módulo banheira<br />\n- Bivolt<br />\n- Sensor de detecação de chama,<br />\n- Limitador de temperatura,<br />\n- Monitoramento de exaustão,<br />\n- Válvula de segurança e termofusível.</p>\n'),
(1677, 337, 'TresB_produto_aplicacoes', 'a:0:{}'),
(1678, 337, 'TresB_produto_beneficios', 'a:0:{}'),
(1679, 337, 'TresB_produto_tabela', '<p><a href=\"http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/720.png\" target=\"_blank\" rel=\"noopener\"><img class=\"alignnone size-medium wp-image-344\" src=\"http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/720-300x162.png\" alt=\"\" width=\"300\" height=\"162\" /></a></p>\n'),
(1680, 337, '_yoast_wpseo_primary_categoriaProduto', '5'),
(1681, 337, '_yoast_wpseo_content_score', '30'),
(1682, 339, '_edit_last', '1'),
(1683, 339, '_edit_lock', '1504551773:1'),
(1684, 340, '_wp_attached_file', '2017/07/sddddddd.png'),
(1685, 340, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:561;s:6:\"height\";i:353;s:4:\"file\";s:20:\"2017/07/sddddddd.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"sddddddd-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"sddddddd-300x189.png\";s:5:\"width\";i:300;s:6:\"height\";i:189;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1686, 339, '_thumbnail_id', '334'),
(1687, 339, 'TresB_produto_tipo', 'THERM 1000F'),
(1688, 339, 'TresB_produto_marca', 'BOSCH - Exaustão Forçada (Bivolt)'),
(1689, 339, 'TresB_produto_descricao', '<p>- Exaustão Forçada<br />\n- Display digital que exibe a temperatura da água<br />\n- Função inverno/verão com queimador bipartido<br />\n- Regulagem de temperatura pelo controle de água e gás</p>\n'),
(1690, 339, 'TresB_produto_aplicacoes', 'a:0:{}'),
(1691, 339, 'TresB_produto_beneficios', 'a:0:{}'),
(1692, 339, 'TresB_produto_tabela', '<p><a href=\"http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/1000.png\" target=\"_blank\" rel=\"noopener\"><img class=\"alignnone size-medium wp-image-346\" src=\"http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/1000-300x184.png\" alt=\"\" width=\"300\" height=\"184\" /></a></p>\n'),
(1693, 339, '_yoast_wpseo_primary_categoriaProduto', '5'),
(1694, 339, '_yoast_wpseo_content_score', '30'),
(1696, 341, '_edit_last', '1'),
(1697, 341, '_edit_lock', '1505942694:1'),
(1698, 342, '_wp_attached_file', '2017/07/zzzzz.png'),
(1699, 342, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:555;s:6:\"height\";i:357;s:4:\"file\";s:17:\"2017/07/zzzzz.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"zzzzz-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"zzzzz-300x193.png\";s:5:\"width\";i:300;s:6:\"height\";i:193;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1700, 341, '_thumbnail_id', '334'),
(1701, 341, 'TresB_produto_tipo', 'GWH 320 DE'),
(1702, 341, 'TresB_produto_marca', 'BOSCH - Exaustão Forçada (Bivolt)'),
(1703, 341, 'TresB_produto_descricao', '<p>- Exaustão Forçada<br />\n- Display digital que exibe a temperatura da água<br />\n- Função inverno/verão com queimador bipartido<br />\n- Regulagem de temperatura pelo controle de água e gás</p>\n'),
(1705, 341, 'TresB_produto_aplicacoes', 'a:0:{}'),
(1706, 341, 'TresB_produto_beneficios', 'a:0:{}'),
(1707, 341, 'TresB_produto_tabela', '<p><a href=\"http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/320.png\" target=\"_blank\" rel=\"noopener\"><img class=\"alignnone size-medium wp-image-345\" src=\"http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/320-300x182.png\" alt=\"\" width=\"300\" height=\"182\" /></a></p>\n'),
(1708, 341, '_yoast_wpseo_primary_categoriaProduto', '5'),
(1709, 341, '_yoast_wpseo_content_score', '30'),
(1710, 343, '_wp_attached_file', '2017/07/Aquecedor_Digital_GWH_720.jpg'),
(1711, 343, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:827;s:6:\"height\";i:1391;s:4:\"file\";s:37:\"2017/07/Aquecedor_Digital_GWH_720.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"Aquecedor_Digital_GWH_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"Aquecedor_Digital_GWH_720-178x300.jpg\";s:5:\"width\";i:178;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:38:\"Aquecedor_Digital_GWH_720-768x1292.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1292;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:38:\"Aquecedor_Digital_GWH_720-609x1024.jpg\";s:5:\"width\";i:609;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:38:\"Aquecedor_Digital_GWH_720-600x1009.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:1009;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1712, 337, '_thumbnail_id', '343'),
(1713, 344, '_wp_attached_file', '2017/07/720.png'),
(1714, 344, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:778;s:6:\"height\";i:421;s:4:\"file\";s:15:\"2017/07/720.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"720-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"720-300x162.png\";s:5:\"width\";i:300;s:6:\"height\";i:162;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"720-768x416.png\";s:5:\"width\";i:768;s:6:\"height\";i:416;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:15:\"720-600x325.png\";s:5:\"width\";i:600;s:6:\"height\";i:325;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1715, 345, '_wp_attached_file', '2017/07/320.png');
INSERT INTO `3b_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1716, 345, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:694;s:6:\"height\";i:421;s:4:\"file\";s:15:\"2017/07/320.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"320-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"320-300x182.png\";s:5:\"width\";i:300;s:6:\"height\";i:182;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:15:\"320-600x364.png\";s:5:\"width\";i:600;s:6:\"height\";i:364;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1717, 341, 'TresB_produto_imagens', '334'),
(1718, 346, '_wp_attached_file', '2017/07/1000.png'),
(1719, 346, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:735;s:6:\"height\";i:452;s:4:\"file\";s:16:\"2017/07/1000.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"1000-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"1000-300x184.png\";s:5:\"width\";i:300;s:6:\"height\";i:184;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:16:\"1000-600x369.png\";s:5:\"width\";i:600;s:6:\"height\";i:369;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1721, 347, '_wp_attached_file', '2017/07/520.png'),
(1722, 347, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:706;s:6:\"height\";i:593;s:4:\"file\";s:15:\"2017/07/520.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"520-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"520-300x252.png\";s:5:\"width\";i:300;s:6:\"height\";i:252;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:15:\"520-600x504.png\";s:5:\"width\";i:600;s:6:\"height\";i:504;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1723, 331, 'TresB_produto_imagens', '334'),
(1724, 348, '_wp_attached_file', '2017/07/800.png'),
(1725, 348, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:461;s:6:\"height\";i:502;s:4:\"file\";s:15:\"2017/07/800.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"800-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"800-275x300.png\";s:5:\"width\";i:275;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1726, 350, '_wp_attached_file', '2016/05/banner1.jpg'),
(1727, 350, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1359;s:6:\"height\";i:474;s:4:\"file\";s:19:\"2016/05/banner1.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner1-300x105.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:105;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"banner1-768x268.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:268;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"banner1-1024x357.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:357;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:19:\"banner1-600x209.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1728, 351, '_wp_attached_file', '2016/05/banner2.jpg'),
(1729, 351, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1359;s:6:\"height\";i:474;s:4:\"file\";s:19:\"2016/05/banner2.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner2-300x105.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:105;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"banner2-768x268.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:268;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"banner2-1024x357.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:357;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:19:\"banner2-600x209.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1730, 352, '_wp_attached_file', '2016/05/banner3.jpg'),
(1731, 352, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1359;s:6:\"height\";i:474;s:4:\"file\";s:19:\"2016/05/banner3.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner3-300x105.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:105;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"banner3-768x268.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:268;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"banner3-1024x357.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:357;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:19:\"banner3-600x209.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1732, 353, '_wp_attached_file', '2016/05/contato.jpg'),
(1733, 353, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:665;s:6:\"height\";i:688;s:4:\"file\";s:19:\"2016/05/contato.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"contato-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"contato-290x300.jpg\";s:5:\"width\";i:290;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:19:\"contato-600x621.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:621;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1734, 354, '_wp_attached_file', '2016/05/manutencao.jpg'),
(1735, 354, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:390;s:6:\"height\";i:374;s:4:\"file\";s:22:\"2016/05/manutencao.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"manutencao-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"manutencao-300x288.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:288;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1736, 355, '_wp_attached_file', '2016/05/manutencao2.jpg'),
(1737, 355, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:390;s:6:\"height\";i:374;s:4:\"file\";s:23:\"2016/05/manutencao2.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"manutencao2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"manutencao2-300x288.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:288;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1738, 29, '_yoast_wpseo_content_score', '30'),
(1739, 301, '_yoast_wpseo_content_score', '30'),
(1740, 27, '_yoast_wpseo_content_score', '30'),
(1741, 357, '_wp_attached_file', '2016/05/banner5.jpg'),
(1742, 357, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1359;s:6:\"height\";i:474;s:4:\"file\";s:19:\"2016/05/banner5.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner5-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner5-300x105.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:105;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"banner5-768x268.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:268;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"banner5-1024x357.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:357;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:19:\"banner5-600x209.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1743, 358, '_wp_attached_file', '2016/05/banner6.jpg'),
(1744, 358, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1359;s:6:\"height\";i:474;s:4:\"file\";s:19:\"2016/05/banner6.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner6-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner6-300x105.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:105;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"banner6-768x268.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:268;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"banner6-1024x357.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:357;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:19:\"banner6-600x209.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1745, 359, '_edit_last', '1'),
(1746, 359, '_edit_lock', '1504548911:1'),
(1747, 359, '_thumbnail_id', '357'),
(1748, 359, 'TresB_destaque_link', '#'),
(1749, 359, '_yoast_wpseo_content_score', '30'),
(1750, 339, 'TresB_produto_catalago', 'http://3baquecedores.pixd.com.br/assistencia-tecnica/'),
(1751, 339, 'TresB_produto_imagens', '334'),
(1755, 61, '_config_errors', 'a:1:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:103;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:70:\"https://contactform7.com/configuration-errors/email-not-in-site-domain\";}}}}'),
(1756, 158, '_config_errors', 'a:1:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:103;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:70:\"https://contactform7.com/configuration-errors/email-not-in-site-domain\";}}}}');

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_posts`
--

CREATE TABLE `3b_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `3b_posts`
--

INSERT INTO `3b_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(2, 1, '2016-05-09 14:08:50', '2016-05-09 17:08:50', 'Esta é uma página de exemplo. É diferente de um post porque ela ficará em um local e será exibida na navegação do seu site (na maioria dos temas). A maioria das pessoas começa com uma página de introdução aos potenciais visitantes do site. Ela pode ser assim:\n\n<blockquote>Olá! Eu sou um bike courrier de dia, ator amador à noite e este é meu blog. Eu moro em São Paulo, tenho um cachorro chamado Tonico e eu gosto de caipirinhas. (E de ser pego pela chuva.)</blockquote>\n\nou assim:\n\n<blockquote>A XYZ foi fundada em 1971 e desde então vem proporcionando produtos de qualidade a seus clientes. Localizada em Valinhos, XYZ emprega mais de 2.000 pessoas e faz várias contribuições para a comunidade local.</blockquote>\nComo um novo usuário do WordPress, você deve ir até o <a href=\"http://3baquecedores.pixd.com.br/wp-admin/\">seu painel</a> para excluir essa página e criar novas páginas com seu próprio conteúdo. Divirta-se!', 'Página de Exemplo', '', 'publish', 'closed', 'open', '', 'pagina-exemplo', '', '', '2016-05-09 14:08:50', '2016-05-09 17:08:50', '', 0, 'http://3baquecedores.pixd.com.br/?page_id=2', 0, 'page', '', 0),
(5, 1, '2016-05-09 14:31:59', '2016-05-09 17:31:59', '[wysija_page]', 'Confirmação de assinatura', '', 'publish', 'closed', 'closed', '', 'subscriptions', '', '', '2016-05-09 14:31:59', '2016-05-09 17:31:59', '', 0, 'http://3baquecedores.pixd.com.br/?wysijap=subscriptions', 0, 'wysijap', '', 0),
(25, 1, '2016-05-10 14:57:02', '2016-05-10 17:57:02', '', 'assessoria', '', 'inherit', 'open', 'closed', '', 'assessoria', '', '', '2016-05-10 14:57:02', '2016-05-10 17:57:02', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/assessoria.jpg', 0, 'attachment', 'image/jpeg', 0),
(27, 1, '2016-05-11 09:32:29', '2016-05-11 12:32:29', '', '3B Aquecedores', '', 'publish', 'closed', 'closed', '', '3b-aquecedores', '', '', '2017-09-04 10:21:24', '2017-09-04 13:21:24', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=destaque&#038;p=27', 0, 'destaque', '', 0),
(28, 1, '2016-05-11 09:28:36', '2016-05-11 12:28:36', '', 'foto-destaque', '', 'inherit', 'open', 'closed', '', 'foto-destaque', '', '', '2016-05-11 09:28:36', '2016-05-11 12:28:36', '', 27, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto-destaque.png', 0, 'attachment', 'image/png', 0),
(29, 1, '2016-05-11 09:35:44', '2016-05-11 12:35:44', '', 'Assistência Técnica Autorizada 3B', '', 'publish', 'closed', 'closed', '', 'instalacao-de-aquecedores', '', '', '2017-09-04 10:20:56', '2017-09-04 13:20:56', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=destaque&#038;p=29', 2, 'destaque', '', 0),
(30, 1, '2016-05-11 09:34:02', '2016-05-11 12:34:02', '', 'foto-que-fazemos', '', 'inherit', 'open', 'closed', '', 'foto-que-fazemos', '', '', '2016-05-11 09:34:02', '2016-05-11 12:34:02', '', 29, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto-que-fazemos.png', 0, 'attachment', 'image/png', 0),
(32, 1, '2016-05-11 09:47:12', '2016-05-11 12:47:12', '', 'produto', '', 'inherit', 'open', 'closed', '', 'produto', '', '', '2016-05-11 09:47:12', '2016-05-11 12:47:12', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/produto.png', 0, 'attachment', 'image/png', 0),
(33, 1, '2016-05-11 09:47:13', '2016-05-11 12:47:13', '', 'produto-carrossel', '', 'inherit', 'open', 'closed', '', 'produto-carrossel', '', '', '2016-05-11 09:47:13', '2016-05-11 12:47:13', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/produto-carrossel.png', 0, 'attachment', 'image/png', 0),
(34, 1, '2016-05-11 09:47:13', '2016-05-11 12:47:13', '', 'foto-produto', '', 'inherit', 'open', 'closed', '', 'foto-produto', '', '', '2016-05-11 09:47:13', '2016-05-11 12:47:13', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto-produto.png', 0, 'attachment', 'image/png', 0),
(37, 1, '2016-05-11 09:56:30', '2016-05-11 12:56:30', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital', '', '', '2016-05-11 09:56:30', '2016-05-11 12:56:30', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital.jpg', 0, 'attachment', 'image/jpeg', 0),
(38, 1, '2016-05-11 09:56:30', '2016-05-11 12:56:30', '', 'aquecedor-de-agua-a-gas', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-a-gas', '', '', '2016-05-11 09:56:30', '2016-05-11 12:56:30', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/aquecedor-de-agua-a-gas.jpg', 0, 'attachment', 'image/jpeg', 0),
(39, 1, '2016-05-11 09:59:39', '2016-05-11 12:59:39', '', 'João', '', 'publish', 'closed', 'closed', '', 'rascunho-automatico', '', '', '2016-05-12 12:42:15', '2016-05-12 15:42:15', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=depoimento&#038;p=39', 0, 'depoimento', '', 0),
(40, 1, '2016-05-11 10:00:42', '2016-05-11 13:00:42', '', 'Maria', '', 'publish', 'closed', 'closed', '', 'maria', '', '', '2016-05-12 12:42:05', '2016-05-12 15:42:05', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=depoimento&#038;p=40', 0, 'depoimento', '', 0),
(41, 1, '2016-05-11 10:03:13', '2016-05-11 13:03:13', '', 'João 2', '', 'publish', 'closed', 'closed', '', 'joao-2', '', '', '2016-05-12 12:41:55', '2016-05-12 15:41:55', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=depoimento&#038;p=41', 0, 'depoimento', '', 0),
(43, 1, '2016-05-11 10:05:01', '2016-05-11 13:05:01', '', 'Maria 2', '', 'publish', 'closed', 'closed', '', 'maria-2', '', '', '2016-05-12 12:10:39', '2016-05-12 15:10:39', '', 0, 'http://3baquecedores.pixd.com.br/depoimento/maria-2/', 0, 'depoimento', '', 0),
(44, 1, '2016-05-11 10:17:52', '2016-05-11 13:17:52', '', 'Fujitsu', '', 'publish', 'closed', 'closed', '', 'fujitsu', '', '', '2016-05-13 10:50:25', '2016-05-13 13:50:25', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=parceiro&#038;p=44', 0, 'parceiro', '', 0),
(45, 1, '2016-05-11 10:17:38', '2016-05-11 13:17:38', '', 'logo1', '', 'inherit', 'open', 'closed', '', 'logo1', '', '', '2016-05-11 10:17:38', '2016-05-11 13:17:38', '', 44, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/logo1.png', 0, 'attachment', 'image/png', 0),
(46, 1, '2016-05-11 10:18:21', '2016-05-11 13:18:21', '', 'Elgin', '', 'publish', 'closed', 'closed', '', 'elgin', '', '', '2016-05-13 10:50:22', '2016-05-13 13:50:22', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=parceiro&#038;p=46', 0, 'parceiro', '', 0),
(47, 1, '2016-05-11 10:18:14', '2016-05-11 13:18:14', '', 'logo2', '', 'inherit', 'open', 'closed', '', 'logo2', '', '', '2016-05-11 10:18:14', '2016-05-11 13:18:14', '', 46, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/logo2.png', 0, 'attachment', 'image/png', 0),
(48, 1, '2016-05-11 10:18:49', '2016-05-11 13:18:49', '', 'Rinnai', '', 'publish', 'closed', 'closed', '', 'rinnai', '', '', '2016-05-13 10:50:19', '2016-05-13 13:50:19', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=parceiro&#038;p=48', 0, 'parceiro', '', 0),
(49, 1, '2016-05-11 10:18:40', '2016-05-11 13:18:40', '', 'logo3', '', 'inherit', 'open', 'closed', '', 'logo3', '', '', '2016-05-11 10:18:40', '2016-05-11 13:18:40', '', 48, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/logo3.png', 0, 'attachment', 'image/png', 0),
(50, 1, '2016-05-11 10:19:27', '2016-05-11 13:19:27', '', 'Komeco', '', 'publish', 'closed', 'closed', '', 'komeco', '', '', '2016-08-01 15:16:34', '2016-08-01 18:16:34', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=parceiro&#038;p=50', 0, 'parceiro', '', 0),
(51, 1, '2016-05-11 10:19:16', '2016-05-11 13:19:16', '', 'logo4', '', 'inherit', 'open', 'closed', '', 'logo4', '', '', '2016-05-11 10:19:16', '2016-05-11 13:19:16', '', 50, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/logo4.png', 0, 'attachment', 'image/png', 0),
(52, 1, '2016-05-11 10:25:08', '2016-05-11 13:25:08', '', 'fotos1', '', 'inherit', 'open', 'closed', '', 'fotos1', '', '', '2016-05-11 10:25:08', '2016-05-11 13:25:08', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/fotos1.png', 0, 'attachment', 'image/png', 0),
(53, 1, '2016-05-11 10:25:09', '2016-05-11 13:25:09', '', 'fotos2', '', 'inherit', 'open', 'closed', '', 'fotos2', '', '', '2016-05-11 10:25:09', '2016-05-11 13:25:09', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/fotos2.png', 0, 'attachment', 'image/png', 0),
(54, 1, '2016-05-11 10:25:11', '2016-05-11 13:25:11', '', 'fotos3', '', 'inherit', 'open', 'closed', '', 'fotos3', '', '', '2016-05-11 10:25:11', '2016-05-11 13:25:11', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/fotos3.png', 0, 'attachment', 'image/png', 0),
(55, 1, '2016-05-11 10:25:12', '2016-05-11 13:25:12', '', 'fotos4', '', 'inherit', 'open', 'closed', '', 'fotos4', '', '', '2016-05-11 10:25:12', '2016-05-11 13:25:12', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/fotos4.png', 0, 'attachment', 'image/png', 0),
(56, 1, '2016-05-11 10:25:22', '2016-05-11 13:25:22', '', 'fotos1', '', 'inherit', 'open', 'closed', '', 'fotos1-2', '', '', '2016-05-11 10:25:22', '2016-05-11 13:25:22', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/fotos1-1.png', 0, 'attachment', 'image/png', 0),
(57, 1, '2016-05-11 10:25:23', '2016-05-11 13:25:23', '', 'fotos2', '', 'inherit', 'open', 'closed', '', 'fotos2-2', '', '', '2016-05-11 10:25:23', '2016-05-11 13:25:23', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/fotos2-1.png', 0, 'attachment', 'image/png', 0),
(58, 1, '2016-05-11 10:25:24', '2016-05-11 13:25:24', '', 'fotos3', '', 'inherit', 'open', 'closed', '', 'fotos3-2', '', '', '2016-05-11 10:25:24', '2016-05-11 13:25:24', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/fotos3-1.png', 0, 'attachment', 'image/png', 0),
(59, 1, '2016-05-11 10:25:25', '2016-05-11 13:25:25', '', 'fotos4', '', 'inherit', 'open', 'closed', '', 'fotos4-2', '', '', '2016-05-11 10:25:25', '2016-05-11 13:25:25', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/fotos4-1.png', 0, 'attachment', 'image/png', 0),
(60, 1, '2016-05-11 10:26:10', '2016-05-11 13:26:10', '', 'banner', '', 'inherit', 'open', 'closed', '', 'banner', '', '', '2016-05-11 10:26:10', '2016-05-11 13:26:10', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/banner.png', 0, 'attachment', 'image/png', 0),
(61, 1, '2016-05-12 09:41:45', '2016-05-12 12:41:45', '<div class=\"form-group\">\r\n    <label for=\"nome\" class=\"hidden\">*Nome completo</label>\r\n    [text*  nome id:nome placeholder \"Nome completo*\"]\r\n</div>\r\n\r\n<div class=\"form-group\">\r\n    <label for=\"email\" class=\"hidden\">*E-mail</label>\r\n    [email* email id:email placeholder \"E-mail*\" ]\r\n</div>\r\n\r\n<div class=\"form-group\">\r\n    <label for=\"tel\" class=\"hidden\">*Telefone</label>\r\n    [tel  tel id:tel placeholder \"Telefone*\"]\r\n</div>\r\n\r\n<div class=\"form-group mesagem\" style=\"border: none;\">\r\n    <label for=\"mensagem\" class=\"hidden\">*Mensagem</label>\r\n    [textarea*  mensagem id:mensagem placeholder \"Mensagem*\"]\r\n</div>\r\n\r\n[submit class:botao \"Enviar\"]\n1\nMensagem enviada pelo formulario de contato do site 3B Aquecedores\n[nome] <3baquecedores@palupa.com.br>\n3baquecedores@palupa.com.br\nDe: [nome] <[email]>\r\nTelefone: [tel]\r\n\r\nCorpo da mensagem:\r\n[mensagem]\r\n\r\n--\nReply-To: [email]\n\n\n\n\n3B Aquecedores \"[your-subject]\"\n3B Aquecedores <3baquecedores@palupa.com.br>\n[your-email]\nCorpo da mensagem:\r\n[your-message]\r\n\r\n--\r\nEste e-mail foi enviado de um formulário de contato em 3B Aquecedores (http://3baquecedores.pixd.com.br)\nReply-To: 3baquecedores@palupa.com.br\n\n\n\nObrigado pela sua mensagem. Ela foi enviada.\nHouve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde.\nUm ou mais campos têm um erro. Por favor verifique e tente novamente.\nHouve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde..\nVocê deve aceitar os termos e condições antes de enviar a sua mensagem.\nO campo é obrigatório .\nO campo é muito longo.\nO campo é muito curto.\nO formato da data é incorreta.\nA data é antes da primeira um permitido.\nA data é após o mais recente permitida.\nHouve um erro desconhecido upload do arquivo .\nVocê não tem permissão para fazer upload de arquivos deste tipo.\nO arquivo é muito grande.\nOcorreu um erro ao fazer o upload do arquivo.\nO formato de número é inválido.\nO número é menor do que o mínimo permitido .\nO número é maior do que o máximo permitido .\nA resposta ao questionário é incorrecto ..\nO código digitado está incorreto.\nO endereço de e -mail digitado é inválido.\nA URL é inválido.\nO número de telefone é inválido.', 'Contato', '', 'publish', 'closed', 'closed', '', 'contato', '', '', '2017-09-05 16:17:43', '2017-09-05 19:17:43', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=wpcf7_contact_form&#038;p=61', 0, 'wpcf7_contact_form', '', 0),
(62, 1, '2016-05-12 09:47:42', '2016-05-12 12:47:42', '', 'Contato', '', 'publish', 'closed', 'closed', '', 'contato', '', '', '2016-05-12 09:47:42', '2016-05-12 12:47:42', '', 0, 'http://3baquecedores.pixd.com.br/?page_id=62', 0, 'page', '', 0),
(63, 1, '2016-05-12 09:47:42', '2016-05-12 12:47:42', '', 'Contato', '', 'inherit', 'closed', 'closed', '', '62-revision-v1', '', '', '2016-05-12 09:47:42', '2016-05-12 12:47:42', '', 62, 'http://3baquecedores.pixd.com.br/2016/05/12/62-revision-v1/', 0, 'revision', '', 0),
(64, 1, '2016-05-12 10:47:43', '2016-05-12 13:47:43', '', 'Inicial', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2016-08-01 11:43:41', '2016-08-01 14:43:41', '', 0, 'http://3baquecedores.pixd.com.br/?page_id=64', 0, 'page', '', 0),
(65, 1, '2016-05-12 10:47:43', '2016-05-12 13:47:43', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '64-revision-v1', '', '', '2016-05-12 10:47:43', '2016-05-12 13:47:43', '', 64, 'http://3baquecedores.pixd.com.br/2016/05/12/64-revision-v1/', 0, 'revision', '', 0),
(66, 1, '2016-05-12 11:04:42', '2016-05-12 14:04:42', '', 'Quem Somos', '', 'publish', 'closed', 'closed', '', 'quem-somos', '', '', '2016-07-29 11:29:34', '2016-07-29 14:29:34', '', 0, 'http://3baquecedores.pixd.com.br/?page_id=66', 0, 'page', '', 0),
(67, 1, '2016-05-12 11:04:42', '2016-05-12 14:04:42', '', 'Quem Somos', '', 'inherit', 'closed', 'closed', '', '66-revision-v1', '', '', '2016-05-12 11:04:42', '2016-05-12 14:04:42', '', 66, 'http://3baquecedores.pixd.com.br/2016/05/12/66-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2016-05-12 11:07:38', '2016-05-12 14:07:38', '', 'arrows (1)', '', 'inherit', 'open', 'closed', '', 'arrows-1', '', '', '2016-05-12 11:07:38', '2016-05-12 14:07:38', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/arrows-1.png', 0, 'attachment', 'image/png', 0),
(69, 1, '2016-05-12 11:07:39', '2016-05-12 14:07:39', '', 'banner', '', 'inherit', 'open', 'closed', '', 'banner-2', '', '', '2016-05-12 11:07:39', '2016-05-12 14:07:39', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/banner-1.png', 0, 'attachment', 'image/png', 0),
(70, 1, '2016-05-12 11:07:41', '2016-05-12 14:07:41', '', 'bg-assistencia', '', 'inherit', 'open', 'closed', '', 'bg-assistencia', '', '', '2016-05-12 11:07:41', '2016-05-12 14:07:41', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/bg-assistencia.png', 0, 'attachment', 'image/png', 0),
(71, 1, '2016-05-12 11:07:42', '2016-05-12 14:07:42', '', 'bg-depoimentos', '', 'inherit', 'open', 'closed', '', 'bg-depoimentos', '', '', '2016-05-12 11:07:42', '2016-05-12 14:07:42', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/bg-depoimentos.png', 0, 'attachment', 'image/png', 0),
(72, 1, '2016-05-12 11:07:44', '2016-05-12 14:07:44', '', 'bg-rodape', '', 'inherit', 'open', 'closed', '', 'bg-rodape', '', '', '2016-05-12 11:07:44', '2016-05-12 14:07:44', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/bg-rodape.png', 0, 'attachment', 'image/png', 0),
(73, 1, '2016-05-12 11:07:45', '2016-05-12 14:07:45', '', 'blog1', '', 'inherit', 'open', 'closed', '', 'blog1', '', '', '2016-05-12 11:07:45', '2016-05-12 14:07:45', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/blog1.png', 0, 'attachment', 'image/png', 0),
(74, 1, '2016-05-12 11:07:46', '2016-05-12 14:07:46', '', 'blog2', '', 'inherit', 'open', 'closed', '', 'blog2', '', '', '2016-05-12 11:07:46', '2016-05-12 14:07:46', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/blog2.png', 0, 'attachment', 'image/png', 0),
(75, 1, '2016-05-12 11:07:47', '2016-05-12 14:07:47', '', 'blog3', '', 'inherit', 'open', 'closed', '', 'blog3', '', '', '2016-05-12 11:07:47', '2016-05-12 14:07:47', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/blog3.png', 0, 'attachment', 'image/png', 0),
(76, 1, '2016-05-12 11:07:48', '2016-05-12 14:07:48', '', 'contato1', '', 'inherit', 'open', 'closed', '', 'contato1', '', '', '2016-05-12 11:07:48', '2016-05-12 14:07:48', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/contato1.jpg', 0, 'attachment', 'image/jpeg', 0),
(77, 1, '2016-05-12 11:07:49', '2016-05-12 14:07:49', '', 'depoimento', '', 'inherit', 'open', 'closed', '', 'depoimento', '', '', '2016-05-12 11:07:49', '2016-05-12 14:07:49', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/depoimento.png', 0, 'attachment', 'image/png', 0),
(78, 1, '2016-05-12 11:07:51', '2016-05-12 14:07:51', '', 'depoimento2', '', 'inherit', 'open', 'closed', '', 'depoimento2', '', '', '2016-05-12 11:07:51', '2016-05-12 14:07:51', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/depoimento2.png', 0, 'attachment', 'image/png', 0),
(79, 1, '2016-05-12 11:07:52', '2016-05-12 14:07:52', '', 'foto1', '', 'inherit', 'open', 'closed', '', 'foto1', '', '', '2016-05-12 11:07:52', '2016-05-12 14:07:52', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto1.png', 0, 'attachment', 'image/png', 0),
(80, 1, '2016-05-12 11:07:54', '2016-05-12 14:07:54', '', 'foto2', '', 'inherit', 'open', 'closed', '', 'foto2', '', '', '2016-05-12 11:07:54', '2016-05-12 14:07:54', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto2.png', 0, 'attachment', 'image/png', 0),
(81, 1, '2016-05-12 11:07:55', '2016-05-12 14:07:55', '', 'foto3', '', 'inherit', 'open', 'closed', '', 'foto3', '', '', '2016-05-12 11:07:55', '2016-05-12 14:07:55', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto3.png', 0, 'attachment', 'image/png', 0),
(82, 1, '2016-05-12 11:07:56', '2016-05-12 14:07:56', '', 'foto-blog', '', 'inherit', 'open', 'closed', '', 'foto-blog', '', '', '2016-05-12 11:07:56', '2016-05-12 14:07:56', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto-blog.png', 0, 'attachment', 'image/png', 0),
(83, 1, '2016-05-12 11:07:57', '2016-05-12 14:07:57', '', 'foto-destaque', '', 'inherit', 'open', 'closed', '', 'foto-destaque-2', '', '', '2016-05-12 11:07:57', '2016-05-12 14:07:57', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto-destaque-1.png', 0, 'attachment', 'image/png', 0),
(84, 1, '2016-05-12 11:07:59', '2016-05-12 14:07:59', '', 'foto-produto', '', 'inherit', 'open', 'closed', '', 'foto-produto-2', '', '', '2016-05-12 11:07:59', '2016-05-12 14:07:59', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto-produto-1.png', 0, 'attachment', 'image/png', 0),
(85, 1, '2016-05-12 11:08:00', '2016-05-12 14:08:00', '', 'foto-que-fazemos', '', 'inherit', 'open', 'closed', '', 'foto-que-fazemos-2', '', '', '2016-05-12 11:08:00', '2016-05-12 14:08:00', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto-que-fazemos-1.png', 0, 'attachment', 'image/png', 0),
(86, 1, '2016-05-12 11:08:02', '2016-05-12 14:08:02', '', 'foto-quemsomos', '', 'inherit', 'open', 'closed', '', 'foto-quemsomos', '', '', '2016-05-12 11:08:02', '2016-05-12 14:08:02', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto-quemsomos.png', 0, 'attachment', 'image/png', 0),
(87, 1, '2016-05-12 11:08:03', '2016-05-12 14:08:03', '', 'fotos1', '', 'inherit', 'open', 'closed', '', 'fotos1-3', '', '', '2016-05-12 11:08:03', '2016-05-12 14:08:03', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/fotos1-2.png', 0, 'attachment', 'image/png', 0),
(88, 1, '2016-05-12 11:08:04', '2016-05-12 14:08:04', '', 'fotos2', '', 'inherit', 'open', 'closed', '', 'fotos2-3', '', '', '2016-05-12 11:08:04', '2016-05-12 14:08:04', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/fotos2-2.png', 0, 'attachment', 'image/png', 0),
(89, 1, '2016-05-12 11:08:05', '2016-05-12 14:08:05', '', 'fotos3', '', 'inherit', 'open', 'closed', '', 'fotos3-3', '', '', '2016-05-12 11:08:05', '2016-05-12 14:08:05', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/fotos3-2.png', 0, 'attachment', 'image/png', 0),
(90, 1, '2016-05-12 11:08:06', '2016-05-12 14:08:06', '', 'fotos4', '', 'inherit', 'open', 'closed', '', 'fotos4-3', '', '', '2016-05-12 11:08:06', '2016-05-12 14:08:06', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/fotos4-2.png', 0, 'attachment', 'image/png', 0),
(91, 1, '2016-05-12 11:08:07', '2016-05-12 14:08:07', '', 'fundo-rodape', '', 'inherit', 'open', 'closed', '', 'fundo-rodape', '', '', '2016-05-12 11:08:07', '2016-05-12 14:08:07', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/fundo-rodape.png', 0, 'attachment', 'image/png', 0),
(92, 1, '2016-05-12 11:08:08', '2016-05-12 14:08:08', '', 'hover', '', 'inherit', 'open', 'closed', '', 'hover', '', '', '2016-05-12 11:08:08', '2016-05-12 14:08:08', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/hover.png', 0, 'attachment', 'image/png', 0),
(93, 1, '2016-05-12 11:08:09', '2016-05-12 14:08:09', '', 'logo1', '', 'inherit', 'open', 'closed', '', 'logo1-2', '', '', '2016-05-12 11:08:09', '2016-05-12 14:08:09', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/logo1-1.png', 0, 'attachment', 'image/png', 0),
(94, 1, '2016-05-12 11:08:10', '2016-05-12 14:08:10', '', 'logo2', '', 'inherit', 'open', 'closed', '', 'logo2-2', '', '', '2016-05-12 11:08:10', '2016-05-12 14:08:10', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/logo2-1.png', 0, 'attachment', 'image/png', 0),
(95, 1, '2016-05-12 11:08:11', '2016-05-12 14:08:11', '', 'logo3', '', 'inherit', 'open', 'closed', '', 'logo3-2', '', '', '2016-05-12 11:08:11', '2016-05-12 14:08:11', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/logo3-1.png', 0, 'attachment', 'image/png', 0),
(96, 1, '2016-05-12 11:08:12', '2016-05-12 14:08:12', '', 'logo3b', '', 'inherit', 'open', 'closed', '', 'logo3b', '', '', '2016-05-12 11:08:12', '2016-05-12 14:08:12', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/logo3b.png', 0, 'attachment', 'image/png', 0),
(97, 1, '2016-05-12 11:08:13', '2016-05-12 14:08:13', '', 'logo4', '', 'inherit', 'open', 'closed', '', 'logo4-2', '', '', '2016-05-12 11:08:13', '2016-05-12 14:08:13', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/logo4-1.png', 0, 'attachment', 'image/png', 0),
(98, 1, '2016-05-12 11:08:14', '2016-05-12 14:08:14', '', 'logo-contato', '', 'inherit', 'open', 'closed', '', 'logo-contato', '', '', '2016-05-12 11:08:14', '2016-05-12 14:08:14', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/logo-contato.png', 0, 'attachment', 'image/png', 0),
(99, 1, '2016-05-12 11:08:15', '2016-05-12 14:08:15', '', 'logo-marca', '', 'inherit', 'open', 'closed', '', 'logo-marca', '', '', '2016-05-12 11:08:15', '2016-05-12 14:08:15', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/logo-marca.png', 0, 'attachment', 'image/png', 0),
(100, 1, '2016-05-12 11:08:16', '2016-05-12 14:08:16', '', 'logo-rodape', '', 'inherit', 'open', 'closed', '', 'logo-rodape', '', '', '2016-05-12 11:08:16', '2016-05-12 14:08:16', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/logo-rodape.png', 0, 'attachment', 'image/png', 0),
(101, 1, '2016-05-12 11:08:18', '2016-05-12 14:08:18', '', 'marketing', '', 'inherit', 'open', 'closed', '', 'marketing', '', '', '2016-05-12 11:08:18', '2016-05-12 14:08:18', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/marketing.png', 0, 'attachment', 'image/png', 0),
(102, 1, '2016-05-12 11:08:19', '2016-05-12 14:08:19', '', 'miniatura', '', 'inherit', 'open', 'closed', '', 'miniatura', '', '', '2016-05-12 11:08:19', '2016-05-12 14:08:19', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/miniatura.png', 0, 'attachment', 'image/png', 0),
(103, 1, '2016-05-12 11:08:20', '2016-05-12 14:08:20', '', 'miniatura2', '', 'inherit', 'open', 'closed', '', 'miniatura2', '', '', '2016-05-12 11:08:20', '2016-05-12 14:08:20', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/miniatura2.png', 0, 'attachment', 'image/png', 0),
(104, 1, '2016-05-12 11:08:22', '2016-05-12 14:08:22', '', 'money-increase', '', 'inherit', 'open', 'closed', '', 'money-increase', '', '', '2016-05-12 11:08:22', '2016-05-12 14:08:22', '', 66, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/money-increase.png', 0, 'attachment', 'image/png', 0),
(105, 1, '2016-05-12 11:09:13', '2016-05-12 14:09:13', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical', 'Quem Somos', '', 'inherit', 'closed', 'closed', '', '66-revision-v1', '', '', '2016-05-12 11:09:13', '2016-05-12 14:09:13', '', 66, 'http://3baquecedores.pixd.com.br/2016/05/12/66-revision-v1/', 0, 'revision', '', 0),
(106, 1, '2016-05-12 11:38:36', '2016-05-12 14:38:36', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2016-05-12 11:38:36', '2016-05-12 14:38:36', '', 0, 'http://3baquecedores.pixd.com.br/?page_id=106', 0, 'page', '', 0),
(107, 1, '2016-05-12 11:38:36', '2016-05-12 14:38:36', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '106-revision-v1', '', '', '2016-05-12 11:38:36', '2016-05-12 14:38:36', '', 106, 'http://3baquecedores.pixd.com.br/2016/05/12/106-revision-v1/', 0, 'revision', '', 0),
(109, 1, '2016-05-12 11:40:30', '2016-05-12 14:40:30', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Aquecedores pequenos para uso em casa?', '', 'publish', 'open', 'open', '', 'aquecedores-pequenos-para-uso-em-casa', '', '', '2016-05-12 11:40:30', '2016-05-12 14:40:30', '', 0, 'http://3baquecedores.pixd.com.br/?p=109', 12, 'post', '', 0),
(110, 1, '2016-05-12 11:40:30', '2016-05-12 14:40:30', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Aquecedores pequenos para uso em casa?', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2016-05-12 11:40:30', '2016-05-12 14:40:30', '', 109, 'http://3baquecedores.pixd.com.br/2016/05/12/109-revision-v1/', 0, 'revision', '', 0),
(111, 1, '2016-05-12 11:41:40', '2016-05-12 14:41:40', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'publish', 'open', 'open', '', 'necaucibus-ipsum-pulvinar-atin-tincidunts', '', '', '2016-05-12 12:14:14', '2016-05-12 15:14:14', '', 0, 'http://3baquecedores.pixd.com.br/?p=111', 3, 'post', '', 0),
(112, 1, '2016-05-12 11:41:40', '2016-05-12 14:41:40', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '111-revision-v1', '', '', '2016-05-12 11:41:40', '2016-05-12 14:41:40', '', 111, 'http://3baquecedores.pixd.com.br/2016/05/12/111-revision-v1/', 0, 'revision', '', 0),
(113, 1, '2016-05-12 11:42:28', '2016-05-12 14:42:28', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'publish', 'open', 'open', '', 'necaucibus-ipsum-pulvinar-atin-tincidunts-2', '', '', '2016-05-12 12:14:34', '2016-05-12 15:14:34', '', 0, 'http://3baquecedores.pixd.com.br/?p=113', 9, 'post', '', 0),
(116, 1, '2016-05-12 11:42:28', '2016-05-12 14:42:28', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '113-revision-v1', '', '', '2016-05-12 11:42:28', '2016-05-12 14:42:28', '', 113, 'http://3baquecedores.pixd.com.br/2016/05/12/113-revision-v1/', 0, 'revision', '', 0),
(117, 1, '2016-05-12 11:42:48', '2016-05-12 14:42:48', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'publish', 'open', 'open', '', 'necaucibus-ipsum-pulvinar-atin-tincidunts-2-2', '', '', '2016-05-12 12:14:29', '2016-05-12 15:14:29', '', 0, 'http://3baquecedores.pixd.com.br/2016/05/12/necaucibus-ipsum-pulvinar-atin-tincidunts-3/', 7, 'post', '', 0),
(118, 1, '2016-05-12 11:42:48', '2016-05-12 14:42:48', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '117-revision-v1', '', '', '2016-05-12 11:42:48', '2016-05-12 14:42:48', '', 117, 'http://3baquecedores.pixd.com.br/2016/05/12/117-revision-v1/', 0, 'revision', '', 0),
(119, 1, '2016-05-12 11:42:55', '2016-05-12 14:42:55', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'publish', 'open', 'open', '', 'necaucibus-ipsum-pulvinar-atin-tincidunts-3', '', '', '2016-05-12 12:14:37', '2016-05-12 15:14:37', '', 0, 'http://3baquecedores.pixd.com.br/2016/05/12/necaucibus-ipsum-pulvinar-atin-tincidunts-3/', 10, 'post', '', 0),
(120, 1, '2016-05-12 11:42:56', '2016-05-12 14:42:56', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '119-revision-v1', '', '', '2016-05-12 11:42:56', '2016-05-12 14:42:56', '', 119, 'http://3baquecedores.pixd.com.br/2016/05/12/119-revision-v1/', 0, 'revision', '', 0),
(121, 1, '2016-05-12 11:43:00', '2016-05-12 14:43:00', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Aquecedores pequenos para uso em casa?', '', 'publish', 'open', 'open', '', 'aquecedores-pequenos-para-uso-em-casa-2', '', '', '2016-05-12 12:14:22', '2016-05-12 15:14:22', '', 0, 'http://3baquecedores.pixd.com.br/2016/05/12/aquecedores-pequenos-para-uso-em-casa-2/', 5, 'post', '', 0),
(122, 1, '2016-05-12 11:43:01', '2016-05-12 14:43:01', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Aquecedores pequenos para uso em casa?', '', 'inherit', 'closed', 'closed', '', '121-revision-v1', '', '', '2016-05-12 11:43:01', '2016-05-12 14:43:01', '', 121, 'http://3baquecedores.pixd.com.br/2016/05/12/121-revision-v1/', 0, 'revision', '', 0),
(123, 1, '2016-05-12 11:43:05', '2016-05-12 14:43:05', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Aquecedores pequenos para uso em casa?', '', 'publish', 'open', 'open', '', 'aquecedores-pequenos-para-uso-em-casa-3', '', '', '2016-05-12 12:14:08', '2016-05-12 15:14:08', '', 0, 'http://3baquecedores.pixd.com.br/2016/05/12/aquecedores-pequenos-para-uso-em-casa-3/', 1, 'post', '', 0),
(124, 1, '2016-05-12 11:43:06', '2016-05-12 14:43:06', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Aquecedores pequenos para uso em casa?', '', 'inherit', 'closed', 'closed', '', '123-revision-v1', '', '', '2016-05-12 11:43:06', '2016-05-12 14:43:06', '', 123, 'http://3baquecedores.pixd.com.br/2016/05/12/123-revision-v1/', 0, 'revision', '', 0),
(125, 1, '2016-05-12 11:43:11', '2016-05-12 14:43:11', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Aquecedores pequenos para uso em casa?', '', 'publish', 'open', 'open', '', 'aquecedores-pequenos-para-uso-em-casa-4', '', '', '2016-05-12 12:14:12', '2016-05-12 15:14:12', '', 0, 'http://3baquecedores.pixd.com.br/2016/05/12/aquecedores-pequenos-para-uso-em-casa-4/', 2, 'post', '', 0),
(126, 1, '2016-05-12 11:43:11', '2016-05-12 14:43:11', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Aquecedores pequenos para uso em casa?', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2016-05-12 11:43:11', '2016-05-12 14:43:11', '', 125, 'http://3baquecedores.pixd.com.br/2016/05/12/125-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2016-05-12 11:43:17', '2016-05-12 14:43:17', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'publish', 'open', 'open', '', 'necaucibus-ipsum-pulvinar-atin-tincidunts-4', '', '', '2016-05-12 12:14:05', '2016-05-12 15:14:05', '', 0, 'http://3baquecedores.pixd.com.br/2016/05/12/necaucibus-ipsum-pulvinar-atin-tincidunts-4/', 0, 'post', '', 0),
(128, 1, '2016-05-12 11:43:17', '2016-05-12 14:43:17', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2016-05-12 11:43:17', '2016-05-12 14:43:17', '', 127, 'http://3baquecedores.pixd.com.br/2016/05/12/127-revision-v1/', 0, 'revision', '', 0),
(129, 1, '2016-05-12 11:43:22', '2016-05-12 14:43:22', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'publish', 'open', 'open', '', 'necaucibus-ipsum-pulvinar-atin-tincidunts-2-2-2', '', '', '2016-05-12 12:14:32', '2016-05-12 15:14:32', '', 0, 'http://3baquecedores.pixd.com.br/2016/05/12/necaucibus-ipsum-pulvinar-atin-tincidunts-5/', 8, 'post', '', 0),
(130, 1, '2016-05-12 11:43:23', '2016-05-12 14:43:23', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '129-revision-v1', '', '', '2016-05-12 11:43:23', '2016-05-12 14:43:23', '', 129, 'http://3baquecedores.pixd.com.br/2016/05/12/129-revision-v1/', 0, 'revision', '', 0),
(131, 1, '2016-05-12 11:43:28', '2016-05-12 14:43:28', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Aquecedores pequenos para uso em casa?', '', 'publish', 'open', 'open', '', 'aquecedores-pequenos-para-uso-em-casa-2-2', '', '', '2016-05-12 11:43:28', '2016-05-12 14:43:28', '', 0, 'http://3baquecedores.pixd.com.br/2016/05/12/aquecedores-pequenos-para-uso-em-casa-5/', 13, 'post', '', 0),
(132, 1, '2016-05-12 11:43:28', '2016-05-12 14:43:28', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Aquecedores pequenos para uso em casa?', '', 'inherit', 'closed', 'closed', '', '131-revision-v1', '', '', '2016-05-12 11:43:28', '2016-05-12 14:43:28', '', 131, 'http://3baquecedores.pixd.com.br/2016/05/12/131-revision-v1/', 0, 'revision', '', 0),
(133, 1, '2016-05-12 11:43:34', '2016-05-12 14:43:34', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'publish', 'open', 'open', '', 'necaucibus-ipsum-pulvinar-atin-tincidunts-4-2', '', '', '2016-05-12 12:14:25', '2016-05-12 15:14:25', '', 0, 'http://3baquecedores.pixd.com.br/2016/05/12/necaucibus-ipsum-pulvinar-atin-tincidunts-5/', 6, 'post', '', 0),
(134, 1, '2016-05-12 11:43:34', '2016-05-12 14:43:34', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '133-revision-v1', '', '', '2016-05-12 11:43:34', '2016-05-12 14:43:34', '', 133, 'http://3baquecedores.pixd.com.br/2016/05/12/133-revision-v1/', 0, 'revision', '', 0),
(135, 1, '2016-05-12 11:43:39', '2016-05-12 14:43:39', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'publish', 'open', 'open', '', 'necaucibus-ipsum-pulvinar-atin-tincidunts-5', '', '', '2016-05-12 12:14:40', '2016-05-12 15:14:40', '', 0, 'http://3baquecedores.pixd.com.br/2016/05/12/necaucibus-ipsum-pulvinar-atin-tincidunts-5/', 11, 'post', '', 0),
(136, 1, '2016-05-12 11:43:40', '2016-05-12 14:43:40', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '135-revision-v1', '', '', '2016-05-12 11:43:40', '2016-05-12 14:43:40', '', 135, 'http://3baquecedores.pixd.com.br/2016/05/12/135-revision-v1/', 0, 'revision', '', 0),
(137, 1, '2016-05-12 11:43:46', '2016-05-12 14:43:46', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'publish', 'open', 'open', '', 'necaucibus-ipsum-pulvinar-atin-tincidunts-2-2-2-2', '', '', '2016-05-12 12:14:17', '2016-05-12 15:14:17', '', 0, 'http://3baquecedores.pixd.com.br/2016/05/12/necaucibus-ipsum-pulvinar-atin-tincidunts-6/', 4, 'post', '', 0),
(138, 1, '2016-05-12 11:43:46', '2016-05-12 14:43:46', 'Contrary to popular belief, Lorem Ipsum is not simply random text.', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '137-revision-v1', '', '', '2016-05-12 11:43:46', '2016-05-12 14:43:46', '', 137, 'http://3baquecedores.pixd.com.br/2016/05/12/137-revision-v1/', 0, 'revision', '', 0),
(139, 1, '2016-05-12 12:14:05', '2016-05-12 15:14:05', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2016-05-12 12:14:05', '2016-05-12 15:14:05', '', 127, 'http://3baquecedores.pixd.com.br/2016/05/12/127-revision-v1/', 0, 'revision', '', 0),
(140, 1, '2016-05-12 12:14:08', '2016-05-12 15:14:08', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Aquecedores pequenos para uso em casa?', '', 'inherit', 'closed', 'closed', '', '123-revision-v1', '', '', '2016-05-12 12:14:08', '2016-05-12 15:14:08', '', 123, 'http://3baquecedores.pixd.com.br/2016/05/12/123-revision-v1/', 0, 'revision', '', 0),
(141, 1, '2016-05-12 12:14:12', '2016-05-12 15:14:12', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Aquecedores pequenos para uso em casa?', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2016-05-12 12:14:12', '2016-05-12 15:14:12', '', 125, 'http://3baquecedores.pixd.com.br/2016/05/12/125-revision-v1/', 0, 'revision', '', 0),
(142, 1, '2016-05-12 12:14:14', '2016-05-12 15:14:14', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '111-revision-v1', '', '', '2016-05-12 12:14:14', '2016-05-12 15:14:14', '', 111, 'http://3baquecedores.pixd.com.br/2016/05/12/111-revision-v1/', 0, 'revision', '', 0),
(143, 1, '2016-05-12 12:14:17', '2016-05-12 15:14:17', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '137-revision-v1', '', '', '2016-05-12 12:14:17', '2016-05-12 15:14:17', '', 137, 'http://3baquecedores.pixd.com.br/2016/05/12/137-revision-v1/', 0, 'revision', '', 0),
(144, 1, '2016-05-12 12:14:20', '2016-05-12 15:14:20', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Aquecedores pequenos para uso em casa?', '', 'inherit', 'closed', 'closed', '', '121-revision-v1', '', '', '2016-05-12 12:14:20', '2016-05-12 15:14:20', '', 121, 'http://3baquecedores.pixd.com.br/2016/05/12/121-revision-v1/', 0, 'revision', '', 0),
(145, 1, '2016-05-12 12:14:25', '2016-05-12 15:14:25', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '133-revision-v1', '', '', '2016-05-12 12:14:25', '2016-05-12 15:14:25', '', 133, 'http://3baquecedores.pixd.com.br/2016/05/12/133-revision-v1/', 0, 'revision', '', 0),
(146, 1, '2016-05-12 12:14:29', '2016-05-12 15:14:29', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '117-revision-v1', '', '', '2016-05-12 12:14:29', '2016-05-12 15:14:29', '', 117, 'http://3baquecedores.pixd.com.br/2016/05/12/117-revision-v1/', 0, 'revision', '', 0),
(147, 1, '2016-05-12 12:14:32', '2016-05-12 15:14:32', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '129-revision-v1', '', '', '2016-05-12 12:14:32', '2016-05-12 15:14:32', '', 129, 'http://3baquecedores.pixd.com.br/2016/05/12/129-revision-v1/', 0, 'revision', '', 0),
(148, 1, '2016-05-12 12:14:34', '2016-05-12 15:14:34', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '113-revision-v1', '', '', '2016-05-12 12:14:34', '2016-05-12 15:14:34', '', 113, 'http://3baquecedores.pixd.com.br/2016/05/12/113-revision-v1/', 0, 'revision', '', 0),
(149, 1, '2016-05-12 12:14:37', '2016-05-12 15:14:37', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '119-revision-v1', '', '', '2016-05-12 12:14:37', '2016-05-12 15:14:37', '', 119, 'http://3baquecedores.pixd.com.br/2016/05/12/119-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `3b_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(150, 1, '2016-05-12 12:14:40', '2016-05-12 15:14:40', 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum', 'Necaucibus ipsum pulvinar atIn tincidunts', '', 'inherit', 'closed', 'closed', '', '135-revision-v1', '', '', '2016-05-12 12:14:40', '2016-05-12 15:14:40', '', 135, 'http://3baquecedores.pixd.com.br/2016/05/12/135-revision-v1/', 0, 'revision', '', 0),
(153, 1, '2016-05-12 14:45:05', '2016-05-12 17:45:05', '', 'João', '', 'publish', 'closed', 'closed', '', 'rascunho-automatico-2', '', '', '2016-05-12 14:45:06', '2016-05-12 17:45:06', '', 0, 'http://3baquecedores.pixd.com.br/depoimento/joao/', 0, 'depoimento', '', 0),
(154, 1, '2016-05-12 14:45:23', '2016-05-12 17:45:23', '', 'João 2', '', 'publish', 'closed', 'closed', '', 'joao-2-2', '', '', '2016-05-16 10:47:40', '2016-05-16 13:47:40', '', 0, 'http://3baquecedores.pixd.com.br/depoimento/joao-2-2/', 0, 'depoimento', '', 0),
(155, 1, '2016-05-12 14:56:02', '2016-05-12 17:56:02', '', 'Mantenha seu banho sempre no clima perfeito', '', 'publish', 'closed', 'closed', '', 'assistencia-tecnica', '', '', '2016-10-18 12:55:21', '2016-10-18 14:55:21', '', 0, 'http://3baquecedores.pixd.com.br/?page_id=155', 0, 'page', '', 0),
(156, 1, '2016-05-12 14:56:02', '2016-05-12 17:56:02', '', 'Assistência Técnica', '', 'inherit', 'closed', 'closed', '', '155-revision-v1', '', '', '2016-05-12 14:56:02', '2016-05-12 17:56:02', '', 155, 'http://3baquecedores.pixd.com.br/2016/05/12/155-revision-v1/', 0, 'revision', '', 0),
(157, 1, '2016-05-12 14:57:59', '2016-05-12 17:57:59', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin', 'Assistência Técnica', '', 'inherit', 'closed', 'closed', '', '155-revision-v1', '', '', '2016-05-12 14:57:59', '2016-05-12 17:57:59', '', 155, 'http://3baquecedores.pixd.com.br/2016/05/12/155-revision-v1/', 0, 'revision', '', 0),
(158, 1, '2016-05-12 15:12:18', '2016-05-12 18:12:18', '<div class=\"form-group\"><label class=\"hidden\" for=\"nome\">Nome</label>[text* Nome id:Nome class:form-control placeholder \"*Nome\"]</div><div class=\"form-group\"><label class=\"hidden\" for=\"nome\">Nome</label>[email* email id:email class:form-control akismet:author_email \"*E-mail\"]</div><div class=\"form-group\"><label class=\"hidden\" for=\"nome\">Nome</label>[text* Empresa id:Empresa class:form-control placeholder \"*Empresa\"]</div><div class=\"form-group\"><label class=\"hidden\" for=\"nome\">Nome</label>[tel* Telefone id:Telefone class:form-control placeholder \"*Telefone\"]</div>\r\n<div class=\"form-group\"><label class=\"hidden\" for=\"nome\">Nome</label>[text Cidade id:Cidade class:form-control placeholder \"Cidade\"]</div><div class=\"form-group\"><label class=\"hidden\" for=\"nome\">Nome</label>[text CEP id:CEP class:form-control placeholder \"CEP\"]</div>\r\n<div class=\"form-group-mensagem\"><label class=\"\" for=\"nome\">*Descrição</label>[textarea* Mensagem id:Mensagem class:Mensagem ]</div>[submit id:enviar class:enviar \"Enviar\"]\n1\nMensagem enviada pelo formulario de Assistência técnica do site 3B Aquecedores\n[Nome] <3baquecedores@palupa.com.br>\n3baquecedores@palupa.com.br\nDe:[Nome]\r\nEmail:[email]\r\nEmpresa:[Empresa]\r\nTelefone:[Telefone]\r\nCidade[Cidade]\r\nCep:[CEP]\r\nProdução/Solução:[ProduoSoluo]\r\n\r\nCorpo da mensagem:\r\n[Mensagem]\r\n\r\n--\r\nEste e-mail foi enviado de um formulário de Assistência técnica em 3B Aquecedores\nReply-To: [email]\n\n\n\n\n3B Aquecedores \"[your-subject]\"\n3B Aquecedores <3baquecedores@palupa.com.br>\n[your-email]\nCorpo da mensagem:\r\n[your-message]\r\n\r\n--\r\nEste e-mail foi enviado de um formulário de contato em 3B Aquecedores (http://3baquecedores.pixd.com.br)\nReply-To: 3baquecedores@palupa.com.br\n\n\n\nObrigado pela sua mensagem. Ela foi enviada.\nHouve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde.\nUm ou mais campos têm um erro. Por favor verifique e tente novamente.\nHouve um erro ao tentar enviar a sua mensagem. Por favor, tente novamente mais tarde..\nVocê deve aceitar os termos e condições antes de enviar a sua mensagem.\nO campo é obrigatório .\nO campo é muito longo.\nO campo é muito curto.\nO formato da data é incorreta.\nA data é antes da primeira um permitido.\nA data é após o mais recente permitida.\nHouve um erro desconhecido upload do arquivo .\nVocê não tem permissão para fazer upload de arquivos deste tipo.\nO arquivo é muito grande.\nOcorreu um erro ao fazer o upload do arquivo.\nO formato de número é inválido.\nO número é menor do que o mínimo permitido .\nO número é maior do que o máximo permitido .\nA resposta ao questionário é incorrecto ..\nO código digitado está incorreto.\nO endereço de e -mail digitado é inválido.\nA URL é inválido.', 'Formulário Assistência técnica', '', 'publish', 'closed', 'closed', '', 'formulario-assistencia-tecnica', '', '', '2017-09-13 10:03:45', '2017-09-13 13:03:45', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=wpcf7_contact_form&#038;p=158', 0, 'wpcf7_contact_form', '', 0),
(160, 1, '2016-05-12 15:30:25', '2016-05-12 18:30:25', '', 'miniatura-1', '', 'inherit', 'open', 'closed', '', 'miniatura-1', '', '', '2016-05-12 15:30:25', '2016-05-12 18:30:25', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/miniatura-1.png', 0, 'attachment', 'image/png', 0),
(161, 1, '2016-05-12 15:30:25', '2016-05-12 18:30:25', '', 'miniatura2-1', '', 'inherit', 'open', 'closed', '', 'miniatura2-1', '', '', '2016-05-12 15:30:25', '2016-05-12 18:30:25', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/miniatura2-1.png', 0, 'attachment', 'image/png', 0),
(162, 1, '2016-05-12 15:30:26', '2016-05-12 18:30:26', '', 'miniatura-2', '', 'inherit', 'open', 'closed', '', 'miniatura-2', '', '', '2016-05-12 15:30:26', '2016-05-12 18:30:26', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/miniatura-2.png', 0, 'attachment', 'image/png', 0),
(165, 1, '2016-05-12 16:55:45', '2016-05-12 19:55:45', '', 'foto-produto-2', '', 'inherit', 'open', 'closed', '', 'foto-produto-2-2', '', '', '2016-05-12 16:55:45', '2016-05-12 19:55:45', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto-produto-2.png', 0, 'attachment', 'image/png', 0),
(166, 1, '2016-05-12 16:57:37', '2016-05-12 19:57:37', '', 'miniatura2-2', '', 'inherit', 'open', 'closed', '', 'miniatura2-2', '', '', '2016-05-12 16:57:37', '2016-05-12 19:57:37', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/miniatura2-2.png', 0, 'attachment', 'image/png', 0),
(167, 1, '2016-05-12 16:57:38', '2016-05-12 19:57:38', '', 'miniatura-3', '', 'inherit', 'open', 'closed', '', 'miniatura-3', '', '', '2016-05-12 16:57:38', '2016-05-12 19:57:38', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/miniatura-3.png', 0, 'attachment', 'image/png', 0),
(168, 1, '2016-05-12 16:58:37', '2016-05-12 19:58:37', '', 'miniatura-4', '', 'inherit', 'open', 'closed', '', 'miniatura-4', '', '', '2016-05-12 16:58:37', '2016-05-12 19:58:37', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/miniatura-4.png', 0, 'attachment', 'image/png', 0),
(169, 1, '2016-05-12 17:02:52', '2016-05-12 20:02:52', '', 'miniatura-5', '', 'inherit', 'open', 'closed', '', 'miniatura-5', '', '', '2016-05-12 17:02:52', '2016-05-12 20:02:52', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/miniatura-5.png', 0, 'attachment', 'image/png', 0),
(170, 1, '2016-05-12 17:02:53', '2016-05-12 20:02:53', '', 'miniatura2-3', '', 'inherit', 'open', 'closed', '', 'miniatura2-3', '', '', '2016-05-12 17:02:53', '2016-05-12 20:02:53', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/miniatura2-3.png', 0, 'attachment', 'image/png', 0),
(171, 1, '2016-05-12 17:02:53', '2016-05-12 20:02:53', '', 'miniatura-6', '', 'inherit', 'open', 'closed', '', 'miniatura-6', '', '', '2016-05-12 17:02:53', '2016-05-12 20:02:53', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/miniatura-6.png', 0, 'attachment', 'image/png', 0),
(173, 1, '2016-05-12 17:05:57', '2016-05-12 20:05:57', '', 'miniatura-2', '', 'inherit', 'open', 'closed', '', 'miniatura-2-2', '', '', '2016-05-12 17:05:57', '2016-05-12 20:05:57', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2/miniatura-2-2/', 0, 'attachment', 'image/png', 0),
(174, 1, '2016-05-12 17:05:57', '2016-05-12 20:05:57', '', 'miniatura-1', '', 'inherit', 'open', 'closed', '', 'miniatura-1-2', '', '', '2016-05-12 17:05:57', '2016-05-12 20:05:57', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2/miniatura-1-2/', 0, 'attachment', 'image/png', 0),
(175, 1, '2016-05-12 17:05:57', '2016-05-12 20:05:57', '', 'miniatura2-1', '', 'inherit', 'open', 'closed', '', 'miniatura2-1-2', '', '', '2016-05-12 17:05:57', '2016-05-12 20:05:57', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2/miniatura2-1-2/', 0, 'attachment', 'image/png', 0),
(176, 1, '2016-05-12 17:05:57', '2016-05-12 20:05:57', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-2', '', '', '2016-05-12 17:05:57', '2016-05-12 20:05:57', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-2/', 0, 'attachment', 'image/jpeg', 0),
(178, 1, '2016-05-12 17:05:57', '2016-05-12 20:05:57', '', 'aquecedor-de-agua-a-gas', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-a-gas-2', '', '', '2016-05-12 17:05:57', '2016-05-12 20:05:57', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2/aquecedor-de-agua-a-gas-2/', 0, 'attachment', 'image/jpeg', 0),
(180, 1, '2016-05-12 17:05:58', '2016-05-12 20:05:58', '', 'miniatura-4', '', 'inherit', 'open', 'closed', '', 'miniatura-4-2', '', '', '2016-05-12 17:05:58', '2016-05-12 20:05:58', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2/miniatura-4-2/', 0, 'attachment', 'image/png', 0),
(181, 1, '2016-05-12 17:05:58', '2016-05-12 20:05:58', '', 'miniatura-3', '', 'inherit', 'open', 'closed', '', 'miniatura-3-2', '', '', '2016-05-12 17:05:58', '2016-05-12 20:05:58', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2/miniatura-3-2/', 0, 'attachment', 'image/png', 0),
(182, 1, '2016-05-12 17:05:58', '2016-05-12 20:05:58', '', 'miniatura2-2', '', 'inherit', 'open', 'closed', '', 'miniatura2-2-2', '', '', '2016-05-12 17:05:58', '2016-05-12 20:05:58', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2/miniatura2-2-2/', 0, 'attachment', 'image/png', 0),
(183, 1, '2016-05-12 17:05:59', '2016-05-12 20:05:59', '', 'foto-produto-2', '', 'inherit', 'open', 'closed', '', 'foto-produto-2-3', '', '', '2016-05-12 17:05:59', '2016-05-12 20:05:59', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2/foto-produto-2-3/', 0, 'attachment', 'image/png', 0),
(184, 1, '2016-05-12 17:05:59', '2016-05-12 20:05:59', '', 'miniatura2-3', '', 'inherit', 'open', 'closed', '', 'miniatura2-3-2', '', '', '2016-05-12 17:05:59', '2016-05-12 20:05:59', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3/miniatura2-3-2/', 0, 'attachment', 'image/png', 0),
(185, 1, '2016-05-12 17:05:59', '2016-05-12 20:05:59', '', 'miniatura-6', '', 'inherit', 'open', 'closed', '', 'miniatura-6-2', '', '', '2016-05-12 17:05:59', '2016-05-12 20:05:59', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3/miniatura-6-2/', 0, 'attachment', 'image/png', 0),
(186, 1, '2016-05-12 17:05:59', '2016-05-12 20:05:59', '', 'miniatura-5', '', 'inherit', 'open', 'closed', '', 'miniatura-5-2', '', '', '2016-05-12 17:05:59', '2016-05-12 20:05:59', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3/miniatura-5-2/', 0, 'attachment', 'image/png', 0),
(187, 1, '2016-05-12 17:05:59', '2016-05-12 20:05:59', '', 'produto-carrossel', '', 'inherit', 'open', 'closed', '', 'produto-carrossel-2', '', '', '2016-05-12 17:05:59', '2016-05-12 20:05:59', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3/produto-carrossel-2/', 0, 'attachment', 'image/png', 0),
(188, 1, '2016-05-12 17:06:00', '2016-05-12 20:06:00', '', 'foto-produto', '', 'inherit', 'open', 'closed', '', 'foto-produto-3', '', '', '2016-05-12 17:06:00', '2016-05-12 20:06:00', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3/foto-produto-3/', 0, 'attachment', 'image/png', 0),
(189, 1, '2016-05-12 17:06:00', '2016-05-12 20:06:00', '', 'produto', '', 'inherit', 'open', 'closed', '', 'produto-2', '', '', '2016-05-12 17:06:00', '2016-05-12 20:06:00', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3/produto-2/', 0, 'attachment', 'image/png', 0),
(192, 1, '2016-05-12 17:06:05', '2016-05-12 20:06:05', '', 'foto-produto', '', 'inherit', 'open', 'closed', '', 'foto-produto-4', '', '', '2016-05-12 17:06:05', '2016-05-12 20:06:05', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-2/foto-produto-4/', 0, 'attachment', 'image/png', 0),
(193, 1, '2016-05-12 17:06:05', '2016-05-12 20:06:05', '', 'produto', '', 'inherit', 'open', 'closed', '', 'produto-3', '', '', '2016-05-12 17:06:05', '2016-05-12 20:06:05', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-2/produto-3/', 0, 'attachment', 'image/png', 0),
(194, 1, '2016-05-12 17:06:05', '2016-05-12 20:06:05', '', 'miniatura2-3', '', 'inherit', 'open', 'closed', '', 'miniatura2-3-3', '', '', '2016-05-12 17:06:05', '2016-05-12 20:06:05', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-2/miniatura2-3-3/', 0, 'attachment', 'image/png', 0),
(195, 1, '2016-05-12 17:06:06', '2016-05-12 20:06:06', '', 'miniatura-6', '', 'inherit', 'open', 'closed', '', 'miniatura-6-3', '', '', '2016-05-12 17:06:06', '2016-05-12 20:06:06', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-2/miniatura-6-3/', 0, 'attachment', 'image/png', 0),
(197, 1, '2016-05-12 17:06:06', '2016-05-12 20:06:06', '', 'miniatura-5', '', 'inherit', 'open', 'closed', '', 'miniatura-5-3', '', '', '2016-05-12 17:06:06', '2016-05-12 20:06:06', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-2/miniatura-5-3/', 0, 'attachment', 'image/png', 0),
(198, 1, '2016-05-12 17:06:06', '2016-05-12 20:06:06', '', 'foto-produto-2', '', 'inherit', 'open', 'closed', '', 'foto-produto-2-4', '', '', '2016-05-12 17:06:06', '2016-05-12 20:06:06', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2-2/foto-produto-2-4/', 0, 'attachment', 'image/png', 0),
(199, 1, '2016-05-12 17:06:06', '2016-05-12 20:06:06', '', 'produto-carrossel', '', 'inherit', 'open', 'closed', '', 'produto-carrossel-3', '', '', '2016-05-12 17:06:06', '2016-05-12 20:06:06', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-2/produto-carrossel-3/', 0, 'attachment', 'image/png', 0),
(200, 1, '2016-05-12 17:06:06', '2016-05-12 20:06:06', '', 'miniatura-4', '', 'inherit', 'open', 'closed', '', 'miniatura-4-3', '', '', '2016-05-12 17:06:06', '2016-05-12 20:06:06', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2-2/miniatura-4-3/', 0, 'attachment', 'image/png', 0),
(201, 1, '2016-05-12 17:06:07', '2016-05-12 20:06:07', '', 'miniatura-3', '', 'inherit', 'open', 'closed', '', 'miniatura-3-3', '', '', '2016-05-12 17:06:07', '2016-05-12 20:06:07', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2-2/miniatura-3-3/', 0, 'attachment', 'image/png', 0),
(203, 1, '2016-05-12 17:06:07', '2016-05-12 20:06:07', '', 'miniatura2-2', '', 'inherit', 'open', 'closed', '', 'miniatura2-2-3', '', '', '2016-05-12 17:06:07', '2016-05-12 20:06:07', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2-2/miniatura2-2-3/', 0, 'attachment', 'image/png', 0),
(204, 1, '2016-05-12 17:06:07', '2016-05-12 20:06:07', '', 'miniatura-2', '', 'inherit', 'open', 'closed', '', 'miniatura-2-3', '', '', '2016-05-12 17:06:07', '2016-05-12 20:06:07', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-2/miniatura-2-3/', 0, 'attachment', 'image/png', 0),
(205, 1, '2016-05-12 17:06:07', '2016-05-12 20:06:07', '', 'miniatura-1', '', 'inherit', 'open', 'closed', '', 'miniatura-1-3', '', '', '2016-05-12 17:06:07', '2016-05-12 20:06:07', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-2/miniatura-1-3/', 0, 'attachment', 'image/png', 0),
(207, 1, '2016-05-12 17:06:07', '2016-05-12 20:06:07', '', 'miniatura2-1', '', 'inherit', 'open', 'closed', '', 'miniatura2-1-3', '', '', '2016-05-12 17:06:07', '2016-05-12 20:06:07', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-2/miniatura2-1-3/', 0, 'attachment', 'image/png', 0),
(208, 1, '2016-05-12 17:06:08', '2016-05-12 20:06:08', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-3', '', '', '2016-05-12 17:06:08', '2016-05-12 20:06:08', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-2/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-3/', 0, 'attachment', 'image/jpeg', 0),
(209, 1, '2016-05-12 17:06:08', '2016-05-12 20:06:08', '', 'miniatura-2', '', 'inherit', 'open', 'closed', '', 'miniatura-2-4', '', '', '2016-05-12 17:06:08', '2016-05-12 20:06:08', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-3/miniatura-2-4/', 0, 'attachment', 'image/png', 0),
(210, 1, '2016-05-12 17:06:08', '2016-05-12 20:06:08', '', 'aquecedor-de-agua-a-gas', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-a-gas-3', '', '', '2016-05-12 17:06:08', '2016-05-12 20:06:08', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-2/aquecedor-de-agua-a-gas-3/', 0, 'attachment', 'image/jpeg', 0),
(211, 1, '2016-05-12 17:06:08', '2016-05-12 20:06:08', '', 'miniatura-1', '', 'inherit', 'open', 'closed', '', 'miniatura-1-4', '', '', '2016-05-12 17:06:08', '2016-05-12 20:06:08', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-3/miniatura-1-4/', 0, 'attachment', 'image/png', 0),
(212, 1, '2016-05-12 17:06:08', '2016-05-12 20:06:08', '', 'miniatura2-1', '', 'inherit', 'open', 'closed', '', 'miniatura2-1-4', '', '', '2016-05-12 17:06:08', '2016-05-12 20:06:08', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-3/miniatura2-1-4/', 0, 'attachment', 'image/png', 0),
(213, 1, '2016-05-12 17:06:08', '2016-05-12 20:06:08', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-4', '', '', '2016-05-12 17:06:08', '2016-05-12 20:06:08', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-3/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-4/', 0, 'attachment', 'image/jpeg', 0),
(215, 1, '2016-05-12 17:06:09', '2016-05-12 20:06:09', '', 'aquecedor-de-agua-a-gas', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-a-gas-4', '', '', '2016-05-12 17:06:09', '2016-05-12 20:06:09', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-3/aquecedor-de-agua-a-gas-4/', 0, 'attachment', 'image/jpeg', 0),
(216, 1, '2016-05-12 17:06:09', '2016-05-12 20:06:09', '', 'miniatura-4', '', 'inherit', 'open', 'closed', '', 'miniatura-4-4', '', '', '2016-05-12 17:06:09', '2016-05-12 20:06:09', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-3/miniatura-4-4/', 0, 'attachment', 'image/png', 0),
(217, 1, '2016-05-12 17:06:09', '2016-05-12 20:06:09', '', 'miniatura-3', '', 'inherit', 'open', 'closed', '', 'miniatura-3-4', '', '', '2016-05-12 17:06:09', '2016-05-12 20:06:09', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-3/miniatura-3-4/', 0, 'attachment', 'image/png', 0),
(218, 1, '2016-05-12 17:06:09', '2016-05-12 20:06:09', '', 'miniatura2-2', '', 'inherit', 'open', 'closed', '', 'miniatura2-2-4', '', '', '2016-05-12 17:06:09', '2016-05-12 20:06:09', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-3/miniatura2-2-4/', 0, 'attachment', 'image/png', 0),
(219, 1, '2016-05-12 17:06:09', '2016-05-12 20:06:09', '', 'foto-produto-2', '', 'inherit', 'open', 'closed', '', 'foto-produto-2-5', '', '', '2016-05-12 17:06:09', '2016-05-12 20:06:09', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-3/foto-produto-2-5/', 0, 'attachment', 'image/png', 0),
(220, 1, '2016-05-12 17:06:10', '2016-05-12 20:06:10', '', 'miniatura2-3', '', 'inherit', 'open', 'closed', '', 'miniatura2-3-4', '', '', '2016-05-12 17:06:10', '2016-05-12 20:06:10', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-4/miniatura2-3-4/', 0, 'attachment', 'image/png', 0),
(221, 1, '2016-05-12 17:06:10', '2016-05-12 20:06:10', '', 'miniatura-6', '', 'inherit', 'open', 'closed', '', 'miniatura-6-4', '', '', '2016-05-12 17:06:10', '2016-05-12 20:06:10', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-4/miniatura-6-4/', 0, 'attachment', 'image/png', 0),
(223, 1, '2016-05-12 17:06:10', '2016-05-12 20:06:10', '', 'miniatura-5', '', 'inherit', 'open', 'closed', '', 'miniatura-5-4', '', '', '2016-05-12 17:06:10', '2016-05-12 20:06:10', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-4/miniatura-5-4/', 0, 'attachment', 'image/png', 0),
(224, 1, '2016-05-12 17:06:10', '2016-05-12 20:06:10', '', 'produto-carrossel', '', 'inherit', 'open', 'closed', '', 'produto-carrossel-4', '', '', '2016-05-12 17:06:10', '2016-05-12 20:06:10', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-4/produto-carrossel-4/', 0, 'attachment', 'image/png', 0),
(225, 1, '2016-05-12 17:06:10', '2016-05-12 20:06:10', '', 'foto-produto', '', 'inherit', 'open', 'closed', '', 'foto-produto-5', '', '', '2016-05-12 17:06:10', '2016-05-12 20:06:10', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-4/foto-produto-5/', 0, 'attachment', 'image/png', 0),
(226, 1, '2016-05-12 17:06:10', '2016-05-12 20:06:10', '', 'produto', '', 'inherit', 'open', 'closed', '', 'produto-4', '', '', '2016-05-12 17:06:10', '2016-05-12 20:06:10', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-4/produto-4/', 0, 'attachment', 'image/png', 0),
(228, 1, '2016-05-12 17:06:11', '2016-05-12 20:06:11', '', 'foto-produto', '', 'inherit', 'open', 'closed', '', 'foto-produto-6', '', '', '2016-05-12 17:06:11', '2016-05-12 20:06:11', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-3/foto-produto-6/', 0, 'attachment', 'image/png', 0),
(229, 1, '2016-05-12 17:06:11', '2016-05-12 20:06:11', '', 'produto', '', 'inherit', 'open', 'closed', '', 'produto-5', '', '', '2016-05-12 17:06:11', '2016-05-12 20:06:11', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-3/produto-5/', 0, 'attachment', 'image/png', 0),
(230, 1, '2016-05-12 17:06:11', '2016-05-12 20:06:11', '', 'miniatura2-3', '', 'inherit', 'open', 'closed', '', 'miniatura2-3-5', '', '', '2016-05-12 17:06:11', '2016-05-12 20:06:11', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-3/miniatura2-3-5/', 0, 'attachment', 'image/png', 0),
(232, 1, '2016-05-12 17:06:12', '2016-05-12 20:06:12', '', 'miniatura-6', '', 'inherit', 'open', 'closed', '', 'miniatura-6-5', '', '', '2016-05-12 17:06:12', '2016-05-12 20:06:12', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-3/miniatura-6-5/', 0, 'attachment', 'image/png', 0),
(233, 1, '2016-05-12 17:06:12', '2016-05-12 20:06:12', '', 'miniatura-5', '', 'inherit', 'open', 'closed', '', 'miniatura-5-5', '', '', '2016-05-12 17:06:12', '2016-05-12 20:06:12', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-3/miniatura-5-5/', 0, 'attachment', 'image/png', 0),
(234, 1, '2016-05-12 17:06:12', '2016-05-12 20:06:12', '', 'foto-produto-2', '', 'inherit', 'open', 'closed', '', 'foto-produto-2-6', '', '', '2016-05-12 17:06:12', '2016-05-12 20:06:12', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2-3/foto-produto-2-6/', 0, 'attachment', 'image/png', 0),
(235, 1, '2016-05-12 17:06:12', '2016-05-12 20:06:12', '', 'produto-carrossel', '', 'inherit', 'open', 'closed', '', 'produto-carrossel-5', '', '', '2016-05-12 17:06:12', '2016-05-12 20:06:12', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-3/produto-carrossel-5/', 0, 'attachment', 'image/png', 0),
(236, 1, '2016-05-12 17:06:12', '2016-05-12 20:06:12', '', 'miniatura-4', '', 'inherit', 'open', 'closed', '', 'miniatura-4-5', '', '', '2016-05-12 17:06:12', '2016-05-12 20:06:12', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2-3/miniatura-4-5/', 0, 'attachment', 'image/png', 0),
(238, 1, '2016-05-12 17:06:12', '2016-05-12 20:06:12', '', 'miniatura-3', '', 'inherit', 'open', 'closed', '', 'miniatura-3-5', '', '', '2016-05-12 17:06:12', '2016-05-12 20:06:12', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2-3/miniatura-3-5/', 0, 'attachment', 'image/png', 0),
(239, 1, '2016-05-12 17:06:13', '2016-05-12 20:06:13', '', 'miniatura2-2', '', 'inherit', 'open', 'closed', '', 'miniatura2-2-5', '', '', '2016-05-12 17:06:13', '2016-05-12 20:06:13', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2-3/miniatura2-2-5/', 0, 'attachment', 'image/png', 0),
(240, 1, '2016-05-12 17:06:13', '2016-05-12 20:06:13', '', 'miniatura-2', '', 'inherit', 'open', 'closed', '', 'miniatura-2-5', '', '', '2016-05-12 17:06:13', '2016-05-12 20:06:13', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-3/miniatura-2-5/', 0, 'attachment', 'image/png', 0),
(242, 1, '2016-05-12 17:06:13', '2016-05-12 20:06:13', '', 'miniatura-1', '', 'inherit', 'open', 'closed', '', 'miniatura-1-5', '', '', '2016-05-12 17:06:13', '2016-05-12 20:06:13', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-3/miniatura-1-5/', 0, 'attachment', 'image/png', 0),
(243, 1, '2016-05-12 17:06:13', '2016-05-12 20:06:13', '', 'miniatura2-1', '', 'inherit', 'open', 'closed', '', 'miniatura2-1-5', '', '', '2016-05-12 17:06:13', '2016-05-12 20:06:13', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-3/miniatura2-1-5/', 0, 'attachment', 'image/png', 0),
(244, 1, '2016-05-12 17:06:13', '2016-05-12 20:06:13', '', 'miniatura-2', '', 'inherit', 'open', 'closed', '', 'miniatura-2-6', '', '', '2016-05-12 17:06:13', '2016-05-12 20:06:13', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-4/miniatura-2-6/', 0, 'attachment', 'image/png', 0),
(245, 1, '2016-05-12 17:06:13', '2016-05-12 20:06:13', '', 'miniatura-1', '', 'inherit', 'open', 'closed', '', 'miniatura-1-6', '', '', '2016-05-12 17:06:13', '2016-05-12 20:06:13', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-4/miniatura-1-6/', 0, 'attachment', 'image/png', 0),
(246, 1, '2016-05-12 17:06:13', '2016-05-12 20:06:13', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-5', '', '', '2016-05-12 17:06:13', '2016-05-12 20:06:13', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-3/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-5/', 0, 'attachment', 'image/jpeg', 0),
(247, 1, '2016-05-12 17:06:14', '2016-05-12 20:06:14', '', 'miniatura2-1', '', 'inherit', 'open', 'closed', '', 'miniatura2-1-6', '', '', '2016-05-12 17:06:14', '2016-05-12 20:06:14', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-4/miniatura2-1-6/', 0, 'attachment', 'image/png', 0),
(248, 1, '2016-05-12 17:06:14', '2016-05-12 20:06:14', '', 'aquecedor-de-agua-a-gas', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-a-gas-5', '', '', '2016-05-12 17:06:14', '2016-05-12 20:06:14', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-3/aquecedor-de-agua-a-gas-5/', 0, 'attachment', 'image/jpeg', 0),
(249, 1, '2016-05-12 17:06:14', '2016-05-12 20:06:14', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-6', '', '', '2016-05-12 17:06:14', '2016-05-12 20:06:14', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-4/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-6/', 0, 'attachment', 'image/jpeg', 0),
(251, 1, '2016-05-12 17:06:14', '2016-05-12 20:06:14', '', 'aquecedor-de-agua-a-gas', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-a-gas-6', '', '', '2016-05-12 17:06:14', '2016-05-12 20:06:14', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-4/aquecedor-de-agua-a-gas-6/', 0, 'attachment', 'image/jpeg', 0),
(252, 1, '2016-05-12 17:06:14', '2016-05-12 20:06:14', '', 'miniatura-4', '', 'inherit', 'open', 'closed', '', 'miniatura-4-6', '', '', '2016-05-12 17:06:14', '2016-05-12 20:06:14', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-4/miniatura-4-6/', 0, 'attachment', 'image/png', 0),
(253, 1, '2016-05-12 17:06:14', '2016-05-12 20:06:14', '', 'miniatura-3', '', 'inherit', 'open', 'closed', '', 'miniatura-3-6', '', '', '2016-05-12 17:06:14', '2016-05-12 20:06:14', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-4/miniatura-3-6/', 0, 'attachment', 'image/png', 0),
(254, 1, '2016-05-12 17:06:15', '2016-05-12 20:06:15', '', 'miniatura2-2', '', 'inherit', 'open', 'closed', '', 'miniatura2-2-6', '', '', '2016-05-12 17:06:15', '2016-05-12 20:06:15', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-4/miniatura2-2-6/', 0, 'attachment', 'image/png', 0),
(256, 1, '2016-05-12 17:06:15', '2016-05-12 20:06:15', '', 'foto-produto-2', '', 'inherit', 'open', 'closed', '', 'foto-produto-2-7', '', '', '2016-05-12 17:06:15', '2016-05-12 20:06:15', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-4/foto-produto-2-7/', 0, 'attachment', 'image/png', 0),
(257, 1, '2016-05-12 17:06:15', '2016-05-12 20:06:15', '', 'miniatura2-3', '', 'inherit', 'open', 'closed', '', 'miniatura2-3-6', '', '', '2016-05-12 17:06:15', '2016-05-12 20:06:15', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-5/miniatura2-3-6/', 0, 'attachment', 'image/png', 0),
(258, 1, '2016-05-12 17:06:15', '2016-05-12 20:06:15', '', 'miniatura-6', '', 'inherit', 'open', 'closed', '', 'miniatura-6-6', '', '', '2016-05-12 17:06:15', '2016-05-12 20:06:15', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-5/miniatura-6-6/', 0, 'attachment', 'image/png', 0),
(259, 1, '2016-05-12 17:06:15', '2016-05-12 20:06:15', '', 'miniatura-5', '', 'inherit', 'open', 'closed', '', 'miniatura-5-6', '', '', '2016-05-12 17:06:15', '2016-05-12 20:06:15', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-5/miniatura-5-6/', 0, 'attachment', 'image/png', 0),
(261, 1, '2016-05-12 17:06:16', '2016-05-12 20:06:16', '', 'produto-carrossel', '', 'inherit', 'open', 'closed', '', 'produto-carrossel-6', '', '', '2016-05-12 17:06:16', '2016-05-12 20:06:16', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-5/produto-carrossel-6/', 0, 'attachment', 'image/png', 0),
(262, 1, '2016-05-12 17:06:16', '2016-05-12 20:06:16', '', 'foto-produto', '', 'inherit', 'open', 'closed', '', 'foto-produto-7', '', '', '2016-05-12 17:06:16', '2016-05-12 20:06:16', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-5/foto-produto-7/', 0, 'attachment', 'image/png', 0),
(263, 1, '2016-05-12 17:06:16', '2016-05-12 20:06:16', '', 'foto-produto', '', 'inherit', 'open', 'closed', '', 'foto-produto-8', '', '', '2016-05-12 17:06:16', '2016-05-12 20:06:16', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-4/foto-produto-8/', 0, 'attachment', 'image/png', 0),
(264, 1, '2016-05-12 17:06:16', '2016-05-12 20:06:16', '', 'produto', '', 'inherit', 'open', 'closed', '', 'produto-6', '', '', '2016-05-12 17:06:16', '2016-05-12 20:06:16', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-5/produto-6/', 0, 'attachment', 'image/png', 0),
(265, 1, '2016-05-12 17:06:16', '2016-05-12 20:06:16', '', 'produto', '', 'inherit', 'open', 'closed', '', 'produto-7', '', '', '2016-05-12 17:06:16', '2016-05-12 20:06:16', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-4/produto-7/', 0, 'attachment', 'image/png', 0),
(267, 1, '2016-05-12 17:06:16', '2016-05-12 20:06:16', '', 'miniatura2-3', '', 'inherit', 'open', 'closed', '', 'miniatura2-3-7', '', '', '2016-05-12 17:06:16', '2016-05-12 20:06:16', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-4/miniatura2-3-7/', 0, 'attachment', 'image/png', 0),
(268, 1, '2016-05-12 17:06:17', '2016-05-12 20:06:17', '', 'miniatura-6', '', 'inherit', 'open', 'closed', '', 'miniatura-6-7', '', '', '2016-05-12 17:06:17', '2016-05-12 20:06:17', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-4/miniatura-6-7/', 0, 'attachment', 'image/png', 0),
(269, 1, '2016-05-12 17:06:17', '2016-05-12 20:06:17', '', 'foto-produto-2', '', 'inherit', 'open', 'closed', '', 'foto-produto-2-8', '', '', '2016-05-12 17:06:17', '2016-05-12 20:06:17', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2-4/foto-produto-2-8/', 0, 'attachment', 'image/png', 0),
(270, 1, '2016-05-12 17:06:17', '2016-05-12 20:06:17', '', 'miniatura-5', '', 'inherit', 'open', 'closed', '', 'miniatura-5-7', '', '', '2016-05-12 17:06:17', '2016-05-12 20:06:17', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-4/miniatura-5-7/', 0, 'attachment', 'image/png', 0),
(271, 1, '2016-05-12 17:06:17', '2016-05-12 20:06:17', '', 'miniatura-4', '', 'inherit', 'open', 'closed', '', 'miniatura-4-7', '', '', '2016-05-12 17:06:17', '2016-05-12 20:06:17', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2-4/miniatura-4-7/', 0, 'attachment', 'image/png', 0),
(272, 1, '2016-05-12 17:06:17', '2016-05-12 20:06:17', '', 'produto-carrossel', '', 'inherit', 'open', 'closed', '', 'produto-carrossel-7', '', '', '2016-05-12 17:06:17', '2016-05-12 20:06:17', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-3-4/produto-carrossel-7/', 0, 'attachment', 'image/png', 0),
(273, 1, '2016-05-12 17:06:17', '2016-05-12 20:06:17', '', 'miniatura-3', '', 'inherit', 'open', 'closed', '', 'miniatura-3-7', '', '', '2016-05-12 17:06:17', '2016-05-12 20:06:17', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2-4/miniatura-3-7/', 0, 'attachment', 'image/png', 0),
(274, 1, '2016-05-12 17:06:17', '2016-05-12 20:06:17', '', 'Bomba Pressurizadora TBHCI INOX – Texius', '', 'publish', 'closed', 'closed', '', 'bomba-pressurizadora-tbhci-inox-texius', '', '', '2016-10-18 12:35:27', '2016-10-18 14:35:27', '', 0, 'http://3baquecedores.pixd.com.br/produto/linha-pratatherm-1000f-bosch/', 0, 'produto', '', 0),
(275, 1, '2016-05-12 17:06:18', '2016-05-12 20:06:18', '', 'miniatura-2', '', 'inherit', 'open', 'closed', '', 'miniatura-2-7', '', '', '2016-05-12 17:06:18', '2016-05-12 20:06:18', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-4/miniatura-2-7/', 0, 'attachment', 'image/png', 0),
(276, 1, '2016-05-12 17:06:18', '2016-05-12 20:06:18', '', 'miniatura2-2', '', 'inherit', 'open', 'closed', '', 'miniatura2-2-7', '', '', '2016-05-12 17:06:18', '2016-05-12 20:06:18', '', 0, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-2-4/miniatura2-2-7/', 0, 'attachment', 'image/png', 0),
(277, 1, '2016-05-12 17:06:18', '2016-05-12 20:06:18', '', 'miniatura-1', '', 'inherit', 'open', 'closed', '', 'miniatura-1-7', '', '', '2016-05-12 17:06:18', '2016-05-12 20:06:18', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-4/miniatura-1-7/', 0, 'attachment', 'image/png', 0),
(278, 1, '2016-05-12 17:06:18', '2016-05-12 20:06:18', '', 'miniatura2-1', '', 'inherit', 'open', 'closed', '', 'miniatura2-1-7', '', '', '2016-05-12 17:06:18', '2016-05-12 20:06:18', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-4/miniatura2-1-7/', 0, 'attachment', 'image/png', 0),
(279, 1, '2016-05-12 17:06:18', '2016-05-12 20:06:18', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-7', '', '', '2016-05-12 17:06:18', '2016-05-12 20:06:18', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-4/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-7/', 0, 'attachment', 'image/jpeg', 0),
(280, 1, '2016-05-12 17:06:18', '2016-05-12 20:06:18', '', 'Aquecedor de água a gás Fluxo Balanceado KO 07B – Komeco', '', 'publish', 'closed', 'closed', '', 'aquecedor-de-agua-a-gas-fluxo-balanceado-ko-07b-komeco', '', '', '2016-10-18 12:35:25', '2016-10-18 14:35:25', '', 0, 'http://3baquecedores.pixd.com.br/produto/linha-brancatherm-1200f-bosch/', 0, 'produto', '', 0),
(281, 1, '2016-05-12 17:06:19', '2016-05-12 20:06:19', '', 'aquecedor-de-agua-a-gas', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-a-gas-7', '', '', '2016-05-12 17:06:19', '2016-05-12 20:06:19', '', 0, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-2-4/aquecedor-de-agua-a-gas-7/', 0, 'attachment', 'image/jpeg', 0),
(282, 1, '2016-05-12 17:06:19', '2016-05-12 20:06:19', '', 'miniatura-2', '', 'inherit', 'open', 'closed', '', 'miniatura-2-8', '', '', '2016-05-12 17:06:19', '2016-05-12 20:06:19', '', 274, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-5/miniatura-2-8/', 0, 'attachment', 'image/png', 0),
(283, 1, '2016-05-12 17:06:19', '2016-05-12 20:06:19', '', 'Aquecedor a gás Therm 1000F – Bosch', '', 'publish', 'closed', 'closed', '', 'aquecedor-a-gas-therm-1000f-bosch', '', '', '2016-10-18 12:35:23', '2016-10-18 14:35:23', '', 0, 'http://3baquecedores.pixd.com.br/produto/linha-pratatherm-1000f-bosch/', 0, 'produto', '', 0),
(284, 1, '2016-05-12 17:06:19', '2016-05-12 20:06:19', '', 'miniatura-1', '', 'inherit', 'open', 'closed', '', 'miniatura-1-8', '', '', '2016-05-12 17:06:19', '2016-05-12 20:06:19', '', 274, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-5/miniatura-1-8/', 0, 'attachment', 'image/png', 0),
(285, 1, '2016-05-12 17:06:19', '2016-05-12 20:06:19', '', 'miniatura2-1', '', 'inherit', 'open', 'closed', '', 'miniatura2-1-8', '', '', '2016-05-12 17:06:19', '2016-05-12 20:06:19', '', 274, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-5/miniatura2-1-8/', 0, 'attachment', 'image/png', 0),
(286, 1, '2016-05-12 17:06:19', '2016-05-12 20:06:19', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-8', '', '', '2016-05-12 17:06:19', '2016-05-12 20:06:19', '', 274, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-5/aquecedor-de-agua-lorenzetti-a-gas-multitemperatura-lz-1600-glp-digital-8/', 0, 'attachment', 'image/jpeg', 0),
(287, 1, '2016-05-12 17:06:19', '2016-05-12 20:06:19', '', 'miniatura-4', '', 'inherit', 'open', 'closed', '', 'miniatura-4-8', '', '', '2016-05-12 17:06:19', '2016-05-12 20:06:19', '', 280, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-5/miniatura-4-8/', 0, 'attachment', 'image/png', 0),
(288, 1, '2016-05-12 17:06:20', '2016-05-12 20:06:20', '', 'aquecedor-de-agua-a-gas', '', 'inherit', 'open', 'closed', '', 'aquecedor-de-agua-a-gas-8', '', '', '2016-05-12 17:06:20', '2016-05-12 20:06:20', '', 274, 'http://3baquecedores.pixd.com.br/produto/lz-1600-digital-5/aquecedor-de-agua-a-gas-8/', 0, 'attachment', 'image/jpeg', 0),
(289, 1, '2016-05-12 17:06:20', '2016-05-12 20:06:20', '', 'miniatura-3', '', 'inherit', 'open', 'closed', '', 'miniatura-3-8', '', '', '2016-05-12 17:06:20', '2016-05-12 20:06:20', '', 280, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-5/miniatura-3-8/', 0, 'attachment', 'image/png', 0),
(290, 1, '2016-05-12 17:06:20', '2016-05-12 20:06:20', '', 'miniatura2-2', '', 'inherit', 'open', 'closed', '', 'miniatura2-2-8', '', '', '2016-05-12 17:06:20', '2016-05-12 20:06:20', '', 280, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-5/miniatura2-2-8/', 0, 'attachment', 'image/png', 0),
(291, 1, '2016-05-12 17:06:20', '2016-05-12 20:06:20', '', 'miniatura2-3', '', 'inherit', 'open', 'closed', '', 'miniatura2-3-8', '', '', '2016-05-12 17:06:20', '2016-05-12 20:06:20', '', 283, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-6/miniatura2-3-8/', 0, 'attachment', 'image/png', 0),
(292, 1, '2016-05-12 17:06:20', '2016-05-12 20:06:20', '', 'foto-produto-2', '', 'inherit', 'open', 'closed', '', 'foto-produto-2-9', '', '', '2016-05-12 17:06:20', '2016-05-12 20:06:20', '', 280, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-2-5/foto-produto-2-9/', 0, 'attachment', 'image/png', 0),
(293, 1, '2016-05-12 17:06:20', '2016-05-12 20:06:20', '', 'miniatura-6', '', 'inherit', 'open', 'closed', '', 'miniatura-6-8', '', '', '2016-05-12 17:06:20', '2016-05-12 20:06:20', '', 283, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-6/miniatura-6-8/', 0, 'attachment', 'image/png', 0),
(294, 1, '2016-05-12 17:06:20', '2016-05-12 20:06:20', '', 'miniatura-5', '', 'inherit', 'open', 'closed', '', 'miniatura-5-8', '', '', '2016-05-12 17:06:20', '2016-05-12 20:06:20', '', 283, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-6/miniatura-5-8/', 0, 'attachment', 'image/png', 0),
(295, 1, '2016-05-12 17:06:21', '2016-05-12 20:06:21', '', 'produto-carrossel', '', 'inherit', 'open', 'closed', '', 'produto-carrossel-8', '', '', '2016-05-12 17:06:21', '2016-05-12 20:06:21', '', 283, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-6/produto-carrossel-8/', 0, 'attachment', 'image/png', 0),
(296, 1, '2016-05-12 17:06:21', '2016-05-12 20:06:21', '', 'foto-produto', '', 'inherit', 'open', 'closed', '', 'foto-produto-9', '', '', '2016-05-12 17:06:21', '2016-05-12 20:06:21', '', 283, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-6/foto-produto-9/', 0, 'attachment', 'image/png', 0),
(297, 1, '2016-05-12 17:06:21', '2016-05-12 20:06:21', '', 'produto', '', 'inherit', 'open', 'closed', '', 'produto-8', '', '', '2016-05-12 17:06:21', '2016-05-12 20:06:21', '', 283, 'http://3baquecedores.pixd.com.br/produto/aquecedor-a-gas-6/produto-8/', 0, 'attachment', 'image/png', 0),
(298, 1, '2016-05-13 11:18:26', '2016-05-13 14:18:26', '', 'banner', '', 'inherit', 'open', 'closed', '', 'banner-3', '', '', '2016-05-13 11:18:26', '2016-05-13 14:18:26', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/banner-2.png', 0, 'attachment', 'image/png', 0),
(299, 1, '2016-05-13 11:18:28', '2016-05-13 14:18:28', '', 'foto-que-fazemos', '', 'inherit', 'open', 'closed', '', 'foto-que-fazemos-3', '', '', '2016-05-13 11:18:28', '2016-05-13 14:18:28', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto-que-fazemos-2.png', 0, 'attachment', 'image/png', 0),
(300, 1, '2016-05-13 11:18:29', '2016-05-13 14:18:29', '', 'foto-quemsomos', '', 'inherit', 'open', 'closed', '', 'foto-quemsomos-2', '', '', '2016-05-13 11:18:29', '2016-05-13 14:18:29', '', 0, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto-quemsomos-1.png', 0, 'attachment', 'image/png', 0),
(301, 1, '2016-05-13 14:39:31', '2016-05-13 17:39:31', '', 'Água quente todos os dias', '', 'publish', 'closed', 'closed', '', '3b-aquecedores-2', '', '', '2017-09-04 13:35:15', '2017-09-04 16:35:15', '', 0, 'http://3baquecedores.pixd.com.br/destaque/3b-aquecedores-2/', 1, 'destaque', '', 0),
(302, 1, '2016-05-13 14:39:32', '2016-05-13 17:39:32', '', 'foto-destaque', '', 'inherit', 'open', 'closed', '', 'foto-destaque-3', '', '', '2016-05-13 14:39:32', '2016-05-13 17:39:32', '', 301, 'http://3baquecedores.pixd.com.br/destaque/3b-aquecedores-2/foto-destaque-3/', 0, 'attachment', 'image/png', 0),
(303, 1, '2016-05-13 14:40:02', '2016-05-13 17:40:02', '', 'hover', '', 'inherit', 'open', 'closed', '', 'hover-2', '', '', '2016-05-13 14:40:02', '2016-05-13 17:40:02', '', 301, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/hover-1.png', 0, 'attachment', 'image/png', 0),
(304, 1, '2016-05-16 11:06:16', '2016-05-16 14:06:16', '', 'João 2', '', 'publish', 'closed', 'closed', '', 'joao-2', '', '', '2016-08-01 13:24:12', '2016-08-01 16:24:12', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=depoimentos&#038;p=304', 0, 'depoimentos', '', 0),
(309, 1, '2016-07-29 11:29:34', '2016-07-29 14:29:34', '', 'Quem Somos', '', 'inherit', 'closed', 'closed', '', '66-revision-v1', '', '', '2016-07-29 11:29:34', '2016-07-29 14:29:34', '', 66, 'http://3baquecedores.pixd.com.br/66-revision-v1/', 0, 'revision', '', 0),
(310, 1, '2016-07-29 11:59:59', '2016-07-29 14:59:59', '<table>\n<tbody>\n<tr>\n<td style=\"text-align: center;\" colspan=\"3\" width=\"574\"><strong>INFORMAÇÃO NUTRICIONAL</strong>\n<p style=\"text-align: center;\">Porção de 4,0 g = 4 cápsulas</p>\n</td>\n</tr>\n<tr>\n<td colspan=\"2\" width=\"387\">Quantidade por Porção</td>\n<td width=\"187\">% VD (*)</td>\n</tr>\n<tr>\n<td width=\"218\">Valor Energético</td>\n<td width=\"169\">36 kcal = 151 kJ</td>\n<td width=\"187\">2%</td>\n</tr>\n<tr>\n<td width=\"218\">Gorduras Totais</td>\n<td width=\"169\">4,0 g, das quais:</td>\n<td width=\"187\">8%</td>\n</tr>\n<tr>\n<td width=\"218\">Gorduras Saturadas</td>\n<td width=\"169\">3,0 g</td>\n<td width=\"187\">14%</td>\n</tr>\n<tr>\n<td width=\"218\">Ácido láurico</td>\n<td width=\"169\">2,0 g</td>\n<td width=\"187\"></td>\n</tr>\n<tr>\n<td width=\"218\">Ácido mirístico</td>\n<td width=\"169\">1,0 g</td>\n<td width=\"187\">**</td>\n</tr>\n<tr>\n<td width=\"218\">Gorduras Mono-insaturadas</td>\n<td width=\"169\">1,0 g</td>\n<td width=\"187\">**</td>\n</tr>\n<tr>\n<td width=\"218\">Ácido oleico</td>\n<td width=\"169\">1,0 g</td>\n<td width=\"187\">**</td>\n</tr>\n<tr>\n<td colspan=\"3\" width=\"574\">Não contém quantidades significativas de Carboidratos, Proteínas, Gorduras trans, Fibra alimentar e Sódio.</td>\n</tr>\n</tbody>\n</table>', 'Quem Somos', '', 'inherit', 'closed', 'closed', '', '66-autosave-v1', '', '', '2016-07-29 11:59:59', '2016-07-29 14:59:59', '', 66, 'http://3baquecedores.pixd.com.br/66-autosave-v1/', 0, 'revision', '', 0),
(311, 1, '2016-07-29 15:46:25', '2016-07-29 18:46:25', '', 'Aquecedor de água a gás Fluxo Balanceado KO 07B – Komeco', '', 'inherit', 'closed', 'closed', '', '280-autosave-v1', '', '', '2016-07-29 15:46:25', '2016-07-29 18:46:25', '', 280, 'http://3baquecedores.pixd.com.br/280-autosave-v1/', 0, 'revision', '', 0),
(312, 1, '2016-07-29 16:13:00', '2016-07-29 19:13:00', '', 'Bomba Pressurizadora TBHCI INOX – Texius', '', 'inherit', 'closed', 'closed', '', '274-autosave-v1', '', '', '2016-07-29 16:13:00', '2016-07-29 19:13:00', '', 274, 'http://3baquecedores.pixd.com.br/274-autosave-v1/', 0, 'revision', '', 0),
(313, 1, '2016-07-29 16:28:50', '2016-07-29 19:28:50', '<strong>Mantenha seu banho sempre no clima perfeito.</strong>\r\n\r\nSempre faça uma revisão preventiva de seu aquecedor a gás. A 3B Aquecedores conta com uma Assistência Técnica Especializada para qualquer tipo de eventualidade com seu aparelho, utilizando equipamento de teste que identifica rapidamente os defeitos, além de técnicos e engenheiros especializados que melhor atenderão os clientes.\r\n\r\nPreencha o formulário abaixo, um de nossos atendentes entrará em contato com você.', 'Assistência Técnica', '', 'inherit', 'closed', 'closed', '', '155-revision-v1', '', '', '2016-07-29 16:28:50', '2016-07-29 19:28:50', '', 155, 'http://3baquecedores.pixd.com.br/155-revision-v1/', 0, 'revision', '', 0),
(314, 1, '2016-07-29 16:29:51', '2016-07-29 19:29:51', '', 'Assistência Técnica', '', 'inherit', 'closed', 'closed', '', '155-revision-v1', '', '', '2016-07-29 16:29:51', '2016-07-29 19:29:51', '', 155, 'http://3baquecedores.pixd.com.br/155-revision-v1/', 0, 'revision', '', 0),
(315, 1, '2016-07-29 16:40:44', '2016-07-29 19:40:44', '', 'fallbackProduto', '', 'inherit', 'open', 'closed', '', 'fallbackproduto', '', '', '2016-07-29 16:40:44', '2016-07-29 19:40:44', '', 283, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/fallbackProduto.png', 0, 'attachment', 'image/png', 0),
(316, 1, '2016-08-01 11:12:59', '2016-08-01 14:12:59', '', 'casalFeliz', '', 'inherit', 'open', 'closed', '', 'casalfeliz', '', '', '2016-08-01 11:12:59', '2016-08-01 14:12:59', '', 301, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/casalFeliz.jpg', 0, 'attachment', 'image/jpeg', 0),
(317, 1, '2016-08-01 13:20:10', '2016-08-01 16:20:10', '', 'Autor exemplo', '', 'publish', 'closed', 'closed', '', '317', '', '', '2016-08-01 15:13:44', '2016-08-01 18:13:44', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=depoimentos&#038;p=317', 0, 'depoimentos', '', 0),
(320, 1, '2016-10-18 12:55:21', '2016-10-18 14:55:21', '', 'Mantenha seu banho sempre no clima perfeito', '', 'inherit', 'closed', 'closed', '', '155-revision-v1', '', '', '2016-10-18 12:55:21', '2016-10-18 14:55:21', '', 155, 'http://3baquecedores.pixd.com.br/155-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `3b_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(321, 1, '2017-04-12 17:20:26', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2017-04-12 17:20:26', '0000-00-00 00:00:00', '', 0, 'http://3baquecedores.pixd.com.br/?p=321', 0, 'post', '', 0),
(322, 1, '2017-07-27 15:56:26', '2017-07-27 18:56:26', '', 'IN-180D  - Digital Eletrônico (Bivolt)', '', 'publish', 'closed', 'closed', '', 'in-180d', '', '', '2017-07-27 16:32:30', '2017-07-27 19:32:30', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=produto&#038;p=322', 0, 'produto', '', 0),
(323, 1, '2017-07-27 15:58:47', '2017-07-27 18:58:47', '', 'aquecedor-IN-180D', '', 'inherit', 'open', 'closed', '', 'aquecedor-in-180d', '', '', '2017-07-27 15:58:47', '2017-07-27 18:58:47', '', 322, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/aquecedor-IN-180D.jpg', 0, 'attachment', 'image/jpeg', 0),
(324, 1, '2017-07-27 15:59:56', '2017-07-27 18:59:56', '', 'Capturar', '', 'inherit', 'open', 'closed', '', 'capturar', '', '', '2017-07-27 15:59:56', '2017-07-27 18:59:56', '', 322, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/Capturar.png', 0, 'attachment', 'image/png', 0),
(325, 1, '2017-07-27 16:18:43', '2017-07-27 19:18:43', '', 'IN-230D - Digital Eletrônico (Bivolt)', '', 'publish', 'closed', 'closed', '', 'in-230d-digital-eletronico-bivolt', '', '', '2017-07-27 16:18:43', '2017-07-27 19:18:43', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=produto&#038;p=325', 0, 'produto', '', 0),
(326, 1, '2017-07-27 16:18:15', '2017-07-27 19:18:15', '', 'aquecedor-IN-230D_3523 (2)', '', 'inherit', 'open', 'closed', '', 'aquecedor-in-230d_3523-2', '', '', '2017-07-27 16:18:15', '2017-07-27 19:18:15', '', 325, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/aquecedor-IN-230D_3523-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(327, 1, '2017-07-27 16:23:40', '2017-07-27 19:23:40', '', 'IN-800EFP - Exaustão Forçada (Bivolt)', '', 'publish', 'closed', 'closed', '', 'in-800efp-exaustao-forcada-bivolt', '', '', '2017-07-27 17:20:27', '2017-07-27 20:20:27', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=produto&#038;p=327', 0, 'produto', '', 0),
(328, 1, '2017-07-27 16:22:06', '2017-07-27 19:22:06', '', 'asd', '', 'inherit', 'open', 'closed', '', 'asd', '', '', '2017-07-27 16:22:06', '2017-07-27 19:22:06', '', 327, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/asd.png', 0, 'attachment', 'image/png', 0),
(329, 1, '2017-07-27 16:22:58', '2017-07-27 19:22:58', '', 'aquecedor-IN-600_1000_1800_2200_3540 (2)', '', 'inherit', 'open', 'closed', '', 'aquecedor-in-600_1000_1800_2200_3540-2', '', '', '2017-07-27 16:22:58', '2017-07-27 19:22:58', '', 327, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/aquecedor-IN-600_1000_1800_2200_3540-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(330, 1, '2017-07-27 16:29:22', '2017-07-27 19:29:22', '', 'IN-1800EFP - Exaustão Forçada (Bivolt)', '', 'publish', 'closed', 'closed', '', 'in-1800efp-exaustao-forcada-bivolt', '', '', '2017-07-27 16:32:04', '2017-07-27 19:32:04', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=produto&#038;p=330', 0, 'produto', '', 0),
(331, 1, '2017-07-27 16:36:09', '2017-07-27 19:36:09', '', 'GWH 520 CTDE - Automudalação Eletrônica', '', 'publish', 'closed', 'closed', '', 'gwh-520-ctde-automudalacao-eletronica', '', '', '2017-07-27 17:17:11', '2017-07-27 20:17:11', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=produto&#038;p=331', 0, 'produto', '', 0),
(332, 1, '2017-07-27 16:34:12', '2017-07-27 19:34:12', '', 'assas', '', 'inherit', 'open', 'closed', '', 'assas', '', '', '2017-07-27 16:34:12', '2017-07-27 19:34:12', '', 331, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/assas.png', 0, 'attachment', 'image/png', 0),
(333, 1, '2017-07-27 16:35:18', '2017-07-27 19:35:18', '', 'GWH_350_520_E', '', 'inherit', 'open', 'closed', '', 'gwh_350_520_e', '', '', '2017-07-27 16:35:18', '2017-07-27 19:35:18', '', 331, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/GWH_350_520_E.tif', 0, 'attachment', 'image/tiff', 0),
(334, 1, '2017-07-27 16:36:02', '2017-07-27 19:36:02', '', 'GWH_350_520_E', '', 'inherit', 'open', 'closed', '', 'gwh_350_520_e-2', '', '', '2017-07-27 16:36:02', '2017-07-27 19:36:02', '', 331, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/GWH_350_520_E.jpg', 0, 'attachment', 'image/jpeg', 0),
(335, 1, '2017-07-27 16:46:52', '2017-07-27 19:46:52', '', 'GWH 350 CTDE - Automudalação Eletrônica', '', 'publish', 'closed', 'closed', '', 'gwh-350-ctde-automudalacao-eletronica', '', '', '2017-07-27 16:46:52', '2017-07-27 19:46:52', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=produto&#038;p=335', 0, 'produto', '', 0),
(336, 1, '2017-07-27 16:46:07', '2017-07-27 19:46:07', '', '3323', '', 'inherit', 'open', 'closed', '', '3323', '', '', '2017-07-27 16:46:07', '2017-07-27 19:46:07', '', 335, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/3323.png', 0, 'attachment', 'image/png', 0),
(337, 1, '2017-07-27 16:49:27', '2017-07-27 19:49:27', '', 'GWH 720 CTDE - Automudalação Eletrônica', '', 'publish', 'closed', 'closed', '', 'gwh-720-ctde-automudalacao-eletronica', '', '', '2017-07-27 17:10:34', '2017-07-27 20:10:34', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=produto&#038;p=337', 0, 'produto', '', 0),
(338, 1, '2017-07-27 16:49:02', '2017-07-27 19:49:02', '', '444', '', 'inherit', 'open', 'closed', '', '444', '', '', '2017-07-27 16:49:02', '2017-07-27 19:49:02', '', 337, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/444.png', 0, 'attachment', 'image/png', 0),
(339, 1, '2017-07-27 16:54:09', '2017-07-27 19:54:09', '', 'THERM 1000F - Exaustão Forçada (Bivolt)', '', 'publish', 'closed', 'closed', '', 'therm-1000f-exaustao-forcada-bivolt', '', '', '2017-09-04 14:58:02', '2017-09-04 17:58:02', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=produto&#038;p=339', 0, 'produto', '', 0),
(340, 1, '2017-07-27 16:51:59', '2017-07-27 19:51:59', '', 'sddddddd', '', 'inherit', 'open', 'closed', '', 'sddddddd', '', '', '2017-07-27 16:51:59', '2017-07-27 19:51:59', '', 339, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/sddddddd.png', 0, 'attachment', 'image/png', 0),
(341, 1, '2017-07-27 16:55:59', '2017-07-27 19:55:59', '', 'GWH 320 DE - Exaustão Forçada (Bivolt)', '', 'publish', 'closed', 'closed', '', 'gwh-320-de-exaustao-forcada-bivolt', '', '', '2017-07-27 17:13:13', '2017-07-27 20:13:13', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=produto&#038;p=341', 0, 'produto', '', 0),
(342, 1, '2017-07-27 16:55:40', '2017-07-27 19:55:40', '', 'zzzzz', '', 'inherit', 'open', 'closed', '', 'zzzzz', '', '', '2017-07-27 16:55:40', '2017-07-27 19:55:40', '', 341, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/zzzzz.png', 0, 'attachment', 'image/png', 0),
(343, 1, '2017-07-27 16:57:40', '2017-07-27 19:57:40', '', 'Aquecedor_Digital_GWH_720', '', 'inherit', 'open', 'closed', '', 'aquecedor_digital_gwh_720', '', '', '2017-07-27 16:57:40', '2017-07-27 19:57:40', '', 337, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/Aquecedor_Digital_GWH_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(344, 1, '2017-07-27 17:07:50', '2017-07-27 20:07:50', '', '720', '', 'inherit', 'open', 'closed', '', '720', '', '', '2017-07-27 17:07:50', '2017-07-27 20:07:50', '', 337, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/720.png', 0, 'attachment', 'image/png', 0),
(345, 1, '2017-07-27 17:12:43', '2017-07-27 20:12:43', '', '320', '', 'inherit', 'open', 'closed', '', '320', '', '', '2017-07-27 17:12:43', '2017-07-27 20:12:43', '', 341, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/320.png', 0, 'attachment', 'image/png', 0),
(346, 1, '2017-07-27 17:14:31', '2017-07-27 20:14:31', '', '1000', '', 'inherit', 'open', 'closed', '', '1000', '', '', '2017-07-27 17:14:31', '2017-07-27 20:14:31', '', 339, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/1000.png', 0, 'attachment', 'image/png', 0),
(347, 1, '2017-07-27 17:16:43', '2017-07-27 20:16:43', '', '520', '', 'inherit', 'open', 'closed', '', '520', '', '', '2017-07-27 17:16:43', '2017-07-27 20:16:43', '', 331, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/520.png', 0, 'attachment', 'image/png', 0),
(348, 1, '2017-07-27 17:20:00', '2017-07-27 20:20:00', '', '800', '', 'inherit', 'open', 'closed', '', '800', '', '', '2017-07-27 17:20:00', '2017-07-27 20:20:00', '', 327, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2017/07/800.png', 0, 'attachment', 'image/png', 0),
(350, 1, '2017-09-04 10:20:30', '2017-09-04 13:20:30', '', 'banner1', '', 'inherit', 'open', 'closed', '', 'banner1', '', '', '2017-09-04 10:20:30', '2017-09-04 13:20:30', '', 29, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/banner1.jpg', 0, 'attachment', 'image/jpeg', 0),
(351, 1, '2017-09-04 10:20:37', '2017-09-04 13:20:37', '', 'banner2', '', 'inherit', 'open', 'closed', '', 'banner2', '', '', '2017-09-04 10:20:37', '2017-09-04 13:20:37', '', 29, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/banner2.jpg', 0, 'attachment', 'image/jpeg', 0),
(352, 1, '2017-09-04 10:20:41', '2017-09-04 13:20:41', '', 'banner3', '', 'inherit', 'open', 'closed', '', 'banner3', '', '', '2017-09-04 10:20:41', '2017-09-04 13:20:41', '', 29, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/banner3.jpg', 0, 'attachment', 'image/jpeg', 0),
(353, 1, '2017-09-04 10:20:44', '2017-09-04 13:20:44', '', 'contato', '', 'inherit', 'open', 'closed', '', 'contato-2', '', '', '2017-09-04 10:20:44', '2017-09-04 13:20:44', '', 29, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/contato.jpg', 0, 'attachment', 'image/jpeg', 0),
(354, 1, '2017-09-04 10:20:46', '2017-09-04 13:20:46', '', 'manutencao', '', 'inherit', 'open', 'closed', '', 'manutencao', '', '', '2017-09-04 10:20:46', '2017-09-04 13:20:46', '', 29, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/manutencao.jpg', 0, 'attachment', 'image/jpeg', 0),
(355, 1, '2017-09-04 10:20:48', '2017-09-04 13:20:48', '', 'manutencao2', '', 'inherit', 'open', 'closed', '', 'manutencao2', '', '', '2017-09-04 10:20:48', '2017-09-04 13:20:48', '', 29, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/manutencao2.jpg', 0, 'attachment', 'image/jpeg', 0),
(356, 1, '2017-09-04 13:34:36', '2017-09-04 16:34:36', '', 'Assistência Técnica Autorizada 3B', '', 'inherit', 'closed', 'closed', '', '29-autosave-v1', '', '', '2017-09-04 13:34:36', '2017-09-04 16:34:36', '', 29, 'http://3baquecedores.pixd.com.br/29-autosave-v1/', 0, 'revision', '', 0),
(357, 1, '2017-09-04 13:35:06', '2017-09-04 16:35:06', '', 'banner5', '', 'inherit', 'open', 'closed', '', 'banner5', '', '', '2017-09-04 13:35:06', '2017-09-04 16:35:06', '', 301, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/banner5.jpg', 0, 'attachment', 'image/jpeg', 0),
(358, 1, '2017-09-04 13:35:10', '2017-09-04 16:35:10', '', 'banner6', '', 'inherit', 'open', 'closed', '', 'banner6', '', '', '2017-09-04 13:35:10', '2017-09-04 16:35:10', '', 301, 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/banner6.jpg', 0, 'attachment', 'image/jpeg', 0),
(359, 1, '2017-09-04 13:36:00', '2017-09-04 16:36:00', '', 'Destaque 5', '', 'publish', 'closed', 'closed', '', 'destaque-5', '', '', '2017-09-04 13:36:00', '2017-09-04 16:36:00', '', 0, 'http://3baquecedores.pixd.com.br/?post_type=destaque&#038;p=359', 0, 'destaque', '', 0),
(360, 1, '2017-09-05 11:53:11', '2017-09-05 14:53:11', '', 'Mantenha seu banho sempre no clima perfeito', '', 'inherit', 'closed', 'closed', '', '155-autosave-v1', '', '', '2017-09-05 11:53:11', '2017-09-05 14:53:11', '', 155, 'http://3baquecedores.pixd.com.br/155-autosave-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_termmeta`
--

CREATE TABLE `3b_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_terms`
--

CREATE TABLE `3b_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `3b_terms`
--

INSERT INTO `3b_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'Categoria 1', 'categoria-1', 0),
(3, 'Categoria 2', 'categoria-2', 0),
(4, 'Categoria 3', 'categoria-3', 0),
(5, 'Aquecedores de passagem', 'aquecedores-de-passagem', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_term_relationships`
--

CREATE TABLE `3b_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `3b_term_relationships`
--

INSERT INTO `3b_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(109, 2, 0),
(111, 3, 0),
(113, 4, 0),
(117, 4, 0),
(119, 3, 0),
(121, 2, 0),
(123, 2, 0),
(125, 2, 0),
(127, 3, 0),
(129, 4, 0),
(131, 2, 0),
(133, 3, 0),
(135, 3, 0),
(137, 4, 0),
(280, 5, 0),
(283, 5, 0),
(322, 5, 0),
(325, 5, 0),
(327, 5, 0),
(330, 5, 0),
(331, 5, 0),
(335, 5, 0),
(337, 5, 0),
(339, 5, 0),
(341, 5, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_term_taxonomy`
--

CREATE TABLE `3b_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `3b_term_taxonomy`
--

INSERT INTO `3b_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'category', '', 0, 5),
(3, 3, 'category', '', 0, 5),
(4, 4, 'category', '', 0, 4),
(5, 5, 'categoriaProduto', 'http://3baquecedores.pixd.com.br/wp-content/uploads/2016/05/foto3.png', 0, 11);

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_usermeta`
--

CREATE TABLE `3b_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `3b_usermeta`
--

INSERT INTO `3b_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', '3baquecedores'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, '3b_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(11, 1, '3b_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', ''),
(13, 1, 'show_welcome_panel', '0'),
(15, 1, '3b_dashboard_quick_press_last_post_id', '321'),
(16, 1, 'wporg_favorites', 'palupa'),
(17, 1, 'wpseo_ignore_tour', '1'),
(18, 1, 'closedpostboxes_produto', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(19, 1, 'metaboxhidden_produto', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(20, 1, 'meta-box-order_produto', 'a:3:{s:4:\"side\";s:42:\"submitdiv,categoriaProdutodiv,postimagediv\";s:6:\"normal\";s:41:\"detalhesMetaboxProduto,wpseo_meta,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(21, 1, 'screen_layout_produto', '2'),
(22, 1, 'closedpostboxes_destaque', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(23, 1, 'metaboxhidden_destaque', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(24, 1, 'meta-box-order_destaque', 'a:3:{s:4:\"side\";s:9:\"submitdiv\";s:6:\"normal\";s:56:\"postimagediv,detalhesMetaboxDestaques,wpseo_meta,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(25, 1, 'screen_layout_destaque', '2'),
(26, 1, 'meta-box-order_depoimento', 'a:3:{s:4:\"side\";s:9:\"submitdiv\";s:6:\"normal\";s:57:\"postimagediv,detalhesMetaboxDepoimento,wpseo_meta,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(27, 1, 'screen_layout_depoimento', '2'),
(28, 1, 'closedpostboxes_depoimento', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(29, 1, 'metaboxhidden_depoimento', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(30, 1, 'meta-box-order_parceiro', 'a:3:{s:4:\"side\";s:9:\"submitdiv\";s:6:\"normal\";s:31:\"postimagediv,wpseo_meta,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(31, 1, 'screen_layout_parceiro', '2'),
(32, 1, 'closedpostboxes_parceiro', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(33, 1, 'metaboxhidden_parceiro', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(34, 1, '3b_r_tru_u_x', 'a:2:{s:2:\"id\";s:0:\"\";s:7:\"expires\";i:86400;}'),
(35, 1, 'ignore_admin_config', '1'),
(36, 1, 'ignore_share_config', '1'),
(37, 1, 'ignore_redux_blast_1462889991', '1'),
(38, 1, 'wpseo_seen_about_version', '3.2.5'),
(39, 1, '3b_user-settings', 'libraryContent=browse&editor=html'),
(40, 1, '3b_user-settings-time', '1504547879'),
(41, 1, 'last_login_time', '2017-09-26 14:11:53'),
(42, 1, 'closedpostboxes_page', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(43, 1, 'metaboxhidden_page', 'a:4:{i:0;s:10:\"postcustom\";i:1;s:16:\"commentstatusdiv\";i:2;s:7:\"slugdiv\";i:3;s:9:\"authordiv\";}'),
(44, 1, 'closedpostboxes_dashboard', 'a:3:{i:0;s:19:\"dashboard_right_now\";i:1;s:24:\"wpseo-dashboard-overview\";i:2;s:18:\"dashboard_activity\";}'),
(45, 1, 'metaboxhidden_dashboard', 'a:2:{i:0;s:21:\"dashboard_quick_press\";i:1;s:17:\"dashboard_primary\";}'),
(46, 1, 'meta-box-order_dashboard', 'a:4:{s:6:\"normal\";s:19:\"dashboard_right_now\";s:4:\"side\";s:46:\"dashboard_quick_press,wpseo-dashboard-overview\";s:7:\"column3\";s:36:\"dashboard_primary,dashboard_activity\";s:7:\"column4\";s:0:\"\";}'),
(47, 1, 'wysija_pref', 'YTowOnt9'),
(48, 1, 'session_tokens', 'a:1:{s:64:\"c31b9017ee776ecfe71d94c1850a027c066c870e60e9c8c96e766b894638682d\";a:4:{s:10:\"expiration\";i:1506618713;s:2:\"ip\";s:13:\"187.95.127.39\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36\";s:5:\"login\";i:1506445913;}}'),
(49, 1, 'meta-box-order_depoimentos', 'a:3:{s:4:\"side\";s:22:\"submitdiv,postimagediv\";s:6:\"normal\";s:44:\"detalhesMetaboxDepoimento,wpseo_meta,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(50, 1, 'screen_layout_depoimentos', '2'),
(51, 1, 'closedpostboxes_depoimentos', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(52, 1, 'metaboxhidden_depoimentos', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(53, 1, '3b_yoast_notifications', 'a:4:{i:0;a:2:{s:7:\"message\";s:559:\"To make sure all the links in your texts are counted, we need to analyze all your texts.\n					All you have to do is press the following button and we\'ll go through all your texts for you.\n					\n					<button type=\"button\" id=\"noticeRunLinkIndex\" class=\"button\">Count links</button>\n					\n					The Text link counter feature provides insights in how many links are found in your text and how many links are referring to your text. This is very helpful when you are improving your <a href=\"https://yoa.st/15m?utm_content=5.1\" target=\"_blank\">internal linking</a>.\";s:7:\"options\";a:8:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:19:\"wpseo-reindex-links\";s:5:\"nonce\";N;s:8:\"priority\";d:0.8000000000000000444089209850062616169452667236328125;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:14:\"manage_options\";s:16:\"capability_check\";s:3:\"all\";}}i:1;a:2:{s:7:\"message\";s:840:\"Percebemos que você já está usando o Yoast SEO há algum tempo; esperamos que esteja gostando! Nós adoraríamos se você pudesse <a href=\"https://yoa.st/rate-yoast-seo?utm_content=5.1\">nos classificar com 5 estrelas no WordPress.org</a>!\n\nSe estiver enfrentando problemas, <a href=\"https://yoa.st/bugreport?utm_content=5.1\">envie um relatório de erro</a> e faremos tudo o que pudermos para ajudar você.\n\nA propósito, sabia que também temos um <a href=\'https://yoa.st/premium-notification?utm_content=5.1\'>plugin Premium</a>? Ele tem recursos avançados, como um gerenciador de redirecionamentos e suporte para múltiplas palavras-chave em foco. Também inclui suporte pessoal 24/7.\n\n<a class=\"button\" href=\"http://3baquecedores.pixd.com.br/wp-admin/?page=wpseo_dashboard&yoast_dismiss=upsell\">Não mostre mais essa notificação</a>\";s:7:\"options\";a:8:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:19:\"wpseo-upsell-notice\";s:5:\"nonce\";N;s:8:\"priority\";d:0.8000000000000000444089209850062616169452667236328125;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:14:\"manage_options\";s:16:\"capability_check\";s:3:\"all\";}}i:2;a:2:{s:7:\"message\";s:196:\"Não deixe escapar seus erros de rastreamento: <a href=\"http://3baquecedores.pixd.com.br/wp-admin/admin.php?page=wpseo_search_console&tab=settings\">conecte-se com o Google Search Console aqui</a>.\";s:7:\"options\";a:8:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:17:\"wpseo-dismiss-gsc\";s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:14:\"manage_options\";s:16:\"capability_check\";s:3:\"all\";}}i:3;a:2:{s:7:\"message\";s:270:\"<strong>Problema grave de SEO: Você está bloqueando o acesso dos robôs.</strong> Você deve <a href=\"http://3baquecedores.pixd.com.br/wp-admin/options-reading.php\">acessar as Configurações de leitura</a> e desmarcar a opção \"Visibilidade nos mecanismos de busca\".\";s:7:\"options\";a:8:{s:4:\"type\";s:5:\"error\";s:2:\"id\";s:32:\"wpseo-dismiss-blog-public-notice\";s:5:\"nonce\";N;s:8:\"priority\";i:1;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:14:\"manage_options\";s:16:\"capability_check\";s:3:\"all\";}}}');

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_users`
--

CREATE TABLE `3b_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `3b_users`
--

INSERT INTO `3b_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, '3baquecedores', '$P$BAY23Lje.Wr9jtyo5LmoxtAtu4nLN/.', '3baquecedores', '3baquecedores@palupa.com.br', '', '2016-05-09 17:08:50', '', 0, '3baquecedores');

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_campaign`
--

CREATE TABLE `3b_wysija_campaign` (
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `3b_wysija_campaign`
--

INSERT INTO `3b_wysija_campaign` (`campaign_id`, `name`, `description`) VALUES
(1, 'Guia de Usuário de 5 Minutos', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_campaign_list`
--

CREATE TABLE `3b_wysija_campaign_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `filter` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_custom_field`
--

CREATE TABLE `3b_wysija_custom_field` (
  `id` mediumint(9) NOT NULL,
  `name` tinytext NOT NULL,
  `type` tinytext NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_email`
--

CREATE TABLE `3b_wysija_email` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(250) NOT NULL DEFAULT '',
  `body` longtext,
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `modified_at` int(10) UNSIGNED DEFAULT NULL,
  `sent_at` int(10) UNSIGNED DEFAULT NULL,
  `from_email` varchar(250) DEFAULT NULL,
  `from_name` varchar(250) DEFAULT NULL,
  `replyto_email` varchar(250) DEFAULT NULL,
  `replyto_name` varchar(250) DEFAULT NULL,
  `attachments` text,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `number_sent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_opened` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_unsub` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_bounce` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_forward` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text,
  `wj_data` longtext,
  `wj_styles` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `3b_wysija_email`
--

INSERT INTO `3b_wysija_email` (`email_id`, `campaign_id`, `subject`, `body`, `created_at`, `modified_at`, `sent_at`, `from_email`, `from_name`, `replyto_email`, `replyto_name`, `attachments`, `status`, `type`, `number_sent`, `number_opened`, `number_clicked`, `number_unsub`, `number_bounce`, `number_forward`, `params`, `wj_data`, `wj_styles`) VALUES
(1, 1, 'Guia de Usuário de 5 Minutos', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\"  >\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n    <title>Guia de Usuário de 5 Minutos</title>\n    <style type=\"text/css\">body {\n        width:100% !important;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        margin:0;\n        padding:0;\n    }\n\n    body,table,td,p,a,li,blockquote{\n        -ms-text-size-adjust:100%;\n        -webkit-text-size-adjust:100%;\n    }\n\n    .ReadMsgBody{\n        width:100%;\n    }.ExternalClass {width:100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important; background:#e8e8e8;}img {\n        outline:none;\n        text-decoration:none;\n        -ms-interpolation-mode: bicubic;\n    }\n    a img {border:none;}\n    .image_fix {display:block;}p {\n        font-family: \"Arial\";\n        font-size: 16px;\n        line-height: 150%;\n        margin: 1em 0;\n        padding: 0;\n    }h1,h2,h3,h4,h5,h6{\n        margin:0;\n        padding:0;\n    }h1 {\n        color:#000000 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:40px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h2 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:30px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h3 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:24px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }table td {border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;}table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }a {\n        color:#4a91b0;\n        word-wrap:break-word;\n    }\n    #outlook a {padding:0;}\n    .yshortcuts { color:#4a91b0; }\n\n    #wysija_wrapper {\n        background:#e8e8e8;\n        color:#000000;\n        font-family:\"Arial\";\n        font-size:16px;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        \n    }\n\n    .wysija_header_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_block {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        background:#ffffff;\n    }\n\n    .wysija_footer_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_viewbrowser_container, .wysija_viewbrowser_container a {\n        font-family: \"Arial\" !important;\n        font-size: 12px !important;\n        color: #000000 !important;\n    }\n    .wysija_unsubscribe_container, .wysija_unsubscribe_container a {\n        text-align:center;\n        color: #000000 !important;\n        font-size:12px;\n    }\n    .wysija_viewbrowser_container a, .wysija_unsubscribe_container a {\n        text-decoration:underline;\n    }\n    .wysija_list_item {\n        margin:0;\n    }@media only screen and (max-device-width: 480px), screen and (max-width: 480px) {a[href^=\"tel\"], a[href^=\"sms\"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }body, table, td, p, a, li, blockquote { -webkit-text-size-adjust:none !important; }body{ width:100% !important; min-width:100% !important; }\n    }@media only screen and (min-device-width: 768px) and (max-device-width: 1024px), screen and (min-width: 768px) and (max-width: 1024px) {a[href^=\"tel\"],\n        a[href^=\"sms\"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }\n    }\n\n    @media only screen and (-webkit-min-device-pixel-ratio: 2) {\n    }@media only screen and (-webkit-device-pixel-ratio:.75){}\n    @media only screen and (-webkit-device-pixel-ratio:1){}\n    @media only screen and (-webkit-device-pixel-ratio:1.5){}</style><!--[if IEMobile 7]>\n<style type=\"text/css\">\n\n</style>\n<![endif]--><!--[if gte mso 9]>\n<style type=\"text/css\">.wysija_image_container {\n        padding-top:0 !important;\n    }\n    .wysija_image_placeholder {\n        mso-text-raise:0;\n        mso-table-lspace:0;\n        mso-table-rspace:0;\n        margin-bottom: 0 !important;\n    }\n    .wysija_block .wysija_image_placeholder {\n        margin:2px 1px 0 1px !important;\n    }\n    p {\n        line-height: 110% !important;\n    }\n    h1, h2, h3 {\n        line-height: 110% !important;\n        margin:0 !important;\n        padding: 0 !important;\n    }\n</style>\n<![endif]-->\n\n<!--[if gte mso 15]>\n<style type=\"text/css\">table { font-size:1px; mso-line-height-alt:0; line-height:0; mso-margin-top-alt:0; }\n    tr { font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px; }\n</style>\n<![endif]-->\n\n</head>\n<body bgcolor=\"#e8e8e8\" yahoo=\"fix\">\n    <span style=\"margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;display:block;background:#e8e8e8;\">\n    <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"wysija_wrapper\">\n        <tr>\n            <td valign=\"top\" align=\"center\">\n                <table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"   >\n                            <p class=\"wysija_viewbrowser_container\" style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;\" >Problemas de visualização? <a style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;\" href=\"[view_in_browser_link]\" target=\"_blank\">Veja esta newsletter em seu navegador.</a></p>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\">\n                            \n<table class=\"wysija_header\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n <td height=\"1\" align=\"center\" class=\"wysija_header_container\" style=\"font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;\" >\n \n <img width=\"600\" height=\"72\" src=\"http://localhost/projetos/3baquecedores_site/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/header.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:600px; height:72px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"left\">\n                            \n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 1:</strong> ei, clique neste texto!</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Para editar, simplesmente clique neste bloco de texto.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://localhost/projetos/3baquecedores_site/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 2:</strong> brinque com esta imagem</h2></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n \n \n <table style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;\" width=\"1%\" height=\"190\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td class=\"wysija_image_container left\" style=\"border: 0;border-collapse: collapse;border: 1px solid #ffffff;display: block;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 10px;padding-bottom: 0;padding-left: 0;\" width=\"1%\" height=\"190\" valign=\"top\">\n <div align=\"left\" class=\"wysija_image_placeholder left\" style=\"height:190px;width:281px;border: 0;display: block;margin-top: 0;margin-right: 10px;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\" >\n \n <img width=\"281\" height=\"190\" src=\"http://localhost/projetos/3baquecedores_site/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/pigeon.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:281px; height:190px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n </table>\n\n <div class=\"wysija_text_container\"><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Posicione o seu mouse acima da imagem à esquerda.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://localhost/projetos/3baquecedores_site/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 3:</strong> solte conteúdo aqui</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Arraste e solte <strong>texto, posts, divisores.</strong> Veja no lado direito!</p><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Você pode até criar <strong>compartilhamentos sociais</strong> como estes:</p></div>\n </td>\n \n </tr>\n</table>\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n <td class=\"wysija_gallery_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" >\n <table class=\"wysija_gallery_table center\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;text-align: center;margin-top: 0;margin-right: auto;margin-bottom: 0;margin-left: auto;\" width=\"184\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"http://www.facebook.com/mailpoetplugin\"><img src=\"http://localhost/projetos/3baquecedores_site/wp-content/uploads/wysija/bookmarks/medium/02/facebook.png\" border=\"0\" alt=\"Facebook\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"http://www.twitter.com/mail_poet\"><img src=\"http://localhost/projetos/3baquecedores_site/wp-content/uploads/wysija/bookmarks/medium/02/twitter.png\" border=\"0\" alt=\"Twitter\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"https://plus.google.com/+Mailpoet\"><img src=\"http://localhost/projetos/3baquecedores_site/wp-content/uploads/wysija/bookmarks/medium/02/google.png\" border=\"0\" alt=\"Google\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n </tr>\n </table>\n </td>\n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://localhost/projetos/3baquecedores_site/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 4:</strong> e o rodapé?</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Modifique o conteúdo do rodapé na <strong>página de opções</strong> do MailPoet.</p></div>\n </td>\n \n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"   >\n                            \n<table class=\"wysija_footer\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n <td height=\"1\" align=\"center\" class=\"wysija_footer_container\" style=\"font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;\" >\n \n <img width=\"600\" height=\"46\" src=\"http://localhost/projetos/3baquecedores_site/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/footer.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:600px; height:46px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"  >\n                            <p class=\"wysija_unsubscribe_container\" style=\"font-family: Verdana, Geneva, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;\" ><a style=\"color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;\" href=\"[unsubscribe_link]\" target=\"_blank\">Cancelar Assinatura</a><br /><br /></p>\n                        </td>\n                    </tr>\n                    \n                </table>\n            </td>\n        </tr>\n    </table>\n    </span>\n</body>\n</html>', 1462815118, 1462815118, NULL, 'info@localhost', '3baquecedores', 'info@localhost', '3baquecedores', NULL, 0, 1, 0, 0, 0, 0, 0, 0, 'YToxOntzOjE0OiJxdWlja3NlbGVjdGlvbiI7YToxOntzOjY6IndwLTMwMSI7YTo1OntzOjEwOiJpZGVudGlmaWVyIjtzOjY6IndwLTMwMSI7czo1OiJ3aWR0aCI7aToyODE7czo2OiJoZWlnaHQiO2k6MTkwO3M6MzoidXJsIjtzOjEyNzoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy8zYmFxdWVjZWRvcmVzX3NpdGUvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvcGlnZW9uLnBuZyI7czo5OiJ0aHVtYl91cmwiO3M6MTM1OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zLzNiYXF1ZWNlZG9yZXNfc2l0ZS93cC1jb250ZW50L3BsdWdpbnMvd3lzaWphLW5ld3NsZXR0ZXJzL2ltZy9kZWZhdWx0LW5ld3NsZXR0ZXIvbmV3c2xldHRlci9waWdlb24tMTUweDE1MC5wbmciO319fQ==', 'YTo0OntzOjc6InZlcnNpb24iO3M6NToiMi43LjEiO3M6NjoiaGVhZGVyIjthOjU6e3M6NDoidGV4dCI7TjtzOjU6ImltYWdlIjthOjU6e3M6Mzoic3JjIjtzOjEyNzoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy8zYmFxdWVjZWRvcmVzX3NpdGUvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvaGVhZGVyLnBuZyI7czo1OiJ3aWR0aCI7aTo2MDA7czo2OiJoZWlnaHQiO2k6NzI7czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjY6InN0YXRpYyI7YjowO31zOjk6ImFsaWdubWVudCI7czo2OiJjZW50ZXIiO3M6Njoic3RhdGljIjtiOjA7czo0OiJ0eXBlIjtzOjY6ImhlYWRlciI7fXM6NDoiYm9keSI7YTo5OntzOjc6ImJsb2NrLTEiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MTYwOiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdNVG84TDNOMGNtOXVaejRnWldrc0lHTnNhWEYxWlNCdVpYTjBaU0IwWlhoMGJ5RThMMmd5UGp4d1BsQmhjbUVnWldScGRHRnlMQ0J6YVcxd2JHVnpiV1Z1ZEdVZ1kyeHBjWFZsSUc1bGMzUmxJR0pzYjJOdklHUmxJSFJsZUhSdkxqd3ZjRDQ9Ijt9czo1OiJpbWFnZSI7TjtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6MTtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stMiI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjI7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjg5OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zLzNiYXF1ZWNlZG9yZXNfc2l0ZS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2RpdmlkZXJzL3NvbGlkLmpwZyI7czo1OiJ3aWR0aCI7aTo1NjQ7czo2OiJoZWlnaHQiO2k6MTt9czo3OiJibG9jay0zIjthOjY6e3M6NDoidGV4dCI7YToxOntzOjU6InZhbHVlIjtzOjgwOiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdNam84TDNOMGNtOXVaejRnWW5KcGJuRjFaU0JqYjIwZ1pYTjBZU0JwYldGblpXMDhMMmd5UGc9PSI7fXM6NToiaW1hZ2UiO047czo5OiJhbGlnbm1lbnQiO3M6NDoibGVmdCI7czo2OiJzdGF0aWMiO2I6MDtzOjg6InBvc2l0aW9uIjtpOjM7czo0OiJ0eXBlIjtzOjc6ImNvbnRlbnQiO31zOjc6ImJsb2NrLTQiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6NzY6IlBIQStVRzl6YVdOcGIyNWxJRzhnYzJWMUlHMXZkWE5sSUdGamFXMWhJR1JoSUdsdFlXZGxiU0REb0NCbGMzRjFaWEprWVM0OEwzQSsiO31zOjU6ImltYWdlIjthOjU6e3M6Mzoic3JjIjtzOjEyNzoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy8zYmFxdWVjZWRvcmVzX3NpdGUvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvcGlnZW9uLnBuZyI7czo1OiJ3aWR0aCI7aToyODE7czo2OiJoZWlnaHQiO2k6MTkwO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7fXM6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aTo0O3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9czo3OiJibG9jay01IjthOjU6e3M6ODoicG9zaXRpb24iO2k6NTtzOjQ6InR5cGUiO3M6NzoiZGl2aWRlciI7czozOiJzcmMiO3M6ODk6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3MvM2JhcXVlY2Vkb3Jlc19zaXRlL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTYiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MzAwOiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdNem84TDNOMGNtOXVaejRnYzI5c2RHVWdZMjl1ZEdYRHVtUnZJR0Z4ZFdrOEwyZ3lQanh3UGtGeWNtRnpkR1VnWlNCemIyeDBaU0E4YzNSeWIyNW5QblJsZUhSdkxDQndiM04wY3l3Z1pHbDJhWE52Y21Wekxqd3ZjM1J5YjI1blBpQldaV3BoSUc1dklHeGhaRzhnWkdseVpXbDBieUU4TDNBK1BIQStWbTlqdzZvZ2NHOWtaU0JoZE1PcElHTnlhV0Z5SUR4emRISnZibWMrWTI5dGNHRnlkR2xzYUdGdFpXNTBiM01nYzI5amFXRnBjend2YzNSeWIyNW5QaUJqYjIxdklHVnpkR1Z6T2p3dmNEND0iO31zOjU6ImltYWdlIjtOO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aTo2O3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9czo3OiJibG9jay03IjthOjU6e3M6NToid2lkdGgiO2k6MTg0O3M6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo1OiJpdGVtcyI7YTozOntpOjA7YTo3OntzOjM6InVybCI7czozODoiaHR0cDovL3d3dy5mYWNlYm9vay5jb20vbWFpbHBvZXRwbHVnaW4iO3M6MzoiYWx0IjtzOjg6IkZhY2Vib29rIjtzOjk6ImNlbGxXaWR0aCI7aTo2MTtzOjEwOiJjZWxsSGVpZ2h0IjtpOjMyO3M6Mzoic3JjIjtzOjEwMzoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy8zYmFxdWVjZWRvcmVzX3NpdGUvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS9ib29rbWFya3MvbWVkaXVtLzAyL2ZhY2Vib29rLnBuZyI7czo1OiJ3aWR0aCI7aTozMjtzOjY6ImhlaWdodCI7aTozMjt9aToxO2E6Nzp7czozOiJ1cmwiO3M6MzI6Imh0dHA6Ly93d3cudHdpdHRlci5jb20vbWFpbF9wb2V0IjtzOjM6ImFsdCI7czo3OiJUd2l0dGVyIjtzOjk6ImNlbGxXaWR0aCI7aTo2MTtzOjEwOiJjZWxsSGVpZ2h0IjtpOjMyO3M6Mzoic3JjIjtzOjEwMjoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy8zYmFxdWVjZWRvcmVzX3NpdGUvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS9ib29rbWFya3MvbWVkaXVtLzAyL3R3aXR0ZXIucG5nIjtzOjU6IndpZHRoIjtpOjMyO3M6NjoiaGVpZ2h0IjtpOjMyO31pOjI7YTo3OntzOjM6InVybCI7czozMzoiaHR0cHM6Ly9wbHVzLmdvb2dsZS5jb20vK01haWxwb2V0IjtzOjM6ImFsdCI7czo2OiJHb29nbGUiO3M6OToiY2VsbFdpZHRoIjtpOjYxO3M6MTA6ImNlbGxIZWlnaHQiO2k6MzI7czozOiJzcmMiO3M6MTAxOiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zLzNiYXF1ZWNlZG9yZXNfc2l0ZS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2Jvb2ttYXJrcy9tZWRpdW0vMDIvZ29vZ2xlLnBuZyI7czo1OiJ3aWR0aCI7aTozMjtzOjY6ImhlaWdodCI7aTozMjt9fXM6ODoicG9zaXRpb24iO2k6NztzOjQ6InR5cGUiO3M6NzoiZ2FsbGVyeSI7fXM6NzoiYmxvY2stOCI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjg7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjg5OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zLzNiYXF1ZWNlZG9yZXNfc2l0ZS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2RpdmlkZXJzL3NvbGlkLmpwZyI7czo1OiJ3aWR0aCI7aTo1NjQ7czo2OiJoZWlnaHQiO2k6MTt9czo3OiJibG9jay05IjthOjY6e3M6NDoidGV4dCI7YToxOntzOjU6InZhbHVlIjtzOjE4ODoiUEdneVBqeHpkSEp2Ym1jK1VHRnpjMjhnTkRvOEwzTjBjbTl1Wno0Z1pTQnZJSEp2WkdGd3c2ay9QQzlvTWo0OGNENU5iMlJwWm1seGRXVWdieUJqYjI1MFpjTzZaRzhnWkc4Z2NtOWtZWEREcVNCdVlTQThjM1J5YjI1blBuRERvV2RwYm1FZ1pHVWdiM0REcDhPMVpYTThMM04wY205dVp6NGdaRzhnVFdGcGJGQnZaWFF1UEM5d1BnPT0iO31zOjU6ImltYWdlIjtOO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aTo5O3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9fXM6NjoiZm9vdGVyIjthOjU6e3M6NDoidGV4dCI7TjtzOjU6ImltYWdlIjthOjU6e3M6Mzoic3JjIjtzOjEyNzoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy8zYmFxdWVjZWRvcmVzX3NpdGUvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvZm9vdGVyLnBuZyI7czo1OiJ3aWR0aCI7aTo2MDA7czo2OiJoZWlnaHQiO2k6NDY7czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjY6InN0YXRpYyI7YjowO31zOjk6ImFsaWdubWVudCI7czo2OiJjZW50ZXIiO3M6Njoic3RhdGljIjtiOjA7czo0OiJ0eXBlIjtzOjY6ImZvb3RlciI7fX0=', 'YToxMDp7czo0OiJodG1sIjthOjE6e3M6MTA6ImJhY2tncm91bmQiO3M6NjoiZThlOGU4Ijt9czo2OiJoZWFkZXIiO2E6MTp7czoxMDoiYmFja2dyb3VuZCI7czo2OiJlOGU4ZTgiO31zOjQ6ImJvZHkiO2E6NDp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjU6IkFyaWFsIjtzOjQ6InNpemUiO2k6MTY7czoxMDoiYmFja2dyb3VuZCI7czo2OiJmZmZmZmYiO31zOjY6ImZvb3RlciI7YToxOntzOjEwOiJiYWNrZ3JvdW5kIjtzOjY6ImU4ZThlOCI7fXM6MjoiaDEiO2E6Mzp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjEyOiJUcmVidWNoZXQgTVMiO3M6NDoic2l6ZSI7aTo0MDt9czoyOiJoMiI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjQyNDI0MiI7czo2OiJmYW1pbHkiO3M6MTI6IlRyZWJ1Y2hldCBNUyI7czo0OiJzaXplIjtpOjMwO31zOjI6ImgzIjthOjM6e3M6NToiY29sb3IiO3M6NjoiNDI0MjQyIjtzOjY6ImZhbWlseSI7czoxMjoiVHJlYnVjaGV0IE1TIjtzOjQ6InNpemUiO2k6MjQ7fXM6MToiYSI7YToyOntzOjU6ImNvbG9yIjtzOjY6IjRhOTFiMCI7czo5OiJ1bmRlcmxpbmUiO2I6MDt9czoxMToidW5zdWJzY3JpYmUiO2E6MTp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO31zOjExOiJ2aWV3YnJvd3NlciI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjAwMDAwMCI7czo2OiJmYW1pbHkiO3M6NToiQXJpYWwiO3M6NDoic2l6ZSI7aToxMjt9fQ=='),
(2, 0, 'Confirme sua assinatura em 3B Aquecedores', 'Olá!\n\nLegal! Você se inscreveu como assinante em nosso site.\nPrecisamos que você ative sua assinatura para a(s) lista(s) [lists_to_confirm] clicando no link abaixo: \n\n[activation_link]Clique aqui para confirmar sua assinatura[/activation_link]\n\nObrigado,\n\nA equipe!\n', 1462815119, 1462815119, NULL, 'info@localhost', '3baquecedores', 'info@localhost', '3baquecedores', NULL, 99, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_email_user_stat`
--

CREATE TABLE `3b_wysija_email_user_stat` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED NOT NULL,
  `sent_at` int(10) UNSIGNED NOT NULL,
  `opened_at` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_email_user_url`
--

CREATE TABLE `3b_wysija_email_user_url` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `url_id` int(10) UNSIGNED NOT NULL,
  `clicked_at` int(10) UNSIGNED DEFAULT NULL,
  `number_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_form`
--

CREATE TABLE `3b_wysija_form` (
  `form_id` int(10) UNSIGNED NOT NULL,
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_bin,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `styles` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `subscribed` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `3b_wysija_form`
--

INSERT INTO `3b_wysija_form` (`form_id`, `name`, `data`, `styles`, `subscribed`) VALUES
(1, 'Assine nossa Newsletter', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMC40IjtzOjg6InNldHRpbmdzIjthOjQ6e3M6MTA6Im9uX3N1Y2Nlc3MiO3M6NzoibWVzc2FnZSI7czoxNToic3VjY2Vzc19tZXNzYWdlIjtzOjczOiJWZWphIHN1YSBjYWl4YSBkZSBlbnRyYWRhIG91IHBhc3RhIGRlIHNwYW0gcGFyYSBjb25maXJtYXIgc3VhIGFzc2luYXR1cmEuIjtzOjU6Imxpc3RzIjthOjE6e2k6MDtzOjE6IjEiO31zOjE3OiJsaXN0c19zZWxlY3RlZF9ieSI7czo1OiJhZG1pbiI7fXM6NDoiYm9keSI7YToyOntpOjA7YTo0OntzOjQ6Im5hbWUiO3M6NToiRW1haWwiO3M6NDoidHlwZSI7czo1OiJpbnB1dCI7czo1OiJmaWVsZCI7czo1OiJlbWFpbCI7czo2OiJwYXJhbXMiO2E6Mjp7czo1OiJsYWJlbCI7czo1OiJFbWFpbCI7czo4OiJyZXF1aXJlZCI7YjoxO319aToxO2E6NDp7czo0OiJuYW1lIjtzOjY6IkVudmlhciI7czo0OiJ0eXBlIjtzOjY6InN1Ym1pdCI7czo1OiJmaWVsZCI7czo2OiJzdWJtaXQiO3M6NjoicGFyYW1zIjthOjE6e3M6NToibGFiZWwiO3M6ODoiQXNzaW5hciEiO319fXM6NzoiZm9ybV9pZCI7aToxO30=', NULL, 0),
(2, 'Novo Formulário', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMC40IjtzOjg6InNldHRpbmdzIjthOjU6e3M6NzoiZm9ybV9pZCI7czoxOiIyIjtzOjU6Imxpc3RzIjthOjE6e2k6MDtzOjE6IjMiO31zOjEwOiJvbl9zdWNjZXNzIjtzOjc6Im1lc3NhZ2UiO3M6MTU6InN1Y2Nlc3NfbWVzc2FnZSI7czo3MzoiVmVqYSBzdWEgY2FpeGEgZGUgZW50cmFkYSBvdSBwYXN0YSBkZSBzcGFtIHBhcmEgY29uZmlybWFyIHN1YSBhc3NpbmF0dXJhLiI7czoxNzoibGlzdHNfc2VsZWN0ZWRfYnkiO3M6NToiYWRtaW4iO31zOjQ6ImJvZHkiO2E6Mzp7czo3OiJibG9jay0xIjthOjU6e3M6NjoicGFyYW1zIjthOjM6e3M6NToibGFiZWwiO3M6MTM6Ik5vbWUgY29tcGxldG8iO3M6MTI6ImxhYmVsX3dpdGhpbiI7czoxOiIwIjtzOjg6InJlcXVpcmVkIjtzOjE6IjEiO31zOjg6InBvc2l0aW9uIjtpOjE7czo0OiJ0eXBlIjtzOjU6ImlucHV0IjtzOjU6ImZpZWxkIjtzOjk6ImZpcnN0bmFtZSI7czo0OiJuYW1lIjtzOjQ6Ik5vbWUiO31zOjc6ImJsb2NrLTIiO2E6NTp7czo2OiJwYXJhbXMiO2E6Mzp7czo1OiJsYWJlbCI7czo1OiJFbWFpbCI7czoxMjoibGFiZWxfd2l0aGluIjtzOjE6IjAiO3M6ODoicmVxdWlyZWQiO3M6MToiMSI7fXM6ODoicG9zaXRpb24iO2k6MjtzOjQ6InR5cGUiO3M6NToiaW5wdXQiO3M6NToiZmllbGQiO3M6NToiZW1haWwiO3M6NDoibmFtZSI7czo1OiJFbWFpbCI7fXM6NzoiYmxvY2stMyI7YTo1OntzOjY6InBhcmFtcyI7YToxOntzOjU6ImxhYmVsIjtzOjExOiJDYWRhc3RyZS1zZSI7fXM6ODoicG9zaXRpb24iO2k6MztzOjQ6InR5cGUiO3M6Njoic3VibWl0IjtzOjU6ImZpZWxkIjtzOjY6InN1Ym1pdCI7czo0OiJuYW1lIjtzOjY6IkVudmlhciI7fX1zOjc6ImZvcm1faWQiO2k6Mjt9', NULL, 3);

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_list`
--

CREATE TABLE `3b_wysija_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `namekey` varchar(255) DEFAULT NULL,
  `description` text,
  `unsub_mail_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `welcome_mail_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_enabled` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `is_public` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `3b_wysija_list`
--

INSERT INTO `3b_wysija_list` (`list_id`, `name`, `namekey`, `description`, `unsub_mail_id`, `welcome_mail_id`, `is_enabled`, `is_public`, `created_at`, `ordering`) VALUES
(2, 'Usuários do WordPress', 'users', 'A lista criada automaticamente na importação dos assinantes do plugin : \"WordPress', 0, 0, 0, 0, 1462815119, 0),
(3, 'Assinantes 3BAquecedores', 'assinantes-3baquecedores', '', 0, 0, 1, 0, 1463143756, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_queue`
--

CREATE TABLE `3b_wysija_queue` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED NOT NULL,
  `send_at` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  `number_try` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_url`
--

CREATE TABLE `3b_wysija_url` (
  `url_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `url` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_url_mail`
--

CREATE TABLE `3b_wysija_url_mail` (
  `email_id` int(11) NOT NULL,
  `url_id` int(10) UNSIGNED NOT NULL,
  `unique_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `total_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_user`
--

CREATE TABLE `3b_wysija_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `wpuser_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL DEFAULT '',
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(100) NOT NULL,
  `confirmed_ip` varchar(100) NOT NULL DEFAULT '0',
  `confirmed_at` int(10) UNSIGNED DEFAULT NULL,
  `last_opened` int(10) UNSIGNED DEFAULT NULL,
  `last_clicked` int(10) UNSIGNED DEFAULT NULL,
  `keyuser` varchar(255) NOT NULL DEFAULT '',
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `domain` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `3b_wysija_user`
--

INSERT INTO `3b_wysija_user` (`user_id`, `wpuser_id`, `email`, `firstname`, `lastname`, `ip`, `confirmed_ip`, `confirmed_at`, `last_opened`, `last_clicked`, `keyuser`, `created_at`, `status`, `domain`) VALUES
(1, 1, '3baquecedores@palupa.com.br', '', '', '', '0', NULL, NULL, NULL, 'cc8ce3a2ddde67ba1f002a5b0e166eef', 1462815120, 1, 'palupa.com.br'),
(2, 0, 'hudson@palupa.com.br', 'Hudson', '', '127.0.0.1', '200.150.69.12', 1463429785, NULL, NULL, '95f470f015d48418142ec36c4991023b', 1463148994, 1, 'palupa.com.br'),
(3, 0, 'sanztss@gmail.com', 'sanzio', '', '200.150.69.12', '0', NULL, NULL, NULL, '254bb5f7aec90d1b281d5cd741b51e85', 1463403659, 0, 'gmail.com'),
(4, 0, 'julisozir@hotmail.com', 'Juliana', '', '170.82.201.156', '0', NULL, NULL, NULL, '7f31968f50ed33a3909e602adf715175', 1504634143, 0, 'hotmail.com');

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_user_field`
--

CREATE TABLE `3b_wysija_user_field` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `column_name` varchar(250) NOT NULL DEFAULT '',
  `type` tinyint(3) UNSIGNED DEFAULT '0',
  `values` text,
  `default` varchar(250) NOT NULL DEFAULT '',
  `is_required` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `error_message` varchar(250) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `3b_wysija_user_field`
--

INSERT INTO `3b_wysija_user_field` (`field_id`, `name`, `column_name`, `type`, `values`, `default`, `is_required`, `error_message`) VALUES
(1, 'Nome', 'firstname', 0, NULL, '', 0, 'Por favor, digite seu nome'),
(2, 'Sobrenome', 'lastname', 0, NULL, '', 0, 'Por favor, digite seu sobrenome');

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_user_history`
--

CREATE TABLE `3b_wysija_user_history` (
  `history_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED DEFAULT '0',
  `type` varchar(250) NOT NULL DEFAULT '',
  `details` text,
  `executed_at` int(10) UNSIGNED DEFAULT NULL,
  `executed_by` int(10) UNSIGNED DEFAULT NULL,
  `source` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_wysija_user_list`
--

CREATE TABLE `3b_wysija_user_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sub_date` int(10) UNSIGNED DEFAULT '0',
  `unsub_date` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `3b_wysija_user_list`
--

INSERT INTO `3b_wysija_user_list` (`list_id`, `user_id`, `sub_date`, `unsub_date`) VALUES
(2, 1, 1462815119, 0),
(3, 2, 1463429785, 0),
(3, 3, 0, 0),
(3, 4, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_yoast_seo_links`
--

CREATE TABLE `3b_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `3b_yoast_seo_meta`
--

CREATE TABLE `3b_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `3b_yoast_seo_meta`
--

INSERT INTO `3b_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(322, 0, NULL),
(325, 0, NULL),
(327, 0, NULL),
(330, 0, NULL),
(331, 0, NULL),
(335, 0, NULL),
(337, 0, NULL),
(339, 0, NULL),
(341, 0, NULL),
(29, 0, NULL),
(301, 0, NULL),
(27, 0, NULL),
(359, 0, NULL);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `3b_aiowps_events`
--
ALTER TABLE `3b_aiowps_events`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `3b_aiowps_failed_logins`
--
ALTER TABLE `3b_aiowps_failed_logins`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `3b_aiowps_global_meta`
--
ALTER TABLE `3b_aiowps_global_meta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Índices de tabela `3b_aiowps_login_activity`
--
ALTER TABLE `3b_aiowps_login_activity`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `3b_aiowps_login_lockdown`
--
ALTER TABLE `3b_aiowps_login_lockdown`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `3b_aiowps_permanent_block`
--
ALTER TABLE `3b_aiowps_permanent_block`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `3b_commentmeta`
--
ALTER TABLE `3b_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191)),
  ADD KEY `disqus_dupecheck` (`meta_key`(191),`meta_value`(11));

--
-- Índices de tabela `3b_comments`
--
ALTER TABLE `3b_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Índices de tabela `3b_links`
--
ALTER TABLE `3b_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Índices de tabela `3b_options`
--
ALTER TABLE `3b_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Índices de tabela `3b_postmeta`
--
ALTER TABLE `3b_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `3b_posts`
--
ALTER TABLE `3b_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Índices de tabela `3b_termmeta`
--
ALTER TABLE `3b_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `3b_terms`
--
ALTER TABLE `3b_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Índices de tabela `3b_term_relationships`
--
ALTER TABLE `3b_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Índices de tabela `3b_term_taxonomy`
--
ALTER TABLE `3b_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Índices de tabela `3b_usermeta`
--
ALTER TABLE `3b_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `3b_users`
--
ALTER TABLE `3b_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Índices de tabela `3b_wysija_campaign`
--
ALTER TABLE `3b_wysija_campaign`
  ADD PRIMARY KEY (`campaign_id`);

--
-- Índices de tabela `3b_wysija_campaign_list`
--
ALTER TABLE `3b_wysija_campaign_list`
  ADD PRIMARY KEY (`list_id`,`campaign_id`);

--
-- Índices de tabela `3b_wysija_custom_field`
--
ALTER TABLE `3b_wysija_custom_field`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `3b_wysija_email`
--
ALTER TABLE `3b_wysija_email`
  ADD PRIMARY KEY (`email_id`);

--
-- Índices de tabela `3b_wysija_email_user_stat`
--
ALTER TABLE `3b_wysija_email_user_stat`
  ADD PRIMARY KEY (`user_id`,`email_id`);

--
-- Índices de tabela `3b_wysija_email_user_url`
--
ALTER TABLE `3b_wysija_email_user_url`
  ADD PRIMARY KEY (`user_id`,`email_id`,`url_id`);

--
-- Índices de tabela `3b_wysija_form`
--
ALTER TABLE `3b_wysija_form`
  ADD PRIMARY KEY (`form_id`);

--
-- Índices de tabela `3b_wysija_list`
--
ALTER TABLE `3b_wysija_list`
  ADD PRIMARY KEY (`list_id`);

--
-- Índices de tabela `3b_wysija_queue`
--
ALTER TABLE `3b_wysija_queue`
  ADD PRIMARY KEY (`user_id`,`email_id`),
  ADD KEY `SENT_AT_INDEX` (`send_at`);

--
-- Índices de tabela `3b_wysija_url`
--
ALTER TABLE `3b_wysija_url`
  ADD PRIMARY KEY (`url_id`);

--
-- Índices de tabela `3b_wysija_url_mail`
--
ALTER TABLE `3b_wysija_url_mail`
  ADD PRIMARY KEY (`email_id`,`url_id`);

--
-- Índices de tabela `3b_wysija_user`
--
ALTER TABLE `3b_wysija_user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `EMAIL_UNIQUE` (`email`);

--
-- Índices de tabela `3b_wysija_user_field`
--
ALTER TABLE `3b_wysija_user_field`
  ADD PRIMARY KEY (`field_id`);

--
-- Índices de tabela `3b_wysija_user_history`
--
ALTER TABLE `3b_wysija_user_history`
  ADD PRIMARY KEY (`history_id`);

--
-- Índices de tabela `3b_wysija_user_list`
--
ALTER TABLE `3b_wysija_user_list`
  ADD PRIMARY KEY (`list_id`,`user_id`);

--
-- Índices de tabela `3b_yoast_seo_links`
--
ALTER TABLE `3b_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`);

--
-- Índices de tabela `3b_yoast_seo_meta`
--
ALTER TABLE `3b_yoast_seo_meta`
  ADD UNIQUE KEY `object_id` (`object_id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `3b_aiowps_events`
--
ALTER TABLE `3b_aiowps_events`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `3b_aiowps_failed_logins`
--
ALTER TABLE `3b_aiowps_failed_logins`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `3b_aiowps_global_meta`
--
ALTER TABLE `3b_aiowps_global_meta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `3b_aiowps_login_activity`
--
ALTER TABLE `3b_aiowps_login_activity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT de tabela `3b_aiowps_login_lockdown`
--
ALTER TABLE `3b_aiowps_login_lockdown`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `3b_aiowps_permanent_block`
--
ALTER TABLE `3b_aiowps_permanent_block`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `3b_commentmeta`
--
ALTER TABLE `3b_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `3b_comments`
--
ALTER TABLE `3b_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `3b_links`
--
ALTER TABLE `3b_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `3b_options`
--
ALTER TABLE `3b_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2234;
--
-- AUTO_INCREMENT de tabela `3b_postmeta`
--
ALTER TABLE `3b_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1757;
--
-- AUTO_INCREMENT de tabela `3b_posts`
--
ALTER TABLE `3b_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=361;
--
-- AUTO_INCREMENT de tabela `3b_termmeta`
--
ALTER TABLE `3b_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `3b_terms`
--
ALTER TABLE `3b_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de tabela `3b_term_taxonomy`
--
ALTER TABLE `3b_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de tabela `3b_usermeta`
--
ALTER TABLE `3b_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT de tabela `3b_users`
--
ALTER TABLE `3b_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `3b_wysija_campaign`
--
ALTER TABLE `3b_wysija_campaign`
  MODIFY `campaign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `3b_wysija_custom_field`
--
ALTER TABLE `3b_wysija_custom_field`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `3b_wysija_email`
--
ALTER TABLE `3b_wysija_email`
  MODIFY `email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `3b_wysija_form`
--
ALTER TABLE `3b_wysija_form`
  MODIFY `form_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `3b_wysija_list`
--
ALTER TABLE `3b_wysija_list`
  MODIFY `list_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `3b_wysija_url`
--
ALTER TABLE `3b_wysija_url`
  MODIFY `url_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `3b_wysija_url_mail`
--
ALTER TABLE `3b_wysija_url_mail`
  MODIFY `email_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `3b_wysija_user`
--
ALTER TABLE `3b_wysija_user`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `3b_wysija_user_field`
--
ALTER TABLE `3b_wysija_user_field`
  MODIFY `field_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `3b_wysija_user_history`
--
ALTER TABLE `3b_wysija_user_history`
  MODIFY `history_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `3b_yoast_seo_links`
--
ALTER TABLE `3b_yoast_seo_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
